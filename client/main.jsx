/**
 * Import polyfills for older browsers
 */
import "core-js/stable";
import "regenerator-runtime/runtime";

/**
 * Import Meteor client startup code
 */
import "/imports/startup/client";
