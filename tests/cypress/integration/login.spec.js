/**
 * Tests for User Login
 */
import errors from '../fixtures/home/loginErrors.json';

describe("Login", () => {

  const usernameField = "input[name=username]";
  const passwordField = "input[name=password]";
  const loginButton = "button:contains('Login')";
  const dashboardLayout = "[data-test=dashboard-layout]";
  const loginForm = "[data-test=login-form]";

  const user = { username: "test", email: "test@qrder.io", password: "password" };

  before(() => {
    cy.resetDatabase();
    cy.deleteStripeTestCustomer();
    cy.createUser();
  });

  beforeEach(() => {
    cy.visit("/login");
  });

  it("successfully logs in with username", () => {
    cy.get(usernameField).type(user.username);
    cy.get(passwordField).type(user.password);

    cy.get(loginButton).click();

    cy.url().should("eq", "http://localhost:3000/dashboard/venues");
    cy.get(dashboardLayout).should('be.visible');
  });

  it("successfully logs in with email", () => {
    cy.get(usernameField).type(user.email);
    cy.get(passwordField).type(user.password);

    cy.get(loginButton).click();

    cy.url().should("eq", "http://localhost:3000/dashboard/venues");
    cy.get(dashboardLayout).should('be.visible');
  });

  errors.forEach((error) => {
    it('throws an error if ' + error.condition, () => {
      cy.get(usernameField).type(error.data.username);
      cy.get(passwordField).type(error.data.password);
  
      cy.get(loginButton).click();
      
      cy.get(loginForm).contains(error.message).should('be.visible');
    })
  });
})