/**
 * Tests for 'Home' / Landing Page
 */

describe('Home', () => {

  const homeNavbar = "[data-test=home-navbar]";
  const loginForm = "[data-test=login-form]";
  const registrationForm = "[data-test=register-form]";
  const forgotPasswordForm = "[data-test=forgot-password-form]";
  const privacyPolicy = "[data-test=privacy-policy]";

  it('successfully loads homepage', () => {
    cy.visit('/');
    cy.url().should('eq', 'http://localhost:3000/');
    cy.get(homeNavbar).should('be.visible');
  });
  
  it('successfully loads registration page', () => {
    cy.visit('/register');
    cy.url().should('eq', 'http://localhost:3000/register');
    cy.get(registrationForm).should('be.visible');
  });

  it('successfully loads login page', () => {
    cy.visit('/login');
    cy.url().should('eq', 'http://localhost:3000/login');
    cy.get(loginForm).should('be.visible');
  });

  it('successfully loads forgotten password page', () => {
    cy.visit('/forgotten-password');
    cy.url().should('eq', 'http://localhost:3000/forgotten-password');
    cy.get(forgotPasswordForm).should('be.visible');
  });

  it('successfully loads privacy page', () => {
    cy.visit('/privacy');
    cy.url().should('eq', 'http://localhost:3000/privacy');
    cy.get(privacyPolicy).should('be.visible');
  });
})