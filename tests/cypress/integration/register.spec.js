/**
 * Tests for User Registration
 */
import errors from '../fixtures/home/registrationErrors.json';

describe("Registration", () => {

  const usernameField = "input[name=username]";
  const emailField = "input[name=email]";
  const passwordField = "input[name=password]";
  const confirmPasswordField = "input[name=confirmPassword]";
  const registerButton = "button:contains('Register')";
  const dashboardLayout = "[data-test=dashboard-layout]";
  const registrationForm = "[data-test=register-form]";

  before(() => {
    cy.resetDatabase();
    cy.deleteStripeTestCustomer();
  });

  beforeEach(() => {
    cy.visit("/register");
  });

  it("successfully registers a user", () => {
    cy.get(usernameField).type("test");
    cy.get(emailField).type("test@qrder.io");
    cy.get(passwordField).type("password");
    cy.get(confirmPasswordField).type("password");

    cy.get(registerButton).click();

    cy.url({ timeout: 10000 }).should("eq", "http://localhost:3000/dashboard/venues"); 
    cy.get(dashboardLayout).should('be.visible');
  });

  errors.forEach((error) => {
    it('throws an error if ' + error.condition, () => {
      cy.get(usernameField).type(error.data.username);
      cy.get(emailField).type(error.data.email);
      cy.get(passwordField).type(error.data.password);
      cy.get(confirmPasswordField).type(error.data.confirmPassword);
  
      cy.get(registerButton).click();
      
      cy.get(registrationForm).contains(error.message).should('be.visible');
    })
  });
})