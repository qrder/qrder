/**
 * Tests for User Forgotten Password
 */
import errors from '../fixtures/home/forgotPasswordErrors.json';

// TODO: Reset Password page & flow is dependent on the link that is emailed to the user.
//          So I need to figure out how to get access to that email.

describe("Forgotten Password", () => {

  const forgotPasswordForm = "[data-test=forgot-password-form]";
  const forgottenPasswordButton = "button:contains('Forgot Password')";
  const emailField = "input[name=email]";
  const forgotPasswordButton = "button:contains('Send Password Reset Email')";
  const emailSent = "[data-test=forgot-password-email-sent]";
  
  before(() => {
    cy.resetDatabase();
    cy.deleteStripeTestCustomer();
  });

  context("User Navigation", () => {
    it("can be navigated to from login page", () => {
      cy.visit("/login");
      cy.get(forgottenPasswordButton).click();
      cy.url().should("eq", "http://localhost:3000/forgotten-password");
      cy.get(forgotPasswordForm).should('be.visible');
    });
  });

  context("Form Submission & Errors", () => {
    before(() => {
      cy.createUser("0", "test", "test@qrder.io");
    });

    beforeEach(() => {
      cy.visit("/forgotten-password");
    });

    it("successfully submits", () => {
      cy.get(emailField).type("test@qrder.io");

      cy.get(forgotPasswordButton).click();

      cy.url({ timeout: 10000 }).should("eq", "http://localhost:3000/forgotten-password");
      cy.get(emailSent).contains("An email has been sent to test@qrder.io").should('be.visible');
    });

    errors.forEach((error) => {
      it('button is disabled if ' + error.condition, () => {
        cy.get(emailField).type(error.email);

        cy.get(forgotPasswordButton).should('be.disabled');
      });
    });
  });
  
  // TODO: Need to read content of email to get link
  context("Entire Flow [Forgotten --> Reset]", () => {
    const testId = new Date().getTime();
    const gmailUserEmail = `qrder.test@gmail.com`;

    before(() => {
      cy.createUser("1", "test-gmail", gmailUserEmail);
    });

    beforeEach(() => {
      cy.visit("/forgotten-password");
    });

    it("successfully resets a password", () => {
      cy.get(emailField).type(gmailUserEmail);

      cy.get(forgotPasswordButton).click();

      cy.url({ timeout: 10000 }).should("eq", "http://localhost:3000/forgotten-password");
      cy.get(emailSent).contains(`An email has been sent to ${gmailUserEmail}`).should('be.visible');

      cy.task("gmail:check", {
          from: "no-reply@qrder.io",
          to: gmailUserEmail,
          subject: "QRDER - Reset Password"
        })
        .then(email => {
          assert.isNotNull(email, `Reset password email was not found.`);
        });
    });
  });
});
