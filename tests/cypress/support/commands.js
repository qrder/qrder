/**
 * Commands (functions) to assist with tests
 */

// Remove everything from database
Cypress.Commands.add("resetDatabase", () =>
  cy.exec('mongo mongodb://localhost:3001/meteor --eval "db.dropDatabase()"')
);

// Delete customer in Stripe
Cypress.Commands.add("deleteStripeTestCustomer", () => {
  const bearerToken = "sk_test_51HkBJ8E4yZVK3kWPorkkc1vAmQniyIbOI3XROuFjhI5pv0qCLubKh40AkeF12e0maYO7omvPuIZ7oT94MLpE41e300GcR5Hql5";
  // Get customers Stripe id by the email address
  cy.request({
    method: "GET",
    url: "https://api.stripe.com/v1/customers",
    qs: {
      email: "test@qrder.io",
      limit: 1
    },
    headers: { 
      authorization: `bearer ${bearerToken}` 
    }
  })
  // then delete the customer in Stripe if it exists
  .then((response) => {
    expect(response.body.data).to.be.a('array');

    if(response.body.data.length == 0) {
      cy.log("Customer does not exist in Stripe so test can continue.");
    } else if(response.body.data.length == 1) {
      expect(response.body.data[0].email).to.equal("test@qrder.io");

      cy.request({
        method: "DELETE",
        url: `https://api.stripe.com/v1/customers/${response.body.data[0].id}`,
        headers: { 
          authorization: `bearer ${bearerToken}` 
        }
      }).then((res) => {
        expect(res.body).to.be.a('object');
        expect(res.body.deleted).to.be.true;
      });
    }
  }); 
});

// Add user to database
Cypress.Commands.add("createUser", (id, username, email) => {
  var user = {
    "_id": `dohvaj4oho31vn43o${id}`,
    "createdAt": new Date(),
    "emails": [ 
      {
        "address": email
      }
    ],
    "username": username,
    "services": {
      "password": {
        "bcrypt": "$2b$10$uHrYHKVlH9xLsG586cehaeWEvuqntI8ED7cKhFAeJyGUdc2zJTI7u"
      }
    }
  }

  cy.exec(`mongo mongodb://localhost:3001/meteor --eval 'db.users.insert(${JSON.stringify(user)})'`)
});