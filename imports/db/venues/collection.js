import { VenueSchema } from './schema';

VenueCollection = new Mongo.Collection('venues');

VenueCollection.attachSchema(VenueSchema);

export { VenueCollection };
