import { Meteor } from 'meteor/meteor';
import { createQuery } from 'meteor/cultofcoders:grapher';

const query = createQuery('venuesByUser', {
  venues: {
    $filter({filters}) {
      filters.userId = Meteor.userId()
    },
    name: 1,
    paymentOptions: 1,
    isAcceptingOrders: 1
  }
});

if (Meteor.isServer) {
  query.expose();
}

export default query;
