import { createQuery } from 'meteor/cultofcoders:grapher';
// TODO: For some reason, need to import MenuCollection here otherwise
//        gives an error (even though it didn't before)
import { VenueCollection } from '/imports/db/venues/collection';

// TODO: Move this into a shared query folder?  As it's relevant to both 
//    venues and orders.

const query = createQuery('ordersCustomisation', {
  venues: {
    $filter({filters, params}) {
      filters.userId = Meteor.userId(),
      filters._id = params.venueId
    },
    name: 1,
    settings: 1,
    paymentOptions: 1 // not needed for code but required to fix errors
  }
});

if (Meteor.isServer) {
  query.expose();
}

export default query;
