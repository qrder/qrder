import { createQuery } from 'meteor/cultofcoders:grapher';
import moment from 'moment';
// TODO: For some reason, need to import MenuCollection here otherwise
//        gives an error (even though it didn't before)
import { VenueCollection } from '/imports/db/venues/collection';

// TODO: Move this into a shared query folder?  As it's relevant to both 
//    venues and menus.

const query = createQuery('venuesAndOrdersByUser', {
  venues: {
    $filter({filters}) {
      filters.userId = Meteor.userId()
    },
    name: 1,
    orders: {
      $filter({filters, params}) {
        filters.createdAtDate = moment(new Date(params.date)).format("DD/MM/YYYY")
      },
      items: 1,
      orderTotal: 1,
      comments: 1,
      paymentOptionSelected: 1,
      status: 1,
      hasBeenAcknowledged: 1,
      hasBeenCompleted: 1,
      hasBeenPaidFor: 1,
      tableToDeliverTo: 1,
      venueOrderedAt: 1,
      acknowledgedAt: 1,
      completedAt: 1,
      paidForAt: 1,
      createdAtDate: 1,
      createdAtTime: 1,
      lastUpdatedAtDate: 1,
      lastUpdatedAtTime: 1
    }
  }
});

if (Meteor.isServer) {
  query.expose();
}

export default query;
