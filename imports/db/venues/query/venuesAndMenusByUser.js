import { createQuery } from 'meteor/cultofcoders:grapher';
// TODO: For some reason, need to import MenuCollection here otherwise
//        gives an error (even though it didn't before)
import { VenueCollection } from '/imports/db/venues/collection';

// TODO: Move this into a shared query folder?  As it's relevant to both 
//    venues and menus.

// TODO: Do we really need this many nested sections?

const query = createQuery('venuesAndMenusByUser', {
  venues: {
    $filter({filters}) {
      filters.userId = Meteor.userId()
    },
    name: 1,
    menus: {
      $filter({filters, params}) {
        filters.venueId = params.venue
      },
      name: 1,
      publish: 1,
      items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish:1, parentId:1 },
      // TODO: This seems like a very hacky way to handle nested sections but it's better than a lot of database calls (I think?  This needs looking into.)
      sections: { name:1, publish: 1, parentId: 1, items: { name: 1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
        sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
          sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
            sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
              sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
                sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
                  sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
                    sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
                      sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
                        sections: { name: 1, publish: 1, parentId: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, options:1, publish: 1, parentId: 1 },
                          // Current lowest nested level possible
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
});

if (Meteor.isServer) {
  query.expose();
}

export default query;

/**
 * The sections above can be better viewed like so:
 * 
    sections: { 
      name:1, 
      items: { name: 1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
      sections: { 
        name: 1, 
        items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
        sections: { 
          name: 1, 
          items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
          sections: { name: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
            ...
          }
        }
      }
    }

    Each section contains a name property, a property for it's items and a property for nested sections.
    These nested sections then also have the same properties.  Hence, the horribly nested structure.    
 *  
 */