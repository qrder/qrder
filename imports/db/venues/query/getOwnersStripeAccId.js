import { Meteor } from 'meteor/meteor';
import { createQuery, MemoryResultCacher } from 'meteor/cultofcoders:grapher';
// TODO: For some reason, need to import VenueCollection here otherwise
//        gives an error (even though it didn't before)
import { VenueCollection } from '/imports/db/venues/collection';

const query = createQuery('getOwnersStripeAccId', {
  venues: {
    $filter({filters, params}) {
      filters._id = params.venueId
    },
    user: { 
      integrations: {
        stripe: {
          stripeAccountId: 1
        }
      }
    } 
  }
});

if (Meteor.isServer) {
  query.expose();

  // cache results (60 secs)
  query.cacheResults(
    new MemoryResultCacher({ ttl: Meteor.settings.private.cache_duration * 1000 })
  );
}

export default query;
