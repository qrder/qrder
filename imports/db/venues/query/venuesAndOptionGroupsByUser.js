import { createQuery } from 'meteor/cultofcoders:grapher';
// TODO: For some reason, need to import MenuCollection here otherwise
//        gives an error (even though it didn't before)
import { OptionGroupsCollection } from '/imports/db/optionGroups/collection';

const query = createQuery('venuesAndOptionGroupsByUser', {
  venues: {
    $filter({filters}) {
      filters.userId = Meteor.userId()
    },
    name: 1,
    optionGroups: {
      name: 1,
      max: 1,
      min: 1,
      options: 1
    }
  }
});

if (Meteor.isServer) {
  query.expose();
}

export default query;