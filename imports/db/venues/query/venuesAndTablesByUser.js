import { createQuery } from 'meteor/cultofcoders:grapher';
// TODO: For some reason, need to import MenuCollection here otherwise
//        gives an error (even though it didn't before)
import { VenueCollection } from '/imports/db/venues/collection';

// TODO: Move this into a shared query folder?  As it's relevant to both 
//    venues and menus.

const query = createQuery('venuesAndTablesByUser', {
  venues: {
    $filter({filters}) {
      filters.userId = Meteor.userId()
    },
    name: 1,
    tables: {
      name: 1
    }
  },
});

if (Meteor.isServer) {
  query.expose();
}

export default query;
