import { Meteor } from 'meteor/meteor';
import { VenueCollection } from './collection';
import { MenuCollection } from '/imports/db/menu/collection';
import { TableCollection } from '/imports/db/table/collection';
import { OptionGroupsCollection } from '/imports/db/optionGroups/collection';

VenueCollection.addLinks({
  menus: {
    collection: MenuCollection,
    inversedBy: 'venue'
  },
  tables: {
    collection: TableCollection,
    inversedBy: 'venue'
  },
  orders: {
    collection: OrdersCollection,
    inversedBy: 'venue'
  },
  optionGroups: {
    collection: OptionGroupsCollection,
    inversedBy: 'venue'
  },
  user: {
    type: 'one',
    field: 'userId',
    collection: Meteor.users
  },
});
