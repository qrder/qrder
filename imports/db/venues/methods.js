import { check } from 'meteor/check';
import axios from 'axios';
import { Meteor } from 'meteor/meteor';
import { sanitizeString } from '/imports/utils/shared';
import { VenueCollection } from './collection';
import { MenuCollection } from '/imports/db/menu/collection';
import { TableCollection } from '/imports/db/table/collection';
import { OptionGroupsCollection } from "/imports/db/optionGroups/collection";
import getOwnersStripeAccId from '/imports/db/venues/query/getOwnersStripeAccId';

Meteor.methods({
  "venues.insert": function(venue) {
    if(Meteor.isServer) {
      check(venue.name, String);
      check(venue.paymentOptions, Object);
      check(venue.paymentOptions.cash, Boolean);
      check(venue.paymentOptions.cardMachine, Boolean);
      check(venue.paymentOptions.cardOnline, Boolean);

      venue.name = sanitizeString(venue.name);
  
      // Check if any other venues exist with the same name
      if(VenueCollection.findOne({name: venue.name, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('venue-with-name-already-exists', `You already have a venue called '${venue.name}'.`);
      }
      
      return VenueCollection.insert({ ...venue });
    }
  },

  "venues.update": function(id, venue) {
    if(Meteor.isServer) {
      check(venue.name, String);
      check(venue.paymentOptions, Object);
      check(venue.paymentOptions.cash, Boolean);
      check(venue.paymentOptions.cardMachine, Boolean);
      check(venue.paymentOptions.cardOnline, Boolean);

      venue.name = sanitizeString(venue.name);
  
      // Check if any OTHER venues exist with the same name (ensures the error is not thrown for its own venue)
      const res = VenueCollection.findOne({name: venue.name, userId: Meteor.userId()});
      if(res !== undefined && res._id !== id ) {
        throw new Meteor.Error('venue-with-name-already-exists', `You already have a venue called '${venue.name}'.`);
      }
    
      VenueCollection.update(id, { $set: { ...venue } });
    }
  },

  "venues.delete": function(venueId) {
    if(Meteor.isServer) {
      check(venueId, String);

      if(VenueCollection.findOne({_id: venueId, userId: Meteor.userId()}) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-venue', `It seems you do not own this venue. If you think this is a mistake, please get in contact.`);
      }
      
      VenueCollection.remove(venueId);
    }
  },

  "venues.count": function() {
    if(Meteor.isServer) {
      return VenueCollection.find({userId: Meteor.userId()}).count();
    }
  },

  "venues.updateActiveSubscription": function(customerId, value) {
    if(Meteor.isServer) {
      check(value, Boolean);
      check(customerId, String);
      
      // Get userId by stripeCustomerId so that we can update all venues for the relevant customer
      const user = Meteor.users.findOne({'subscription.stripeCustomerId': customerId});

      VenueCollection.update({ userId: user._id }, { 
        $set: { 
          userId: user._id,
          isUnderActiveSubscription: value 
        },
      }, { multi: true });
    }
  },

  "venues.getOwnersStripeAccId": (venueId) => {
    if(Meteor.isServer) {
      try {
        return getOwnersStripeAccId.clone({ venueId }).fetchOne().user.integrations.stripe.stripeAccountId;
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  },

  "venues.updateOrderConfirmationMessage": (venueId, message) => {
    if(Meteor.isServer) {
      check(venueId, String);
      check(message, String);

      VenueCollection.update({ userId: Meteor.userId(), _id: venueId }, { 
        $set: { 'settings.orders.confirmationMessage': message }
      });
    }
  },

  "venues.logoUploaded": (uuid, cdnUrl, venueId) => {
    if(Meteor.isServer) {
      check(uuid, String);
      check(cdnUrl, String);
      check(venueId, String);

      // TODO: Error checking / handling?

      VenueCollection.update({ userId: Meteor.userId(), _id: venueId }, { 
        $set: { 
          'settings.logo.uuid': uuid,
          'settings.logo.cdnUrl': cdnUrl
        }
      });
    }    
  },

  "venues.removeLogo": async (uuid, venueId) => {
    if(Meteor.isServer) {
      check(uuid, String);
      check(venueId, String);
      
      const publicKey = Meteor.settings.private.uploadcare.public_key;
      const secretKey = Meteor.settings.private.uploadcare.secret_key;

      try {
        return await axios({
            method: 'delete',
            url: `https://api.uploadcare.com/files/${uuid}/`,
            headers: {
              Authorization: `Uploadcare.Simple ${publicKey}:${secretKey}`,
            }
          })
          .then(() => {
            VenueCollection.update({ userId: Meteor.userId(), _id: venueId }, { 
              $set: { 
                'settings.logo.uuid': "",
                'settings.logo.cdnUrl': ""
              }
            });
          });
      } catch (error) {
        throw new Meteor.Error(
          error.response.status,
          `[${error.response.status}] ${error.response.statusText} - Please try again or contact us.`
        );
      }
    }
  },

  "venues.updateBrandColours": (colours, venueId) => {
    if(Meteor.isServer) {
      check(colours, Object);
      check(colours.navbarBg, String);
      check(colours.navbarIcon, String);
      check(colours.addBtn, String);
      check(colours.nextBtn, String);
      check(venueId, String);

      VenueCollection.update({ userId: Meteor.userId(), _id: venueId }, { 
        $set: { 
          'settings.brand.colour.navbarBg': colours.navbarBg,
          'settings.brand.colour.navbarIcon': colours.navbarIcon,
          'settings.brand.colour.addBtn': colours.addBtn,
          'settings.brand.colour.nextBtn': colours.nextBtn
        }
      });
    }   
  }
});

// Cascade Deletion - Delete Menus, Tables & Option Groups related to Venue
// Orders are not deleted if their venue is deleted. 
VenueCollection.before.remove(function (userId, doc) {
  MenuCollection.remove({venueId: doc._id, userId: userId});
  TableCollection.remove({venueId: doc._id, userId: userId});
  OptionGroupsCollection.remove({venueId: doc._id, userId: userId});
});
