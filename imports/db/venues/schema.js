import SimpleSchema from "simpl-schema";
import { Meteor } from 'meteor/meteor';
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";

// Custom Validation Error Messages
SimpleSchema.setDefaultMessages({
  messages: {
    en: {
      "no_payment_option_selected": "At least one payment option must be selected.",
    },
  },
});

const getPaymentOptionValue = (po) => {
  if(po.isSet) {
    return po.value;
  } else {
    return false;
  }
}

VenueSchema = new SimpleSchema(
  {
    name: { type: String, required: true, index: 1 },
    createdAt: { type: Date, autoValue: () => { return new Date() } },
    userId: { 
      type: String, 
      index: 1,
      autoValue: function() { 
        // if isSet, update is being called 
        // (mainly to prevent userId being set to null on venues.updateActiveSubscription)
        if(this.isSet) return this.value;
        // else, insert is being called
        else return Meteor.userId();
      } 
    },
    isAcceptingOrders: { type: Boolean, label: "Accept Orders", defaultValue: true, required: true },
    isUnderActiveSubscription: { type: Boolean, defaultValue: true, required: true },
    paymentOptions: { 
      type: Object, 
      label: "Payment Options", 
      defaultValue: {},
      custom: function() {
        if(
          !this.field('paymentOptions.cash').value && 
          !this.field('paymentOptions.cardMachine').value && 
          !this.field('paymentOptions.cardOnline').value
        ) {
          return "no_payment_option_selected";
        } 
      }
    },
    'paymentOptions.cash': { type: Boolean, label: "Cash", defaultValue: false, required: true },
    'paymentOptions.cardMachine': { type: Boolean, label: "Card Machine", defaultValue: false, required: true },
    'paymentOptions.cardOnline': { type: Boolean, label: "Online Card Payments", defaultValue: false, required: true },
    settings: { type: Object, defaultValue: {} },
    'settings.orders': { type: Object, defaultValue: {} },
    'settings.orders.confirmationMessage':  { type: String, defaultValue: "" },
    'settings.logo': { type: Object, defaultValue: {} },
    'settings.logo.uuid': { type: String, defaultValue: "" },
    'settings.logo.cdnUrl': { type: String, defaultValue: "" },
    'settings.brand': { type: Object, defaultValue: {} },
    'settings.brand.colour': { type: Object, defaultValue: {} },
    'settings.brand.colour.navbarBg': { type: String, defaultValue: "#4066D4" },
    'settings.brand.colour.navbarIcon': { type: String, defaultValue: "#EDEDED" }, 
    'settings.brand.colour.addBtn': { type: String, defaultValue: "#28a745" }, 
    'settings.brand.colour.nextBtn': { type: String, defaultValue: "#28a745" } 
  },
  { tracker: Tracker, requiredByDefault: false }
);

const VenueBridge = new SimpleSchema2Bridge(VenueSchema);

export { VenueBridge, VenueSchema };
