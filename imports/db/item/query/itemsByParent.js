import { createQuery } from 'meteor/cultofcoders:grapher';
// TODO: For some reason, need to import MenuCollection here otherwise
//        gives an error (even though it didn't before)
import { ItemCollection } from '/imports/db/item/collection';

const query = createQuery('itemsByParent', {
  items: {
    $filter({filters, params}) {
      filters.userId = Meteor.userId(),
      filters.parentId = params.parentId
    },
    name: 1,
    description: 1,
    prices: 1,
    vegetarian: 1,
    vegan: 1,
    glutenFree: 1,
    containsAlcohol: 1,
    containsFish: 1,
    customOptionGroups: 1
  }
});

if (Meteor.isServer) {
  query.expose();
}

export default query;