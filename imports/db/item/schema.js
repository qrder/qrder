import { Meteor } from "meteor/meteor";
import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";

ItemSchema = new SimpleSchema(
  {
    name: { type: String, required: true },
    description: String,
    prices: { type: Array, label: "Price(s)", required: true },
    // TODO: create subschema for prices object
    'prices.$': { type: Object, required: true },
    'prices.$.name': {
      type: String,
      required: true,
      autoValue: function() {
        // Name field is not shown if length is 1, so set it to empty string
        if(this.field('prices').value.length == 1) return ''; 
      }
    },
    'prices.$.value': { type: Number, required: true },
    vegetarian: { type: Boolean, defaultValue: false },
    vegan: { type: Boolean, defaultValue: false },
    glutenFree: { type: Boolean, defaultValue: false, label: "Gluten Free" },
    containsAlcohol: { type: Boolean, defaultValue: false, label: "Alcohol" },
    containsFish: { type: Boolean, defaultValue: false, label: "Fish" },
    options: { type: Array, label: "Option Groups" },
    'options.$': String,
    publish: { type: Boolean, defaultValue: true },
    createdAt: { type: Date, autoValue: () => { return new Date() } },
    userId: { type: String, autoValue: () => { return Meteor.userId() }, index: 1 },
    parentId: { type: String, uniforms: () => null, index: 1 }, 
  },
  { tracker: Tracker, requiredByDefault: false }
);

const ItemBridge = new SimpleSchema2Bridge(ItemSchema);

export { ItemBridge, ItemSchema };
