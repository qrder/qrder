import { ItemSchema } from './schema';

ItemCollection = new Mongo.Collection('items');

ItemCollection.attachSchema(ItemSchema);

export { ItemCollection };