import { check, Match} from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { ItemCollection } from './collection';
import { sanitizeString } from '/imports/utils/shared';

Meteor.methods({

  // TODO: Contemplate adding warnings as well as errors
  // E.g. Not adding a price will mean it is free/0.00 (allowed but warned about it)

  "items.insert": function(item) {
    if(Meteor.isServer) {
      validateItem(item);
      item = sanitizeItem(item);

      // Check if any other items exist with the same name in this section or menu
      if(ItemCollection.findOne({name: item.name, parentId: item.parentId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('item-with-name-already-exists', `You already have an item called '${item.name}' in this menu / section.`);
      }
      
      ItemCollection.insert({ ...item });
    }
  },
  
  "items.duplicate": function(item) {
    if(Meteor.isServer) {
      validateItem(item);
      item = sanitizeItem(item);

      // Check if any other items exist with the same name in this section or menu
      if(ItemCollection.findOne({name: item.name, parentId: item.parentId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('item-with-name-already-exists', `You already have an item called '${item.name}' in this menu / section.`);
      }

      ItemCollection.insert({ ...item });
    }
  },

  "items.update": function(id, item) {
    if(Meteor.isServer) {
      validateItem(item);
      item = sanitizeItem(item);
      check(id, String);

      // Check if any OTHER items exist with the same name (ensures the error is not thrown for its own item)
      const res = ItemCollection.findOne({name: item.name, parentId: item.parent, userId: Meteor.userId()});
      if(res !== undefined && res._id !== id) {
        throw new Meteor.Error('item-with-name-already-exists', `You already have an item called '${item.name}' in this menu / section.`);
      }
      
      ItemCollection.update(id, { $set: { ...item } });
    }
  },

  "items.delete": function(itemId) {
    if(Meteor.isServer) {
      check(itemId, String);
  
      // Check that logged in user definitely owns this item
      if(ItemCollection.findOne({_id: itemId, userId: Meteor.userId()}) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-item', `It seems you do not own this item. If you think this is a mistake, please get in contact.`);
      }
      
      ItemCollection.remove(itemId);
    }
  }
});

/**
 * Run checks on each value in the object before adding to db
 * @param {Object} item 
 */
const validateItem = (item) => {
  check(item.name, String);
  check(item.description, Match.Optional(String));
  check(item.prices, Match.Optional(Array));
  check(item.parentId, String);
  if(item.prices !== undefined) {
    check(item.prices, Match.Where((prices) => {
      prices.forEach(price => {
        check(price.name, Match.Optional(String));
        check(price.value, Match.Optional(Number));
      });
      return true;
    }));
  }
  check(item.vegetarian, Match.Optional(Boolean));
  check(item.vegan, Match.Optional(Boolean));
  check(item.glutenFree, Match.Optional(Boolean));
  check(item.containsAlcohol, Match.Optional(Boolean));
  check(item.containsFish, Match.Optional(Boolean));
  check(item.publish, Match.Optional(Boolean));
}


/**
 * Sanitize each text field in item
 * @param {Object} item 
 */
const sanitizeItem = (item) => {
  const newItem = item;
  // Clean name & description
  newItem.name = sanitizeString(newItem.name);
  if(newItem.description) newItem.description = sanitizeString(newItem.description);
  // Clean price names
  if(newItem.prices !== undefined) {
    newItem.prices.map(price => {
      price.name = sanitizeString(price.name);
    });
  }
  return newItem;
}