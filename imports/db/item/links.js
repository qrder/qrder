import { ItemCollection } from './collection';
import { MenuCollection } from '/imports/db/menu/collection';
import { SectionCollection } from '/imports/db/section/collection';
import { OptionGroupsCollection } from '/imports/db/optionGroups/collection';

ItemCollection.addLinks({
  parent: {
    type: 'one',
    field: 'parentId',
    collection: MenuCollection || SectionCollection
  },
  optionGroups: {
    type: 'many',
    field: 'options',
    collection: OptionGroupsCollection
  }
});