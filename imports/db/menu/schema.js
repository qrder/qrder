import { Meteor } from 'meteor/meteor';
import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";

MenuSchema = new SimpleSchema(
  {
    name: { type: String, required: true },
    venueId: { type: String, label: "Venue", required: true, index: 1 },
    publish: { type: Boolean, defaultValue: true, required: true },
    createdAt: { type: Date, autoValue: () => { return new Date() } },
    userId: { type: String, autoValue: () => { return Meteor.userId() }, index: 1 }
  },
  { tracker: Tracker, requiredByDefault: false }
);

const MenuBridge = new SimpleSchema2Bridge(MenuSchema);

export { MenuBridge, MenuSchema };