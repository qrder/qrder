import { MenuSchema } from './schema';

MenuCollection = new Mongo.Collection('menus');

MenuCollection.attachSchema(MenuSchema);

export { MenuCollection };
