import { createQuery, MemoryResultCacher } from 'meteor/cultofcoders:grapher';

// TODO: Move into shared folder?

const query = createQuery('menusAndPaymentOptionsByVenue', {
  venues: {
    $filter({filters, params}) {
      filters._id = params.venueId
    },
    name: 1,
    isAcceptingOrders: 1,
    isUnderActiveSubscription: 1,
    paymentOptions: 1,
    settings: 1,
    menus: {
      $filters: { publish: true },
      name: 1,
      items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
      // TODO: This seems like a very hacky way to handle nested sections but it's better than a lot of database calls (I think?  Might need to clarify this)
      sections: { $filters: { publish: true }, name:1, items: { $filters: { publish: true }, name: 1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
        sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
          sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
            sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
              sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
                sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
                  sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
                    sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
                      sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
                        sections: { $filters: { publish: true }, name: 1, items: { $filters: { publish: true }, name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1, optionGroups: { name:1, min:1, max:1, options:1 } },
                          // Current lowest nested level possible
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },  
});

if (Meteor.isServer) {
  query.expose();

  // cache results (60 secs)
  query.cacheResults(
    new MemoryResultCacher({ ttl: Meteor.settings.private.cache_duration * 1000 })
  );
}

export default query;


/**
 * The sections above can be better viewed like so:
 * 
    sections: { 
      name:1, 
      items: { name: 1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
      sections: { 
        name: 1, 
        items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
        sections: { 
          name: 1, 
          items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
          sections: { name: 1, items: { name:1, description:1, prices:1, vegetarian:1, vegan:1, glutenFree:1, containsAlcohol:1, containsFish:1, customOptionGroups:1 },
            ...
          }
        }
      }
    }

    Each section contains a name property, a property for it's items and a property for nested sections.
    These nested sections then also have the same properties.  Hence, the horribly nested structure.
    Each item and section (and all nested items and sections) apply the  $filters: { publish: true }
    to ensure only published items and sections get fetched.
 *  
 */