import { MenuCollection } from './collection';
import { ItemCollection } from '/imports/db/item/collection';
import { SectionCollection } from '/imports/db/section/collection';
import { VenueCollection } from '/imports/db/venues/collection';

MenuCollection.addLinks({
  venue: {
    type: 'one',
    field: 'venueId',
    collection: VenueCollection
  },
  sections: {
    collection: SectionCollection,
    inversedBy: 'parent'
  },
  items: {
    collection: ItemCollection,
    inversedBy: 'parent'
  }
});
