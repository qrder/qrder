import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { sanitizeString } from '/imports/utils/shared';
import { MenuCollection } from './collection';
import { SectionCollection } from '/imports/db/section/collection';
import { ItemCollection } from '/imports/db/item/collection';
import { insertItems, insertSections } from '/imports/utils/menu';
 
Meteor.methods({
  "menus.insert": function(menu) {
    if(Meteor.isServer) {
      check(menu.name, String);
      check(menu.venueId, String);
      check(menu.publish, Boolean);
  
      menu.name = sanitizeString(menu.name);
  
      // Check if any other menus exist with the same name
      if(MenuCollection.findOne({name: menu.name, venueId: menu.venueId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('menu-with-name-already-exists', `You already have a menu called '${menu.name}' in this venue.`);
      }
      
      MenuCollection.insert({ ...menu });
    }
  },
  
  "menus.duplicate": function(menu) {
    if(Meteor.isServer) {
      check(menu.name, String);
      check(menu.venueId, String);
      check(menu.publish, Boolean);
  
      menu.name = sanitizeString(menu.name);
  
      // Check if any other menus exist with the same name
      if(MenuCollection.findOne({name: menu.name, venueId: menu.venueId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('menu-with-name-already-exists', `You already have a menu called '${menu.name}' in this venue.`);
      }

      // Create new menu document
      const insertedMenuId = MenuCollection.insert({ ...menu });
      insertItems(menu.items, insertedMenuId);
      // starts a recursive call to insert sub-items and sub-sections
      insertSections(menu.sections, insertedMenuId); 
    }
  },

  "menus.update": function(id, menu) {
    if(Meteor.isServer) {
      check(id, String);
      check(menu.name, String);
      check(menu.venueId, String);
      check(menu.publish, Boolean);
  
      menu.name = sanitizeString(menu.name);
  
      // Check if any OTHER menus exist with the same name (ensures the error is not thrown for its own menu)
      const res = MenuCollection.findOne({name: menu.name, venueId: menu.venueId, userId: Meteor.userId()});
      if(res !== undefined && res._id !== id) {
        throw new Meteor.Error('menu-with-name-already-exists', `You already have a menu called '${menu.name}' in this venue.`);
      }
      
      MenuCollection.update(id, { $set: { ...menu } });
    }
  },

  "menus.delete": function(menuId) {
    if(Meteor.isServer) {
      check(menuId, String); 
      
      if(MenuCollection.findOne({_id: menuId, userId: Meteor.userId()}) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-menu', `It seems you do not own this menu. If you think this is a mistake, please get in contact.`);
      }

      MenuCollection.remove(menuId);
    }
  },

  "menus.count": function() {
    if(Meteor.isServer) {
      return MenuCollection.find({userId: Meteor.userId()}).count();
    }
  },
});

// Cascade Deletion - Delete Sections & Items related to Menu
MenuCollection.before.remove(function (userId, doc) {
  SectionCollection.remove({parentId: doc._id, userId: userId});
  ItemCollection.remove({parentId: doc._id, userId: userId});
});
