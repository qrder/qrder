import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { sendApplicationErrorEmail } from "/imports/utils/shared/emails.js";

Meteor.methods({
  "shared.sendApplicationErrorEmail": function(subject, preheader, page, component, area, error) {
    if(Meteor.isServer) {
      check(subject, String);
      check(preheader, String);   
      check(page, String);   
      check(component, String);   
      check(area, String);   
      check(error, String);
      
      sendApplicationErrorEmail(subject, preheader, page, component, area, error);
    }
  },
});
