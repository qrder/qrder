import { SectionSchema } from './schema';

SectionCollection = new Mongo.Collection('sections');

SectionCollection.attachSchema(SectionSchema);

export { SectionCollection };