import { SectionCollection } from './collection';
import { MenuCollection } from '/imports/db/menu/collection';
import { ItemCollection } from '/imports/db/item/collection';

SectionCollection.addLinks({
  parent: {
    type: 'one',
    field: 'parentId',
    collection: MenuCollection || SectionCollection
  },
  sections: {
    collection: SectionCollection,
    inversedBy: 'parent'
  },
  items: {
    collection: ItemCollection,
    inversedBy: 'parent'
  }
});