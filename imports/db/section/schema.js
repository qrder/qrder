import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";

SectionSchema = new SimpleSchema(
  {
    name: { type: String, required: true },
    publish: { type: Boolean, defaultValue: true, required: true },
    createdAt: { type: Date, autoValue: () => { return new Date() } },
    userId: { type: String, autoValue: () => { return Meteor.userId() }, index: 1 },
    parentId: { type: String, index: 1 }
  },
  { tracker: Tracker, requiredByDefault: false }
);

const SectionBridge = new SimpleSchema2Bridge(SectionSchema);

export { SectionBridge, SectionSchema };
