import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { sanitizeString } from '/imports/utils/shared';
import { SectionCollection } from './collection';
import { ItemCollection } from '/imports/db/item/collection';
import { insertItems, insertSections } from '/imports/utils/menu';

Meteor.methods({
  "sections.insert": function(section) {
    if(Meteor.isServer) {
      check(section.name, String);
      check(section.parentId, String);
      check(section.publish, Boolean);

      section.name = sanitizeString(section.name);
  
      // Check if any other venues exist with the same name
      if(SectionCollection.findOne({name: section.name, parentId: section.parentId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('section-with-name-already-exists', `You already have a section called '${section.name}' in this section / menu.`);
      }

      SectionCollection.insert({ ...section });
    }
  },
  
  "sections.duplicate": function(section) {
    if(Meteor.isServer) {
      check(section.name, String);
      check(section.parentId, String);
      check(section.publish, Boolean);

      section.name = sanitizeString(section.name);
  
      // Check if any other venues exist with the same name
      if(SectionCollection.findOne({name: section.name, parentId: section.parentId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('section-with-name-already-exists', `You already have a section called '${section.name}' in this section / menu.`);
      }

      const insertedSectionId = SectionCollection.insert({ ...section });
      insertItems(section.items, insertedSectionId);
      // starts a recursive call to insert sub-items and sub-sections
      insertSections(section.sections, insertedSectionId); 
    }
  },

  "sections.update": function(id, section) {
    if(Meteor.isServer) {
      check(section.name, String);
      check(section.publish, Boolean);

      section.name = sanitizeString(section.name);

      // Check if any OTHER sections exist with the same name (ensures the error is not thrown for its own section)
      const res = SectionCollection.findOne({name: section.name, userId: Meteor.userId()});
      if(res !== undefined && res._id !== id) {
        throw new Meteor.Error('section-with-name-already-exists', `You already have a section called '${section.name}' in this section / menu.`);
      }

      SectionCollection.update(id, { $set: { ...section } });
    }
  },

  "sections.delete": function(sectionId) {
    if(Meteor.isServer) {
      check(sectionId, String);
      
      if(SectionCollection.findOne({_id: sectionId, userId: Meteor.userId()}) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-section', `It seems you do not own this section. If you think this is a mistake, please get in contact.`);
      }

      SectionCollection.remove(sectionId);
    }
  }
});

// Cascade Deletion - Delete Sections & Items related to Section
SectionCollection.before.remove(function (userId, doc) {
  SectionCollection.remove({parentId: doc._id, userId: userId});
  ItemCollection.remove({parentId: doc._id, userId: userId});
});