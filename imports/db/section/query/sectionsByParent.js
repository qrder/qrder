import { createQuery } from 'meteor/cultofcoders:grapher';
// TODO: For some reason, need to import MenuCollection here otherwise
//        gives an error (even though it didn't before)
import { SectionCollection } from '/imports/db/section/collection';

const query = createQuery('sectionsByParent', {
  sections: {
    $filter({filters, params}) {
      filters.userId = Meteor.userId(),
      filters.parentId = params.parentId
    },
    name: 1,
    sections: {
      name: 1
    },
    items: {
      name: 1
    }
  }
});

if (Meteor.isServer) {
  query.expose();
}

export default query;