import SimpleSchema from "simpl-schema";
import { Tracker } from "meteor/tracker";

/**
 * Option Schema 
 * Used as nested schema for the OptionGroupsSchema used in
 * pre-defined option groups and custom options group on items.
 */

// Custom Validation Error Messages
SimpleSchema.setDefaultMessages({
  messages: {
    en: {
      "no_price_set": "Value is required.  Use '0' or '0.00' if the option has not extra cost."
    },
  },
});

export const OptionSchema = new SimpleSchema({
  name: { 
    type: String, 
    required: true 
  },
  value: { 
    type: Number, 
    // required: true, // required handled in 'custom' below
    defaultValue: 0.00,
    custom: function() {
      // custom required message
      if(!this.isSet) return "no_price_set";
    }
  }
}, { tracker: Tracker, requiredByDefault: false }); 
