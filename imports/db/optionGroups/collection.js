import { OptionGroupsSchema } from './schema';

OptionGroupsCollection = new Mongo.Collection('optionGroups');

OptionGroupsCollection.attachSchema(OptionGroupsSchema);

export { OptionGroupsCollection };
