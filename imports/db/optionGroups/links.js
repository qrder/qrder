import { OptionGroupsCollection } from './collection';
import { ItemCollection } from '/imports/db/item/collection';

OptionGroupsCollection.addLinks({
  venue: {
    type: 'one',
    field: 'venueId',
    collection: VenueCollection
  },
  item: {
    collection: ItemCollection,
    inversedBy: 'optionGroups'
  }
});
