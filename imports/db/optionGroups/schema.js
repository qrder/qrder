import { Meteor } from 'meteor/meteor';
import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";
import { OptionSchema } from "/imports/db/optionGroups/subschema/OptionSchema";

// Custom Validation Error Messages
SimpleSchema.setDefaultMessages({
  messages: {
    en: {
      "min_higher_than_max": "MIN cannot be higher than MAX.",
    },
  },
});

OptionGroupsSchema = new SimpleSchema(
  {
    name: { type: String, required: true },
    options: { type: Array, label: "Options", minCount: 1, required: true },
    'options.$': OptionSchema,
    min: {
      type: SimpleSchema.Integer,
      uniforms: { 
        decimal: false, 
        min: 0 
      },
      custom: function() {
        if(this.value > this.siblingField("max").value) 
          return "min_higher_than_max";
      },
      autoValue: function() { 
        return this.isSet ? this.value : 0;
      }
    },
    max: {
      type: SimpleSchema.Integer,
      uniforms: { 
        decimal: false, 
        min: 0 
      },
      autoValue: function() { 
        if(this.siblingField("min").value == 0 && this.value == 0) {
          return this.siblingField("options").value.length;
        }
        if(this.isSet) return this.value;
        return 0;
      }
    },
    createdAt: { type: Date, autoValue: () => { return new Date() } },
    venueId: { type: String, label: "Venue", required: true, index: 1 },
    userId: { type: String, autoValue: () => { return Meteor.userId() }, index: 1 }
  },
  { tracker: Tracker, requiredByDefault: false }
);

const OptionGroupsBridge = new SimpleSchema2Bridge(OptionGroupsSchema);

export { OptionGroupsBridge, OptionGroupsSchema };