import { check, Match } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { OptionGroupsCollection } from './collection';
import { sanitizeString } from '/imports/utils/shared';

Meteor.methods({
  "optionGroups.insert": function(og) {
    if(Meteor.isServer) {
      check(og.name, String);
      check(og.venueId, String);
      check(og.min, Match.Maybe(Number));
      check(og.max, Match.Maybe(Number));

      og.name = sanitizeString(og.name);
      
      // Check if any other option groups exist with the same name
      if(OptionGroupsCollection.findOne({name: og.name, venueId: og.venueId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('option-group-with-name-already-exists', `You already have an option group called '${og.name}' in this venue.`);
      }
      
      OptionGroupsCollection.insert({ ...og });
    }
  },

  "optionGroups.createSelectOptions": function() {
    return OptionGroupsCollection.find({ userId: Meteor.userId() }).map(opt =>{
      return { label: opt.name, value: opt._id };
    });
  },

  "optionGroups.duplicate": function(og) {
    if(Meteor.isServer) {
      check(og.name, String);
      check(og.venueId, String);
      check(og.min, Match.Maybe(Number));
      check(og.max, Match.Maybe(Number));

      og.name = sanitizeString(og.name);

      // Check if any other option groups exist with the same name
      if(OptionGroupsCollection.findOne({name: og.name, venueId: og.venueId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('option-group-with-name-already-exists', `You already have an option group called '${og.name}' in this venue.`);
      }
      
      OptionGroupsCollection.insert({ ...og });
    }
  },

  "optionGroups.update": function(id, og) {
    if(Meteor.isServer) {
      check(og.name, String);
      check(og.venueId, String);
      check(og.min, Match.Maybe(Number));
      check(og.max, Match.Maybe(Number));

      og.name = sanitizeString(og.name);

      // Check if any OTHER option groups exist with the same name (ensures the error is not thrown for itself)
      const res = OptionGroupsCollection.findOne({name: og.name, venueId: og.venueId, userId: Meteor.userId()});
      if(res !== undefined && res._id !== id) {
        throw new Meteor.Error('option-group-with-name-already-exists', `You already have an option group called '${og.name}' in this venue.`);
      }
      
      OptionGroupsCollection.update(id, { $set: { ...og } });
    }
  },

  "optionGroups.delete": function(ogId) {
    if(Meteor.isServer) {
      check(ogId, String);

      if(OptionGroupsCollection.findOne({_id: ogId, userId: Meteor.userId()}) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-option-group', `It seems you do not own this option group. If you think this is a mistake, please get in contact.`);
      }
      
      OptionGroupsCollection.remove(ogId);
    }
  },
});
