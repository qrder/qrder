import { UserSchema } from './schema';

Meteor.users.attachSchema(UserSchema);

// Deny all client-side updates to user documents
Meteor.users.deny({
  update() { return true; }
});