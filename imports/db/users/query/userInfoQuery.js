import { Meteor } from 'meteor/meteor';
import { createQuery } from 'meteor/cultofcoders:grapher';

const query = createQuery('userInfoQuery', {
  users: {
    $filter({filters}) {
      filters._id = Meteor.userId()
    },
    username: 1,
    emails: 1,
    notifications: 1,
    integrations: 1,
    subscription: 1,
    settings: 1,
    venues: {
      _id: 1,
      name: 1
    }
  }
});

if (Meteor.isServer) {
  query.expose();
}


export default query;
