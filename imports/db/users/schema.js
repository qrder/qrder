import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";

// Meteor docs for database schemas:
// https://guide.meteor.com/collections.html#denormalization

UserSchema = new SimpleSchema(
  {
    username: { type: String, required: true },
    // Services needs to be blackboxed otherwise it throws an error on sending email verification link:
    // Error: After filtering out keys not in the schema, your modifier is now empty
    services: { type: Object, blackbox: true },
    'services.password': Object,
    'services.password.bcrypt': String,
    'services.resume': { type: Object, blackbox: true },
    emails: { type: Array, label: "Email Addresses" },
    'emails.$': Object,
    'emails.$.address': String,
    'emails.$.verified': Boolean,
    createdAt: Date,
    profile: { type: Object, blackbox: true },
    notifications: { type: Array, defaultValue: [] },
    'notifications.$': { type: Object, blackbox: true },
    subscription: { type: Object, defaultValue: {} },
    'subscription.stripeSubscriptionId': { type: String, required: true },
    'subscription.stripeCustomerId': { type: String, required: true, index: 1 },
    'subscription.stripePriceId': String,
    // important to include price in case prices of subscriptions change (and if current users are included in that price change)
    'subscription.price': { type: Number, label: "Subscription Price" }, 
    'subscription.type': { 
      type: String, 
      defaultValue: "Free", 
      label: "Subscription Tier",
      allowedValues: ["Free", "Standard", "Enterprise"]
    },
    'subscription.stripeStatus': { 
      type: String,
      allowedValues: ["incomplete", "incomplete_expired", "trialing", "active", "past_due", "canceled", "unpaid"]
    },
    'subscription.status': { 
      type: String,
      defaultValue: "Trial", 
      label: "Subscription Status",
      allowedValues: ["Trial", "Trial Ended", "Active", "Cancelled", "Paused"]
    },
    'subscription.freeTrialEnd': { type: String, label: "Trial Ends" },
    'subscription.currentPeriodEnd': { type: String, label: "Current Period Ends" },
    integrations: { type: Object, defaultValue: {} },
    'integrations.stripe': Object,
    'integrations.stripe.stripeAccountId': String,
     // completed onboarding - payments refers to them being able to get payments from their customers
    'integrations.stripe.paymentsEnabled': { type: Boolean, defaultValue: false },
    // completed verification (id and address) - payouts refers to users being able to recieve payouts from Stripe
    'integrations.stripe.payoutsEnabled': { type: Boolean, defaultValue: false }, 
    // completed initial onboarding, waiting for status change so that verification step can be done.
    'integrations.stripe.verificationStepReady': { type: Boolean, defaultValue: false }, 
    // TODO: add paypal related information that needs to be stored
    'integrations.paypal': Object,
    settings: { type: Object, defaultValue: {} },
    'settings.currency': { type: Object, defaultValue: {} },
    'settings.currency.code': { 
      type: String, 
      defaultValue: "GBP", 
      label: "Currency Code", 
      // TODO: If and when I add more currencies, they will need to be added here
      // Probably using a function to return all currency codes from JSON file.
      allowedValues: ["GBP"] 
    },
    'settings.currency.name': { type: String, defaultValue: "British Pound Sterling" },
    'settings.currency.symbol': { type: String, defaultValue: "£" },
  },
  { tracker: Tracker, requiredByDefault: false }
);

const UserBridge = new SimpleSchema2Bridge(UserSchema);

export { UserBridge, UserSchema };
