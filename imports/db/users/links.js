import { Meteor } from 'meteor/meteor';
import { VenueCollection } from '/imports/db/venues/collection';

Meteor.users.addLinks({
  venues: {
    collection: VenueCollection,
    inversedBy: 'user'
  }
});
