import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { Accounts } from 'meteor/accounts-base';
import { EJSON } from 'meteor/ejson';
import moment from 'moment';
import { createUserWithStripeInfoObject, emailExistsInStripe } from "/imports/utils/stripe";
import { usernameExistsInQrder, emailExistsInQrder } from "/imports/utils/users";
import { sendVerificationEmail, sendPasswordResetEmail } from "/imports/utils/shared/emails.js";

const genericErrorMessage = `An error occurred.  Please try again.  If this issue persists, please get in contact.`;

Meteor.methods({
  /**
   * Return Meteor.user() data to client
   */
  "users.userInfo": () => {
    if(Meteor.isServer) {
      return Meteor.user({
        fields: {
          username: 1,
          emails: 1,
          notifications: 1,
          integrations: 1,
          subscription: 1,
          settings: 1
        }
      });
    }
  }, 

  /**
   * Register user:
   * - Create customer in stripe
   * - Add them to the 'standard' subscription with trial period
   * - Add user to our database
   * - Send verification email to user
   * @param {*} user info from registration form
   */
  "users.register": async (user) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      // TODO: Send email to qrder to notify us that stripe failed to load (as customers will likely not bother)
      if(!stripe) throw new Meteor.Error('failed-to-load-stripe', genericErrorMessage);

      // Check our database for user with email
      if(emailExistsInQrder(user.email)) {
        throw new Meteor.Error(
          'user-with-email-already-exists', 'A user with this email address already exists.'
        );
      } 
      // Check our database for user with username
      if(usernameExistsInQrder(user.username)) {
        throw new Meteor.Error(
          'user-with-username-already-exists', 'This username has been taken, please choose a different one.'
        );
      }

      // Check Stripe for existing customer with email 
      if(await emailExistsInStripe(user.email)) {
        throw new Meteor.Error(
          'user-with-email-already-exists', 'A user with this email address already exists.'
        );
      }

      // TODO: If a customer is created in stripe, but it errors on subscription creation, how do we handle this?
      // Do a stripe check to see if a customer exists with no subscription?

      // Create a new Stripe customer 
      const customer = await stripe.customers.create({ email: user.email });

      // Create subscription
      const subscription = await stripe.subscriptions.create({
        customer: customer.id,
        items: [{ price: Meteor.settings.private.stripe.products.standard.priceId }], 
        trial_end: moment().add(2, 'months').unix()
      });
      
      // Create user object to add to database
      user = createUserWithStripeInfoObject(user, customer.id, subscription);
      // Create user in db
      const userId = Accounts.createUser(user);
      
      // send verification email
      const verificationUrl = Meteor.call("users.getVerificationUrl", userId, user.email);
      sendVerificationEmail(user.email, verificationUrl);
    }
  },

  "users.delete": (itemId) => {
    
    // TODO: dont delete, set status to inactive  
    
  },

  "users.update": (user) => {

    // TODO: Add logging

    // TODO: Handle this better as update is being used now!

    Meteor.users.update(Meteor.userId(), { $set: { ...user } });
  },

  "users.getVerificationUrl": (userId, email) => {
    if(Meteor.isServer) {
      try {
        check(userId, String);
        check(email, String);

        // create token record
        const tokenRecord = {
          token: Random.secret(),
          address: email,
          when: new Date()
        };
        // add to database
        Meteor.users.update(userId, {$push: {'services.email.verificationTokens': tokenRecord}});
        // create url to send to user
        const verifyEmailUrl = Accounts.urls.verifyEmail(tokenRecord.token);
        
        return verifyEmailUrl;
      } catch (error) {
        // TODO: handle errors
        console.error(error)
      }
    }
  },

  "users.sendResetPasswordEmail": (email) => {
    if(Meteor.isServer) {
      try {
        check(email, String);
        // create token record
        const tokenRecord = {
          token: Random.secret(),
          email: email,
          when: new Date(),
          reason: 'reset'
        };
        // add to database
        Meteor.users.update({emails: { $elemMatch: { address: email }}}, {
          $set: {'services.password.reset': tokenRecord}
        });
        // create url to send to user
        const resetUrl = Accounts.urls.resetPassword(tokenRecord.token);
        
        sendPasswordResetEmail(email, resetUrl);
      } catch (error) {
        // TODO: handle errors
        console.error(error)
      }
    }
  },

  /**
   * Update Payment Info:
   * - Attach payment method to customer
   * - Update invoice settings
   * - Return subscription info for follow up function calls
   * @param {*} customerId (stripe)
   * @param {*} subscriptionId (stripe)
   * @param {*} paymentMethodId (payment info to add to stripe)
   */
  "users.addPaymentMethod": async (customerId, subscriptionId, paymentMethodId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      // Attach the payment method to the customer
      try {
        await stripe.paymentMethods.attach(paymentMethodId, {
          customer: customerId,
        });
      } catch (error) {
        // TODO: Confirm if card declination is only reason for this error (402)
        console.error(error)
        throw new Meteor.Error(
          "402-payment-failed", 
          "[402] Sorry, the payment failed.  Your card was declined."
        );
      }
      // Change the default invoice settings on the customer to the new payment method
      await stripe.customers.update(customerId, {
          invoice_settings: {
            default_payment_method: paymentMethodId,
          },
        }
      );
      // get subscription
      const subscription = await stripe.subscriptions.retrieve(subscriptionId);
      
      return { subscription, priceId: Meteor.settings.private.stripe.products.standard.priceId }
    }
  },

  /**
   * Delete (Detach) Payment Method from Customer
   * @param {*} paymentMethodId (stripe)
   */
  "users.deletePaymentMethod": async (paymentMethodId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      // Return all card payment methods for customer
      try {
        return await stripe.paymentMethods.detach(paymentMethodId);
      } catch (error) {
        console.error(error)
      }
    }
  },

  "users.setDefaultPaymentMethod": async (customerId, paymentMethodId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      try {
        return await stripe.customers.update(customerId, {
          invoice_settings: {
            default_payment_method: paymentMethodId
          }
        });
      } catch (error) {
        console.error(error)
      }
    }
  },

  "users.getDefaultPaymentMethod": async (customerId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      try {
        const customer = await stripe.customers.retrieve(customerId);
        return customer.invoice_settings.default_payment_method;
      } catch (error) {
        console.error(error)
      }
    }
  },

  /**
   * Get Payment Methods For Customer
   * @param {*} customerId (stripe)
   */
  "users.listPaymentMethods": async (customerId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      // Return all card payment methods for customer
      try {
        return await stripe.paymentMethods.list({
          customer: customerId,
          type: 'card'
        });
      } catch (error) {
        console.error(error)
      }
    }
  },

  "users.payInvoice": async (invoiceId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      // Pay invoice using customers default payment method
      try {
        return await stripe.invoices.pay(invoiceId);
      } catch (error) {
        console.error(error);
        return error;
      }
    }
  },

  "users.getInvoices": async (customerId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      // Return all invoices for customer
      try {
        return await stripe.invoices.list({
          customer: customerId,
          // expand returns the entire object, rather than just the ID
          expand: ['data.payment_intent'] 
        });
      } catch (error) {
        console.error(error)
      }
    }
  },

  /**
   * Cancel Subscription
   * @param {*} subscriptionId (stripe)
   */
  "users.cancelSubscription": async (subscriptionId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      
      try {
        await stripe.subscriptions.del(subscriptionId, { prorate: true });
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  },

  /**
   * Restart cancelled Subscription
   * @param {*} subscriptionId (stripe)
   */
  "users.restartSubscription": async (customerId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      
      try {
        // Create new stripe subscription
        const subscription = await stripe.subscriptions.create({
          customer: customerId,
          items: [{ price: Meteor.settings.private.stripe.products.standard.priceId }]
        });
        // Update our DB with new subscription info
        Meteor.users.update(Meteor.userId(), { 
          $set: { 
            'subscription.stripeSubscriptionId': subscription.id,
            'subscription.type': "Standard",
            'subscription.price': (subscription.plan.amount_decimal / 100).toFixed(2),
            'subscription.stripeStatus': subscription.status,
            'subscription.status': "Active",
            'subscription.currentPeriodEnd': moment.unix(subscription.current_period_end).format('DD/MM/YYYY')
          } 
        });
        // Update existing users' venues either way
        Meteor.call("venues.updateActiveSubscription", customerId, true);
        // Return updated user info
        return Meteor.call("users.userInfo");
      } catch (error) {
        console.log(error)
        switch (error.type) {
          case 'StripeInvalidRequestError':
            // TODO: StripeInvalidRequestError is not only for payment method issues
            //        so handle these better
            throw new Meteor.Error(
              'no-payment-method', 
              `Please attach a payment method to your account before restarting your subscription.  
                This can be done in the 'Payment Information' tab.`
            ); 
          default:
            throw new Meteor.Error(
              'unknown-error', 
              `We're really sorry, but an unknown error occurred.  
                Please contact customer support.`
            ); 
        }
      }
    }
  },

  /**
   * Pause Subscription by stopping invoices / payment collection in stripe
   * and updating our DB to reflect this (used to provision access)
   * @param {*} subscriptionId (stripe)
   */
  "users.pauseSubscription": async (subscriptionId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      try {
        // Pause subscription
        const subscription = await stripe.subscriptions.update(subscriptionId, {
          pause_collection: { behavior: 'void' }
        });
        
        // stripeStatus does not change when paused - remains active but we need to update our own status to paused
        Meteor.users.update(Meteor.userId(), { 
          $set: { 'subscription.status': "Paused" } 
        });

        Meteor.call('venues.updateActiveSubscription', subscription.customer, false, (error, result) => {
          if(error) {
            console.log(error);
          } else {
            // console.log(result);
          }
        });
        // return updated user info
        return Meteor.call("users.userInfo");
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  },

  /**
   * Resume Subscription by unsetting pause_collection field in Stripe
   * and updating our DB to reflect this (used to provision access)
   * @param {*} subscriptionId (stripe)
   */
  "users.resumeSubscription": async (customerId, subscriptionId) => {
    if(Meteor.isServer) {
      const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
      // Resume paused subscription
      try {
        await stripe.subscriptions.update(subscriptionId, { pause_collection: '' });
        // stripeStatus does not change when resumed - remains active
        // but we need to update our own status back to active
        Meteor.users.update(Meteor.userId(), { 
          $set: { 'subscription.status': "Active" } 
        });
        Meteor.call("venues.updateActiveSubscription", customerId, true);
        // return updated user info
        return Meteor.call("users.userInfo");
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  },

  // TODO: This will not be needed straight away as GBP will be the 
  // only supported currency on launch
  "users.getCurrencyInfo": (code) => {
    if(Meteor.isServer) {
      // Get all currency info based on code field
      const currencyInfo = EJSON.parse(Assets.getText('currencies.json'))[code];
      return currencyInfo;
    }
  },

  // Clear pending notifications attached to user
  "users.clearNotifications": () => {
    if(Meteor.isServer) {
      try {
        Meteor.users.update(Meteor.userId(), { 
          $set: { notifications: [] } 
        });
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  },

  "users.createConnectedAccount": async () => {
    if(Meteor.isServer) {
      try {
        const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
        const account = await stripe.accounts.create({type: 'standard'});
        Meteor.users.update(Meteor.userId(), { 
          $set: { 'integrations.stripe.stripeAccountId': account.id } 
        });
        return account.id;
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  },

  "users.linkConnectedAccount": async (accountId) => {
    if(Meteor.isServer) {
      try {
        const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
        const accountLink = await stripe.accountLinks.create({
          account: accountId,
          // TODO: refresh url should redirect users back to Stripe flow to connect
          refresh_url: `${Meteor.settings.public.url}/dashboard/settings`,
          return_url: `${Meteor.settings.public.url}/dashboard/settings`,
          type: 'account_onboarding',
        });
        return accountLink.url;
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  }
});
