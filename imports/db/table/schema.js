import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";

TableSchema = new SimpleSchema(
  {
    name: { type: String, required: true },
    venueId: { type: String, required: true, index: 1 },
    createdAt: { type: Date, autoValue: () => { return new Date() } },
    userId: { type: String, autoValue: () => { return Meteor.userId() }, index: 1 }
  },
  { tracker: Tracker, requiredByDefault: false }
);

const TableBridge = new SimpleSchema2Bridge(TableSchema);

export { TableBridge, TableSchema };