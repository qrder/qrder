import { TableCollection } from './collection';
import { VenueCollection } from '/imports/db/venues/collection';

TableCollection.addLinks({
  venue: {
    type: 'one',
    field: 'venueId',
    collection: VenueCollection
  }
});
