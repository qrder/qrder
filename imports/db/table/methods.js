import { check } from 'meteor/check';
import { sanitizeString } from '/imports/utils/shared';
import { TableCollection } from './collection';

Meteor.methods({
  "tables.insert": function(table) {
    if(Meteor.isServer) {
      check(table.name, String);
      check(table.venueId, String);

      table.name = sanitizeString(table.name);
  
      // Check if any other venues exist with the same name
      if(TableCollection.findOne({name: table.name, venueId: table.venueId, userId: Meteor.userId()}) !== undefined) {
        throw new Meteor.Error('table-with-name-already-exists', `You already have a table called '${table.name}' in this venue.`);
      }
      
      TableCollection.insert({ ...table });
    }
  },

  "tables.update": function(id, table) {
    if(Meteor.isServer) {
      check(table.name, String);
      check(table.venueId, String);

      table.name = sanitizeString(table.name);

      // Check if any OTHER tables exist with the same name (ensures the error is not thrown for its own table)
      const res = TableCollection.findOne({name: table.name, venueId: table.venueId, userId: Meteor.userId()});
      if(res !== undefined && res._id !== id) {
        throw new Meteor.Error('table-with-name-already-exists', `You already have a table called '${table.name}' in this venue.`);
      }
      
      TableCollection.update(id, { $set: { ...table } });
    }  
  },

  "tables.delete": function(tableId) {
    if(Meteor.isServer) {
      check(tableId, String);
      
      if(TableCollection.findOne({_id: tableId, userId: Meteor.userId()}) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-table', `It seems you do not own this table. If you think this is a mistake, please get in contact.`);
      }

      TableCollection.remove(tableId);
    }
  },
});
