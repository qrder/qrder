import { TableSchema } from './schema';

TableCollection = new Mongo.Collection('tables');

TableCollection.attachSchema(TableSchema);

export { TableCollection };
