import { Meteor } from 'meteor/meteor';
import { VenueCollection } from '/imports/db/venues/collection';

/**
 * Methods to update / mgirate database schema
 * when it changes.
 * Needed to use Methods like this as it threw errors when running
 * update in migrations.js
 * "Error: Meteor.userId can only be invoked in method calls or publications."
 */

Meteor.methods({

  // Version 5
  "migrations.v5Up": () => {
    if(Meteor.isServer) {
      VenueCollection.find().forEach(venue => {
        VenueCollection.update(venue._id, { 
          $set: { 
            ...venue,
            settings: {
              orders: {
                confirmationMessage: ""
              },
              logo: {
                cdnUrl: "",
                uuid: ""
              },
              brand: {
                colour: {
                  navbarBg: "#4066D4",
                  navbarIcon: "#EDEDED",
                  addBtn: "#28a745",
                  nextBtn: "#28a745"
                }
              }
            }
          } 
        })
      })
    }
  }
});
