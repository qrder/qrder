import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { OrdersCollection } from './collection';
import { sanitizeString } from '/imports/utils/shared';
import { sendOrderConfirmationEmail, sendOrderErrorEmail } from "/imports/utils/shared/emails.js";

Meteor.methods({
  "orders.insert": function(order) {
    if(Meteor.isServer) {
      validateOrder(order);
      order = sanitizeOrder(order);
  
      // TODO: Error handling 
      //      Check price against database?
      //      Create total price using database? - Much safer - prevents client editing
      //      Check opening times of venue? Could be done client side?
      
      return OrdersCollection.insert({ ...order });
    }
  },

  "orders.sendErrorEmail": function(to, venue) {
    if(Meteor.isServer) {
      check([to, venue], [String]);

      // Let other method calls run
      this.unblock();

      sendOrderErrorEmail(to, venue);
    }
  },

  "orders.sendConfirmationEmail": function(to, venue, total, orderId) {
    if(Meteor.isServer) {
      check([to, venue, orderId], [String]);

      // Let other method calls run
      this.unblock();

      sendOrderConfirmationEmail(to, venue, total, orderId);
    }
  },

  "orders.acknowledge": (order) => {
    if(Meteor.isServer) {
      check(order._id, String);

      if(OrdersCollection.findOne({ _id: order._id, "venueOrderedAt.id": order.venueOrderedAt.id  }) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-order', `It seems you do not own this order. If you think this is a mistake, please get in contact.`);
      }
  
      OrdersCollection.update(order._id, { 
        $set: { 
          ...order,
          hasBeenAcknowledged: true,
          acknowledgedAt: new Date()
        } 
      });
    }
  },

  "orders.complete": async (order) => {
    if(Meteor.isServer) {
      check(order._id, String);

      if(OrdersCollection.findOne({ _id: order._id, "venueOrderedAt.id": order.venueOrderedAt.id  }) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-order', `It seems you do not own this order. If you think this is a mistake, please get in contact.`);
      }

      OrdersCollection.update(order._id, { 
        $set: { 
          ...order,
          hasBeenAcknowledged: true,
          hasBeenCompleted: true,
          completedAt: new Date()
        } 
      });
    }
  },

  "orders.paidFor": async (order) => {
    if(Meteor.isServer) {
      check(order._id, String);

      if(OrdersCollection.findOne({ _id: order._id, "venueOrderedAt.id": order.venueOrderedAt.id  }) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-order', `It seems you do not own this order. If you think this is a mistake, please get in contact.`);
      }

      OrdersCollection.update(order._id, { 
        $set: { 
          ...order,
          hasBeenPaidFor: true,
          paidForAt: new Date()
        } 
      });
    }
  },

  "orders.reopen": (order) => {
    if(Meteor.isServer) {
      check(order._id, String);
      check(order.venueOrderedAt.id, String);
  
      if(OrdersCollection.findOne({ _id: order._id, "venueOrderedAt.id": order.venueOrderedAt.id  }) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-order', `It seems you do not own this order. If you think this is a mistake, please get in contact.`);
      }
      
      OrdersCollection.update(order._id, { $set: { ...order } });
    }
  },

  "orders.cancel": (order) => {
    if(Meteor.isServer) {
      check(order._id, String);
      check(order.venueOrderedAt.id, String);
  
      if(OrdersCollection.findOne({ _id: order._id, "venueOrderedAt.id": order.venueOrderedAt.id  }) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-order', `It seems you do not own this order. If you think this is a mistake, please get in contact.`);
      }
      
      OrdersCollection.update(order._id, { 
        $set: { 
          ...order,
          hasBeenCancelled: true,
          cancelledAt: new Date()
        } 
      });
    }
  },

  "orders.uncancel": (order) => {
    if(Meteor.isServer) {
      check(order._id, String);
      check(order.venueOrderedAt.id, String);
  
      if(OrdersCollection.findOne({ _id: order._id, "venueOrderedAt.id": order.venueOrderedAt.id  }) == undefined) {
        throw new Meteor.Error('not-the-owner-of-the-order', `It seems you do not own this order. If you think this is a mistake, please get in contact.`);
      }
      
      OrdersCollection.update(order._id, { 
        $set: { 
          ...order,
          hasBeenCancelled: false,
          cancelledAt: ""
        } 
      });
    }
  },

  // Create a payment intent for our users' customers.
  // When online card payment option is selected during an order.
  // Called when  
  "orders.createCustomerPaymentIntent": async (venueId, amount) => {
    if(Meteor.isServer) {
      try {
        const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);

        // TODO: Can these be condensed into one Method call? 
        // Get owner of venue
        const venue = VenueCollection.findOne(venueId);  
        // Get connected stripe account id for that owner
        const user = Meteor.users.findOne(venue.userId);

        // TODO: Make this more secure by checking amount against database menu items directly

        const paymentIntent = await stripe.paymentIntents.create({
          payment_method_types: ['card'],
          amount: (amount * 100), // x100 as Stripe needs currency in pence
          currency: 'gbp',
          application_fee_amount: 0,
        }, {
          stripeAccount: user.integrations.stripe.stripeAccountId,
        });
        return paymentIntent.client_secret;
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  },

  "orders.paymentIntentSuccessful": (pi) => {
    if(Meteor.isServer) {
      try {
        check(pi, String);

        OrdersCollection.update({stripePaymentIntent: pi}, { 
          $set: { 
            hasBeenPaidFor: true,
            paidForAt: new Date()
          } 
        });
      } catch (error) {
        // TODO: throw errors properly
        console.error(error)
        return error;
      }
    }
  }
});



/**
 * Run checks on each value in the object before adding to db
 * @param {Object} order 
 */
const validateOrder = (order) => {
  check(order.items, Array);
  check(order.orderTotal, Number);
  check(order.comments, String);
  check(order.paymentOptionSelected, Match.OneOf('cash', 'cardMachine', 'cardOnline'));
  check(order.tableToDeliverTo, String);
  check(order.venueOrderedAt, Match.ObjectIncluding({ id: String, name: String }));

  check(order.items, Match.Where((items) => {
    items.forEach(item => {        
      check(item.name, String);
      check(item.priceName, String);
      check(item.price, Number);
      check(item.options, Array);
      check(item.quantity, Number);
    });
    return true;
  }));
}


/**
 * Sanitize each text field in item
 * @param {Object} order 
 */
const sanitizeOrder= (order) => {
  const newOrder = order;
  // Clean comments, payment option & table
  newOrder.comments = sanitizeString(newOrder.comments);
  newOrder.paymentOptionSelected = sanitizeString(newOrder.paymentOptionSelected);
  newOrder.comments = sanitizeString(newOrder.comments);
  newOrder.venueOrderedAt.name = sanitizeString(newOrder.venueOrderedAt.name);
  // Clean price names
  newOrder.items.map(item => {
    item.name = sanitizeString(item.name);
    item.priceName = sanitizeString(item.priceName);
  });
  // Return cleaned order
  return newOrder;
}