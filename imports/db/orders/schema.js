import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import { Tracker } from "meteor/tracker";
import moment from 'moment';

OrdersSchema = new SimpleSchema(
  {
    items: Array,
    'items.$': { type: Object, blackbox: true },
    orderTotal: Number,
    email: String,
    comments: String,
    paymentOptionSelected: String,
    status: { 
      type: String,
      allowedValues: ["OPEN", "CLOSED", "CANCELLED"],
      autoValue: function() {
        const acknowledged = this.field('hasBeenAcknowledged').value;
        const completed = this.field('hasBeenCompleted').value;
        const paid = this.field('hasBeenPaidFor').value;
        const cancelled = this.field('hasBeenCancelled').value;

        if(cancelled) {
          return "CANCELLED";
        } else if(acknowledged && completed && paid) {
          return "CLOSED";
        } else {
          return "OPEN";
        }
      }
    },
    hasBeenCancelled: { type: Boolean, defaultValue: false },
    hasBeenAcknowledged: { type: Boolean, defaultValue: false },
    hasBeenCompleted: { type: Boolean, defaultValue: false },
    hasBeenPaidFor: { type: Boolean, defaultValue: false },
    stripePaymentIntent: { type: String, index: 1 }, // only used for online card payments with Stripe
    tableToDeliverTo: String,
    venueOrderedAt: Object, 
    'venueOrderedAt.id': { type: String, index: 1 },
    'venueOrderedAt.name': String,
    acknowledgedAt: {
      type: Date,
      autoValue: function() {
        // if acknowledged directly
        if(this.isSet) {
          return this.value;
        }
        // else it marked completed before acknowledged was ticked  
        else if(this.field('completedAt').isSet) {
          return new Date();
        }
      }
    },
    completedAt: Date,
    paidForAt: Date,
    cancelledAt: Date,
    createdAtDate: {
      type: String,
      index: 1,
      autoValue: function() { 
        if(this.isSet) {
          return this.value;
        } else {
          return moment(new Date()).format("DD/MM/YYYY");
        }
      }
    },
    createdAtTime: {
      type: String, 
      autoValue: function() { 
        if(this.isSet) {
          return this.value;
        } else {
          return moment(new Date()).format("HH:mm:ss");
        }
      }
    },
    lastUpdatedAtDate: {
      type: String,
      autoValue: function() { 
        if(!this.isSet) {
          return this.field('createdAtDate').value;
        } else {
          return moment(new Date()).format("DD/MM/YYYY");
        }
      }
    },
    lastUpdatedAtTime: {
      type: String,
      autoValue: function() { 
        if(!this.isSet) {
          return this.field('createdAtTime').value;
        } else {
          return moment(new Date()).format("HH:mm:ss");
        }
      }
    },
  },
  { tracker: Tracker, requiredByDefault: false }
);

const OrdersBridge = new SimpleSchema2Bridge(OrdersSchema);

export { OrdersBridge, OrdersSchema };