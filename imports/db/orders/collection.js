import { OrdersSchema } from './schema';

OrdersCollection = new Mongo.Collection('orders');

OrdersCollection.attachSchema(OrdersSchema);

export { OrdersCollection };
