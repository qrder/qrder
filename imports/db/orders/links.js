import { OrdersCollection } from './collection';
import { VenueCollection } from '/imports/db/venues/collection';

OrdersCollection.addLinks({
  venue: {
    type: 'one',
    field: 'venueOrderedAt.id',
    collection: VenueCollection
  }
});
