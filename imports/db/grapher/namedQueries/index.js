// Import all named queries in here
// Need to import all named queries in here so that a publication gets made for it
import '/imports/db/menu/query/menusAndPaymentOptionsByVenue';


import '/imports/db/section/query/sectionsByParent';


import '/imports/db/item/query/itemsByParent';


import '/imports/db/venues/query/venuesByUser';
import '/imports/db/venues/query/venuesAndMenusByUser';
import '/imports/db/venues/query/venuesAndTablesByUser';
import '/imports/db/venues/query/venuesAndOrdersByUser';
import '/imports/db/venues/query/venuesAndOptionGroupsByUser';
import '/imports/db/venues/query/ordersCustomisation';


import '/imports/db/users/query/userInfoQuery';