// It is a good idea to import these collections to have them registered
// Because when we use createQuery({collection: 'xxx'}), it will try to search to see
// if any collection with that name has been loaded, and it could throw an exception
import '/imports/db/users/collection';
import '/imports/db/menu/collection';
import '/imports/db/section/collection';
import '/imports/db/item/collection';
import '/imports/db/table/collection';
import '/imports/db/venues/collection';
import '/imports/db/orders/collection';
import '/imports/db/optionGroups/collection';

// Links are imported outside collection level
// The reason is that we may have two collections referencing each other, and this results
// in having the Collection object as {__esModule: ...}, and we need the true instantation of it
import '/imports/db/menu/links';
import '/imports/db/section/links';
import '/imports/db/item/links';
import '/imports/db/table/links';
import '/imports/db/venues/links';
import '/imports/db/orders/links';
import '/imports/db/optionGroups/links';
import '/imports/db/users/links';

// Methods
import '/imports/db/users/methods';
import '/imports/db/menu/methods';
import '/imports/db/section/methods';
import '/imports/db/item/methods';
import '/imports/db/table/methods';
import '/imports/db/venues/methods';
import '/imports/db/orders/methods';
import '/imports/db/optionGroups/methods';
import '/imports/db/shared/methods';
import '/imports/db/migrations/methods';

// We also need to import the named queries
// Otherwise they will be unusable without specifically importing them.
import './namedQueries';
