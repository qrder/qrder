// In order to expose collections, the expose method needs to be run server-side
// when the server starts.

// TODO: Figure out which of these is actually needed as the table import was not included
// at one point but it was all working as expected.

import '/imports/db/menu/expose.js';
import '/imports/db/section/expose.js';
import '/imports/db/item/expose.js';
import '/imports/db/table/expose.js';
import '/imports/db/venues/expose.js';
import '/imports/db/orders/expose.js';
import '/imports/db/optionGroups/expose.js';
