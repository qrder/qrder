import React, { useEffect, useState } from 'react';
import { Session } from 'meteor/session';
import { Tracker } from 'meteor/tracker';
import { Navbar, Image } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import { useIdleTimer } from 'react-idle-timer';
import CookieConsent from "react-cookie-consent";
import { ShoppingCartIcon } from '/imports/client/components/customer/ShoppingCartIcon/ShoppingCartIcon.jsx';
import { BackIcon } from '/imports/client/components/customer/BackIcon';
import { getVenue, getBaseUrl, clearSession, getCurrentPage } from '/imports/utils/customers';

export default ({ children }) => {
  const history = useHistory();

  const [logo, setLogo] = useState();
  const [navbarBgColour, setNavbarBgColour] = useState();
 
  // Redirect back to menu & in-turn reset session variables & destroy cart 
  useIdleTimer({
    timeout: 120000, // 2 minutes of inactivity
    onIdle: () => {
      // Do not idle redirect if page is no-orders or already on menu
      if(getCurrentPage().includes("no-orders") || getCurrentPage().includes("menu")) return;

      clearSession();
      history.push(getBaseUrl()); // back to menu
    }
  });

  // Update logo when CustomerMenu query has set the logo session var
  Tracker.autorun(() => {
    if(Session.equals('logo', "") || Session.equals('logo', undefined)) return;
    if(Session.get('logo') !== logo) setLogo(Session.get('logo'));
  });
  // Update background colour when session var set
  Tracker.autorun(() => {
    if(Session.equals('brandColours', "") || Session.equals('brandColours', undefined)) return;
    if(JSON.parse(Session.get('brandColours')).navbarBg !== navbarBgColour) {
      setNavbarBgColour(JSON.parse(Session.get('brandColours')).navbarBg);
    }
  });

  return (
    <>
      <Navbar 
        variant="dark" 
        sticky="top"
        style={{ backgroundColor: navbarBgColour }}
      >
        <Navbar.Text onClick={() => history.goBack()}>
          <BackIcon />
        </Navbar.Text>

        <Navbar.Brand className="mx-auto">
          {
            logo ? (
              <Image src={logo + '-/preview/100x100/-/quality/smart/'} />
            ) : (
              <span className="text-dark">{getVenue('name')}</span>
            )
          }
        </Navbar.Brand> 

        <Navbar.Text onClick={() => history.push(`${getBaseUrl()}/order`)}>
          <ShoppingCartIcon />
        </Navbar.Text>

      </Navbar>

      {children}

      <CookieConsent style={{zIndex: 9999}}>This website uses cookies to enhance the user experience.</CookieConsent>
    </>
  )
};
