import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import CookieConsent from "react-cookie-consent";
import { useHistory } from "react-router-dom";

export default ({children}) => {
  const history = useHistory();

  return (    
    <Container fluid>
      <Row>
        <Col md={8} lg={6}>
          <Container className="position-absolute">
            <Row className="mx-auto">
              <Col md={9} lg={8} className="mx-auto">
                <Button onClick={() => history.push("/")} className="mt-3">
                  <FontAwesomeIcon icon={faChevronLeft} className="mr-2" />
                  Home
                </Button>
              </Col>
            </Row>
          </Container>

          <div className="login d-flex align-items-center">
            <Container>
              <Row>
                <Col md={9} lg={8} className="mx-auto">

                  {children}
                
                </Col>
              </Row>
            </Container>
          </div>
        </Col>

        <Col md={4} lg={6} className="d-none d-md-flex bg-image"></Col>

      </Row>
      
      <CookieConsent>This website uses cookies to enhance the user experience.</CookieConsent>
    </Container>
  )
};