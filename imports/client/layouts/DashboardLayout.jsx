import React, { useEffect, useState, useContext } from 'react';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { Tracker } from 'meteor/tracker';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import userInfoQuery from '/imports/db/users/query/userInfoQuery';
import { GlobalContext } from '/imports/context/GlobalState';
import CookieConsent from "react-cookie-consent";
import { WelcomeModal } from '/imports/client/components/dashboard/modals/WelcomeModal';
import { SideNav } from '/imports/client/components/dashboard/layout/SideNav/SideNav.jsx';
import { Footer } from '/imports/client/components/dashboard/layout/Footer'
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import { UserTour } from '/imports/client/components/dashboard/shared/UserTour/UserTour.jsx';
import { Notification } from '/imports/client/components/dashboard/shared/Notification';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Loading } from '/imports/client/components/shared/Loading';

const DashboardLayout = ({ data, error, isLoading, children }) => {
  if(error) console.log(error)

  const [welcomeModalShow, setWelcomeModalShow] = useState(false);
  const [isTourOpen, setIsTourOpen] = useState(false);
  const [skipToc, setSkipToc] = useState(false);
  const [loaded, setLoaded] = useState(false);

  const history = useHistory();

  const { addUser, userInfo, mobileNavOpen, setMobileNav } = useContext(GlobalContext);

  // Call correct toast function based on variant specified
  const showNotification = (notification, options = {}) => {
    switch(notification.variant) {
      case "success":
        toast.success(<Notification message={notification.message} variant={notification.variant} />, options);
        break;
      case "error":
        toast.error(<Notification message={notification.message} variant={notification.variant} />, options);
        break;
      case "warning":
        toast.warning(<Notification message={notification.message} variant={notification.variant} />, options);
        break;
      case "info":
        toast.info(<Notification message={notification.message} variant={notification.variant} />, options);
        break;
    }
  }

  // Check if user has any notifications & if so, show them
  const checkNotifications = () => {
    if(userInfo?.notifications?.length > 0) {
      userInfo.notifications.map(n => showNotification(n, { autoClose: false }));
      Meteor.call("users.clearNotifications");
    }
  }

  // check if mobile by checking if hamburger nav icon is visible
  const checkIfMobile = () => {
    const mobileNav = document.querySelector("[data-tour='mobile-nav']");
    if(mobileNav) {
      const style = window.getComputedStyle(mobileNav)
      return style.display == "flex";
    }
  }

  // Check if user has any notifications waiting for them
  useEffect(() => { loaded && checkNotifications() }, [loaded])
  
  // initial load
  useEffect(() => {
    // Redirect to login if not logged in
    if(!Meteor.loggingIn() && !Meteor.userId()) {
      history.push("/login");
    } else if(data) {
      addUser(data); // context
      setLoaded(true);
      data?.venues && Session.setAuth("venueId", data.venues[0]._id);
      // Show welcome modal & automated guide if user just registered
      if(Session.get("loggedInForFirstTime") !== undefined) {
        if(Session.equals("loggedInForFirstTime", "true")) {
          setWelcomeModalShow(true);
        }
      }
    }
  }, [isLoading]);

  useEffect(() => {
    if(data) {
      addUser(data); // context
    }
  }, [data])

  // Track notification session var and show a notification when it changes
  Tracker.autorun(() => {
    if (Session.equals('notification', '' || undefined)) return;

    notification = JSON.parse(Session.get('notification'));
    showNotification(notification); // local version of function
    // Remove notification from session to ensure it is only shown once.
    Session.clear('notification');
  });

  if(isLoading) return <Loading />;

  return (
    <Container fluid className="p-0" data-test="dashboard-layout">
      <div className={`${!mobileNavOpen && 'd-flex'} flex-row min-vh-100`}>
        <div className={`${!mobileNavOpen && 'd-none'} d-xl-block flex-column`} style={{ backgroundColor: "#4166D4" }}>
          <SideNav />
        </div>

        <div className={`${mobileNavOpen ? 'd-none' : 'd-flex'} flex-column w-100 pt-xl-5`} data-tour="main-content">
          {/* TODO: separate this into it's own component?  - MobileNav? */}
          <Container fluid>
            <Row className="d-xl-none text-white px-4 py-3" style={{ backgroundColor: "#4166D4" }} data-tour="mobile-nav">
              <Col className="d-flex align-items-center p-0">
                <FontAwesomeIcon icon={faBars} size="2x" onClick={() => setMobileNav(true)} className="hamburger-icon" data-tour="hamburger-icon" />
              </Col>
              <Col className="d-flex align-items-center justify-content-end p-0">
                {/* TODO: Convert this to logo component as it's used in different places now */}
                <Image 
                  fluid
                  src="/images/qrder-logo/qrder-name-logo-white.png"
                  style={{ height: "40px" }}
                  onClick={() => history.push("/")}
                />
              </Col>
            </Row>
          </Container>

          <Container fluid className="flex-fill my-3 my-xl-0">
            {children}
          </Container>
        
          <Footer welcomeModalClicked={() => setWelcomeModalShow(true)} startTourClicked={() => { setIsTourOpen(true); setSkipToc(false)}} />
        </div>
      </div>

      <UserTour isTourOpen={isTourOpen} closeTour={() => setIsTourOpen(false)} skipToc={skipToc} isMobile={checkIfMobile()} />
      <WelcomeModal show={welcomeModalShow} onHide={() => setWelcomeModalShow(false)} startTourClicked={() => { setIsTourOpen(true); setSkipToc(true)}} />
      <ToastContainer newestOnTop hideProgressBar />
      <CookieConsent>This website uses cookies to enhance the user experience.</CookieConsent>
    </Container>
  )
};

export default withQuery(() => {
  return userInfoQuery.clone()
}, {reactive: false, single: true})(DashboardLayout)