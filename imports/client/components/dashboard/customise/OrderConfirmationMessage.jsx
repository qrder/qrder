import React from 'react';
import { Meteor } from "meteor/meteor";
import { Row, Col } from 'react-bootstrap';
import { AutoForm, LongTextField, SubmitField, ErrorsField } from 'uniforms-bootstrap4';
import "uniforms-bridge-simple-schema-2";
import { sendNotification } from '/imports/utils/shared';
import { VenueBridge } from "/imports/db/venues/schema";

export const OrderConfirmationMessage = ({ data }) => {

  const orderConfirmMessageSubmit = formData => {
    const venueId = formData._id;
    const message = formData.settings.orders.confirmationMessage;
    Meteor.call('venues.updateOrderConfirmationMessage', venueId, message, error => {
      if (error) {
        // setServerError(error.reason);
        sendNotification("An error occurred. Please try again.", "error");
      } else {
        sendNotification(`Order confirmation message updated.`, "success");
      }
    });
  };

  return (
    <AutoForm model={data} schema={VenueBridge} onSubmit={orderConfirmMessageSubmit}>
      {/* TODO: Remove ErrorsField when done */}
      <ErrorsField />
      <Row className="mt-5">
        <Col xs={12} lg={4} className="customisation-labels border-right">
          <strong>Order Confirmation Message</strong>
          <p>Add a custom message to your order confirmation screen.  It is displayed directly below the order number.</p>
        </Col>
        <Col xs={12} lg={8}>
          <LongTextField
            showInlineError
            name="settings.orders.confirmationMessage"
            label={false}
            className="m-0"
            inputClassName="order-confirm-message-textarea"
            placeholder="Enter a custom message for customers to see when they submit an order..."
          />
        </Col>
      </Row>

      <Row>
        <Col className="d-flex justify-content-end pt-4">
          <SubmitField className="mt-3" />
        </Col>
      </Row>
    </AutoForm>
  )
};
