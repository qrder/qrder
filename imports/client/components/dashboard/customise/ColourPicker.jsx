import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { OverlayTrigger, Popover, Row, Col } from 'react-bootstrap';
import { ChromePicker } from 'react-color';

export const ColourPicker = ({ setting, identifier, onColourChange, defaultColour, brandColour }) => {
  const [colour, setColour] = useState(brandColour || defaultColour);

  const handleChange = (c) => {
    setColour(c.hex);
    onColourChange(identifier, c.hex);
  }

  const popover = (
    <Popover id="popover-basic">
      <Popover.Content>
        <ChromePicker 
          color={colour}
          onChangeComplete={(c) => handleChange(c)}
        />
      </Popover.Content>
    </Popover>
  );

  return (
    <Row className="mt-5">
      <Col xs={12} lg={4} className="customisation-labels border-right">
        <p>{setting}</p>
      </Col>
      <Col xs={12} lg={8} className="d-flex align-items-center">
        <span 
          className="mr-3 p-2" 
          style={{ 
            backgroundColor: colour ? colour : defaultColour
          }}
        >
          {colour ? colour : defaultColour}
        </span>
        <OverlayTrigger 
          trigger="click" 
          placement="right" 
          overlay={popover} 
          rootClose
        >
          <FontAwesomeIcon 
            icon={faPen} 
            style={{ color: "#EDEDED" }}
            role="button"
          />
        </OverlayTrigger>
      </Col>
    </Row>
  )
};
