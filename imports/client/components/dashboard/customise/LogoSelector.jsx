import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { ImageUploader } from '/imports/client/components/dashboard/customise/ImageUploader';

export const LogoSelector = ({ logo, venue }) => {

  return (
    <>
      <h4 className="mt-5">Logo</h4>
      <Row className="mt-5">
        <Col xs={12} lg={4} className="customisation-labels border-right">
          <p>Add your logo to make the menu tailored towards your brand.</p>
        </Col>
        <Col xs={12} lg={8}>
          <ImageUploader logo={logo} venue={venue} />
        </Col>
      </Row>
    </>
  )
};
