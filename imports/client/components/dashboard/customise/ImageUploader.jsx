import React, { useState, useEffect } from 'react';
import { Meteor } from "meteor/meteor";
import { Button } from 'react-bootstrap';
import { Widget } from "@uploadcare/react-widget";
import { Error } from '/imports/client/components/shared/Error';

export const ImageUploader = ({ logo, venue }) => {
  const [uuid, setUuid] = useState('');
  const [serverError, setServerError] = useState('');

  useEffect(() => {
    if(logo?.uuid) setUuid(logo.uuid);
  }, [logo]);

  const uploadFile = (file) => {
    if (file) {
      if(file.size > 100000 /* kb / 100mb */) {
        setServerError('Please upload a file that is smaller than 100MB in size.');
        return;
      }
      file.done(info => {
        Meteor.call("venues.logoUploaded", info.uuid, info.cdnUrl, venue, error => {
          error ? setServerError(error) : setUuid(info.uuid);
        })
      })
    }
  }

  const removeFile = () => {
    Meteor.call("venues.removeLogo", uuid, venue, error => {
      error ? setServerError(error.reason) : setUuid('');
    });  
  }

  return (
    <>
      <Widget
        value={uuid}
        publicKey='2f6b2d5c63611445a9f6' 
        id='file' 
        tabs='file url facebook gdrive gphotos dropbox instagram onedrive'
        previewStep='true'
        crop='free'
        imagesOnly
        multiple={false}
        onFileSelect={(file) => uploadFile(file)}
      />
      {
        uuid && (
          <Button 
            size='sm' 
            onClick={() => removeFile()} 
            className="d-block"
            variant='outline-danger'
          >
            Remove logo
          </Button>
        )
      }
      <Error message={serverError} />
    </>
  )
};
