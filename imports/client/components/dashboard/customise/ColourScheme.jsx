import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor';
import { Button } from 'react-bootstrap';
import { sendNotification } from '/imports/utils/shared';
import { ColourPicker } from '/imports/client/components/dashboard/customise/ColourPicker';

export const ColourScheme = ({ colours, venue }) => {
  const [colourSettings, setColourSettings] = useState(colours || {});
  const [changesMade, setChangesMade] = useState(false);

  // TODO: Future:
  // - Under line bar colour for tabs
  // - Background colour
  // - Font colour

  const updateColourSettingsState = (key, colour) => {
    setColourSettings({ ...colourSettings, [key]: colour });
    setChangesMade(true);
  }

  const updateColourSettingsDb = () => {
    Meteor.call("venues.updateBrandColours", colourSettings, venue, error => {
      if(error) {
        sendNotification("An error occurred.  Please try again.", "error");
      } else {
        setChangesMade(false);
        sendNotification("Brand colours updated successfully.", "success");
      }
    });
  }

  return (
    <>
      <h4 className="mt-5">Colour Scheme</h4>

      <ColourPicker 
        setting="Top Bar Background Colour" 
        identifier="navbarBg"
        onColourChange={(key, c) => updateColourSettingsState(key, c)} 
        brandColour={colours?.navbarBg}
        defaultColour={Meteor.settings.public.defaultCustomerMenuColours.navbarBg}
      />
      <ColourPicker 
        setting="Top Bar Icon Colour" 
        identifier="navbarIcon"
        onColourChange={(key, c) => updateColourSettingsState(key, c)} 
        brandColour={colours?.navbarIcon}
        defaultColour={Meteor.settings.public.defaultCustomerMenuColours.navbarIcon}
      />
      <ColourPicker 
        setting="Add Button Colour" 
        identifier="addBtn"
        onColourChange={(key, c) => updateColourSettingsState(key, c)} 
        brandColour={colours?.addBtn}
        defaultColour={Meteor.settings.public.defaultCustomerMenuColours.addBtn}
      />
      <ColourPicker 
        setting="Bottom Button (View Order / Next) Colour" 
        identifier="nextBtn"
        onColourChange={(key, c) => updateColourSettingsState(key, c)} 
        brandColour={colours?.nextBtn}
        defaultColour={Meteor.settings.public.defaultCustomerMenuColours.nextBtn}
      />

      <div className="d-flex align-items-center mt-5">
        <Button 
          onClick={() => updateColourSettingsDb()}
        >Submit</Button>
        {
          changesMade && (
            <span className="ml-3 text-danger">
              You have unsaved changes.  Click 'Submit' to save.
            </span>
          )
        }
      </div>
    </>
  )
};
