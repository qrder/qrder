import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { OptionsListItem } from "/imports/client/components/dashboard/options/OptionsListItem";

export const OptionsList = ({ options }) => {

  if(options == undefined || options.length == 0) return '';

  return (
    <Container>
      <Row>
        <Col className="p-0">
          <hr/>

          {
            options.map((opt, idx) => {
              return <OptionsListItem key={idx} option={opt} />
            })
          }
          
        </Col>
      </Row>
    </Container>
  );
};

