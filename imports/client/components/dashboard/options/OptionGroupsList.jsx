import React from 'react';
import { Container, Row, Col, Accordion } from 'react-bootstrap';
import { OptionGroupsListItem } from "/imports/client/components/dashboard/options/OptionGroupsListItem";

export const OptionGroupsList = ({ options }) => {

  if(options == undefined) return 'Loading...';

  if(options.length == 0) {
    return "You have not created any option groups yet.  Click 'Add Option Group' above to get started.";
  }

  return (
    <Container>
      <Row>
        <Col xl={9} className="p-0">
          <hr/>   

          <Accordion className="mt-3">
            { options.map(opt => <OptionGroupsListItem key={opt._id} optionGroup={opt} />) }
          </Accordion>
          
        </Col>
      </Row>
    </Container>
  );
};

