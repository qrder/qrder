import React from 'react';
import { connectField } from 'uniforms';
import { Row, Col } from 'react-bootstrap';
import { ListDelField, TextField, NumField } from 'uniforms-bootstrap4';

const OptionsListItem = ({ removeIcon }) => {

  return (
    <Row className="m-0">
      <Col xs={2} lg={1} className="p-0">
        <ListDelField removeIcon={removeIcon} className="tour-og-delete-option-button" />
      </Col>
      <Col xs={6} lg={4} className="py-0 px-2">
        <TextField name="name" label={false} placeholder="Name" showInlineError />
      </Col>
      <Col xs={4} lg={3} className="py-0 px-2">
        <NumField decimal name="value" label={false} placeholder="0.00" showInlineError />
      </Col>
    </Row>
  );
}

export default connectField(OptionsListItem, { initialValue: false });