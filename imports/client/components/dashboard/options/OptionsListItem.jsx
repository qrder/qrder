import React from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';

export const OptionsListItem = ({ option }) => {

  return (
    <Container className="p-0 ml-0">
      <Row>
        <Col xs={1} className="d-flex align-items-center justify-content-center p-0">
          {/* TODO: Could our Checkbox comp be used? */}
          <Form.Check className="my-auto" />
        </Col>
        <Col xs={5} className="d-flex align-items-center p-0">
          <span>{option.name}</span>
        </Col>
        <Col xs={3} className="d-flex align-items-center justify-content-center p-0">
          
        </Col>
        <Col xs={3} className="d-flex align-items-center justify-content-end p-0 pr-5">
          <span className="bg-primary text-white py-1 px-2 mr-5">
            £{option.value.toFixed(2)}
          </span>
        </Col>
      </Row>
      <hr/>
    </Container>
  );
};
