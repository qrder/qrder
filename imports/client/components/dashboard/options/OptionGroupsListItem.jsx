import React from 'react';
import { Container, Row, Col, Accordion, Card, Form } from 'react-bootstrap';
import { OptionsList } from '/imports/client/components/dashboard/options/OptionsList';
import { AccordionToggle } from '/imports/client/components/dashboard/shared/AccordionToggle';
import { ActionDropdown } from '/imports/client/components/dashboard/shared/ActionDropdown';

export const OptionGroupsListItem = ({ optionGroup }) => {
  
  return (
    <Card key={optionGroup._id} className="border-0 bg-light tour-og-list-item" style={{ overflow: 'inherit' }}>

      <Card.Header className="p-0 border-0 bg-light">
        <Container>
          <Row>
            <Col xs={1} className="d-flex align-items-center justify-content-center p-0">
              <Form.Check className="my-auto" />
            </Col>
            <Col xs={5} className="p-0">
              <AccordionToggle eventKey={optionGroup._id} className="tour-og-list-item-toggle d-none d-md-inline" />
              {optionGroup.name}
            </Col>
            <Col md={3} className="d-none d-md-flex align-items-center p-0" style={{ color: '#DBDBDB' }}>
              {`MIN: ${optionGroup.min}`}
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {`MAX: ${optionGroup.max}`}
            </Col>
            <Col xs={6} md={3} className="d-flex align-items-center justify-content-end p-0 pr-3">
              <ActionDropdown 
                data={optionGroup} 
                parent="option-group" 
                options={["edit", "duplicate", "delete"]} 
                className="tour-og-hamburger-button"
              />
            </Col>
          </Row>
        </Container>
      </Card.Header>

      <Accordion.Collapse eventKey={optionGroup._id} className="tour-og-list-item-collapse">
        <Card.Body className="p-0 pl-5">

          <OptionsList options={optionGroup.options} parent={optionGroup} />
          
        </Card.Body>
      </Accordion.Collapse>

      <hr/>
    </Card>
  );
};
