import React, { useContext } from 'react';
import { GlobalContext } from '/imports/context/GlobalState';
import { Button } from 'react-bootstrap';

export const OrderButton = ({ buttonToRender, order }) => {

  const { openModal } = useContext(GlobalContext);

  // Render red cancel button
  // Default button - only shown on open orders
  if(buttonToRender == "CANCEL") {
    return (
      <Button 
        variant="danger" 
        onClick={() => openModal('cancel-order', order)}
      >
        Cancel
      </Button>
    );
  }

  // Render grey undo button
  // Only shown on cancelled orders
  if(buttonToRender == "UNDO") {
    return (
      <Button 
        variant="secondary"
        onClick={() => openModal('uncancel-order', order)}
      >
        Undo
      </Button>
    )
  }

  // Render grey open button
  // Only shown on closed orders
  if(buttonToRender == "OPEN") {
    return (
      <Button 
        variant="secondary"
        onClick={() => openModal('reopen-order', order)}
      >
        Reopen
      </Button>
    )
  }

  // Render green view button
  // Shown on all orders regardless of status
  if(buttonToRender == "VIEW") {
    return (
      <Button 
        variant="success" 
        onClick={() => openModal('view-order', order)}
      >
        View
      </Button>
    )
  }
};
