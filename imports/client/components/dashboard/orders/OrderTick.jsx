import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle as SolidCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle as OutlineCheckCircle } from '@fortawesome/free-regular-svg-icons';

export const OrderTick = ({ flag, status, fn }) => {

  return (
    <>
      {
        status === "OPEN" ? (
          <td onClick={fn}>
            <FontAwesomeIcon icon={OutlineCheckCircle} size="2x" hidden={flag} />
            <FontAwesomeIcon icon={SolidCheckCircle} size="2x" className="text-success" hidden={!flag} />
          </td>
        ) : (
          <td>
            <FontAwesomeIcon icon={OutlineCheckCircle} size="2x" className="text-secondary" hidden={flag} />
            <FontAwesomeIcon icon={SolidCheckCircle} size="2x" className="text-secondary" hidden={!flag} />
          </td>
        )
      }
    </>
    
  )
};
