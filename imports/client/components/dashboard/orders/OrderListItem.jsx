import { Meteor } from "meteor/meteor";
import React, { useState, useEffect } from 'react';
import { Badge } from 'react-bootstrap';
import { OrderButton } from '/imports/client/components/dashboard/orders/OrderButton';
import { OrderTick } from '/imports/client/components/dashboard/orders/OrderTick';
import { customDecode } from '/imports/utils/shared';
import { sendNotification } from '/imports/utils/shared';

export const OrderListItem = ({ order }) => {
  const [acknowledged, setAcknowledged] = useState(false);
  const [completed, setCompleted] = useState(false);
  const [paid, setPaid] = useState(false);
  const [buttonToRender, setButtonToRender] = useState("CANCEL");

  // Set state on load & rerender so that correct icon & buttons get rendered
  useEffect(() => {
    setAcknowledged(order.hasBeenAcknowledged ? true : false);
    setCompleted(order.hasBeenCompleted ? true : false);
    setPaid(order.hasBeenPaidFor ? true : false);

    if(order.status == "OPEN") setButtonToRender("CANCEL");
    if(order.status == "CLOSED") setButtonToRender("OPEN");
    if(order.status == "CANCELLED") setButtonToRender("UNDO");
  }, [order]);

  const acknowledgeOrder = () => {
    setAcknowledged(true);
    if(!order.hasBeenAcknowledged) {
      Meteor.call('orders.acknowledge', order, error => {
        if (error) {
          sendNotification("An Error Occurred.", "error");
          console.log(error);
        } else {
          sendNotification("Order Acknowledged.", "success");
        }
      });
    }
  };

  const completeOrder = () => {
    setAcknowledged(true);
    setCompleted(true);
    if(!order.hasBeenCompleted) {
      Meteor.call('orders.complete', order, error => {
        if (error) {
          sendNotification("An Error Occurred.", "error");
          console.log(error);
        } else {
          sendNotification("Order Completed.", "success");
        }
      });
    }
  };

  const orderPaidFor = () => {
    setPaid(true);
    if(!order.hasBeenPaidFor) {
      Meteor.call('orders.paidFor', order, error => {
        if (error) {
          sendNotification("An Error Occurred.", "error");
          console.log(error);
        } else {
          sendNotification("Order Marked as Paid.", "success");
        }
      });
    }
  };

  // Return bootstrap colour class variant depending on status
  const getVariant = (status) => {
    if(status == "OPEN") return "success";
    if(status == "CLOSED") return "secondary";
    if(status == "CANCELLED") return "danger";
  }

  return (
    <tr className="text-center">

      <td>
        <Badge 
          variant={getVariant(order.status)} 
          className="p-2"
        >
          {order.status}
        </Badge>
      </td>

      <td>{order._id}</td>

      <td>{customDecode(order.tableToDeliverTo)}</td>

      <td>{order.createdAtTime}</td>

      <td>{order.lastUpdatedAtTime}</td>

      <OrderTick flag={acknowledged} status={order.status} fn={acknowledgeOrder} />

      <OrderTick flag={completed} status={order.status} fn={completeOrder} />

      <OrderTick flag={paid} status={order.status}  fn={orderPaidFor} />

      <td className="text-capitalize">{order.paymentOptionSelected}</td>

      <td>
        <OrderButton buttonToRender={"VIEW"} order={order} />
      </td>

      <td>
        <OrderButton buttonToRender={buttonToRender} order={order} />
      </td>
      
    </tr>
  );
};
