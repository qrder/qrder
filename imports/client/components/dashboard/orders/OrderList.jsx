import React from 'react';
import { Table } from 'react-bootstrap';
import { OrderListItem } from "./OrderListItem";
import { Loading } from '/imports/client/components/shared/Loading';

export const OrderList = ({ loading, orders }) => {

  if(loading) return <Loading viewHeight={false} />;

  if(orders === undefined || orders.length === 0) {
    return <div>You have not received any orders today.  You can use the date filter above to view older orders.</div>
  }

  return (
    <Table striped bordered hover size="sm">
      <thead>
        <tr className="text-center">
          <th>Status</th>
          <th>Order #</th>
          <th>Table</th>
          <th>Submitted</th>
          <th>Last Updated</th>
          <th>Acknowledged</th>
          <th>Completed</th>
          <th>Paid For</th>
          <th>Payment Type</th>
          <th>View</th>
          <th>Cancel</th>
        </tr>
      </thead>
      <tbody>
        {
          orders.map(order => {
            return <OrderListItem key={order._id} order={order} />
          })
        }
      </tbody>
    </Table>
  )
};
