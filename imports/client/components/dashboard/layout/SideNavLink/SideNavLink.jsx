import React, { useContext }from 'react';
import './SideNavLink.css';
import { useHistory } from "react-router-dom";
import { Container, Row, Col, Nav } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const SideNavLink = ({ link, text, icon }) => {
  const history = useHistory();

  const { setMobileNav } = useContext(GlobalContext);

  const handleClick = () => {
    if(link == "/logout") {
      Meteor.logout();
      history.push("/");
    } else {
      history.push(link);
    }
    setMobileNav(false);
  }
  
  return (
    <Nav.Link 
      eventKey={link}
      onClick={() => handleClick()} 
      id={`tour-side-nav-${text.toLowerCase()}`}
      className="side-nav-link text-white py-3 my-1"
      style={{ fontFamily: "nunito-regular", fontSize: "14pt" }}
    >
      <Container>
        <Row>
          <Col xs={2} className="p-0 text-center">
            <FontAwesomeIcon icon={icon} />
          </Col>
          <Col className="p-0">{text}</Col>
        </Row>
      </Container>
    </Nav.Link>
  );
}
