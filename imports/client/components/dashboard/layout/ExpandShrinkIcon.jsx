import React, { useState } from 'react';

export const ExpandShrinkIcon = ({ onClickCallback }) => {
  const [opacity, setOpacity] = useState("0.25")

  return (
    <svg 
      width="60" 
      height="60" 
      viewBox="0 0 40 40" 
      fill="none" 
      role="button"
      xmlns="http://www.w3.org/2000/svg"
      onMouseOver={() => setOpacity("0.50")} 
      onMouseLeave={() => setOpacity("0.25")} 
      onClick={() => onClickCallback()}
    >
      <circle cx="20" cy="20" r="20" fill="white" fillOpacity={opacity}/>
      <path d="M16.8091 19.6023L21.3641 15.0474C21.5837 14.8278 21.94 14.8278 22.1596 15.0474L22.6909 15.5788C22.9103 15.7981 22.9105 16.1534 22.6919 16.3733L19.0818 20L22.6916 23.627C22.9105 23.8468 22.9101 24.2021 22.6907 24.4215L22.1594 24.9528C21.9398 25.1724 21.5835 25.1724 21.3639 24.9528L16.8091 20.3977C16.5894 20.1781 16.5894 19.8219 16.8091 19.6023Z" fill="white"/>
    </svg>
  )
}
