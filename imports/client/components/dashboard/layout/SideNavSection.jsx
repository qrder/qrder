import React from 'react';
import { SideNavLink } from '/imports/client/components/dashboard/layout/SideNavLink/SideNavLink.jsx';

export const SideNavSection = ({ title, links }) => {
  
  return (
    <div className="mb-3 mt-lg-3">
      <p 
        className="text-uppercase text-white m-0 mb-2 ml-3 ml-lg-0" 
        style={{ opacity: "50%", fontFamily: "nunito-regular" }}
      >
        {title}
      </p>

      {
        links.map((link, idx) => {
          return <SideNavLink key={idx} link={link.link} text={link.text} icon={link.icon} />
        })
      }

    </div>
  );
}
