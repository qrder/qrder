import React, { useEffect, useContext } from 'react';
import './SideNav.css';
import { Nav, Image } from 'react-bootstrap';
import { useHistory, useLocation } from "react-router-dom";
import { GlobalContext } from '/imports/context/GlobalState';
import { isSubscriptionInvalid, isSubscriptionPaused } from '/imports/utils/users';
import { currentPageIsNotSettings, currentPageIsOrders } from '/imports/utils/shared';
import { SideNavSection } from '/imports/client/components/dashboard/layout/SideNavSection';
import { ExpandShrinkIcon } from '/imports/client/components/dashboard/layout/ExpandShrinkIcon';
import { faHome, faQrcode, faUtensils, faListOl, faUserCog, faSignOutAlt, faComments, faPaintBrush } from '@fortawesome/free-solid-svg-icons';

const NavHr = ({ className }) => (
  <hr 
    className={`bg-white mr-4 d-none d-xl-block nav-hr ${className}`} 
    style={{ opacity: "33%" }} 
  />
)

export const SideNav = () => {
  const history = useHistory();
  const location = useLocation();

  const { userInfo, setMobileNav } = useContext(GlobalContext);

  // Redirects
  const validateUserSubscription = () => {
    // always redirect to settings if cancelled / trial ended
    if(isSubscriptionInvalid(userInfo?.subscription?.status) && currentPageIsNotSettings()) {
      history.push("/dashboard/settings");
    }
    // redirect to settings if paused and trying to access orders
    if(isSubscriptionPaused(userInfo?.subscription?.status) && currentPageIsOrders() && currentPageIsNotSettings()) {
      history.push("/dashboard/settings");
    }
  }

  const isObjectEmpty = (obj) => { return JSON.stringify(obj) === '{}'; };

  // on url change, check subscription is valid
  useEffect(() => {
    !isObjectEmpty(userInfo) && validateUserSubscription();
  }, [location.pathname]);

  const nav = [
    {
      title: "management",
      links: [
        { text: "Venues", link: "/dashboard/venues", icon: faHome },
        { text: "Tables", link: "/dashboard/tables", icon: faQrcode },
        { text: "Menu", link: "/dashboard/menu", icon: faUtensils }
      ]
    },
    { 
      title: "orders",
      links: [
        { text: "Orders", link: "/dashboard/orders", icon: faListOl },
        { text: "Customisation", link: "/dashboard/customise", icon: faPaintBrush }
      ]
    },
    {
      title: "help & support",
      classList: "d-xl-none",
      links: [
        { text: "Live Chat", link: "/dashboard/live-chat", icon: faComments }
      ]
    },
    {
      title: "help & support",
      classList: "d-xl-none",
      links: [
        { text: "Live Chat", link: "/dashboard/live-chat", icon: faComments }
      ]
    },
    {
      title: "account",
      classList: "mt-auto",
      links: [
        { text: "Settings", link: "/dashboard/settings", icon: faUserCog },
        { text: "Logout", link: "/logout", icon: faSignOutAlt }
      ]
    }
  ];
  
  return (
    <Nav 
      variant="pills"
      activeKey={location.pathname}
      defaultActiveKey={location.pathname}
      data-tour="side-nav"
      className="navbar-nav flex-column pl-lg-4 sticky-top"
    >
      <div className="text-center mt-4 mb-0">
        <Image 
          fluid
          src="/images/qrder-logo/qrder-name-logo-white.png"
          className="mr-4 d-none d-xl-inline sidenav-qrder-logo"
          style={{ height: "40px" }}
          onClick={() => history.push("/")}
        />
        <NavHr className="ml-3 ml-xl-0" />
      </div>

      {
        nav.map((section, idx) => {
          return (
            <div key={idx} className={`pr-0 ${section.classList}`}>
              {section.title == 'account' ? <NavHr className="mb-4 ml-3 ml-xl-0" /> : ''}
              <SideNavSection title={section.title} links={section.links} />
              <NavHr className="ml-3 ml-xl-0" />
            </div>
          )
        })
      }

      <div className="text-center mb-4 mt-xl-3 mr-xl-4">
        <ExpandShrinkIcon onClickCallback={() => setMobileNav(false)} />
      </div>

    </Nav>
  );
}


        