import React from 'react';
import { useHistory } from "react-router-dom";
import { Container, Row, Col, Button } from 'react-bootstrap';

export const Footer = ({ welcomeModalClicked, startTourClicked }) => {
  const history = useHistory();

  return (
    <footer className="p-4" style={{ backgroundColor: '#F4F4F4' }} data-tour="footer">
      <Container fluid>
        <Row>
          <Col xs={{ span: 12, order: 3 }} lg={{ span: 3, order: 1 }} className="h-100 text-center text-lg-left mt-3 my-lg-auto">
            <p className="text-muted small mb-0">&copy; QRDER 2021. All Rights Reserved.</p>
          </Col>

          <Col xs={{ span: 12, order: 2 }} lg={{ span: 4, order: 2 }} className="text-center">
            <Button size="sm" onClick={() => welcomeModalClicked()} className="mr-3">Welcome Message</Button>
            <Button size="sm" onClick={() => startTourClicked()}>Start Tour</Button>
          </Col>

          <Col xs={{ span: 12, order: 1 }} lg={{ span: 3, order: 3 }} className="h-100 text-center text-lg-right mb-3 my-lg-auto">
            <Button variant="link" size="sm" onClick={() => history.push("/privacy")} className="mr-3">
              Privacy Policy
            </Button>
            <span className="text-muted small mb-4 mb-lg-0">support@qrder.io</span>
          </Col>
          {/* We do not use the entire 12 columns as we need to leave space for the tawk.to widget */}
        </Row>
      </Container>
    </footer>
  );
};
