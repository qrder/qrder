import React from 'react';
import './TableQR.css';

/**
 * IMPORTANT:
 * 
 * We have to use standard HTML and custom CSS.
 * The react-to-print package ignores <link > tags 
 * (which is how we import some Bootstrap style stuff).
 * 
 * From the react-to-print NPM page:
 * 
 * Why does react-to-print skip <link rel="stylesheet" href=""> tags
 * <link>s with empty href attributes are INVALID HTML. 
 * In addition, they can cause all sorts of undesirable behavior. 
 * For example, many browsers - including modern ones, when presented with <link href=""> 
 * will attempt to load the current page. Some even attempt to load the current page's parent directory.
 */

export const TableQR = ({qr}) => {

  return (
    <div className="table-qr">
      <div className="venue-name">{qr.table.venueName}</div>
      <div className="qr-code">
        <img src={qr.qrUrl} style={{ width: '175px' }} />
      </div>
      <div className="table-name">{qr.table.name}</div>
    </div>
  );
};
