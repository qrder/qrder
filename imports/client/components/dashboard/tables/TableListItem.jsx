import React, { useState, useEffect, useContext } from 'react';
import { GlobalContext } from '/imports/context/GlobalState';
import { Container, Row, Col, Form } from 'react-bootstrap';
import { ActionDropdown } from '/imports/client/components/dashboard/shared/ActionDropdown';

export const TableListItem = ({ table, selectAll }) => {
  const [c, setChecked] = useState(false);
  
  const { addTable, deleteTable } = useContext(GlobalContext);

  // Set checked if select all checkbox in parent is checked
  useEffect(() => { setChecked(selectAll) }, [selectAll]);

  // Handle checkbox change
  const handleChange = (tbl, checked) => {
    setChecked(!c);
    // Set table checked value
    tbl.checked = checked;
    // Add if checked, remove if unchecked
    tbl.checked ? addTable(tbl) : deleteTable(tbl._id);
  }

  return (
    <>
      <Container className="p-sm-0 ml-0 tour-table-list-item">
        <Row>
          <Col xs={1} className="d-flex align-items-center justify-content-center p-0">
            {/* TODO: Could our Checkbox comp be used? */}
            <Form.Check 
              type="checkbox"
              checked={c}
              onChange={(e) => handleChange(table, e.target.checked)}
              id={table._id}
              aria-label={table.name}
              className="tour-table-checkbox"
            />
          </Col>
          <Col xs={8} sm={4} className="p-0">
            <span>{table.name}</span>
          </Col>
          <Col sm={4} className="d-none d-sm-flex align-items-center justify-content-center p-0" style={{ color: '#DBDBDB' }}>
            {table._id}
          </Col>
          <Col xs={3} className="d-flex align-items-center justify-content-end p-0 pr-2 pr-sm-5">
            <ActionDropdown 
              data={table} 
              parent="table" 
              options={["edit", "delete"]} 
              className="tour-table-hamburger-button"
              dropdownClassName="tour-table-hamburger-dropdown"
            />
          </Col>
        </Row>
      </Container>
      <hr/>
    </>
  );
};
