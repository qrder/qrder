import React, { useState, useContext } from 'react';
import { GlobalContext } from '/imports/context/GlobalState';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { TableListItem } from "./TableListItem";

export const TableList = ({ tables, columns }) => {
  const [selectAll, setSelectAll] = useState(false);
  
  const { setTables } = useContext(GlobalContext);
  
  // Handle when select all checkbox changes
  const handleAllSelect = (ts, checked) => {
    // Set checked value for each table
    ts.map(t => t.checked = checked);
    // Update selectAll so that it gets passed down to children
    setSelectAll(checked);
    // Update session storage
    checked ? setTables(ts) : setTables([]);
  };

  if(tables === undefined) return 'Loading...';

  if(tables.length === 0) {
    return <div className="mt-4">You have not created any tables yet.  Click 'Add Table' above to get started.</div>
  }

  return (
    <Container className="mt-md-4">
      <Row>
        <Col className="pl-0">
          <Button size="sm" onClick={() => handleAllSelect(tables, !selectAll)}>
            { !selectAll ? 'Select All' : 'Deselect All' }
          </Button>
        </Col>
      </Row>
      <Row>
        <Col xl={columns} className="p-0">
          <hr/>
          {
            tables.map(table => {
              return <TableListItem key={table._id} table={table} selectAll={selectAll} />
            })
          }
        </Col>
      </Row>
    </Container>
  )
};
