import React from 'react';
import { Session } from 'meteor/session';
import { Modal, Button } from 'react-bootstrap';

export const WelcomeModal = ({ onHide, show, startTourClicked }) => {

  const handleStartTourClicked = () => {
    Session.setAuth('loggedInForFirstTime', JSON.stringify(false));
    onHide();
    setTimeout(() => startTourClicked(), 300);
  }

  const handleWelcomeModalClose = () => {
    Session.setAuth('loggedInForFirstTime', JSON.stringify(false));
    onHide();
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Welcome to QRDER!
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="d-md-none">
          <p>QRDER works best on devices with larger screens (tables, laptops and desktop computers).</p>
          <p>If you are using a mobile device, some features may not work properly.</p>
        </div>

        <div className="mt-5 mt-md-0">
          <p>
            <strong className="text-success">The 2 month trial </strong>
            will officially start when pubs, cafes and restaurants can reopen across the UK (April 26th).
          </p>
          <p>In the meantime, you can use QRDER to create your menu and print off your QR codes.</p>
          <p>After the trial, the monthly cost is £16.99.</p>
        </div>

        <div className="mt-5">
          <p>Follow the guided tour by clicking 'Start Tour' at the bottom of this popup.</p>
        </div>

        <div className="mt-5">
          <p>Feel free to email us at support@qrder.io.</p>
          <p className="d-none d-md-block">You can also use the chat box icon in the bottom right to start a live chat with us.</p>
        </div>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="outline-primary" onClick={() => handleWelcomeModalClose()}>Close</Button>
        <Button variant="primary" onClick={() => handleStartTourClicked()}>Start Tour</Button>
      </Modal.Footer>
    </Modal>
  );
};