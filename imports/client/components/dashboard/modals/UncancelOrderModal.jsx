import React, { useState, useEffect, useContext } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';

export const UncancelOrderModal = ({ onHide, show }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError(''); }, [show, onHide]);

  // Uncancel order - set to open
  const submit = () => {
    Meteor.call('orders.uncancel', modalData, error => {
      if(error) {
        setServerError(error.reason);
        sendNotification(`An Error Occurred. [${modalData._id} did not uncancel]`, "error");
      } else {
        sendNotification(`Order Uncancelled. [${modalData._id}]`, "success");
        onHide();
      }
    });
  }; 
  
  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Un-Cancel Order
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Error message={serverError} />
        Are you sure you want to un-cancel order number '{modalData?._id}'?
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={submit}>Undo</Button>
      </Modal.Footer>
    </Modal>
  );
};
