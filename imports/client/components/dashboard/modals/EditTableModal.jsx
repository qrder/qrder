import React, { useEffect, useState, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { Modal } from 'react-bootstrap';
import { AutoForm, AutoField, SubmitField, SelectField } from 'uniforms-bootstrap4';
import { TableBridge } from "/imports/db/table/schema";
import { createVenueSelectOptions } from "/imports/utils/venues";

export const EditTableModal = ({ onHide, show, venues, currentVenue }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Update table in db
  const submit = formData => {
    Meteor.call('tables.update', modalData._id, formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  // On load, set venue id of table selected so that it presets in form
  if(show) modalData.venueId = currentVenue;

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Table
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={TableBridge} onSubmit={submit}>
        <Modal.Body>
          <AutoField name="name" showInlineError />
          <SelectField 
            name="venueId"
            options={createVenueSelectOptions(venues)}
            placeholder="Select Venue"
            showInlineError
            disabled={venues.length == 1 ? true : false}
          />
        </Modal.Body>
        <Modal.Footer>
          <Error message={serverError} />
          <SubmitField />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
