import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { Modal } from 'react-bootstrap';
import { AutoForm, AutoField, BoolField, SubmitField, SelectField } from 'uniforms-bootstrap4';
import { SectionBridge } from "/imports/db/section/schema";
import { createParentSelectOptions } from "/imports/utils/menu";
import { inputFocus } from "/imports/utils/shared";

export const AddSectionModal = ({ show, onHide, data }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);
  // Autofocus name input
  useEffect(() => { show && inputFocus('name') }, [show, onHide]);

  // Handle form submit - insert section into db
  const submit = formData => {
    Meteor.call('sections.insert', formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName="tour-add-section-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Section
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={{parentId: modalData}} schema={SectionBridge} onSubmit={submit} id="add-section-form">
        <Modal.Body>
          <Error message={serverError} />  
          <AutoField name="name" label={false} placeholder="Name" showInlineError className="tour-section-name-input" />
          <SelectField 
            name="parentId"
            options={createParentSelectOptions(data, modalData, 'add')}
            placeholder="Select Parent"
            showInlineError
            label="Parent"
            className="tour-section-parent-input"
            label={false}
          />
        </Modal.Body>

        <Modal.Footer>
          <BoolField name="publish" showInlineError className="tour-section-publish-input" />
          <SubmitField id="tour-section-submit-button" />
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
