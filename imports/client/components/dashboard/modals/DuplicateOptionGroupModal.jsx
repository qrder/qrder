import React, { useEffect, useState, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Row, Col, Modal } from 'react-bootstrap';
import { Error } from '/imports/client/components/shared/Error';
import { Add } from "/imports/client/components/dashboard/menu/Add";
import { Delete } from "/imports/client/components/dashboard/menu/Delete";
import OptionsListField from "/imports/client/components/dashboard/options/OptionsListField";
import { AutoForm, NumField, TextField, SubmitField, SelectField } from 'uniforms-bootstrap4';
import { OptionGroupsBridge } from "/imports/db/optionGroups/schema";
import { createVenueSelectOptions } from "/imports/utils/venues";

export const DuplicateOptionGroupModal = ({ onHide, show, venues, currentVenue }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  const submit = data => {
    delete data._id;
    Meteor.call('optionGroups.duplicate', data, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  // On show, set venue id of menu selected so that it presets in form
  if(show) modalData.venueId = currentVenue;

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Duplicate Option Group
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={OptionGroupsBridge} onSubmit={submit}>
        <Modal.Body>
          <Row>
            <Col>
              <TextField name="name" label={false} placeholder="Name" showInlineError />
            </Col>
          </Row>
          <Row>
            <Col>
              <NumField name="min" label={false} placeholder="Min" showInlineError />
            </Col>
            <Col>
              <NumField name="max" label={false} placeholder="Max" showInlineError />
            </Col>
          </Row>
          <Row>
            <Col>
              <SelectField 
                name="venueId"
                options={createVenueSelectOptions(venues)}
                placeholder="Select Venue"
                label={false}
                showInlineError
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <OptionsListField 
                name="options"
                addIcon={<Add text="Option" />}
                removeIcon={<Delete />}
                initialCount={1}
                showInlineError
                className="tour-og-options-input"
              />
            </Col>
          </Row>
        </Modal.Body>

        <Modal.Footer>
          <Error message={serverError} />
          <SubmitField />
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};