import React, { useEffect, useState, useRef, useContext } from 'react';
import { GlobalContext } from '/imports/context/GlobalState';
import { Modal, Button, CardDeck, Row, Container } from 'react-bootstrap';
import { GenerateMultipleQR } from '/imports/utils/qr';
import { Error } from '/imports/client/components/shared/Error';
import { TableQR } from '/imports/client/components/dashboard/tables/TableQR/TableQR.jsx';
import { useReactToPrint } from 'react-to-print';

export const GenerateQRCodesModal = ({ show, onHide, generate, venue }) => {
  const [qrCodes, setQrCodes] = useState([]);
  const [serverError, setServerError] = useState('');
  const cardDeck = useRef();

  const { tablesChecked } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);
  
  useEffect(() => {
    const qrCodeList = GenerateMultipleQR(venue, tablesChecked);
    if(Array.isArray(qrCodeList)) {
      setQrCodes(qrCodeList);
    } else {
      setServerError("Sorry, an error occurred.  Please try again later.  If this issue persists, please contact customer support.");
    }
  }, [generate]);

  // Prepare CardDeck of QR code items for printing 
  const handlePrint = useReactToPrint({
    content: () => cardDeck.current,
    bodyClass: "m-5 p-5" // cater for print margins
  });

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      dialogClassName="wide-modal"
      contentClassName="tour-generate-qr-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Generate QR Codes
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>

        <Error message={serverError} />

        {/* 
          IMPORTANT: Be very careful editing the CardDeck & TableQR
          components as it could affect how they render in the print window.
        */}

        <CardDeck ref={cardDeck}>
          <Container>
            <Row>
            {
              qrCodes.map(qr => {
                return (
                  <div key={qr.table._id} className="mb-4 mx-3" style={{ width:'233px' }}>
                    <TableQR qr={qr} />
                  </div>
                )
              })
            }
            </Row>
          </Container>
        </CardDeck>

      </Modal.Body>

      <Modal.Footer>
        <Button onClick={() => handlePrint()} id="tour-print-qr-button">Print</Button>
      </Modal.Footer>
    </Modal>
  );
};