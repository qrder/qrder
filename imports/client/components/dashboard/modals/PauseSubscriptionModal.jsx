import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { Modal, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';
import { createUpdateUserInfoObj } from '/imports/utils/settings';

export const PauseSubscriptionModal = ({ onHide, show, sub }) => {
  const [serverError, setServerError] = useState('');
  
  const { updateUser, userInfo } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Pause subscription
  const handlePauseSubmit = async (event) => {
    event.preventDefault();
    Meteor.call("users.pauseSubscription", sub.stripeSubscriptionId, error => {
      if (error) {
        sendNotification("An Error Occurred.", "error");
        setServerError(error.reason);
      } else {
        sendNotification("Subscription Paused.", "success");
        onHide();
        // Temporarily update context as userInfo context is used
        // to make page redirections & it will not update until page refresh
        updateUser(createUpdateUserInfoObj(userInfo, "Paused", "active"));
      }
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Pause Subscription
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>
          Are you sure you want to pause your subscription? 
          You can continue to update your menu, tables and venue information, but you cannot view, manage or receive orders.
        </p>
        <p>
          You can resume at any time.  This feature is useful for COVID-19 lockdowns.
        </p>
      </Modal.Body>
        
      <Modal.Footer>
        <Error message={serverError} />
        <Button variant="warning" onClick={handlePauseSubmit}>Pause</Button>
      </Modal.Footer>
    </Modal>
  );
};
