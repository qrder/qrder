import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Modal, Row, Container, Col } from 'react-bootstrap';
import { AutoForm, AutoField, BoolField, SubmitField, LongTextField, SelectField } from 'uniforms-bootstrap4';
import { Error } from '/imports/client/components/shared/Error';
import { Add } from "/imports/client/components/dashboard/menu/Add";
import { Delete } from "/imports/client/components/dashboard/menu/Delete";
import { ItemBridge } from "/imports/db/item/schema";
import PriceListField from '/imports/client/components/dashboard/menu/PriceListField';
import OptionGroupsListField from '/imports/client/components/dashboard/menu/OptionGroupsListField';
import { createParentSelectOptions } from "/imports/utils/menu";

export const DuplicateItemModal = ({ show, onHide, data }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Duplicate item in db
  const submit = formData => {
    delete formData._id;
    Meteor.call('items.duplicate', formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Duplicate Item
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={ItemBridge} onSubmit={submit}>
        <Modal.Body>

          <AutoField name="name" label={false} placeholder="Name" showInlineError />
          <LongTextField name="description" label={false} placeholder="Description" rows={3} />

          <SelectField 
            name="parentId"
            options={createParentSelectOptions(data, ' ')}
            placeholder="Select Parent"
            showInlineError
            label="Parent"
          />

          <PriceListField
            name="prices"
            addIcon={<Add text="Price" />}
            removeIcon={<Delete />}
            initialCount={1}
            className="tour-item-price-input"
          />

          <OptionGroupsListField 
            name="options"
            addIcon={<Add text="Option" />}
            removeIcon={<Delete />}
            showInlineError
            className="tour-item-options-input"
          />

          <Container className="mt-3">
            <Row>
              <Col><BoolField name="vegetarian" className="m-0" /></Col>
              <Col><AutoField name="vegan" className="m-0" /></Col>
              <Col><AutoField name="glutenFree" className="m-0" /></Col>
              <Col><AutoField name="containsAlcohol" className="m-0" /></Col>
              <Col><AutoField name="containsFish" className="m-0" /></Col>
            </Row>
          </Container>
        </Modal.Body>

        <Modal.Footer>
          <Error message={serverError} />
          <BoolField name="publish" />
          <SubmitField />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
