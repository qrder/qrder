import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { Modal, Button, Form } from 'react-bootstrap';

export const DeleteVenueModal = ({ show, onHide }) => {
  const [serverError, setServerError] = useState('');
  const [disabled, setDisabled] = useState(true);

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Remove venue from db
  const submit = () => {
    Meteor.call('venues.delete', modalData._id, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Delete Venue 
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>Are you sure you want to delete the '{modalData?.name}' venue?</p>
        <p><strong>This will delete all of the menu's, tables and option groups that you have created for this venue.</strong></p>
      </Modal.Body>

      <Modal.Footer>
        <Error message={serverError} />
        <Form.Check 
          label="Confirm"
          id="confirm-venue-deletion" 
          onChange={(e) => e.target.value ? setDisabled(false) : setDisabled(true)} 
        />
        <Button onClick={submit} disabled={disabled} className="ml-2">Submit</Button>
      </Modal.Footer>
    </Modal>
  );
};
