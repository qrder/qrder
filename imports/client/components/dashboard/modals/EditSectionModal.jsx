import React, { useEffect, useState, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { Modal } from 'react-bootstrap';
import { AutoForm, AutoField, BoolField, SubmitField, SelectField } from 'uniforms-bootstrap4';
import { SectionBridge } from "/imports/db/section/schema";
import { createParentSelectOptions } from "/imports/utils/menu";

export const EditSectionModal = ({ show, onHide, data }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Handle form submit - update section into db
  const submit = formData => {
    Meteor.call('sections.update', modalData._id, formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Section
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={SectionBridge} onSubmit={submit}>
        <Modal.Body>
          <Error message={serverError} />  
          <AutoField name="name" label={false} placeholder="Name" showInlineError />
          <SelectField 
            name="parentId"
            options={createParentSelectOptions(data, modalData?._id, 'edit')}
            placeholder="Select Parent"
            showInlineError
            label="Parent"
            label={false}
          />
        </Modal.Body>

        <Modal.Footer>
          <BoolField name="publish" showInlineError />
          <SubmitField />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
