import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Modal, Button } from 'react-bootstrap';
import { Error } from '/imports/client/components/shared/Error';

export const DeleteOptionGroupModal = ({ onHide, show, optionGroup }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  const submit = () => {
    Meteor.call('optionGroups.delete', modalData._id, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Delete Option Group 
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        Are you sure you want to delete the '{modalData?.name}' option group?
      </Modal.Body>

      <Modal.Footer>
        <Error message={serverError} />
        <Button onClick={submit}>Submit</Button>
      </Modal.Footer>
    </Modal>
  );
};
