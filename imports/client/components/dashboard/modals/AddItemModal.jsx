import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Modal, Row, Container, Col } from 'react-bootstrap';
import { AutoForm, AutoField, BoolField, SubmitField, LongTextField, SelectField } from 'uniforms-bootstrap4';
import { Error } from '/imports/client/components/shared/Error';
import { Add } from "/imports/client/components/dashboard/menu/Add";
import { Delete } from "/imports/client/components/dashboard/menu/Delete";
import { ItemBridge } from "/imports/db/item/schema";
import PriceListField from '/imports/client/components/dashboard/menu/PriceListField';
import OptionGroupsListField from '/imports/client/components/dashboard/menu/OptionGroupsListField';
import { createParentSelectOptions } from "/imports/utils/menu";
import { inputFocus } from "/imports/utils/shared";

export const AddItemModal = ({ show, onHide, data }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);
  // Autofocus name input
  useEffect(() => { show && inputFocus('name') }, [show, onHide]);

  // Add item to db
  const submit = formData => {
    Meteor.call('items.insert', formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName="tour-add-item-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Item
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={{parentId: modalData}} schema={ItemBridge} onSubmit={submit} id="add-item-form">
        <Modal.Body>

          <AutoField name="name" label={false} placeholder="Name" showInlineError className="tour-item-name-input" />
          <LongTextField name="description" label={false} placeholder="Description" showInlineError rows={3} className="tour-item-desc-input" />

          <SelectField 
            name="parentId"
            options={createParentSelectOptions(data, ' ')}
            placeholder="Select Parent"
            showInlineError
            label="Parent"
            className="tour-item-parent-input"
            label={false}
          />

          {/* TODO: Move custom form fields into a shared folder to make it clear what they are */}
          <PriceListField
            name="prices"
            addIcon={<Add text="Price" />}
            removeIcon={<Delete />}
            initialCount={1}
            className="tour-item-price-input"
          />

          <OptionGroupsListField 
            name="options"
            addIcon={<Add text="Option" />}
            removeIcon={<Delete />}
            showInlineError
            className="tour-item-options-input"
          />

          <Container className="mt-3 tour-item-filter-input">
            <Row>
              <Col><BoolField name="vegetarian" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
              <Col><AutoField name="vegan" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
              <Col><AutoField name="glutenFree" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
              <Col><AutoField name="containsAlcohol" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
              <Col><AutoField name="containsFish" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
            </Row>
          </Container>
        </Modal.Body>

        <Modal.Footer>
          <Error message={serverError} />
          <BoolField name="publish" showInlineError className="tour-item-publish-input" />
          <SubmitField id="tour-item-submit-button" />
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
