import React, { useContext } from 'react';
import { GlobalContext } from '/imports/context/GlobalState';
import { Modal, Button, Container, Row, Col, Badge, Table } from 'react-bootstrap';

export const ViewOrderModal = ({ onHide, show }) => {

  const { modalData } = useContext(GlobalContext);

  // TODO: Convert to function as used in multiple components
  const getVariant = (status) => {
    if(status == "OPEN") return "success";
    if(status == "CLOSED") return "secondary";
    if(status == "CANCELLED") return "danger";
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      dialogClassName="wide-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <span className="mr-3">View Order</span>
          <span className="text-secondary small">
            {modalData?._id} 
            <Badge 
              variant={getVariant(modalData?.status)} 
              className="p-2 ml-2"
            >
              {modalData?.status}
            </Badge>
          </span>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Container>
          <Row>
            <Col xs={12} md={6}>
              <Row>
                <Col xs={5}>Table:</Col>
                <Col>{modalData?.tableToDeliverTo}</Col>
              </Row>
              <Row>
                <Col xs={5}>Venue:</Col>
                <Col>{modalData?.venueOrderedAt?.name}</Col>
              </Row>
              <Row>
                <Col xs={5}>Payment Type:</Col>
                <Col className="text-capitalize">{modalData?.paymentOptionSelected}</Col>
              </Row>
              <Row>
                <Col xs={5}>Total Order Price:</Col>
                {/* TODO: Config of currency symbol */}
                <Col>£{modalData?.orderTotal.toFixed(2)}</Col>
              </Row>
            </Col>

            <Col>
              <Row>
                <Col xs={5}>Ordered At:</Col>
                <Col>{modalData?.createdAtDate + " " + modalData?.createdAtTime}</Col>
              </Row>
              <Row>
                <Col xs={5}>Last Updated:</Col>
                <Col>{modalData?.lastUpdatedAtDate + " " + modalData?.lastUpdatedAtTime}</Col>
              </Row>
              <Row>
                <Col xs={5}>Customer Comments:</Col>
                <Col>{modalData?.comments}</Col>
              </Row>
            </Col>
          </Row>

          <hr/>

          <Row>
            <Col>
              <Table variant="primary" striped bordered hover size="sm">
                <thead>
                  <tr className="text-center">
                    <th>Item</th>
                    <th>Qty</th>
                    <th>Base Price</th>
                    <th>Options</th>
                    <th>Total Price</th>
                  </tr>
                </thead>
                <tbody>
                {
                  modalData?.items.map(item => {
                    return (
                      <tr className="text-center" key={item.id}>
                        <td>{item.name}</td>
                        <td>{item.quantity}</td>
                        <td>{item.priceName}</td>
                        <td className="text-left">
                          {
                            item.options.map((opt, oidx) => {
                              return (
                                <div key={oidx}>
                                  <span className="font-weight-bold">{opt.name}: </span>
                                  <span className="font-weight-italic">
                                  {
                                    opt.optionsChosen.map((cho, cidx) => {
                                      return <span key={cidx}>{cho}, </span>
                                    })
                                  }
                                  </span>
                                </div>
                              )
                            })
                          }
                        </td>
                        {/* TODO: Config of currency symbol */}
                        <td>£{item.price.toFixed(2)}</td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </Table>
            </Col>
          </Row>
        </Container>
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={() => onHide()}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
};