import React, { useState, useEffect, useContext } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';

export const CancelOrderModal = ({ onHide, show }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError(''); }, [show, onHide]);

  // Cancel order
  const submit = () => {
    Meteor.call('orders.cancel', modalData, error => {
      if(error) {
        setServerError(error.reason);
        sendNotification(`An Error Occurred. [${modalData._id} did not cancel]`, "error");
      } else {
        sendNotification(`Order Cancelled. [${modalData._id}]`, "success");
        onHide();
      }
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Cancel Order
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Error message={serverError} />
        Are you sure you want to cancel order number '{modalData?._id}'?
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={submit}>Submit</Button>
      </Modal.Footer>
    </Modal>
  );
};
