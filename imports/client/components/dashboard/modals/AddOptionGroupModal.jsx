import React, { useEffect, useState } from 'react';
import { Meteor } from "meteor/meteor";
import { Error } from '/imports/client/components/shared/Error';
import { Row, Col, Modal } from 'react-bootstrap';
import { Add } from "/imports/client/components/dashboard/menu/Add";
import { Delete } from "/imports/client/components/dashboard/menu/Delete";
import OptionsListField from "/imports/client/components/dashboard/options/OptionsListField";
import { AutoForm, NumField, TextField, SubmitField, SelectField } from 'uniforms-bootstrap4';
import { OptionGroupsBridge } from "/imports/db/optionGroups/schema";
import { createVenueSelectOptions } from "/imports/utils/venues";

export const AddOptionGroupModal = ({ onHide, show, currentVenue, venues }) => {
  const [serverError, setServerError] = useState('');
  
  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  const submit = data => {
    Meteor.call('optionGroups.insert', data, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName="tour-add-og-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Option Group
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={{venueId: currentVenue}} schema={OptionGroupsBridge} onSubmit={submit} id="add-og-form">
        <Modal.Body>
          <Row>
            <Col>
              <TextField name="name" label={false} placeholder="Name" showInlineError className="tour-og-name-input" />
            </Col>
          </Row>
          <Row>
            <Col>
              <NumField name="min" label={false} placeholder="Min" showInlineError className="tour-og-min-input" />
            </Col>
            <Col>
              <NumField name="max" label={false} placeholder="Max" showInlineError className="tour-og-max-input" />
            </Col>
          </Row>
          <Row>
            <Col>
              <SelectField 
                name="venueId"
                options={createVenueSelectOptions(venues)}
                placeholder="Select Venue"
                label={false}
                showInlineError
                className="tour-og-venue-select"
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <OptionsListField 
                name="options"
                addIcon={<Add text="Option" />}
                removeIcon={<Delete />}
                initialCount={1}
                showInlineError
                className="tour-og-options-input"
              />
            </Col>
          </Row>
        </Modal.Body>

        <Modal.Footer>
          <Error message={serverError} />
          <SubmitField id="tour-og-submit-button" />
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
