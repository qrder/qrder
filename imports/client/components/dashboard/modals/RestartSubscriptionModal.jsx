import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { Modal, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';
import { createUpdateUserInfoObj } from '/imports/utils/settings';

export const RestartSubscriptionModal = ({ onHide, show, sub }) => {
  const [serverError, setServerError] = useState('');
  
  const { updateUser, userInfo } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Restart subscription by creating a new subscription.
  // 'restart' is different to 'resume' - restart is starting a
  // new, previously cancelled subscription.
  // 'Resume' is continuing a previously paused subscription.
  const handleRestartSubmit = async (event) => {
    event.preventDefault();
    Meteor.call("users.restartSubscription", sub.stripeCustomerId, error => {
      if (error) {
        sendNotification("An Error Occurred.", "error");
        setServerError(error.reason);
      } else {
        sendNotification("Subscription Restarted.", "success");
        onHide();
        // Temporarily update context as userInfo context is used
        // to make page redirections & it will not update until page refresh
        updateUser(createUpdateUserInfoObj(userInfo, "Active", "active"));
      }
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Restart Subscription
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>Are you sure you want to restart your subscription?</p>
        <p>This will create a brand new subscription starting from today.</p>
      </Modal.Body>
        
      <Modal.Footer>
        <Error message={serverError} />
        <Button onClick={handleRestartSubmit}>Restart</Button>
      </Modal.Footer>
    </Modal>
  );
};
