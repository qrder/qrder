import React, { useState, useEffect, useContext } from 'react';
import { Form, Modal, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';

export const ReopenOrderModal = ({ onHide, show }) => {
  const [serverError, setServerError] = useState('');
  const [notCompleted, setNotCompleted] = useState(false);
  const [notPaid, setNotPaid] = useState(false);

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError(''); }, [show, onHide]);

  // Reopen order
  // Set order hasBeen flags to reflect form selections
  const submit = (e) => {
    e.preventDefault();
    if(notCompleted) modalData.hasBeenCompleted = false;
    if(notPaid) modalData.hasBeenPaidFor = false;
    Meteor.call('orders.reopen', modalData, error => {
      if (error) {
        setServerError(error.reason);
        sendNotification(`An Error Occurred. [${modalData._id} did not reopen]`, "error");
      } else {
        sendNotification(`Order Reopened. [${modalData._id}]`, "success");
        setNotCompleted(false);
        setNotPaid(false);
        onHide();
      }
    });
  };

  // Disable submit button if neither reasons are ticked
  const getDisabled = () => {
    if(!notCompleted && !notPaid) return true;
    return false;
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Reopen Order
        </Modal.Title>
      </Modal.Header>

      <Form onSubmit={submit}>
        <Modal.Body>
          <Error message={serverError} />

          <p>Reason For Reopening:</p>
          <Form.Check 
            name="notCompleted" 
            label="Not Completed" 
            className="my-3" 
            onChange={e => setNotCompleted(e.target.checked)}
          />
          
          <Form.Check 
            name="notPaid" 
            label="Not Paid For" 
            onChange={e => setNotPaid(e.target.checked)}
          />
        </Modal.Body>

        <Modal.Footer>
          <Button type="submit" disabled={getDisabled()}>Reopen</Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};
