import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { Modal, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';
import { createUpdateUserInfoObj } from '/imports/utils/settings';

export const ResumeSubscriptionModal = ({ onHide, show, sub }) => {
  const [serverError, setServerError] = useState('');
  
  const { updateUser, userInfo } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Resume subscription
  const handleResumeSubmit = async (event) => {
    event.preventDefault();
    // Resume paused subscription.
    // Different to restarting a subscription - restarting a sub
    // is creating a new, previously cancelled sub.
    // This is unpausing a sub.
    Meteor.call("users.resumeSubscription", sub.stripeCustomerId, sub.stripeSubscriptionId, error => {
      if (error) {
        sendNotification("An Error Occurred.", "error");
        setServerError(error.reason);
      } else {
        sendNotification("Subscription Resumed.", "success");
        onHide();
        // Temporarily update context as userInfo context is used
        // to make page redirections & it will not update until page refresh
        updateUser(createUpdateUserInfoObj(userInfo, "Active", "active"));
      }
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Resume Subscription
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>
          Are you sure you want to resume your subscription? 
        </p>
        <p>
          You will be able to view, manage and receive orders again.  Payments will start again.
        </p>
      </Modal.Body>
        
      <Modal.Footer>
        <Error message={serverError} />
        <Button variant="success" onClick={handleResumeSubmit}>Resume</Button>
      </Modal.Footer>
    </Modal>
  );
};
