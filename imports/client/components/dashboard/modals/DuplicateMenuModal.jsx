import React, { useEffect, useState, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { Modal } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { AutoForm, AutoField, BoolField, SubmitField, SelectField } from 'uniforms-bootstrap4';
import { MenuBridge } from "/imports/db/menu/schema";
import { createVenueSelectOptions } from "/imports/utils/venues";

export const DuplicateMenuModal = ({ onHide, show, venues, currentVenue }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Duplicate menu and children in db  
  const submit = formData => {
    delete formData._id;
    Meteor.call('menus.duplicate', formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  // On show, set venue id of menu selected so that it presets in form
  // TODO: Check this works - not sure if context data can be written to like this
  if(show) modalData.venueId = currentVenue;

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Duplicate Menu
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={MenuBridge} onSubmit={submit}>
        <Modal.Body>
          <Error message={serverError} /> 
          <AutoField name="name" showInlineError />
          <SelectField 
            name="venueId"
            options={createVenueSelectOptions(venues)}
            placeholder="Select Venue"
            showInlineError
          />
        </Modal.Body>
        <Modal.Footer>
          <BoolField name="publish" />
          <SubmitField />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};