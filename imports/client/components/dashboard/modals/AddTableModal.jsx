import React, { useState, useEffect } from 'react';
import { Meteor } from "meteor/meteor";
import { Error } from '/imports/client/components/shared/Error';
import { Modal } from 'react-bootstrap';
import { AutoForm, AutoField, SelectField, SubmitField } from 'uniforms-bootstrap4';
import { TableBridge } from "/imports/db/table/schema";
import { createVenueSelectOptions } from "/imports/utils/venues";
import { inputFocus } from "/imports/utils/shared";

export const AddTableModal = ({ onHide, show, currentVenue, venues }) => {
  const [serverError, setServerError] = useState('');

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);
  // Autofocus name input
  useEffect(() => { show && inputFocus('name') }, [show, onHide]);

  // Insert table into db
  const submit = formData => {
    Meteor.call('tables.insert', formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName="tour-add-table-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Tables
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={{venueId: currentVenue}} schema={TableBridge} onSubmit={submit} id="add-table-form">
        <Modal.Body>
          <AutoField name="name" showInlineError inputClassName="tour-table-name-input" />
          <SelectField 
            name="venueId"
            options={createVenueSelectOptions(venues)}
            placeholder="Select Venue"
            showInlineError
            className="tour-table-venue-select"
            disabled={venues.length == 1 ? true : false}
          />
        </Modal.Body>

        <Modal.Footer>
          <Error message={serverError} />
          <SubmitField id="tour-table-submit-button" />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};