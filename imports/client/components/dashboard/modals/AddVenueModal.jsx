import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { Information } from '/imports/client/components/shared/Information';
import { Container, Modal, Row, Col } from 'react-bootstrap';
import { AutoForm, TextField, SubmitField, NestField, BoolField, ErrorField } from 'uniforms-bootstrap4';
import { VenueBridge } from "/imports/db/venues/schema";
import { checkUsersStripeIntegrationStatus } from "/imports/utils/venues";
import { inputFocus } from "/imports/utils/shared";

export const AddVenueModal = ({ show, onHide }) => {
  const [serverError, setServerError] = useState('');
  const [informationMessage, setInformationMessage] = useState('');
  const [cardOnlineChecked, setCardOnlineChecked] = useState(false);
  const [cardOnlineAllowed, setCardOnlineAllowed] = useState(false);

  const { userInfo } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);
  // Autofocus name input
  useEffect(() => { show && inputFocus('name') }, [show, onHide]);
  // Show message depending on users stripe integration status
  useEffect(() => {
    const { integrated, message } = checkUsersStripeIntegrationStatus(userInfo?.integrations);
    setCardOnlineAllowed(integrated);
    setInformationMessage(message);
    // force message to show if no integrations are found 
    if(!integrated) setCardOnlineChecked(true);
  }, [show, onHide]);

  // Add venue to db
  const submit = formData => {
    Meteor.call('venues.insert', formData, (error, result) => {
      if(error) {
        setServerError(error.reason)
      } else {
        Session.setAuth('venueId', result);
        onHide();
      }
    });
  };

  // If cardOnline is selected, change state so info message renders
  const change = (key, value) => {
    if(key == "paymentOptions.cardOnline") {
      if(value) setCardOnlineChecked(true);
      else setCardOnlineChecked(false);      
    }
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName="tour-add-venue-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Venue
        </Modal.Title>
      </Modal.Header>

      <AutoForm schema={VenueBridge} onSubmit={submit} onChange={change} id="add-venue-form">
        <Modal.Body>
          <Container>
            <Row>
              <Col>
                <TextField name="name" showInlineError inputClassName="tour-venue-name-input" />
              </Col>
            </Row>
            <NestField name="paymentOptions" className="tour-venue-payment-options-input">
              <ErrorField className="custom-error text-danger border-0 font-weight-light" />
              <Row>
                <Col><BoolField name="cash" showInlineError /></Col>
                <Col><BoolField name="cardMachine" showInlineError /></Col>
                <Col>
                  <BoolField 
                    name="cardOnline" 
                    showInlineError 
                    disabled={!cardOnlineAllowed} 
                    className={!cardOnlineAllowed && 'text-secondary'} 
                  />
                </Col>
              </Row>
            </NestField>
            { !cardOnlineChecked ? '' : <Information message={informationMessage} /> }
          </Container>
        </Modal.Body>

        <Modal.Footer>
          <Error message={serverError} />
          <BoolField name="isAcceptingOrders" showInlineError className="mx-4 tour-venue-accepting-orders-input" />
          <SubmitField id="tour-venue-submit-button" />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
