import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { Information } from '/imports/client/components/shared/Information';
import { Container, Modal, Row, Col } from 'react-bootstrap';
import { AutoForm, TextField, SubmitField, NestField, BoolField, ErrorField } from 'uniforms-bootstrap4';
import { VenueBridge } from "/imports/db/venues/schema";
import { checkUsersStripeIntegrationStatus } from "/imports/utils/venues";

export const EditVenueModal = ({ show, onHide }) => {
  const [serverError, setServerError] = useState('');
  const [informationMessage, setInformationMessage] = useState('');
  const [cardOnlineChecked, setCardOnlineChecked] = useState(false);
  const [cardOnlineAllowed, setCardOnlineAllowed] = useState(false);

  const { userInfo, modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);
  // Show message depending on users stripe integration status
  useEffect(() => {
    const { integrated, message } = checkUsersStripeIntegrationStatus(userInfo?.integrations);
    setCardOnlineAllowed(integrated);
    setInformationMessage(message);
    // force message to show if no integrations are found 
    if(!integrated) setCardOnlineChecked(true);
  }, [show, onHide]);

  // Update venue in db
  const submit = formData => {
    Meteor.call('venues.update', modalData._id, formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  // If form change is cardOnline checkbox, change state so info message renders
  const change = (key, value) => {
    if(key == "paymentOptions.cardOnline") {
      if(value) setCardOnlineChecked(true);
      else setCardOnlineChecked(false);      
    }
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Venue
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={VenueBridge} onSubmit={submit} onChange={change}>
        <Modal.Body>
          <Error message={serverError} />  
          <Container>
            <Row>
              <Col>
                <TextField name="name" showInlineError />
              </Col>
            </Row>
            <NestField name="paymentOptions">
              <ErrorField className="custom-error text-danger border-0 font-weight-light" />
              <Row>
                <Col><BoolField name="cash" showInlineError /></Col>
                <Col><BoolField name="cardMachine" showInlineError /></Col>
                <Col>
                  <BoolField 
                    name="cardOnline" 
                    showInlineError 
                    disabled={!cardOnlineAllowed} 
                    className={!cardOnlineAllowed && 'text-secondary'} 
                  />
                </Col>
              </Row>
            </NestField>
            { !cardOnlineChecked ? '' : <Information message={informationMessage} /> }
          </Container>
        </Modal.Body>

        <Modal.Footer>
          <BoolField name="isAcceptingOrders" showInlineError className="mr-4" />
          <SubmitField />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
