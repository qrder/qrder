import React, { useEffect, useState, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { Modal } from 'react-bootstrap';
import { AutoForm, AutoField, BoolField, SubmitField, SelectField } from 'uniforms-bootstrap4';
import { SectionBridge } from "/imports/db/section/schema";
import { createParentSelectOptions } from "/imports/utils/menu";

export const DuplicateSectionModal = ({ show, onHide, data }) => {
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Handle form submit - duplicate section and children in db
  const submit = formData => {
    delete formData._id;
    Meteor.call('sections.duplicate', formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Duplicate Section
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={SectionBridge} onSubmit={submit}>
        <Modal.Body>
          <Error message={serverError} />  
          <AutoField name="name" showInlineError />
          <SelectField 
            name="parentId"
            options={createParentSelectOptions(data, modalData?._id)}
            placeholder="Select Parent"
            showInlineError
            label="Parent"
          />
        </Modal.Body>

        <Modal.Footer>
          <BoolField name="publish" />
          <SubmitField />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
