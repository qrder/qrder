import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { GlobalContext } from '/imports/context/GlobalState';
import { Modal, Row, Container, Col } from 'react-bootstrap';
import { ItemBridge } from "/imports/db/item/schema";
import { Error } from '/imports/client/components/shared/Error';
import { Add } from "/imports/client/components/dashboard/menu/Add";
import { Delete } from "/imports/client/components/dashboard/menu/Delete";
import PriceListField from '/imports/client/components/dashboard/menu/PriceListField';
import OptionGroupsListField from '/imports/client/components/dashboard/menu/OptionGroupsListField';
import { AutoForm, AutoField, BoolField, SubmitField, LongTextField, SelectField } from 'uniforms-bootstrap4';
import { createParentSelectOptions } from "/imports/utils/menu";

export const EditItemModal = ({ show, onHide, data }) => { 
  const [serverError, setServerError] = useState('');

  const { modalData } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  const submit = formData => {
    Meteor.call('items.update', modalData._id, formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Edit Item
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={modalData} schema={ItemBridge} onSubmit={submit}>
        <Modal.Body>

        <AutoField name="name" label={false} placeholder="Name" showInlineError />
        <LongTextField name="description" label={false} placeholder="Description" rows={3} />

        <SelectField 
          name="parentId"
          options={createParentSelectOptions(data, ' ')}
          placeholder="Select Parent"
          showInlineError
          label="Parent"
          label={false}
        />

        {/* TODO: Move custom form fields into a shared folder to make it clear what they are */}
        <PriceListField
          name="prices"
          addIcon={<Add text="Price" />}
          removeIcon={<Delete />}
          initialCount={1}
        />

        <OptionGroupsListField 
          name="options"
          addIcon={<Add text="Option" />}
          removeIcon={<Delete />}
          showInlineError
        />

        <Container className="mt-3">
          <Row>
            <Col><BoolField name="vegetarian" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
            <Col><AutoField name="vegan" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
            <Col><AutoField name="glutenFree" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
            <Col><AutoField name="containsAlcohol" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
            <Col><AutoField name="containsFish" className="m-0 text-nowrap mb-2 mb-md-0" /></Col>
          </Row>
        </Container>

        </Modal.Body>

        <Modal.Footer>
          <Error message={serverError} />
          <BoolField name="publish" />
          <SubmitField />         
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
