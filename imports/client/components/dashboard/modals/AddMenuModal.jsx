import React, { useState, useEffect } from 'react';
import { Meteor } from "meteor/meteor";
import { Modal } from 'react-bootstrap';
import { Error } from '/imports/client/components/shared/Error';
import { AutoForm, AutoField, SelectField, BoolField, SubmitField } from 'uniforms-bootstrap4';
import { MenuBridge } from "/imports/db/menu/schema";
import { createVenueSelectOptions } from "/imports/utils/venues";
import { inputFocus } from "/imports/utils/shared";

export const AddMenuModal = ({ onHide, show, currentVenue, venues }) => {
  const [serverError, setServerError] = useState('');
  
  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);
  // Autofocus name input
  useEffect(() => { show && inputFocus('name') }, [show, onHide]);

  // Add menu to db
  const submit = formData => {
    Meteor.call('menus.insert', formData, error => {
      error ? setServerError(error.reason) : onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName="tour-add-menu-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add Menu
        </Modal.Title>
      </Modal.Header>

      <AutoForm model={{venueId: currentVenue}} schema={MenuBridge} onSubmit={submit} id="add-menu-form">
        <Modal.Body>
          <Error message={serverError} />
          <AutoField name="name" showInlineError className="tour-menu-name-input" />
          <SelectField 
            name="venueId"
            options={createVenueSelectOptions(venues)}
            placeholder="Select Venue"
            showInlineError
            className="tour-menu-venue-select"
            disabled={venues.length == 1 ? true : false}
          />
        </Modal.Body>
        <Modal.Footer>
          <BoolField name="publish" className="tour-menu-publish-input" />
          <SubmitField id="tour-menu-submit-button" />
        </Modal.Footer>
      </AutoForm>
    </Modal>
  );
};
