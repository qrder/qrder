import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from "meteor/meteor";
import { Modal, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';
import { createUpdateUserInfoObj } from '/imports/utils/settings';

export const CancelSubscriptionModal = ({ onHide, show, sub }) => {
  const [serverError, setServerError] = useState('');

  const { updateUser, userInfo } = useContext(GlobalContext);

  // Ensure server error message is not shown after modal close
  useEffect(() => { setServerError('') }, [show, onHide]);

  // Cancel subscription
  const handleCancelSubmit = async (event) => {
    event.preventDefault();
    Meteor.call("users.cancelSubscription", sub.stripeSubscriptionId, error => {
      if (error) {
        sendNotification("An Error Occurred.", "error");
        setServerError(error.reason);
      } else {
        sendNotification("Subscription Cancelled.", "success");
        onHide();
        // Temporarily update context as userInfo context is used
        // to make page redirections & it will not update until page refresh
        updateUser(createUpdateUserInfoObj(userInfo, "Cancelled", "canceled"));
      }
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Cancel Subscription
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>
          Are you sure you want to cancel your subscription? 
          This will prevent you from using any of QRDER's features <strong>immediately</strong>.
        </p>
        <p>
          You can restart your subscription at anytime.
        </p>
      </Modal.Body>
        
      <Modal.Footer>
        <Error message={serverError} />
        <Button variant="danger" onClick={handleCancelSubmit}>Cancel</Button>
      </Modal.Footer>
    </Modal>
  );
};
