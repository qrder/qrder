import React from 'react';
import { Container } from 'react-bootstrap';
import { IntegrateStripe } from '/imports/client/components/dashboard/settings/IntegrateStripe';

export const Integrations = ({ integrations }) => {

  return (
    <Container>
      <h4 className="my-4">Integrations</h4>
      
      <hr/>

      <IntegrateStripe stripe={integrations?.stripe} />

      <hr/>

    </Container>
  );
};
