import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { InvoicesList } from '/imports/client/components/dashboard/settings/InvoicesList';

export const Invoices = ({ customerId }) => {

  return (
    <Container>
      <h4 className="mt-4">Invoices</h4>
      
      <div className="small">
        'Paid' - A subscription invoice has been paid successfully.<br/>
        'Authentication' - Authentication with your card issuer is required to complete the payment.<br/>
        'Failed' - A payment has failed.  Add a new payment method or resolve the issues with your current method.  Retries will use your default payment method.
      </div>

      <br/>

      <Row>
        <Col className="p-0">
          <InvoicesList customerId={customerId} />
        </Col>
      </Row>

    </Container>
  );
};
