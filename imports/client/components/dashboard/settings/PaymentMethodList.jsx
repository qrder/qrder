import React, { useEffect, useState } from 'react';
import { Meteor } from 'meteor/meteor';
import { Table } from 'react-bootstrap';
import { PaymentMethodListItem } from "/imports/client/components/dashboard/settings/PaymentMethodListItem";

export const PaymentMethodList = ({ sub, pmAdded }) => {
  const [paymentMethods, setPaymentMethods] = useState();
  // The value of pmDeleted & defaultChanged does not really matter,
  // they are used to force an entire list rerender when a card is deleted or changed
  const [pmDeleted, setPmDeleted] = useState(false);
  const [defaultChanged, setDefaultChanged] = useState(false);
  const [loading, setLoading] = useState(true);

  // Retrieve list of payment methods for current user
  useEffect(() => {
    setLoading(true);
    if(sub !== undefined) {
      // There is a slight delay in adding a payment method & it appearing in stripe
      // The react state normally updates faster than stripe, so using a 1 second delay
      // before fetching the payment methods.
      setTimeout(() => {
        Meteor.call("users.listPaymentMethods", sub.stripeCustomerId, (error, result) => {
          if(error) {
            console.log(error);
          } else {
            const { data } = result; 

            Meteor.call("users.getDefaultPaymentMethod", sub.stripeCustomerId, (err, res) => {
              if(err) {
                console.log(err)
              } else {
                // set default payment method
                data.map(d => d.isDefault = (d.id == res) ? true : false);
                // set list of payment methods
                setPaymentMethods(data);
                setLoading(false);
              }
            });
          }
        })
      }, 1000);
    }
  }, [sub, pmDeleted, pmAdded, defaultChanged]);

  // TODO: Handle error and result better

  if(loading) {
    return (
      <p><i>Loading...</i></p>
    )
  }

  if(paymentMethods?.length === 0) {
    return (
      <div>
        <p><i>You have not added any payment methods.</i></p>
        <p className="text-danger">
          Your subscription will not be renewed until you add a payment method.  
          If a payment method is not added, you will lose access to QRDER until you add one. 
        </p>
      </div>
    )
  }

  return (  
    <Table>
      <thead>
        <tr>
          <th>Type</th>
          <th>Card Number</th>
          <th>Expiration</th>
          <th>Delete</th>
          <th>Default</th>
        </tr>
      </thead>
      <tbody>
        {
          paymentMethods?.map((pm, idx) => {
            return (
              <PaymentMethodListItem 
                key={idx} 
                pm={pm} 
                cid={sub.stripeCustomerId}
                onDelete={() => setPmDeleted(!pmDeleted)} 
                onDefaultChange={() => setDefaultChanged(!defaultChanged)}
              />
            )
          })
        }
      </tbody>
    </Table>
  );
};
