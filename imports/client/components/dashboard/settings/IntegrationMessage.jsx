import React from 'react';

export const IntegrationMessage = ({ message }) => {

  return (
    <i className="small">
      {message.general} 

      <br/>

      {
        message.important ? (
          <><strong>Please Note:</strong> {message.important}</>
        ) : ''
      }
    </i>
  );
};
