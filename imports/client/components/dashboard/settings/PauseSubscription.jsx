import React, { useContext } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { isSubscriptionCancelled, isSubscriptionPaused } from '/imports/utils/users';

export const PauseSubscription = ({ status }) => {
  const { openModal } = useContext(GlobalContext);
  
  // TODO: Combine this with cancel sub comp? As they are very similar 

  return (
    <>
      <p>Pause Subscription</p>

      <Row>
        <Col md={8}>
          <p className="pl-3">
            Pause your QRDER subscription.  Further payments will not be collected until you resume your subscription.  
            You may continue to update your menu, tables and venue information, but you cannot view, manage or receive orders.
            You may resume your subscription at any time.
          </p>
        </Col>
        <Col md={4} className="text-right">
          {
            !isSubscriptionCancelled(status) ? (
              isSubscriptionPaused(status) ? (
                <Button variant="success" onClick={() => openModal('resume-sub')}>
                  Resume Subscription
                </Button>
              ) : (
                <Button variant="warning" onClick={() => openModal('pause-sub')}>
                  Pause Subscription
                </Button>
              )
            ) : ''
          }
        </Col>
      </Row>
    </>
  );
};
