import React, { useContext } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { isSubscriptionCancelled } from '/imports/utils/users';

export const CancelSubscription = ({ status }) => {
  const { openModal } = useContext(GlobalContext);

  // TODO: Combine this with pause sub comp? As they are very similar 

  return (
    <>
      <p>Cancel Subscription</p>

      <Row>
        <Col md={8}>
          <p className="pl-3">
            Permanently cancel your QRDER subscription and receive a prorated refund based on how much you have used this month.
            You will not be able to use any of QRDER's features.  If you are currently in your trial period, this will end it immediately.
            You may restart your subscription at anytime.
          </p>
        </Col>
        <Col md={4} className="text-right">
          {
            isSubscriptionCancelled(status) ? (
              <Button onClick={() => openModal('restart-sub')}>
                Restart Subscription
              </Button>
            ) : (
              <Button variant="danger" onClick={() => openModal('cancel-sub')}>
                Cancel Subscription
              </Button>
            )
          }
        </Col>
      </Row>
    </>
  );
};
