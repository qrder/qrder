import React, { useState } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Error } from '/imports/client/components/shared/Error';
import { PaymentMethodList } from '/imports/client/components/dashboard/settings/PaymentMethodList';
import { PaymentMethodForm } from '/imports/client/components/dashboard/settings/PaymentMethodForm';

export const PaymentSettings = ({ data }) => {
  const [serverError, setServerError] = useState('');
  const [hidden, setHidden] = useState(true);
  // The value of updateList does not really matter - it's used to force a rerender
  const [updateList, setUpdateList] = useState(false);

  return (
    <>
      <h4 className="mt-4">Payment Information</h4>

      {/* TODO: Make page appear / feel more secure */}
      
      <Container>

        <Row>
          <Col className="p-0">
            <p>Payment Methods</p>
            <PaymentMethodList sub={data?.subscription} pmAdded={updateList} />
          </Col>
        </Row>

        <Row hidden={hidden} className="mt-3">
          <Col>
            <Error message={serverError} />
            <PaymentMethodForm 
              sub={data?.subscription} 
              onError={(error) => setServerError(error)} 
              onSuccess={() => { setUpdateList(!updateList); setHidden(true)}}
            />
          </Col>
        </Row>

        <Row>
          <Col className="p-0">
            <Button variant="primary" onClick={() => setHidden(!hidden)}>
              { hidden ? "+ Add Payment Method" : "Close Form" }
            </Button>
          </Col>
        </Row>
        
      </Container>
    </>
  );
};
