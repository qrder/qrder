import React, { useEffect, useState } from 'react';
import { Meteor } from 'meteor/meteor';
import { Button, Form } from 'react-bootstrap';
import { sendNotification } from '/imports/utils/shared';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faCcVisa, faCcMastercard } from '@fortawesome/free-brands-svg-icons';

export const PaymentMethodListItem = ({ pm, cid, onDelete, onDefaultChange }) => {
  
  // Return relevant card icon for each card user has
  const getCardIcon = (brand) => {
    switch(brand) {
      case "visa":
        return faCcVisa;
      case "mastercard":
        return faCcMastercard;
      default:
        return faCreditCard;
    }
  }

  // Delete payment method attached to customer in Stripe
  const handleDeletePaymentMethod = () => {
    Meteor.call("users.deletePaymentMethod", pm.id, error => {
      if(error) {
        console.log(error);
      } else {
        onDelete();
      }
    });
  }

  // Update default payment method for customers invoices
  const handleDefaultSelect = (pmId) => {
    Meteor.call("users.setDefaultPaymentMethod", cid, pmId, error => {
      if(error) {
        console.log(error);
        sendNotification("An Error Occurred.", "error");
      } else {
        sendNotification("Default Payment Method Updated.", "success");
        onDefaultChange();
      }
    })
  }

  return (
    <tr>
      <td><FontAwesomeIcon icon={getCardIcon(pm.card.brand)} /></td>
      <td>**** **** **** {pm.card.last4}</td>
      <td>{pm.card.exp_month} / {pm.card.exp_year}</td>
      <td>
        <Button variant="danger" onClick={() => handleDeletePaymentMethod()}>
          Delete
        </Button>
      </td>
      <td>
        <Form>
          <Form.Check type="checkbox" onChange={() => handleDefaultSelect(pm.id)} checked={pm.isDefault} />
        </Form>
      </td>
    </tr>
  );
};
