import React from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import { StripeElements } from '/imports/client/components/dashboard/settings/StripeElements';
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js';
import { addPaymentMethod } from '/imports/utils/stripe';

export const PaymentMethodForm = ({ sub, onError, onSuccess }) => {
  const stripe = useStripe();
  const elements = useElements();

  // Add payment information to customer in stripe
  const handlePaymentMethodSubmit = async (event) => {
    event.preventDefault();
    // Return if stripe hasn't loaded yet
    if (!stripe || !elements) return;
    // Get CardElement from StripeElements component
    const cardElement = elements.getElement(CardElement);
    // Create payment method
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: cardElement
    });

    if (error) {
      console.log('[createPaymentMethod error]', error);
      onError(error);
    } else {
      const paymentMethodId = paymentMethod.id;
      const customerId = sub.stripeCustomerId;
      const subscriptionId = sub.stripeSubscriptionId;
      addPaymentMethod({ customerId, subscriptionId, paymentMethodId });
      // TODO: Only call onSuccess if addPaymentMethod works, if error do not call onSuccess
      onSuccess();
    }
  };

  return (
    <Form onSubmit={handlePaymentMethodSubmit}>
      <Row>
        <Col className="p-0">
          <span>Add Payment Method</span>
        </Col>
      </Row>
      <Row>
        <Col lg={6} className="px-0">
          {/* TODO: Empty form fields upon successful submit */}
          <StripeElements buttonDisabled={!stripe} />
        </Col>
      </Row>
    </Form>
  );
};
