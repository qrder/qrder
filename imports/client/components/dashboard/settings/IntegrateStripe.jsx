import React, { useState, useEffect } from 'react';
import { Meteor } from "meteor/meteor";
import { Row, Col } from 'react-bootstrap';
import { getUsersStripeConnectionStatus } from '/imports/utils/stripe';
import { IntegrationMessage } from '/imports/client/components/dashboard/settings/IntegrationMessage';
import { IntegrationButton } from '/imports/client/components/dashboard/settings/IntegrationButton';

export const IntegrateStripe = ({ stripe }) => {
  const [button, setButton] = useState({
    variant: 'primary',
    disabled: false,
    text: 'Connect',
    clickEvent: () => connectStripe()
  });

  const [message, setMessage] = useState({
    general: "Click 'Connect' to begin receiving payments into your Stripe account.",
    important: "Stripe charges a 1.4% + 20p charge (European cards) and 2.9% + 20p (non-European cards).  This has nothing to do with QRDER."
  });

  // Not the prettiest use effect, but it's hard to handle this much
  // flexibility otherwise (especially re-usable components like message and button)
  useEffect(() => {
    if(stripe) {
      const status = getUsersStripeConnectionStatus(stripe);
      
      switch(status) {
        case 'not-connected':
          // Button remains as default state
          break;
        case 'started':
          setButton({
            variant: 'primary',
            disabled: false,
            text: 'Continue Setup',
            clickEvent: connectStripe 
          });
          setMessage({general: "Account created but not finished.  You cannot receive payments into your Stripe account or receive payouts from Stripe."});
          break;
        case 'pending':
          setButton({
            variant: 'secondary',
            disabled: true,
            text: 'Pending'
          });
          setMessage({general: "Please wait while Stripe processes your initial onboarding and connects your account (10 minutes approx)."});
          break;
        case 'first-stage-complete':
          setButton({
            variant: 'success',
            disabled: false,
            text: 'Finish Setup',
            clickEvent: continueSetup 
          });
          setMessage({
            general: `Account created.  Payment enabled.  You can receive payments into your Stripe account but not receive payouts from Stripe.
              Please finish the setup to enable Stripe payouts.  
              If you have just completed your account setup, please wait a few minutes for the next phase to become available.`
          });
          break;
        case 'connected':
          setButton({
            variant: 'secondary',
            disabled: true,
            text: 'Connected'
          });
          setMessage({
            general: `Account fully connected.  Payments and payouts are enabled.
              You can receive payments into your Stripe account and receive payouts from Stripe.`,
            important: "Stripe charges a 1.4% + 20p charge (European cards) and 2.9% + 20p (non-European cards).  This has nothing to do with QRDER."
          });
          break;
      }
    }
  }, [stripe]);

  // Continue a previously started setup
  const continueSetup = () => {
    createLink(stripe.stripeAccountId);
  }

  // Create Stripe URL to redirect user to
  const createLink = (accountId) => {  
    Meteor.call("users.linkConnectedAccount", accountId, (error, result) => {
      if(error) {
        console.log(error)
      } else {
        window.location.replace(result); 
      }
    });
  } 

  // Start a fresh stripe connected account setup
  const connectStripe = () => {
    Meteor.call("users.createConnectedAccount", (error, result) => {
      if(error) {
        // TODO: handle errors better
        console.log(error)
      } else {
        createLink(result);
      }
    })
  }

  return (
    <Row className="d-flex align-items-center">
      <Col md={3}><img src="/images/stripe-logo.svg" /></Col>
      <Col md={7}>
        <IntegrationMessage message={message} />
      </Col>
      <Col md={2} className="mt-4 mt-md-0 d-flex justify-content-center">
        <IntegrationButton button={button} />
      </Col>
    </Row>
  );
};
