import React from 'react';
import { Meteor } from "meteor/meteor";
import { Row, Col } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import { Add } from "/imports/client/components/dashboard/menu/Add";
import { Delete } from "/imports/client/components/dashboard/menu/Delete";
import { AutoForm, AutoField, SubmitField, ListField, ListItemField, ListDelField, SelectField } from 'uniforms-bootstrap4';
import { UserBridge } from "/imports/db/users/schema";
import { addCurrencyInfoToSettingsObject } from '/imports/utils/settings';
import { sendNotification } from '/imports/utils/shared';

export const AccountSettings = ({ data }) => {
  const history = useHistory();

  const submit = formData => {
    Meteor.call("users.getCurrencyInfo", formData.settings.currency.code, (error, result) => {
      if(error) {
        // TODO: Handle and Print server errors somewhere on page as well
        sendNotification("Error updating currency.", "error");
      } else {
        formData = addCurrencyInfoToSettingsObject(formData, result);
        // TODO: Should I be using Meteor's 'addEmail' and 'removeEmail' functions?
        Meteor.call('users.update', formData, err => {
          if (err) {
            sendNotification("An Error Occurred.", "error");
          } else {
            sendNotification("Settings Updated", "success");
          }
        });
      }
    });
  };

  return (
    <>
      <h4 className="mt-4">Account Settings</h4> 

      <p>
        Please Note : Changes will not be saved until you click the 
        <strong> "Submit"</strong> button at the bottom.
      </p>
      
      <Row>
        <Col>
          <AutoForm model={data} schema={UserBridge} onSubmit={submit}>
            <AutoField name="username" showInlineError />

            {/* TODO: Custom field for emails? */}
            <ListField 
              name="emails"
              addIcon={<Add text="Email" />}
              removeIcon={<Delete />}
              showInlineError
            >
              <ListItemField name="$">
                <AutoField name="address" label={false} placeholder="Email Address" className="col-8 ml-5" />
              </ListItemField>
            </ListField>

            <Row>
              <Col md={4}>
                <SelectField
                  name="settings.currency.code"
                  placeholder="Select Currency Code"
                  showInlineError
                />
              </Col>
              <Col md={6}>
                <AutoField name="settings.currency.name" disabled />
              </Col>
              <Col md={2}>
                <AutoField name="settings.currency.symbol" disabled />
              </Col>
            </Row>

            <p>
              Some settings are handled on the 
              <a href="" onClick={() => history.push("/dashboard/venues")}> venues page</a>.
              For example, the accepted payment options for your venue.
            </p>

            <Row className="text-right">
              <Col>
                <SubmitField />
              </Col>
            </Row>
          </AutoForm>
        </Col>
      </Row>
    </>
  );
};
