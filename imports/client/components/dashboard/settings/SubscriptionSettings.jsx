import React, { useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import { UserBridge } from "/imports/db/users/schema";
import { GlobalContext } from '/imports/context/GlobalState';
import { AutoForm, AutoField, TextField } from 'uniforms-bootstrap4';
import { CancelSubscriptionModal } from '/imports/client/components/dashboard/modals/CancelSubscriptionModal';
import { RestartSubscriptionModal } from '/imports/client/components/dashboard/modals/RestartSubscriptionModal';
import { PauseSubscriptionModal } from '/imports/client/components/dashboard/modals/PauseSubscriptionModal';
import { ResumeSubscriptionModal } from '/imports/client/components/dashboard/modals/ResumeSubscriptionModal';
import { CancelSubscription } from '/imports/client/components/dashboard/settings/CancelSubscription';
import { PauseSubscription } from '/imports/client/components/dashboard/settings/PauseSubscription';
import { getModalOpenStatus } from "/imports/utils/shared";

export const SubscriptionSettings = ({ data }) => {
  const { modals, closeModal } = useContext(GlobalContext);

  return (
    <>
      <h4 className="mt-4">Subscription Settings</h4> 
      
      <Row>
        <Col>
          <AutoForm model={data} schema={UserBridge}>

            <TextField name="subscription.type" disabled />

            <TextField name="subscription.status" disabled />

            <AutoField name="subscription.price" disabled />
            {/* TODO: Change label to 'Trial Ended' if not in trial */}
            {/* TODO: Add message if they cancelled and restarted? */}
            <AutoField name="subscription.freeTrialEnd" disabled />
            <AutoField name="subscription.currentPeriodEnd" disabled />

            <hr/>

            <PauseSubscription status={data?.subscription.status} />

            <hr/>

            <CancelSubscription status={data?.subscription.status} />
            
          </AutoForm>
        </Col>
      </Row>

      <CancelSubscriptionModal show={getModalOpenStatus("cancel-sub", modals)} onHide={() => closeModal("cancel-sub")} sub={data?.subscription} />
      <RestartSubscriptionModal show={getModalOpenStatus("restart-sub", modals)} onHide={() => closeModal("restart-sub")} sub={data?.subscription} />
      <PauseSubscriptionModal show={getModalOpenStatus("pause-sub", modals)} onHide={() => closeModal("pause-sub")} sub={data?.subscription} />
      <ResumeSubscriptionModal show={getModalOpenStatus("resume-sub", modals)} onHide={() => closeModal("resume-sub")} sub={data?.subscription} />
    </>
  );
};
