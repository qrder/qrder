import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { CardElement } from '@stripe/react-stripe-js';
import './PaymentMethod.css';

const CARD_ELEMENT_OPTIONS = {
  hidePostalCode: true,
  style: {
    base: {
      color: "#32325d",
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "#aab7c4",
      },
    },
    invalid: {
      color: "#fa755a",
      iconColor: "#fa755a",
    },
  },
};

export const StripeElements = () => {

  return (
    <Container className="my-3 p-0">
      <Row>
        <Col>
          <CardElement options={CARD_ELEMENT_OPTIONS} />
        </Col>
        <Col xs={2} className="p-0 pl-1 my-auto">
          {/* TODO: Disable until all are entered */}
          <Button type="submit">
            Confirm
          </Button>
        </Col>
      </Row>
    </Container>
  );
};
