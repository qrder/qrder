import React, { useEffect, useState } from 'react';
import { Meteor } from 'meteor/meteor';
import { Table } from 'react-bootstrap';
import { Error } from '/imports/client/components/shared/Error';
import { sendNotification } from '/imports/utils/shared';
import { InvoiceListItem } from '/imports/client/components/dashboard/settings/InvoiceListItem';

export const InvoicesList = ({ customerId }) => {
  const [loading, setLoading] = useState(true);
  const [invoices, setInvoices] = useState();
  const [serverError, setServerError] = useState('');
  // Doesn't matter what value this has - used to rerender list
  const [update, setUpdate] = useState(false);

  // Retrieve list of payment methods for current user
  useEffect(() => {
    setLoading(true);
    if(customerId !== undefined) {
      Meteor.call("users.getInvoices", customerId, (error, result) => {
        if(error) {
          console.log(error);
          // TODO: handle better (need to throw meteor errors)
          // setServerError(error.reason);
          sendNotification("Sorry, an error occurred when retrieving your invoices.", "error");
        } else {
          const { data } = result;
          setInvoices(data);
          setLoading(false);
        }
      })
    }
  }, [customerId, update]);

  if(loading) {
    return (
      <p><i>Loading...</i></p>
    )
  };

  return (
    <>
      <Error message={serverError} />
      <Table>
        <thead>
          <tr>
            <th>Status</th>
            <th>Price</th>
            <th>Description</th>
            <th>Period</th>
            <th>Created</th>
            <th>Paid</th>
            <th>Complete</th>
            <th>Download</th>
          </tr>
        </thead>
        <tbody>
          {
            invoices?.map((invoice, idx) => {
              return <InvoiceListItem key={idx} invoice={invoice} onUpdate={() => setUpdate(!update)} />
            })
          }
        </tbody>
      </Table>
    </>
  )
};
