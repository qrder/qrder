import React from 'react';
import { Button } from 'react-bootstrap';

export const IntegrationButton = ({ button }) => {

  return (
    <Button 
      variant={button.variant} 
      size="sm" 
      onClick={button.clickEvent}
      disabled={button.disabled}
    >
      {button.text}
    </Button>
  );
};
