import React, { useContext } from 'react';
import { Meteor } from 'meteor/meteor';
import { GlobalContext } from '/imports/context/GlobalState';
import moment from 'moment';
import { Button, Badge } from 'react-bootstrap';
import { useStripe } from '@stripe/react-stripe-js';
import { sendNotification } from '/imports/utils/shared';
import { createUpdateUserInfoObj } from '/imports/utils/settings';

export const InvoiceListItem = ({ invoice, onUpdate }) => {
  const stripe = useStripe();

  const { updateUser, userInfo } = useContext(GlobalContext);

  // Complete payments that require authentication
  const handlePaymentAuthentication = () =>{
    // This opens the Stripe authentication modal
    stripe.confirmCardPayment(invoice.payment_intent.client_secret, {
        payment_method: invoice.payment_intent.payment_method,
      })
      .then((result) => {
        if (result.error) {
          throw result;
        } else {
          if (result.paymentIntent.status === 'succeeded') {
            sendNotification("Authentication Complete.", "success");
          }
        }
        // Temporarily update context as userInfo context is used
        // to make page redirections & it will not update until page refresh
        updateUser(createUpdateUserInfoObj(userInfo, "Active", "active"));
        // bubble call to reload invoices
        onUpdate();
      })
      .catch((error) => {
        switch(error.error.decline_code) {
          case "insufficient_funds":
            sendNotification("Insufficient Funds.", "error");
            break;
          case "expired_card":
            sendNotification("Card Expired.", "error");
            break;
          default:
            sendNotification("An Error Occurred.", "error");
            break;
        }
        // bubble call to reload invoices
        onUpdate();
      });
  };

  // Retry failed payment / invoice (assumes the user has added a new card / fix the issue)
  const handleInvoicePayment = () => {
    // TODO: If they retry without adding a new PM / fixing the currently broken one, it just retries with the same one
    Meteor.call("users.payInvoice", invoice.id, error => {
      if(error) {
        // TODO: handle better
        console.log(error);
        sendNotification("An error occurred.  Have you updated your payment method?", "error");
      } else {
        sendNotification("Payment Successful.  Thank you!", "success");
      }
      // Temporarily update context as userInfo context is used
      // to make page redirections & it will not update until page refresh
      updateUser(createUpdateUserInfoObj(userInfo, "Active", "active"));
      // reload invoices list      
      onUpdate();
    });
  }

  // Return relevant button
  const getActionButton = () => {
    if(invoice.status == 'draft') {
      return (
        <Button variant="success" onClick={() => handleInvoicePayment()}>
          Pay Now
        </Button>
      );
    }

    switch(invoice.payment_intent?.status) {
      case "requires_action":
        return (
          <Button variant="warning" onClick={() => handlePaymentAuthentication()}>
            Authenticate
          </Button>
        );
      
      case "requires_payment_method":
        return (
          <Button variant="warning" onClick={() => handleInvoicePayment()}>
            Retry
          </Button>
        );
      
      default:
        return 'N/A';
    }
  }

  // Return correct bootstrap colour variant for button
  const getVariant = () => {
    if(invoice.paid) return "success"; 

    switch(invoice.payment_intent?.status) {
      case "succeeded":
        return "success";
      case "requires_action":
        return "warning";
      case "requires_payment_method":
        return "danger";
      default:
        return "secondary";
    }
  }

  // Return correct message depending on invoice PI status
  const getMessage = () => {
    if(invoice.paid) return "Paid"; 

    switch(invoice.payment_intent?.status) {
      case "succeeded":
        return "Paid";
      case "requires_action":
        return "Authentication";
      case "requires_payment_method":
        return "Failed";
      default:
        return "Pending";
    }
  }

  return (
    <tr>
      <td>
        <Badge variant={getVariant()} className="p-2 ml-2 text-uppercase">
          {getMessage()}
        </Badge>
      </td>
      <td>£{(invoice.total / 100).toFixed(2)}</td>
      <td>{invoice.lines.data[0].description}</td>
      <td>
        {moment.unix(invoice.lines.data[0].period.start).format("DD/MM/YYYY")}
        -
        {moment.unix(invoice.lines.data[0].period.end).format("DD/MM/YYYY")}
      </td>
      <td>
        {moment.unix(invoice.created).format("DD/MM/YYYY")}
      </td>
      <td>
        {
          invoice.status_transitions.paid_at != null ? (
            moment.unix(invoice.status_transitions.paid_at).format("DD/MM/YYYY")
          ) : ''
        }
      </td>
      <td>{ getActionButton() }</td>
      <td>
        <Button href={invoice.invoice_pdf}>Download</Button>
      </td>
    </tr>
  )
};
