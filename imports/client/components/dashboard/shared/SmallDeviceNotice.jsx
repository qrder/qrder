import React from 'react';

export const SmallDeviceNotice = ({ message }) => {

  return (
    <div className="d-sm-none my-4 small text-secondary">
      {message}
    </div>
  )
};
