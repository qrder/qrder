import React from 'react';
import { Badge } from 'react-bootstrap';

export const StepButton = ({ text, show = true }) => {
  const dispClass = !show ? "d-none" : "";
  
  return (
    <Badge variant="primary" className={`py-2 px-3 ${dispClass}`}>
      <h6 className="m-0">{text}</h6>
    </Badge>
  )
}