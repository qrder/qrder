import React, { useState, useEffect, useContext } from 'react';
import { Meteor } from 'meteor/meteor';
import './UserTour.css';
import Tour from 'reactour';
import { GlobalContext } from '/imports/context/GlobalState';
import { createTableOfContentsStep } from "/imports/utils/tour";
import { IntroSteps, VenueSteps, TableSteps, MenuSteps, OptionGroupsSteps } from '/imports/client/components/dashboard/shared/UserTour/steps';
import { StepButton } from '/imports/client/components/dashboard/shared/UserTour/StepButton';

export const UserTour = ({ isTourOpen, closeTour, short, skipToc, isMobile }) => {
  const { closeModal } = useContext(GlobalContext);

  const [steps, setSteps] = useState([]);  
  const [stepsToHideNextButton, setStepsToHideNextButton] = useState();
  const [stepsToHidePrevButton, setStepsToHidePrevButton] = useState();
  const [showNextButton, setShowNextButton] = useState(true);
  const [showPrevButton, setShowPrevButton] = useState(true);
  const [prevStep, setPrevStep] = useState(0);
  const [userCreatedVenue, setUserCreatedVenue] = useState(true);
  const [userCreatedMenu, setUserCreatedMenu] = useState(true);

  useEffect(() => {
    // Create steps to pass to Tour component (handles differently if mobile)
    let tmpSteps = IntroSteps.concat(VenueSteps).concat(TableSteps).concat(MenuSteps).concat(OptionGroupsSteps);
    if(isMobile) {
      tmpSteps = tmpSteps.filter(step => { return !step.hasOwnProperty("largeScreenOnly") });
      tmpSteps.map(step => step.mobileSelector ? step.selector = step.mobileSelector : null);
    } else {
      tmpSteps = tmpSteps.filter(step => { return !step.hasOwnProperty("mobileOnly") });
    }
    setSteps(tmpSteps);
  }, [skipToc, isMobile]);

  useEffect(() => {
    // check if user has created any venues / menus to determine what to show in TOC
    if(isTourOpen) {
      Meteor.call("venues.count", (error, result) => {
        error ? console.log(error) : setUserCreatedVenue(!!result);
      });
      Meteor.call("menus.count", (error, result) => {
        error ? console.log(error) : setUserCreatedMenu(!!result);
      });
    }
  }, [isTourOpen]);

  useEffect(() => {
    // Create table of contents
    if(isTourOpen) {
      if(skipToc) {
        steps.splice(steps.findIndex(step => step.stepIdentifier == "table-of-contents"), 1)
      } else {
        const tocStep = IntroSteps.find(step => step.stepIdentifier == "table-of-contents");
        tocStep.content = createTableOfContentsStep(steps, userCreatedVenue, userCreatedMenu);
      }
    }
  }, [isTourOpen, steps, userCreatedVenue, userCreatedMenu])

  useEffect(() => {
    // Create a list of steps (indexes) that need to have their next or prev step buttons removed
    setStepsToHideNextButton(steps.map((step, idx) => step.hasOwnProperty('hideNextButton') ? idx : '').filter(String));
    setStepsToHidePrevButton(steps.map((step, idx) => step.hasOwnProperty('hidePrevButton') ? idx : '').filter(String));
  }, [steps]);

  const modalShouldBeClosed = (step) => {
    return (
      steps[step].forceModalClosed || 
      (steps[step].checkModalClosed && prevStep > step)
    );
  }

  const handleTourClose = () => {
    setShowNextButton(true);
    setShowPrevButton(true);
    closeTour();
  }

  const checkStepActions = (step) => {
    if(step == prevStep) return;

    // console.log(`${prevStep+1} --> ${step+1}`);
    // console.log(`Step is ${step} and ${step + 1} for users.`);

    // hide next step button on certain steps
    stepsToHideNextButton.includes(step) ? setShowNextButton(false) : setShowNextButton(true);
    // hide pev step button on certain steps
    stepsToHidePrevButton.includes(step) ? setShowPrevButton(false) : setShowPrevButton(true);

    // close modal if it should not be open
    if(modalShouldBeClosed(step)) closeModal(steps[step].modalToClose);
    
    setPrevStep(step);
  }

  return (
    <Tour 
      rounded={10}
      startAt={0}
      steps={steps} 
      disableFocusLock
      showNavigation={false}
      isOpen={isTourOpen}
      closeWithMask={false}
      disableKeyboardNavigation
      onRequestClose={() => handleTourClose()} 
      getCurrentStep={step => checkStepActions(step)}
      prevButton={<StepButton text="Prev" show={showPrevButton} />}
      nextButton={<StepButton text="Next" show={showNextButton} />}
      lastStepNextButton={<StepButton text="Next" />}
    />
  );
}
