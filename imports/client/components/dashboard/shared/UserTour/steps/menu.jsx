import React from 'react';
import history from '/imports/client/history';
import { createClickEventListenerToChangeStep, createFormValidationEventListener } from "/imports/utils/tour";
import { SectionStep } from '/imports/client/components/dashboard/shared/UserTour/SectionStep';
import { inputFocus, inputUnfocus } from "/imports/utils/shared";

/**
 * Tour steps for Menu, Section & Item introduction
 */

// buttons
const addMenuButton = "#tour-add-menu-button";
const submitAddMenuButton = "#tour-menu-submit-button";
const addItemButton = "[data-tour='add-item-button']";
const submitAddItemButton = "#tour-item-submit-button";
const itemModalAddPriceButton = "#tour-item-add-price-button";
const itemModalDeletePriceButton = ".tour-item-delete-price-button"; 
const addSectionButton = "[data-tour='add-section-button']";
const submitAddSectionButton = "#tour-section-submit-button";

// modals
const addMenuModal = ".tour-add-menu-modal";
const addItemModal = ".tour-add-item-modal";
const addSectionModal = ".tour-add-section-modal";

// side nav
const sideNavMenu = "#tour-side-nav-menu";

export const steps = [
  {
    content: ({ goTo }) => <SectionStep section="Part 4: Menu" goTo={goTo} completed={false} />,
    tocIdentifier: "toc-menu",
    tocText: "4: Menu",
    venueDependent: true,
    stepInteraction: false
  },
  {
    selector: '[data-tour="hamburger-icon"]',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(".hamburger-icon", step, goTo);
      
      return <p>Click this button to open the mobile navigation.</p>
    },
    mobileOnly: true
  },
  {
    selector: sideNavMenu,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(sideNavMenu, step, goTo);
      
      return <p>Click 'Menu' to change page.</p>
    },
    hideNextButton: true
  },
  {
    selector: '[data-tour="main-content"]',
    content: <p>You can manage your menu from this page.</p>,
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 4.1: Menu" goTo={goTo} completed={false} />,
    tocIdentifier: "toc-menu-one",
    tocText: "4.1: Menu",
    venueDependent: true,
    stepInteraction: false,
    action: () => history.push("/dashboard/menu")
  },
  {
    selector: addMenuButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(addMenuButton, step, goTo);
      return <p>Click this button to add a menu.</p>;
    },
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "add-menu"
  },
  {
    selector: addMenuModal,
    content: () => {
      inputUnfocus('name');

      return <>
        <p>Add a menu using this form.</p>
        <p>Click 'Next' to get started.</p>
      </>
    },
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-menu-name-input',
    content: () => {
      inputFocus('name');

      return <>
        <p>Enter the name of your menu.</p>
        <p className="text-danger"><strong>Required</strong></p>
        <p className="d-none d-lg-block">
          For example, 'Drinks', 'Starters', 'Cakes', 'Mains', 'Coffee' - it depends how you want to organise your menu.  
          This will likely reflect the physical menu that you have.
        </p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left'
  },
  {
    selector: '.tour-menu-venue-select',
    content: <>
      <p>If you have other venues, you can select them here.</p>
      <p>It defaults to the current venue.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-menu-publish-input',
    content: () => <>
      <p>You have control over whether customers can see each menu (and it's sections and items).</p>
      <p className="d-none d-lg-block">
        This can be useful if you want to draft a menu over time, 
        or quickly remove a menu before making changes to it later on,
        or create seasonal menus like Christmas, Easter, etc.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: submitAddMenuButton,
    content: ({ goTo, step }) => {
      createFormValidationEventListener(submitAddMenuButton, "#add-menu-form", step, goTo);
      return <p>Click the 'Submit' button to add your menu and close the popup window.</p>
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: addMenuModal,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(submitAddMenuButton, step, goTo);

      return <>
        <p>You have not completed the form correctly.</p>  
        <p>Use the error messages to complete the form successfully.</p>
        <p>Click 'Submit' when the error messages have disappeared.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: '.tour-menu-list-item',
    content: <p>This is the menu you just created.</p>,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    selector: '.tour-menu-hamburger-button',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep('.tour-menu-hamburger-button', step, goTo);

      return <p>Click this to show a dropdown to edit, duplicate and delete your menu.</p>
    },
    position: 'right',
    hideNextButton: true
  },
  {
    selector: '.tour-menu-hamburger-dropdown',
    content: () => <>
      <p>You can click any of these to edit, duplicate or delete your menu.</p>
      <p className="d-none d-lg-block">
        When clicked, a popup will appear. The popups work exactly the 
        same as adding a menu, so we won't cover these again in the tour.
      </p>
      <p className="d-block d-lg-none">We will not cover this again in the tour.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 4.1: Menu" goTo={goTo} />,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 4.2: Items" goTo={goTo} completed={false} />,
    tocIdentifier: "toc-menu-two",
    tocText: "4.2: Items",
    venueDependent: true,
    menuDependent: true,
    stepInteraction: false,
    action: () => history.push("/dashboard/menu")
  },
  {
    selector: addItemButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(addItemButton, step, goTo);
      return <p>Click this button to add an item.</p>;
    },
    position: 'right',
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "add-item"
  },
  {
    selector: addItemModal,
    content: () => {
      inputUnfocus('name');

      return <>
        <p>Add an item using this form.</p>
        <p>
          The only fields that are <strong className="text-danger">required </strong> 
          are <i>Name</i> and <i>Price</i>.
        </p>
        <p>We will go over all of them.</p>
        <p>Click 'Next' to get started.</p>
      </>
    },
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-item-name-input',
    content: () => {
      inputFocus('name');

      return <>
        <p>Enter the name of your item.</p>
        <p className="text-danger"><strong>Required</strong></p>
        <p>For example, 'Coca Cola'.  There is space below to provide a more detailed description.</p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left'
  },
  {
    selector: '.tour-item-desc-input',
    content: () => <>
      <p>Enter a description for your item.</p>
      <p className="text-info"><strong>Optional</strong></p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: '.tour-item-parent-input',
    content: () => <>
      <p>You can select a different menu or section using this dropdown.  It defaults to the current menu or section.</p>
      <p className="text-info"><strong>Optional</strong></p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  }, 
  {
    selector: '.tour-item-price-input',
    content: () => <>
      <p>Enter a price for your item.</p>
      <p className="text-danger"><strong>Required</strong></p>
      <p>If an item is free, just enter '0' or '0.00'.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: itemModalAddPriceButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(itemModalAddPriceButton, step, goTo);
      
      return <>
        <p>Click this button to add extra price options.  For example, a 'small' and 'large' price variety.</p>
        <p className="text-info"><i>Only one price is required (even if it is £0.00), these are extra prices options.</i></p>
      </>
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: '.tour-item-multiple-prices-inputs',
    content: () => <>
      <p>Enter a name and value for each price.  For example, 'Small' and '2.50', 'Large' and '5.00'.</p>
      <p className="text-info"><i>Only one price is required (even if it is £0.00).</i></p>
      <p>A customer can only select one price option.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'right',
    hidePrevButton: true
  },
  {
    selector: itemModalDeletePriceButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(itemModalDeletePriceButton, step, goTo);
      
      return <p>Click this button to delete one of the extra price options.</p>
    },
    position: 'right',
    hideNextButton: true,
    hidePrevButton: true
  },
  {
    selector: '.tour-item-price-input',
    content: <p>As one price is left, it defaults to the one remaining price.</p>,
    position: 'left',
    hidePrevButton: true
  },
  {
    selector: '.tour-item-options-input',
    content: () => <>
      <p>You can add shared, pre-defined option groups to items.</p>
      <p className="text-info"><strong>Optional</strong></p>
      <p className="d-none d-lg-block">
        For example, for a lot of drinks you may want to offer the option of ice and lemon.  
        You could create an "Ice & Lemon" option group and assign it to as many items as you'd like. 
      </p>
      <p>
        <strong> 
          Option Groups will be covered in more detail later in the tour as we need to 
          create an option group first (on a different page).
        </strong>
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-item-filter-input',
    content: () => <>
      <p>Tick the checkboxes if any of these apply to an item.</p>
      <p className="text-info"><strong>Optional</strong></p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: '.tour-item-publish-input',
    content: () => <>
      <p>You have control over whether customers can see each item.</p>
      <p>
        This can be useful if you want to draft an item over time 
        or need to quickly remove an item from the menu before making changes to it later on.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: submitAddItemButton,
    content: ({ goTo, step }) => {
      createFormValidationEventListener(submitAddItemButton, "#add-item-form", step, goTo);
      return <p>Click the 'Submit' button to add your item and close the popup window.</p>;
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: addItemModal,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(submitAddItemButton, step, goTo);

      return <>
        <p>You have not completed the form correctly.</p>  
        <p>Use the error messages to complete the form successfully.</p>
        <p>Click 'Submit' when the error messages have disappeared.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: '.tour-item-list-item:last-of-type',
    content: <p>This is the item you just created.</p>,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    selector: '.tour-item-hamburger-button',
    content: () => <>
      <p>You can click this to show a dropdown to edit, duplicate and delete your item.</p>
      <p>We won't cover the dropdown part again.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'right',
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 4.2: Items" goTo={goTo} />,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 4.3: Sections" goTo={goTo} completed={false} />,
    tocIdentifier: "toc-menu-three",
    tocText: "4.3: Sections",
    venueDependent: true,
    menuDependent: true,
    stepInteraction: false,
    action: () => history.push("/dashboard/menu")
  },
  {
    selector: addSectionButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(addSectionButton, step, goTo);
      return <p>Click this button to add a section.</p>;
    },
    position: 'right',
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "add-section"
  },
  {
    selector: addSectionModal,
    content: () => {
      inputUnfocus('name');

      return <>
        <p>Add a section using this form.</p>
        <p>For example, a 'Drinks' menu might have a section called 'Soft Drinks' or 'Beer'.</p>
        <p className="d-none d-lg-block">For better organisation, you may want to place your 'Coca Cola' item in a specific section instead.</p>
        <p>It depends how you want to organise your menu.</p>
        <p>Click 'Next' to get started.</p>
      </>
    },
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-section-name-input',
    content: () => {
      inputFocus('name');

      return <>
        <p>Enter the name of your section.</p>
        <p className="text-danger"><strong>Required</strong></p>
        <p>For example, 'Soft Drinks'.</p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left'
  },
  {
    selector: '.tour-section-parent-input',
    content: () => <>
      <p>You can change which menu or section you add the section to.</p>
      <p className="d-none d-lg-block">It defaults to the menu or section that you are currently viewing.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-section-publish-input',
    content: () => <>
      <p>You have control over whether customers can see each section (and it's sub-sections and items).</p>
      <p className="d-none d-lg-block">
        This can be useful if you want to draft a section over time 
        or need to quickly remove a section from the menu before making changes to it later on.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: submitAddSectionButton,
    content: ({ goTo, step }) => {
      createFormValidationEventListener(submitAddSectionButton, "#add-section-form", step, goTo);
      return <p>Click the 'Submit' button to add your section and close the popup window.</p>;
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: addSectionModal,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(submitAddSectionButton, step, goTo);

      return <>
        <p>You have not completed the form correctly.</p>  
        <p>Use the error messages to complete the form successfully.</p>
        <p>Click 'Submit' when the error messages have disappeared.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: '.tour-section-list-item:last-of-type',
    content: <p>This is the section you just created.</p>,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    selector: '.tour-section-hamburger-button',
    content: () => <>
      <p>You can click this to show a dropdown to edit, duplicate and delete your section.</p>
      <p>We won't cover the dropdown part again.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'right',
    stepInteraction: false
  },
  {
    selector: '.tour-section-list-item-toggle',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep('.tour-section-list-item-toggle', step, goTo);
      return <p>Click this arrow to expand the section you just created.</p>;
    },
    position: 'left',
    hideNextButton: true,
    largeScreenOnly: true
  },
  {
    selector: '.tour-section-list-item-collapse',
    content: () => <>
      <p>You can add items and sub-sections here in the exact same way.</p>
      <p>We will not cover this again.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false,
    largeScreenOnly: true
  },
  {
    selector: '.tour-section-list-item-link',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep('.tour-section-list-item-link', step, goTo);
      return <p>The section name is blue (rather than black) because you can click it to open it.</p>;
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: '.tour-menu-list-item',
    content: () => <>
      <p>This is the section you just clicked.</p>
      <p className="d-none d-lg-block">
        Clicking into a section can make it easier to view, rather than expanding 
        several layers of sections and sub-sections.
      </p>
      <p>
        You can use the 'Add Item' and 'Add Section' buttons to add items and sections to this section.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  }, 
  {
    selector: '.tour-menu-breadcrumb',
    content: () => <>
      <p>You can see which section or sub-section you are in.</p>
      <p>You can click the blue links to navigate back up the menu.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 4.3: Sections" goTo={goTo} />,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 4: Menu" goTo={goTo} />,
    stepInteraction: false,
    hidePrevButton: true
  }
]