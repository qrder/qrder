import React from 'react';
import { SectionStep } from '/imports/client/components/dashboard/shared/UserTour/SectionStep';
import { StepButton } from '/imports/client/components/dashboard/shared/UserTour/StepButton';

/**
 * Tour steps for QRDER introduction
 * and how to use the tour
 */

export const steps = [
  {
    content: () => (
      <>
        <p>Welcome to the QRDER tour!</p>

        <p>How to use the tour:</p>
        <ul>
          <li>Use the buttons at the bottom to navigate through the tour.</li>
          <li>
            If <StepButton text="Next" /> is not shown, 
            some interaction is required (e.g. clicking a button / ticking a checkbox).
          </li>
          <li>Close the tour at any point using the 'x' in the top right corner.</li>
          <li>
            You can re-open the tour whenever you like using the blue 
            'Start Tour' button at the bottom of the page.
          </li>
        </ul>

        <p className="d-md-none">
          <strong>
            Although the tour works on a mobile device, we strongly 
            recommend using a device with a larger screen.
          </strong>
        </p>
      </>
    ),
    stepInteraction: false
  },
  {
    stepIdentifier: "table-of-contents",
    // content is created in UserTour component
    content: "",
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 1: Introduction" goTo={goTo} completed={false} />,
    tocIdentifier: "toc-intro",
    tocText: "1: Introduction",
    stepInteraction: false
  },
  {
    selector: '[data-tour="side-nav"]',
    mobileSelector: '[data-tour="hamburger-icon"]',
    content: () => <>
      <p>Use this navigation to change page and logout.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    selector: '[data-tour="footer"]',
    content: () => <>
      <p>Open the welcome popup, start the tour or contact us.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    selector: '[data-tour="main-content"]',
    content: () => <>
      <p>The main content for the current page will be displayed here.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 1: Introduction" goTo={goTo} />,
    stepInteraction: false,
    hidePrevButton: true
  }
];