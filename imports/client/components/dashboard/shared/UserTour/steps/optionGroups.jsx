import React from 'react';
import history from '/imports/client/history';
import { createClickEventListenerToChangeStep, createFormValidationEventListener } from "/imports/utils/tour";
import { SectionStep } from '/imports/client/components/dashboard/shared/UserTour/SectionStep';
import { inputFocus, inputUnfocus } from "/imports/utils/shared";

/**
 * Tour steps for Tables introduction
 */

// buttons
const ogButton = "#tour-option-groups-button";
const menuButton = "#tour-menu-button";
const addOgButton = "#tour-add-og-button";
const submitAddOgButton = "#tour-og-submit-button";
const ogModalAddOptionButton = "#tour-og-add-option-button";
const ogModalDeleteOptionButton = ".tour-og-delete-option-button";

const addItemButton = "[data-tour='add-item-button']";
const itemModalAddOptionGroupButton = "#tour-item-add-og-button";
const submitAddItemButton = "#tour-item-submit-button";

// modals
const addOgModal = ".tour-add-og-modal";

const addItemModal = ".tour-add-item-modal";

export const steps = [
  {
    content: ({ goTo }) => <SectionStep section="Part 5: Option Groups" goTo={goTo} completed={false} />,
    tocIdentifier: "toc-og",
    tocText: "5: Option Groups",
    venueDependent: true,
    menuDependent: true,
    stepInteraction: false,
    action: () => history.push("/dashboard/menu"),
  },
  {
    selector: ogButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(ogButton, step, goTo);
      
      return <p>Click this to change page.</p>;
    },
    hideNextButton: true
  },
  {
    selector: '[data-tour="main-content"]',
    content: <>
      <p>You can manage your option groups from this page.</p>
      <p>Click 'Next' to get started.</p>
    </>
  },
  {
    selector: addOgButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(addOgButton, step, goTo);
      
      return <p>Click this button to add a table.</p>
    },
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "add-option-group"
  },
  {
    selector: addOgModal,
    content: () => {
      inputUnfocus('name');

      return <>
        <p>Add an option group using this form.</p>
        <p>Click 'Next' to get started.</p>
      </>
    },
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-og-name-input',
    content: () => {
      inputFocus('name');

      return <>
        <p>Enter the name of your option group.</p>
        <p className="text-danger"><strong>Required</strong></p>
        <p>For example, "Ice & Lemon".</p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left'
  },
  {
    selector: '.tour-og-min-input',
    content: () => <>
      <p>Enter the minimum amount of options a customer can select.</p>
      <p className="text-info"><strong>Optional</strong></p>
      <p>'0' makes it optional.</p>
      <p>'1' makes it required.</p>
      <p className="d-none d-lg-block">Default is 0.</p>
      <p className="d-none d-lg-block">
        For the "Ice & Lemon" example, you would probably set min to 0, and max to 2. 
        They can either choose to have no ice and no lemon, have one of them, or have both.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: '.tour-og-max-input',
    content: () => <>
      <p>Enter the maximum amount of options a customer can select.</p>
      <p className="text-info"><strong>Optional</strong></p>
      <p>Max must be higher than or equal to min.</p>
      <p>Default is 0.</p>
      <p className="d-none d-lg-block">
        For the "Ice & Lemon" example, you would probably set min to 0, and max to 2. 
        They can either choose to have no ice and no lemon, have one of them, or have both.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: '.tour-og-venue-select',
    content: <>
      <p>If you have other venues, you can select them here.</p>
      <p>It defaults to the current venue.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-og-options-input',
    content: () => <>
      <p>Add the available options for this group here.</p>
      <p className="text-danger"><strong>1 Required</strong></p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: ogModalAddOptionButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(ogModalAddOptionButton, step, goTo);
      
      return <>
        <p>Click this button to add another option.</p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: '.tour-og-multiple-options-inputs',
    content: () => <>
      <p>Enter a name and value for each price.  For example, 'Ice' and '0.00', 'Lemon' and '0.00'.</p>
      <p><strong className="text-danger">A name for each option is required.</strong></p>
      <p>The price for each option defaults to '0' or '0.00'.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'right'
  },
  {
    selector: ogModalDeleteOptionButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(ogModalDeleteOptionButton, step, goTo);
      
      return <>
        <p>You can click this button to delete one of the options.  Let's leave 2 options for now.</p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'right',
    stepInteraction: false
  },
  {
    selector: submitAddOgButton,
    content: ({ goTo, step }) => {
      createFormValidationEventListener(submitAddOgButton, "#add-og-form", step, goTo);
      
      return <p>Click the 'Submit' button to add your option group and close the popup window.</p>;
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: addOgModal,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(submitAddOgButton, step, goTo);

      return <>
        <p>You have not completed the form correctly.</p>  
        <p>Use the error messages to complete the form successfully.</p>
        <p>Click 'Submit' when the error messages have disappeared.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: '.tour-og-list-item:last-of-type',
    content: <p>This is the option group you just added.</p>,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    selector: '.tour-og-hamburger-button',
    content: () => <>
      <p>You can click this to show a dropdown to edit, duplicate and delete your option group.</p>
      <p>We won't cover the dropdown part again.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'right',
    stepInteraction: false
  },
  {
    selector: '.tour-og-list-item-toggle',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep('.tour-og-list-item-toggle', step, goTo);
      
      return <p>Click this to arrow to expand the option group you just created.</p>
    },
    position: 'left',
    hideNextButton: true,
    largeScreenOnly: true
  },
  {
    selector: '.tour-og-list-item-collapse',
    content: () => <>
      <p>You can quickly view the options that are in this option group.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false,
    largeScreenOnly: true
  },
  {
    content: () => <>
      <p>We can now assign this option group to an item.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    selector: menuButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(menuButton, step, goTo);
      
      return <p>Click this to change page.</p>
    },
    hideNextButton: true
  },
  {
    selector: addItemButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(addItemButton, step, goTo);
      
      return <p>Click this button to add another item.</p>
    },
    position: 'right',
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "add-item"
  },
  {
    selector: addItemModal,
    content: () => {
      inputUnfocus('name');

      return <>
        <p>Let's add another item using this form.</p>
        <p>
          The only fields that are <strong className="text-danger">required </strong> 
          are <i>Name</i> and <i>Price</i>.
        </p>
        <p>We will only fill in these fields and the option group section.</p>
        <p>Click 'Next' to get started.</p>
      </>
    },
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-item-name-input',
    content: () => {
      inputFocus('name');

      return <>
        <p>Enter the name of your item.</p>
        <p className="text-danger"><strong>Required</strong></p>
        <p>For example, 'Fanta'.</p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left'
  },
  {
    selector: '.tour-item-price-input',
    content: () => <>
      <p>Enter a single price for your item.  We will not cover the extra price options again.</p>
      <p className="text-danger"><strong>Required</strong></p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: '.tour-item-options-input',
    content: () => <>
      <p>We briefly covered this part of the form earlier.</p>
      <p>Now that we have created an option group, we can add it to the item.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: itemModalAddOptionGroupButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(itemModalAddOptionGroupButton, step, goTo);
      
      return <p>Click this button to add an option group.</p>
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: '.tour-item-option-groups-inputs',
    content: () => <>
      <p>Use the dropdown to select the "Ice & Lemon" option group that you created earlier.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'right'
  },
  {
    selector: submitAddItemButton,
    content: ({ goTo, step }) => {
      createFormValidationEventListener(submitAddItemButton, "#add-item-form", step, goTo);
      
      return <p>Click the 'Submit' button to add your second item and close the popup window.</p>
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: addItemModal,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(submitAddItemButton, step, goTo);

      return <>
        <p>You have not completed the form correctly.</p>  
        <p>Use the error messages to complete the form successfully.</p>
        <p>Click 'Submit' when the error messages have disappeared.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: '.tour-item-list-item:last-of-type',
    content: <p>This is the item you just created.</p>,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 5: Option Groups" goTo={goTo} />,
    stepInteraction: false,
    hidePrevButton: true
  }
]
