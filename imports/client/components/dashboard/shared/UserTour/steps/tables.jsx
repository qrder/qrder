import React from 'react';
import { 
  createClickEventListenerToChangeStep, 
  createCheckboxListenerToChangeStep, 
  createFormValidationEventListener
} from "/imports/utils/tour";
import { inputFocus, inputUnfocus } from "/imports/utils/shared";
import { SectionStep } from '/imports/client/components/dashboard/shared/UserTour/SectionStep';

/**
 * Tour steps for Tables introduction
 */

// buttons
const addTableButton = "#tour-add-table-button";
const submitAddTableButton = "#tour-table-submit-button";
const generateQrCodeButton = "#tour-generate-qr-button";
const printQrCodesButton = "#tour-print-qr-button";

// modals
const addTableModal = ".tour-add-table-modal";
const generateQrModal = ".tour-generate-qr-modal";

// side nav
const sideNavTables = "#tour-side-nav-tables";

export const steps = [
  {
    content: ({ goTo }) => <SectionStep section="Part 3: Tables" goTo={goTo} completed={false} />,
    tocIdentifier: "toc-tables",
    tocText: "3: Tables",
    venueDependent: true,
    stepInteraction: false
  },
  {
    selector: '[data-tour="hamburger-icon"]',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(".hamburger-icon", step, goTo);
      
      return <p>Click this button to open the mobile navigation.</p>
    },
    mobileOnly: true
  },
  {
    selector: sideNavTables,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(sideNavTables, step, goTo);
      
      return <p>Click 'Tables' to change page.</p>
    },
    hideNextButton: true
  },
  {
    selector: '[data-tour="main-content"]',
    content: <p>You can add your tables and print QR codes on this page.</p>
  },
  {
    selector: addTableButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(addTableButton, step, goTo);
      
      return <p>Click this button to add a table.</p>
    },
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "add-table"
  },
  {
    selector: addTableModal,
    content: () => {
      inputUnfocus('name');

      return <>
        <p>Add a table using this form.</p>
        <p>Click 'Next' to get started.</p>
      </>
    },
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-table-name-input',
    content: () => {
      inputFocus('name');

      return <>
        <p>Enter the name or number of your table.</p>
        <p className="text-danger"><strong>Required</strong></p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left'
  },
  {
    selector: '.tour-table-venue-select',
    content: <>
      <p>If you have other venues, you can select them here.</p>
      <p>It defaults to the current venue.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: submitAddTableButton,
    content: ({ goTo, step }) => {
      createFormValidationEventListener(submitAddTableButton, "#add-table-form", step, goTo);
      
      return <p>Click the 'Submit' button to add your table and close the popup window.</p>
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: addTableModal,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(submitAddTableButton, step, goTo);

      return <>
        <p>You have not completed the form correctly.</p>  
        <p>Use the error messages to complete the form successfully.</p>
        <p>Click 'Submit' when the error messages have disappeared.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: '.tour-table-list-item:last-of-type',
    content: <p>This is the table you just added.</p>,
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    selector: '.tour-table-hamburger-button',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep('.tour-table-hamburger-button', step, goTo);
      
      return <p>Click this to show a dropdown to edit and delete your table.</p>
    },
    position: 'right',
    hideNextButton: true
  },
  {
    selector: '.tour-table-hamburger-dropdown',
    content: () => <>
      <p>You can click either of these to edit or delete your table.</p>
      <p>
        When clicked, a popup will appear. The popups work exactly the 
        same as adding a table, so we won't cover these again in the tour.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    selector: '.tour-table-checkbox',
    content: ({ goTo, step }) => {
      createCheckboxListenerToChangeStep('.tour-table-checkbox', step, goTo);
      
      return <>
        <p>Tick this checkbox.</p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: generateQrCodeButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(generateQrCodeButton, step, goTo);
      
      return <p>Click the 'Generate QR Codes' button to create printable QR codes.</p>
    },
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "generate-qr"
  },
  {
    selector: generateQrModal,
    content: () => <>
      <p>These are the QR codes for the table(s) that you just ticked in the list.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    selector: printQrCodesButton,
    content: <>
      <p>You can click this button to print your QR codes.  We will not do this in the tour.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left',
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 3: Tables" goTo={goTo} />,
    forceModalClosed: true,
    modalToClose: "generate-qr",
    stepInteraction: false,
    hidePrevButton: true
  }
]