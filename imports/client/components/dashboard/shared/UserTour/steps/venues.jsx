import React from 'react';
import history from '/imports/client/history';
import { createClickEventListenerToChangeStep, createFormValidationEventListener } from "/imports/utils/tour";
import { SectionStep } from '/imports/client/components/dashboard/shared/UserTour/SectionStep';
import { inputFocus, inputUnfocus } from "/imports/utils/shared";

/**
 * Tour steps for Venue introduction
 */

// buttons
const addVenueButton = "#tour-add-venue-button";
const submitAddVenueButton = "#tour-venue-submit-button";

// modals
const addVenueModal = ".tour-add-venue-modal";

export const steps = [
  {
    content: ({ goTo }) => <SectionStep section="Part 2: Venues" goTo={goTo} completed={false} />,
    action: () => history.push("/dashboard/venues"),
    tocIdentifier: "toc-venues",
    tocText: "2: Venues",
    stepInteraction: false
  },
  {
    selector: addVenueButton,
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep(addVenueButton, step, goTo);

      return <p>Click this button to add your venue.</p>
    },
    hideNextButton: true,
    checkModalClosed: true,
    modalToClose: "add-venue"
  },
  {
    selector: addVenueModal,
    content: () => {
      inputUnfocus('name');

      return <>
        <p>Add your venue using this form.</p>
        <p>Click 'Next' to get started.</p>
      </>
    },
    position: 'left',
    stepInteraction: false
  },
  {
    selector: '.tour-venue-name-input',
    content: () => {
      inputFocus('name');

      return <>
        <p>Enter the name of your venue.</p>
        <p className="text-danger"><strong>Required</strong></p>
        <p>Click 'Next' when you're ready.</p>
      </>
    },
    position: 'left'
  },
  {
    selector: '.tour-venue-payment-options-input',
    content: () => <>
      <p>Select the payment options that your venue can accept.</p>
      <p className="text-danger"><strong>1 Required</strong></p>
      <p className="d-none d-lg-block">
        Accepting online payments becomes available when you link your Stripe account.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: '.tour-venue-accepting-orders-input',
    content: () => <>
      <p>Control if customers can view the menu and submit orders.</p>
      <p>This can be useful if you'd like to temporarily stop QR code ordering, but would like to leave your QR codes on the tables.</p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    position: 'left'
  },
  {
    selector: submitAddVenueButton,
    content: ({ goTo, step }) => {
      createFormValidationEventListener(submitAddVenueButton, "#add-venue-form", step, goTo);
      
      return <p>Click the 'Submit' button to add your venue and close the popup window.</p>
    },
    position: 'left',
    hideNextButton: true
  },
  {
    selector: addVenueModal,
    content: ({ goTo, step }) => {
      setTimeout(() => createClickEventListenerToChangeStep(submitAddVenueButton, step, goTo), 2000);

      return <>
        <p>You have not completed the form correctly.</p>  
        <p>Use the error messages to complete the form successfully.</p>
        <p>Click 'Submit' when the error messages have disappeared.</p>
      </>
    },
    hideNextButton: true
  },
  {
    selector: '.tour-venue-list-item',
    content: "This is the venue you just added.",
    stepInteraction: false,
    hidePrevButton: true
  },
  {
    selector: '.tour-venue-hamburger-button',
    content: ({ goTo, step }) => {
      createClickEventListenerToChangeStep('.tour-venue-hamburger-button', step, goTo);
      
      return <p>Click this to show a dropdown to edit and delete your venue.</p>
    },
    position: 'right',
    hideNextButton: true
  },
  {
    selector: '.tour-venue-hamburger-dropdown',
    content: () => <>
      <p>You can click either of these to edit or delete your venue.</p>
      <p>
        When clicked, a popup will appear. The popups work exactly the 
        same as adding a venue, so we won't cover these in the tour.
      </p>
      <p>Click 'Next' when you're ready.</p>
    </>,
    stepInteraction: false
  },
  {
    content: ({ goTo }) => <SectionStep section="Part 2: Venues" goTo={goTo} />,
    stepInteraction: false,
    hidePrevButton: true
  }
]