import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';

export const SectionStep = ({ section, goTo, completed = true }) => {
  const icon = completed ? faCheckCircle : faInfoCircle;
  const variant = completed ? "text-success" : "text-info"

  return <>
    <div>
      <FontAwesomeIcon 
        icon={icon} 
        className={`${variant} mr-3`}
        size="lg" 
      /> 
      {section}
    </div>
    <div className="mt-3">
      <a href="#" onClick={() => goTo(1)}>Return to Table of Contents</a>
    </div>
  </>
}
