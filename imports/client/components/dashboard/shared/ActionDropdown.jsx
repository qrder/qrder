import React, { useContext } from 'react';
import { Dropdown } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

// The forwardRef is important!!
// Dropdown needs access to the DOM node in order to position the Menu
const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a href="" ref={ref} onClick={(e) => {e.preventDefault(); onClick(e);}}>
    {children}
  </a>
));

export const ActionDropdown = ({ options, parent, data, className, dropdownClassName }) => {
  const { openModal } = useContext(GlobalContext);

  return (
    <Dropdown className={className}>
      <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
        <FontAwesomeIcon icon={faBars} size="lg" style={{ color: '#DBDBDB' }} />
      </Dropdown.Toggle>

      <Dropdown.Menu align="right" className={dropdownClassName}>
        {
          options.map((opt, idx) => {
            return (
              <Dropdown.Item 
                eventKey={idx} 
                key={idx} 
                // opt-parent - e.g. edit-item or delete-section (the names of the modals in context)
                onClick={() => openModal(`${opt}-${parent}`, data)}
                className={`text-capitalize tour-item-hamburger-${opt}-button`}
              >
                {opt}
              </Dropdown.Item>
            )
          })
        }
      </Dropdown.Menu>
    </Dropdown>
  );
};
