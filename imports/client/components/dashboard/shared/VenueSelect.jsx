import React, { useState, useEffect } from 'react';
import { DropdownButton, Dropdown, ButtonGroup } from 'react-bootstrap';

export const VenueSelect = ({ venues, onVenueChange, className }) => {

  const [currentVenue, setCurrentVenue] = useState('');

  useEffect(() => {
    setCurrentVenue(venues.length > 0 ? venues[0].name : '');
  }, [venues]);

  // Handle changing venue
  const onVenueChangeHandler = venue => {
    setCurrentVenue(venue.name);
    onVenueChange(venue);
  };

  return (
    <span className={`${className}`}>
      <DropdownButton 
        id="dropdown-basic-button" 
        title="Select Venue"
        as={ButtonGroup}
        variant="warning"
        className="mr-3"
      >
        {
          venues.map(venue => {
            return <Dropdown.Item onClick={() => onVenueChangeHandler(venue)} key={venue._id}>{venue.name}</Dropdown.Item>
          })
        }
      </DropdownButton>

      <span>{currentVenue}</span>
    </span>
  );
};
