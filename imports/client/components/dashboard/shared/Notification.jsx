import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faTimesCircle, faExclamationCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';

export const Notification = ({ message, variant }) => {

  // Return correct icon depending on variant
  const getIcon = () => {
    switch(variant) {
      case "success":
        return faCheckCircle;
      case "error":
        return faTimesCircle;
      case "warning":
        return faExclamationCircle;
      default:
        return faInfoCircle;
    }
  }

  return (
    <Container>
      <Row>
        <Col xs={1} className="my-auto d-flex justify-content-center">
          <FontAwesomeIcon icon={getIcon()} />
        </Col>
        <Col className="p-0">
          <span>{message}</span>
        </Col>
      </Row>
    </Container>
  );
};