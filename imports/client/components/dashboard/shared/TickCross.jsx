import React, { useState, useEffect } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

export const TickCross = ({ value, text = '', showTooltip = true, tooltipTextOptions, className }) => {
  const [icon, setIcon] = useState(null);

  useEffect(() => {
    if(value) {
      setIcon(<FontAwesomeIcon icon={faCheckCircle} className="text-success" />);
    } else {
      setIcon(<FontAwesomeIcon icon={faTimesCircle} className="text-danger" />);
    }
  }, [value]);

  if(!icon) return '';

  return (
    <div className={`${className}`}>
      {
        showTooltip ? (
          <OverlayTrigger overlay={<Tooltip>{value ? tooltipTextOptions[0] : tooltipTextOptions[1]}</Tooltip>}>
            {icon}
          </OverlayTrigger>
        ) : icon
      }
      <span className="ml-2">{text}</span>
    </div>
  );
};

