import React, { useState, useEffect, useContext } from 'react';
import { useAccordionToggle, AccordionContext } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight, faAngleDown } from '@fortawesome/free-solid-svg-icons';

export const AccordionToggle = ({ eventKey, className, callback }) => {
  const [icon, setIcon] = useState(faAngleRight);

  const currentEventKey = useContext(AccordionContext);

  const decoratedOnClick = useAccordionToggle(
    eventKey,
    () => callback && callback(eventKey),
  );

  const isCurrentEventKey = currentEventKey === eventKey;

  useEffect(() => {
    isCurrentEventKey ? setIcon(faAngleDown) : setIcon(faAngleRight)
  });

  return (
    <FontAwesomeIcon 
      icon={icon} 
      className={`mr-2 ${className}`}
      role="button"
      style={{ color: '#C6C6C6' }} 
      onClick={decoratedOnClick} 
    />
  )
}