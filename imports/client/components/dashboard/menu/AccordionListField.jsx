import classnames from 'classnames';
import { Accordion, Card } from 'react-bootstrap';
import React, { Children, cloneElement, isValidElement } from 'react';
import { connectField, filterDOMProps } from 'uniforms';
import { ListAddField } from 'uniforms-bootstrap4';
import AccordionListItemField from '/imports/client/components/dashboard/menu/AccordionListItemField';

/**
 * This is a custom uniforms field based on the ListField component.
 * I wanted to format the list field into an Accordion, as the default ListField
 * took up too much space (especially for the nested CustomOptionsGroups field).
 * 
 * Refer to these if needed:
 * - https://github.com/vazco/uniforms/issues/829
 * - https://github.com/vazco/uniforms/blob/master/packages/uniforms-bootstrap4/src/ListField.tsx
 * - https://uniforms.tools/docs/tutorials-creating-custom-field/
 */

const AccordionList = ({
  addIcon,
  children = <AccordionListItemField name="$" />,
  className,
  error,
  errorMessage,
  initialCount,
  itemProps,
  label,
  removeIcon,
  showInlineError,
  value,
  ...props
}) => {
  
  return (
    <Card className={classnames('mb-3', className)} {...filterDOMProps(props)}>
      <Card.Body>
          
        {
          label && (
            <Card.Title className="mb-4">
              <h6 className="font-weight-normal d-inline">{label}</h6>
              
              <ListAddField addIcon={addIcon} initialCount={initialCount} name="$" id="tour-item-add-cog-button" />

              {!!(error && showInlineError) && (
                <span className="text-danger">{errorMessage}</span>
              )}
            </Card.Title>
          )
        }

        <Accordion className="tour-item-cog-list">
          {
            value?.map((item, itemIndex) =>
              Children.map(children, (child, childIndex) =>
                isValidElement(child)
                  ? cloneElement(child, {
                      key: `${itemIndex}-${childIndex}`,
                      name: child.props.name?.replace('$', '' + itemIndex),
                      ...itemProps,
                      removeIcon,
                    })
                  : child
              )
            )
          }
        </Accordion> 

      </Card.Body>
    </Card>
  );
}

export default connectField(AccordionList);