import React from 'react';
import { Button } from 'react-bootstrap';

// TODO: move to shared folder

export const Add = ({ text, className }) => {
  return (
    <Button size="sm" variant="success" className={className}>
      + Add <span className="d-none d-md-inline">{text}</span>
    </Button>
  );
};
