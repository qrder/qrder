import React, { useContext } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { ItemListItem } from "/imports/client/components/dashboard/menu/ItemListItem";

export const ItemList = ({ columns, parent }) => {
  const { openModal } = useContext(GlobalContext);

  if(parent == undefined || parent.items == undefined) return '';

  // !parent.parentId means this is an item list for a menu, not a sub-section.
  // We only want to add this data-tour value to the highest level add item button
  const getDataTour = () => !parent.parentId ? 'add-item-button' : '';

  if(parent.items.length == 0) {
    return (
      <Button 
        size="sm" 
        variant="primary" 
        onClick={() => openModal('add-item', parent?._id)} 
        className="d-block mt-5"
        data-tour={getDataTour()}
      >
        + Add Item
      </Button>
    );
  };

  return (
    <Container>
      <Row>
        <Col xl={columns} className="p-0">
          <hr/>

          {
            parent.items.map(item => {
              return <ItemListItem key={item._id} item={item} />
            })
          }

          <Button 
            size="sm" 
            variant="primary" 
            onClick={() => openModal('add-item', parent?._id)}
            className="mt-3" 
            data-tour={getDataTour()}
          >
            + Add Item
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

