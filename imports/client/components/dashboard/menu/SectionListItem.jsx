import React from 'react';
import { Container, Row, Col, Accordion, Card, Form } from 'react-bootstrap';
import { SectionList } from '/imports/client/components/dashboard/menu/SectionList';
import { ItemList } from '/imports/client/components/dashboard/menu/ItemList';
import { AccordionToggle } from '/imports/client/components/dashboard/shared/AccordionToggle';
import { ActionDropdown } from '/imports/client/components/dashboard/shared/ActionDropdown';
import { TickCross } from '/imports/client/components/dashboard/shared/TickCross';

export const SectionListItem = ({ section, width, onSectionClick }) => {

  const handleSectionClick = (e) => {
    e.preventDefault();
    onSectionClick(section._id);
  }

  return (
    <Card key={section._id} className={`${width} border-0 tour-section-list-item`} style={{ overflow: 'inherit' }}>

      <Card.Header className="p-0 border-0 bg-transparent">
        <Container>
          <Row>
            <Col xs={1} className="d-flex align-items-center justify-content-center p-0">
              <Form.Check className="my-auto" />
            </Col>
            <Col xs={5} className="p-0">
              <AccordionToggle eventKey={section._id} className="tour-section-list-item-toggle d-none d-md-inline" />
              <a href="" onClick={(e) => handleSectionClick(e)} className="tour-section-list-item-link">
                {section.name}
              </a>
            </Col>
            <Col xs={1} className="d-flex align-items-center justify-content-center p-0">
              <TickCross value={section.publish} tooltipTextOptions={["Published", "Not Published"]} />
            </Col>
            <Col xs={5} className="d-flex align-items-center justify-content-end p-0 pr-3">
              <ActionDropdown 
                data={section}
                parent="section" 
                options={["edit", "duplicate", "delete"]} 
                className="tour-section-hamburger-button"
              />
            </Col>
          </Row>
        </Container>
      </Card.Header>

      <Accordion.Collapse eventKey={section._id} className="tour-section-list-item-collapse">
        <Card.Body className="p-0 pl-5">

          <ItemList columns={12} items={section.items} parent={section} />
          
          <SectionList columns={12} sections={section.sections} parent={section} />
          
        </Card.Body>
      </Accordion.Collapse>

      <hr/>
    </Card>
  );
};
