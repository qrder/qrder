import React from 'react';
import { Row, Container, Col } from 'react-bootstrap';
import { Add } from "/imports/client/components/dashboard/menu/Add";
import { Delete } from "/imports/client/components/dashboard/menu/Delete";
import { TextField, NumField, ListField, ListItemField } from 'uniforms-bootstrap4';

export const Options = () => {
  
  return (
    <ListField
      name="options"
      addIcon={<Add text="Option" className="tour-item-add-cog-option-button" />}
      removeIcon={<Delete />}
      initialCount={1}
      showInlineError
      className="border-0 tour-item-collapsed-cog-options"
    >
      <ListItemField name="$" className="p-0">
        <TextField name="name" label={false} placeholder="Name" className="col-5 offset-1" />
        <NumField name="value" label={false} placeholder="0.00" className="col-4" />
      </ListItemField>
    </ListField>
  );
};
