import React, { useContext } from 'react';
import { GlobalContext } from '/imports/context/GlobalState';
import { Container, Row, Col, Accordion, Button } from 'react-bootstrap';
import { SectionListItem } from "/imports/client/components/dashboard/menu/SectionListItem";

export const SectionList = ({ columns, parent, onSectionClick }) => {
  const { openModal } = useContext(GlobalContext);

  if(parent == undefined || parent.sections == undefined) return '';

  // !parent.parentId means this is a section list for a menu, not a sub-section.
  // We only want to add this data-tour value to the highest level add section button
  const getDataTour = () => !parent.parentId ? 'add-section-button' : '';

  if(parent.sections.length == 0) {
    return (
      <Button 
        size="sm" 
        variant="primary" 
        onClick={() => openModal('add-section', parent?._id)} 
        className="d-block mt-5"
        data-tour={getDataTour()}
      >
        + Add Section
      </Button>
    )
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col xl={columns} className="p-0">
          <hr/>   

          <Accordion className="mt-3">
            {
              parent.sections.map(section => {
                return <SectionListItem key={section._id} section={section} onSectionClick={(section) => onSectionClick(section)} />
              })
            }
          </Accordion>

          <Button 
            size="sm" 
            variant="primary" 
            onClick={() => openModal('add-section', parent?._id)} 
            className="mt-3"
            data-tour={getDataTour()}
          >
            + Add Section
          </Button>
        </Col>
      </Row>
    </Container>
  );
};
