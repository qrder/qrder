import React from 'react';
import { Accordion, Card, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

export const PublishIcon = ({ published, margin = 'mr-3' }) => {

  return (
    <>
      {
        published ? (
          <FontAwesomeIcon icon={faCheckCircle} className={`text-success ${margin}`} size="lg" />
        ) : (
          <FontAwesomeIcon icon={faTimesCircle} className={`text-danger ${margin}`} size="lg" />
        )
      }
    </>
  );
};
