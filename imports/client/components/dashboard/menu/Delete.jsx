import React from 'react';
import { Button } from 'react-bootstrap';

// TODO: move to shared folder

export const Delete = ({ text }) => {
  return (
    <Button size="sm" variant="danger">
      - <span className="d-none d-md-inline">Del {text}</span>
    </Button>
  );
};
