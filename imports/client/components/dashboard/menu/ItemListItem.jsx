import React from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';
import { ActionDropdown } from '/imports/client/components/dashboard/shared/ActionDropdown';
import { TickCross } from '/imports/client/components/dashboard/shared/TickCross';

export const ItemListItem = ({ item, width }) => {

  return (
    <>
      <Container className={`ml-0 tour-item-list-item ${width}`}>
        <Row>
          <Col xs={1} className="d-flex align-items-center justify-content-center p-0">
            {/* TODO: Could our Checkbox comp be used? */}
            <Form.Check className="my-auto" />
          </Col>
          <Col xs={5} className={`p-0 ${!item.description && 'd-flex align-items-center'}`}>
            <div>{item.name}</div>
            <div className="small" style={{ color: '#A4A4A4' }}>{item.description}</div>
          </Col>
          <Col md={1} className="d-none d-md-flex align-items-center justify-content-center p-0">
            <TickCross value={item.publish} tooltipTextOptions={["Published", "Not Published"]} />
          </Col>
          <Col md={2} className="d-none d-md-flex align-items-center justify-content-center p-0">
            {item.vegetarian ? 'V ' : ''}
            {item.vegan ? 'Ve ' : ''}
            {item.glutenFree ? 'Gf ' : ''}
            {item.containsAlcohol ? 'Alc ' : ''}
            {item.containsFish ? 'F ' : ''}
          </Col>
          <Col xs={6} md={3} className="d-flex align-items-center justify-content-end p-0 pr-3">
            <span className="bg-primary text-white py-1 px-2 mr-5">
              {`£${item.prices[0].value.toFixed(2)}`}
            </span>
            <ActionDropdown 
              data={item} 
              parent="item" 
              options={["edit", "duplicate", "delete"]} 
              className="tour-item-hamburger-button"
              dropdownClassName="tour-item-hamburger-dropdown"
            />
          </Col>
        </Row>
      </Container>
      <hr/>
    </>
  );
};
