import React from 'react';
import { connectField } from 'uniforms';
import { Row, Col } from 'react-bootstrap';
import { ListDelField, TextField, NumField, ErrorField } from 'uniforms-bootstrap4';

const PriceListItem = ({ removeIcon, onlyOnePrice }) => {

  if(onlyOnePrice) {
    return (
      <Row>
        <Col xs={{ span: 10, offset: 1 }} lg={{ span: 5, offset: 1 }} className="d-inline-flex">
          <NumField decimal name="value" label={false} placeholder="0.00" className="mb-0" />
        </Col>
        <ErrorField name="value" className="custom-error d-inline-flex text-danger border-0 font-weight-light" />
      </Row>
    )
  }

  return (
    <Row className="m-0">
      <Col xs={2} lg={1} className="p-0">
        <ListDelField removeIcon={removeIcon} className="tour-item-delete-price-button" />
      </Col>
      <Col xs={6} lg={4} className="py-0 px-2">
        <TextField name="name" label={false} placeholder="Name" showInlineError />
      </Col>
      <Col xs={4} lg={3} className="py-0 px-2">
        <NumField decimal name="value" label={false} placeholder="0.00" showInlineError />
      </Col>
    </Row>
  );
}

export default connectField(PriceListItem, { initialValue: false });