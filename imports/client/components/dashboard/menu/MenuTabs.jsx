import React, { useState } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import { MenuTab } from '/imports/client/components/dashboard/menu/MenuTab';

export const MenuTabs = ({ loading, menus }) => {
  const [key, setKey] = useState(menus[0]?._id);

  if(loading) return 'Loading...';

  if(menus === undefined || menus.length === 0) {
    return <div className="mt-md-3">You have not created any menus yet.  Click 'Add Menu' above to get started.</div>
  }

  // TODO: Convert these tabs to a shared component that can be used by this, and settings / customer pages as well

  return ( 
    <Tabs id="menu" activeKey={key} onSelect={(k) => setKey(k)} className="mt-0 mt-md-3 flex-nowrap qrder-tabs">
      {
        menus.map(menu => {
          return (
            <Tab 
              key={menu._id} 
              eventKey={menu._id} 
              title={menu.name} 
              className="my-3 tour-menu-list-item"
              tabClassName="text-nowrap border-top-0 border-left-0 border-right-0"
            >
              <MenuTab menu={menu} />
            </Tab>
          )
        })
      }
    </Tabs>
  );
};
