import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';

export const Breadcrumb = ({ path, onLinkClick }) => {

  if(path == undefined || path.length == 0) return '';

  const length = path.length;

  const paddingLeft = path.length > 1 ? "pl-2" : "pl-4";

  const handleLinkClick = (e, data) => {
    e.preventDefault();
    onLinkClick(data);
  }

  return (
    <div className="mt-4 mb-3">
      <span 
        className={`${paddingLeft} pr-4 py-2 tour-menu-breadcrumb`}
        style={{ backgroundColor: "#EDEDED", fontSize: '10pt', borderRadius: '0px 10px 10px 0px' }}
      >
        {
          // show back button in breadcrumb when there are >1 breadcrumb items
          path.length > 1 && <>
            <span>
              <FontAwesomeIcon icon={faChevronLeft} className="mx-2" size="xs" style={{ color: '#C6C6C6' }} />
              <a href="" onClick={(e) => handleLinkClick(e, 'back')}>Back</a> 
            </span>
            <span className="mx-3" style={{ color: '#C6C6C6' }}>|</span>
          </>
        }
        { 
          // iterate breadcrumb array (path) and print the items
          path.map((p, idx) => {
            if(idx == 0) {
              // first and/or only one
              if(length == 1) return <strong key={p.id}>{p.name}</strong>;
              return <a href="" key={p.id} onClick={(e) => handleLinkClick(e, p.id)}>{p.name}</a>;
            } else {
              // rest
              return (
                <span key={p.id}>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-2" size="xs" style={{ color: '#C6C6C6' }} />
                  {
                    length === idx + 1 ? (
                      <strong>{p.name}</strong>
                    ) : (
                      <a href="" key={p.id} onClick={(e) => handleLinkClick(e, p.id)}>{p.name}</a>
                    )
                  }
                </span>
              );
            }
          })
        }
      </span>
    </div>
  );
};

