import React, { useState, useEffect } from 'react';
import { connectField } from 'uniforms';
import { Row, Col } from 'react-bootstrap';
import { ListDelField, SelectField } from 'uniforms-bootstrap4';

const OptionGroupsListItem = ({ removeIcon }) => {
  const [selectOptions, setSelectOptions] = useState();

  {/* TODO: Add ability to override max and min */}
  // Create select options for option groups
  useEffect(() => {
    Meteor.call("optionGroups.createSelectOptions", (error, result) => {
      // TODO: Handle error better
      error ? console.log(error) : setSelectOptions(result);
    });
  }, []);

  return (
    <Row className="m-0">
      <Col xs={2} lg={1} className="p-0">
        <ListDelField removeIcon={removeIcon} className="tour-og-delete-option-button" />
      </Col>
      <Col xs={10} lg={6} className="py-0 px-2">
        <SelectField
          options={selectOptions}
          placeholder="Select Option Group"
          showInlineError
          label={false}
        />
      </Col>
    </Row>
  );
}

export default connectField(OptionGroupsListItem, { initialValue: false });