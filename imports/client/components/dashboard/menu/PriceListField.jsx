import classnames from 'classnames';
import { Card, Container } from 'react-bootstrap';
import React, { Children, cloneElement, isValidElement } from 'react';
import { connectField, filterDOMProps } from 'uniforms';
import { ListAddField } from 'uniforms-bootstrap4';
import PriceListItemField from '/imports/client/components/dashboard/menu/PriceListItemField';

/**
 * This is a custom uniforms field based on the ListField component.
 * I needed specific functionality for the price(s): 
 * - Default to just a price value
 * - If they add more than 1, show the name fields as well.
 * 
 * IMPORTANT: Take note of the two different returns: 
 * - One to handle initial state of 1 price.
 * - One to handle multiple prices
 * 
 * Refer to these if needed:
 * - https://github.com/vazco/uniforms/blob/master/packages/uniforms-bootstrap4/src/ListField.tsx
 * - https://uniforms.tools/docs/tutorials-creating-custom-field/
 */

const PriceList = ({
  addIcon,
  children = <PriceListItemField name="$" />,
  className,
  error,
  errorMessage,
  initialCount,
  itemProps,
  label,
  removeIcon,
  showInlineError,
  value,
  ...props
}) => {

  if(value.length == 1) {
    return (
      <Card className={classnames('mb-3', className)} {...filterDOMProps(props)}>
        <Card.Body className="py-3">

        {
          label && (
            <Card.Title className="mb-0 d-flex">
              <h6 className="font-weight-normal align-self-center mb-0 d-inline">{label}</h6>
              
              <Container className="d-inline">
              {
                value?.map((item, itemIndex) =>
                  Children.map(children, (child, childIndex) =>
                    isValidElement(child)
                      ? cloneElement(child, {
                          key: `${itemIndex}-${childIndex}`,
                          name: child.props.name?.replace('$', '' + itemIndex),
                          ...itemProps,
                          removeIcon,
                          onlyOnePrice: value.length == 1 ? true : false
                        })
                      : child
                  )
                )
              }
              </Container>

              <ListAddField addIcon={addIcon} initialCount={initialCount} name="$" id="tour-item-add-price-button" />

              { !!(error && showInlineError) && <span className="text-danger">{errorMessage}</span> }
            </Card.Title>
          )
        }
        
      </Card.Body>
    </Card>
    )
  }
  
  return (
    <Card className={classnames('mb-3', className)} {...filterDOMProps(props)}>
      <Card.Body className="py-3">

        {
          label && (
            <Card.Title className="mb-4">
              <h6 className="font-weight-normal d-inline">{label}</h6>
              
              <ListAddField addIcon={addIcon} initialCount={initialCount} name="$" />

              {!!(error && showInlineError) && (
                <span className="text-danger">{errorMessage}</span>
              )}
            </Card.Title>
          )
        }

        <Container className="p-0 tour-item-multiple-prices-inputs">
        {
          value?.map((item, itemIndex) =>
            Children.map(children, (child, childIndex) =>
              isValidElement(child)
                ? cloneElement(child, {
                    key: `${itemIndex}-${childIndex}`,
                    name: child.props.name?.replace('$', '' + itemIndex),
                    ...itemProps,
                    removeIcon,
                    onlyOnePrice: value.length == 1 ? true : false
                  })
                : child
            )
          )
        }
        </Container>

      </Card.Body>
    </Card>
  );
}

export default connectField(PriceList);