import React, { useEffect, useState } from 'react';
import { ItemList } from '/imports/client/components/dashboard/menu/ItemList';
import { SectionList } from '/imports/client/components/dashboard/menu/SectionList';
import { Breadcrumb } from '/imports/client/components/dashboard/menu/Breadcrumb';
import { ActionDropdown } from '/imports/client/components/dashboard/shared/ActionDropdown';
import { TickCross } from '/imports/client/components/dashboard/shared/TickCross';

export const MenuTab = ({ menu }) => {
  const [breadcrumb, setBreadcrumb] = useState();
  // current parent refers to which menu/section is being shown
  const [currentParent, setCurrentParent] = useState();
  // TODO: This could be refactored to use useRef / usePrevious?
  const [prevParents, setPrevParents] = useState([]);

  useEffect(() => {
    // initial state or, if breadcrumb length is 0 or 1, then we're in root of menu
    if(breadcrumb == undefined || breadcrumb?.length == 0 || breadcrumb?.length == 1) {
      // set first section of breadcrumb
      setBreadcrumb([{id: menu?._id, name: menu?.name}]);
      setCurrentParent(menu);
    }
    
    // it's a section / sub-section. Start with menu's sections (so level 1 rather than 0)
    if(breadcrumb?.length > 1) findCurrentSection(menu.sections, 1);
  }, [menu]);

  // Recursive function to find the lowest level section in the current breadcrumb
  const findCurrentSection = (sections, level) => {
    sections.map(section => {
      // found the section for this level
      if(section._id == breadcrumb[level]?.id) {
        // end of breadcrumb so this is the current content / page to rerender 
        if(++level == breadcrumb.length) {
          setCurrentParent(section);
        } else { // carry on deeper
          findCurrentSection(section.sections, level++);
        }
      }
    })
  }

  // change items and sections to render
  const updateMenuContent = (arrayToSearch, selectedContentId) => {
    const newParent = arrayToSearch.find(el => el._id == selectedContentId);
    setCurrentParent(newParent);
    return newParent.name;
  }

  // rerender content & update breadcrumb 
  const handleSectionSelection = (section) => {
    setPrevParents([ ...prevParents, currentParent ]);
    const sectionName = updateMenuContent(currentParent.sections, section);
    setBreadcrumb([ ...breadcrumb, {id: section, name: sectionName} ]);
  }

  // handle breadcrumb link selection (breadcrumb item or back) - rerender menu content
  const handleBreadcumbSelection = (selectedContentId) => {
    // selectedContentId is either the menu/section id or 'back' 
    selectedContentId = selectedContentId == 'back' ? breadcrumb[breadcrumb.length - 2].id : selectedContentId;
    updateMenuContent(prevParents, selectedContentId);
    // Remove items after the selected one in breadcrumb
    const bIndex = breadcrumb.findIndex(b => b.id == selectedContentId);
    setBreadcrumb(breadcrumb.slice(0, bIndex+1));
    // Remove items from prevParents after selected one
    const pIndex = prevParents.findIndex(p => p._id == selectedContentId);
    setPrevParents(prevParents.slice(0, pIndex));
  }

  return ( 
    <>
      <div className="d-flex align-items-center">
        <TickCross value={menu.publish} tooltipTextOptions={["Published", "Not Published"]} />
        <h4 className="mr-3 mb-0">{menu.name}</h4>
        <ActionDropdown 
          data={menu} 
          parent="menu" 
          options={["edit", "duplicate", "delete"]} 
          className="tour-menu-hamburger-button"
          dropdownClassName="tour-menu-hamburger-dropdown"
        />
      </div>
      
      <Breadcrumb path={breadcrumb} onLinkClick={(data) => handleBreadcumbSelection(data)} />

      <ItemList columns={9} parent={currentParent} />

      <SectionList columns={9} onSectionClick={(section) => handleSectionSelection(section)} parent={currentParent} />
    </>
  );
};
