import React from 'react';
import { connectField } from 'uniforms';
import { Accordion, Row, Container, Col, Card } from 'react-bootstrap';
import { ListDelField, TextField, NumField, ErrorField } from 'uniforms-bootstrap4';
import { AccordionToggle } from '/imports/client/components/dashboard/shared/AccordionToggle';
import { Options } from "/imports/client/components/dashboard/menu/Options";

const AccordionListItem = ({ removeIcon, ...props }) => {

  return (
    <>
      <Card.Header className="p-0 border mt-2">
        <Row className="mx-0 d-flex align-items-center">
          <span className="ml-2 tour-item-expand-cog-toggle">
            <AccordionToggle eventKey={props.id}>Expand</AccordionToggle>
          </span>
          <Col xs={4} className="p-0">
            <TextField name="name" label={false} placeholder="Group Name" className="mb-0 tour-item-cog-name-input" />
          </Col>
          <Col xs={4} className="p-0">
            <ErrorField name="name" className="custom-error d-inline-flex text-danger border-0 font-weight-light pl-3 my-auto" />
          </Col>
          <ListDelField removeIcon={removeIcon} className="ml-auto" />
        </Row>
      </Card.Header>

      <Accordion.Collapse eventKey={props.id} className="border border-top-0">
        <Container className="mx-0">
          <Row>
            
            <Col xs={3} className="border-right tour-item-collapsed-cog-limits">
              <Row className="my-2">
                <Col>
                  <span>Limits</span>
                </Col>
              </Row>
              <Row>
                <Col>
                  <NumField name="max" label={false} placeholder="Max" className="mb-2" showInlineError />
                </Col>
              </Row>
              <Row>
                <Col>
                  <NumField name="min" label={false} placeholder="Min" className="mb-2" showInlineError />
                </Col>
              </Row>
            </Col>

            <Col xs={9} className="p-0">
              <Options />
            </Col>

          </Row>
        </Container>  
      </Accordion.Collapse>    
    </>
  );
}

export default connectField(AccordionListItem, { initialValue: false });