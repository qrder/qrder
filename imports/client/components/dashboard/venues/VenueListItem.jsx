import React, { useContext } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { GlobalContext } from '/imports/context/GlobalState';
import { TickCross } from '/imports/client/components/dashboard/shared/TickCross';
import { StripeStatusIcon } from '/imports/client/components/dashboard/venues/StripeStatusIcon';
import { getUsersStripeConnectionStatus } from '/imports/utils/stripe';
import { ActionDropdown } from '/imports/client/components/dashboard/shared/ActionDropdown';

export const VenueListItem = ({ venue }) => {

  const { userInfo } = useContext(GlobalContext);

  return (
    <Card className="mb-4 border-0 rounded-0 tour-venue-list-item">

      <Card.Header className="border-0 rounded-0 d-flex align-items-center" style={{ backgroundColor: "#F0F0F0" }}>
        <h4 className="mr-3 mb-0 d-inline font-weight-normal">{venue.name}</h4>
        <ActionDropdown 
          data={venue}
          parent="venue" 
          options={["edit", "delete"]}
          className="tour-venue-hamburger-button"
          dropdownClassName="tour-venue-hamburger-dropdown"
        />
      </Card.Header>
      
      <Card.Body style={{ border: "1px solid #F0F0F0" }}>
        <Row>
          <Col md={4} className="mb-4 mb-lg-0">
            <Card.Title className="font-weight-normal">Accepting Orders</Card.Title>
            <TickCross value={venue.isAcceptingOrders} text={venue.isAcceptingOrders?'Yes':'No'} showTooltip={false} className="my-2" />
          </Col>
          <Col md={4} className="mb-4 mb-lg-0">
            <Card.Title className="font-weight-normal">Payment Options</Card.Title>
            <TickCross value={venue?.paymentOptions?.cash} text="Cash" tooltipTextOptions={["Enabled", "Disabled"]} className="my-2" />
            <TickCross value={venue?.paymentOptions?.cardMachine} text="Card Machine" tooltipTextOptions={["Enabled", "Disabled"]} className="my-2" />
            <TickCross value={venue?.paymentOptions?.cardOnline} text="Online Card Payments" tooltipTextOptions={["Enabled", "Disabled"]} className="my-2" />
            <TickCross 
              value={venue?.paymentOptions?.cardOnline && userInfo?.integrations?.stripe} 
              text="Stripe"
              tooltipTextOptions={["Enabled", "Disabled"]}
              className="ml-5 my-2" 
            />
            {/* <TickCross value={userInfo?.integrations?.paypal} text="PayPal" className="ml-3" /> */}
          </Col>
          <Col md={4}>
            <Card.Title className="font-weight-normal">Integrations</Card.Title>
            <StripeStatusIcon status={getUsersStripeConnectionStatus(userInfo?.integrations?.stripe)} text="Stripe" />
          </Col>
        </Row>
      </Card.Body>

    </Card>
  );
};
