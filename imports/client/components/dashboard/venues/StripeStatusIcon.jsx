import React, { useState, useEffect } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle as RegularCheck } from '@fortawesome/free-regular-svg-icons';
import { faCheckCircle as SolidCheck, faTimesCircle, faClock } from '@fortawesome/free-solid-svg-icons';

export const StripeStatusIcon = ({ status, text = '', className }) => {
  const [icon, setIcon] = useState(null);

  useEffect(() => {
    switch(status) {
      case 'connected':
        setIcon(<FontAwesomeIcon icon={SolidCheck} className="text-success" />);
        break;
      case 'first-stage-complete':
        setIcon(<FontAwesomeIcon icon={RegularCheck} className="text-success" />);
        break;
      case 'pending':
        setIcon(<FontAwesomeIcon icon={faClock} className="text-warning" />);
        break;
      case 'started':
        setIcon(<FontAwesomeIcon icon={RegularCheck} className="text-success" />);
        break;
      case 'not-connected':
        setIcon(<FontAwesomeIcon icon={faTimesCircle} className="text-danger" />);
        break;
    }
  }, []);

  if(!icon) return '';

  return (
    <div className={`my-2 ${className}`}>
      <OverlayTrigger overlay={<Tooltip className="text-capitalize">{status.replaceAll('-', ' ')}</Tooltip>}>
        {icon}
      </OverlayTrigger>
      <span className="ml-2">{text}</span>
    </div>
  );
};

