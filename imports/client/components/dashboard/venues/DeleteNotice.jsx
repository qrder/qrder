import React from 'react';

export const DeleteNotice = () => {

  return (
    <div className="my-4">
      <p className="font-weight-bold">IMPORTANT NOTE: </p>
      <p>
        <i>
          Deleting a venue will unlink it from any orders that they have had. 
          Orders may not be visible if a venue is deleted - you will need to request these orders if you would like to view them. 
          Creating a venue with the same name will not link them back up.
        </i>
      </p>
    </div>
  );
};

