import React from 'react';
import { VenueListItem } from "/imports/client/components/dashboard/venues/VenueListItem";

export const VenueList = ({ venues }) => {

  return venues.map(venue => {
    return <VenueListItem key={venue._id} venue={venue} />
  })
};
