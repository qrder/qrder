import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { CardNumberElement, CardExpiryElement, CardCvcElement } from '@stripe/react-stripe-js';

const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: "#495057",
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "#aab7c4",
      },
    },
    invalid: {
      color: "#fa755a",
      iconColor: "#fa755a",
    },
  },
};

export const CardPaymentForm = () => {
  
  return (
    <Container>
      <Row className="mb-2">
        <Col className="p-0">
          <CardNumberElement className="border rounded p-2" options={CARD_ELEMENT_OPTIONS} />        
        </Col>
      </Row>
      <Row>
        <Col className="p-0 pr-1">
          <CardExpiryElement className="border rounded p-2" options={CARD_ELEMENT_OPTIONS} />
        </Col>
        <Col className="p-0 pl-1">
          <CardCvcElement className="border rounded p-2" options={CARD_ELEMENT_OPTIONS} />        
        </Col>
      </Row>
    </Container>
  )
};