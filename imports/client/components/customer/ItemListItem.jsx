import React, { useState } from 'react';
import { Tracker } from 'meteor/tracker';
import { Row, Col, Button } from 'react-bootstrap';

export const ItemListItem = ({item, onButtonClick}) => {
  const [colour, setColour] = useState();

  // Update colour when session var is set
  Tracker.autorun(() => {
    if(Session.equals('brandColours', '' || undefined)) return;

    if(JSON.parse(Session.get('brandColours')).addBtn !== colour) {
      setColour(JSON.parse(Session.get('brandColours')).addBtn);
    }
  });

  return (
    <>
      <Row className="my-3">
        <Col xs={8}>
          <Row>
            <Col xs={12}><div>{item.name}</div></Col>
            <Col className="text-info">
              {item.vegetarian ? ' V ' : ''}
              {item.vegan ? ' Ve ' : ''}
              {item.glutenFree ? ' Gf ' : ''}
              {item.containsAlcohol ? ' Alc ' : ''}
              {item.containsFish ? ' F ' : ''}
            </Col>
          </Row>
          <span className="small">{item.description}</span>
        </Col>
        <Col xs={4} className="d-flex align-items-center">
          <span className="small">£{item.prices ? item.prices[0].value.toFixed(2) : 0.00}</span>
          <Button 
            style={{ backgroundColor: colour, borderColor: colour }}
            className="ml-1"
            onClick={() => onButtonClick('add-item-to-cart', item)}
          >
            Add
          </Button>
        </Col>
      </Row>

      <hr className="m-0"/>
    </>
  );
};
