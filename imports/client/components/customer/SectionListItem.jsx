import React from 'react';
import { Accordion, Card } from 'react-bootstrap';
import { SectionList } from './SectionList';
import { ItemList } from './ItemList';

export const SectionListItem = ({section, onButtonClick}) => {

  return (
    <Card key={section._id} className="mt-2 border-0 rounded-0">

      <Card.Header className="p-0 border-0">
        <Accordion.Toggle as={Card.Header} eventKey={section._id} className="border-0">
          {section.name}
        </Accordion.Toggle>
      </Card.Header>

      <Accordion.Collapse eventKey={section._id} className="p-0 border-0">
        <Card.Body className="p-0 border-0">
          {
            section.items === undefined ? '' : 
            <ItemList items={section.items} onButtonClick={(buttonClicked, data) => onButtonClick(buttonClicked, data)} />
          }
          {
            section.sections === undefined ? '' : 
            <SectionList sections={section.sections} onButtonClick={(buttonClicked, data) => onButtonClick(buttonClicked, data)} />
          }
        </Card.Body>
      </Accordion.Collapse>
      
    </Card>
  );
};
