import React from 'react';

export const ExtraScroll = () => {
  // Provide some extra scroll potential to cater for smaller-than-expected screens
  return <div style={{height: '300px'}}></div>;
};