import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { OrderItemCounter } from '/imports/client/components/customer/OrderItemCounter';
import { quantity as CartQty } from '/imports/utils/cart';

export const OrderItem = ({ item, onQuantityZero, changeDetected }) => {
  const [quantity, setQuantity] = useState(item.quantity);

  useEffect(() => {
    if(quantity == 0) {
      // Set quantity back to 1 in case customer closes remove modal as it will delete anyway if they do confirm
      setQuantity(1);
      onQuantityZero(item);
    }
  }, [quantity])

  // Update quantity state and cart localstorage
  // 'diff' may be -1, +1 or the difference b/w current qty and typed qty
  const handleQuantityChange = (qty, diff) => {
    setQuantity(qty); 
    // Prevent item being removed from cart localstorage if 0,
    // Only remove from cart localstorage when 'Remove' clicked in modal
    if(qty !== 0) {
      CartQty(item.id, diff); 
    }
    // Let parent Order component know of the change so it can rerender total price
    changeDetected();
  }

  return (
    <>
      <Container className="my-3">
        <Row>
          <Col>
            <h5 className="font-weight-normal">{item.name}</h5>
          </Col>
          <Col xs={4}>
            <OrderItemCounter qty={quantity} changeQty={(qty, diff) => handleQuantityChange(qty, diff)} />
          </Col>
        </Row>

        <Row className="mb-2">
          <Col>
            <span className="text-secondary">Price: {item.priceName}</span>
          </Col>
        </Row>

        <Row className="text-secondary">
          <Col xs={3}>
            <span>Options:</span>
          </Col>
          <Col>
            {
              item.options.map((opt, oidx) => {
                return (
                  <div key={oidx} className="mb-2"> 
                    <span>{opt.name}</span>
                    <div>
                      {
                        opt.optionsChosen.map((cho, cidx) => {
                          return <div key={cidx} className="small">{cho}</div>
                        })
                      }
                    </div>
                  </div>
                )
              })
            }
          </Col>
          <Col xs={3} className="mt-auto text-dark">
            {/* TODO: Dynamic symbol based on client config */}
            <h5 className="font-weight-normal">£{item.price !== undefined ? (item.price * quantity).toFixed(2) : ""}</h5>
          </Col>
        </Row>
      </Container>

      <hr className="m-0" />
    </>
  );
};
