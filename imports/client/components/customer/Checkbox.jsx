import React, { useState } from 'react';
import { Form } from 'react-bootstrap';

export const Checkbox = ({ option, onSelect, allDisabled }) => {
  const [checked, setChecked] = useState(false);

  const getDisabled = () => {
    if(allDisabled && !checked) return true;
    if(allDisabled && checked) return false;
    return false;
  }

  return (
    <Form.Check
      key={option.name}
      label={`${option.name} (£${ option.value.toFixed(2) })`}
      value={`${option.name} (£${ option.value.toFixed(2) })`}
      onChange={(e) => { onSelect(e); setChecked(!checked) }}
      disabled={getDisabled()}
    />
  );
};
