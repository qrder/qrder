import React, { useState, useEffect } from 'react';
import { Session } from 'meteor/session';
import { Form, Container, Row, Col } from 'react-bootstrap';

export const PaymentOptions = ({ onPaymentOptionChange }) => {
  const [paymentOptions, setPaymentOptions] = useState({});
  
  // On load, get payment options available for venue (from session)
  // Create empty session variable for customers chosen option
  useEffect(() => {
    setPaymentOptions(JSON.parse(Session.get('venuePaymentOptions')));
    // Session.setAuth('chosenPaymentOption', JSON.stringify({}));
  }, []);

  // Update session state with chosen option & bubble change to parent
  // so that it can change the nextPage state (payment info or confirmation pages)
  const handleRadioChange = (type) => {
    Session.setAuth('chosenPaymentOption', JSON.stringify({paymentType: type}));
    onPaymentOptionChange(type);
  }
  
  return (   
    <Container className="mt-4">
      <Form>
        {
          !(paymentOptions.cash) ? "" :
          <Row className="mb-4">
            <Col xs={{ span: 6, offset: 3 }} className="text-left">
              <Form.Check
                type="radio"
                name="paymentOption"      
                label="Cash"
                onChange={() => handleRadioChange("cash")} 
              />
            </Col>
          </Row>
        }

        {
          !(paymentOptions.cardMachine) ? "" :
          <Row className="mb-4">
            <Col xs={{ span: 6, offset: 3 }} className="text-left">
              <Form.Check
                type="radio"
                name="paymentOption" 
                label="Card Machine"
                onChange={() => handleRadioChange("cardMachine")}
              />
            </Col>
          </Row>
        }

        {
          !(paymentOptions.cardOnline) ? "" :
          <Row className="mb-4">
            <Col xs={{ span: 6, offset: 3 }} className="text-left">
              <Form.Check
                type="radio"
                name="paymentOption" 
                label="Pay Now"
                onChange={() => handleRadioChange("cardOnline")}
              />
            </Col>
          </Row>
        }
      </Form>
    </Container>
  )
};
