import React from 'react';
import { InputGroup, FormControl } from 'react-bootstrap';

export const OrderItemCounter = ({ qty, changeQty }) => {

  return (
    <InputGroup size="sm">
      <InputGroup.Prepend onClick={() => changeQty(qty-1, -1)}>
        <InputGroup.Text>-</InputGroup.Text>
      </InputGroup.Prepend>

      <FormControl value={qty} className="text-center" onChange={(e) => changeQty(e.target.value, Number(e.target.value - qty))} />

      <InputGroup.Append onClick={() => changeQty(qty+1, +1)}>
        <InputGroup.Text>+</InputGroup.Text>
      </InputGroup.Append>
    </InputGroup>
  );
};
