import React from 'react';
import { Form } from 'react-bootstrap';
import { formattedPrice } from '/imports/utils/customers';

export const ModalPrices = ({ prices, priceSelected }) => {

  if(prices === undefined || prices.length < 1) return "";

  // If only 1 price option, return a preselected radio button
  if(prices.length === 1) {
    return (
      <Form.Check
        name="prices"
        type="radio"
        checked={true}
        label={formattedPrice(prices[0].name, prices[0].value)}
        value={formattedPrice(prices[0].name, prices[0].value)}
        onChange={e => priceSelected(e.target.value)}
      />
    );
  }

  return (
    prices.map((price, idx) => {
      return (
        <Form.Check
          name="prices"
          key={idx} 
          type="radio"
          label={formattedPrice(price.name, price.value)}
          value={formattedPrice(price.name, price.value)}
          onChange={e => priceSelected(e.target.value)}
        />
      )
    })
  );
};
