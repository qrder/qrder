import React from 'react';
import { Form, Container, Row, Col } from 'react-bootstrap';
import { CardPaymentForm } from '/imports/client/components/customer/CardPaymentForm';

export const PaymentInformationForm = ({ nameChange, lineOneChange, lineTwoChange, cityChange, postcodeChange }) => {
  

  return (
    <Container className="mt-4">
      <Form>
        <h6 className="font-weight-normal">Card Details</h6>
        <Row className="mt-3">
          <Col>
            <CardPaymentForm />
          </Col>
        </Row>

        <h6 className="font-weight-normal my-3">Billing Address</h6>
        {/* Name */}
        <Row className="mb-2">
          <Col>
            <Form.Control type="text" placeholder="Full Name" name="name" onChange={(e) => nameChange(e.target.value)} />
          </Col>
        </Row>
        {/* Address */}
        <Row className="mb-2">
          <Col>
            <Form.Control type="text" placeholder="Address Line 1" name="line1" onChange={(e) => lineOneChange(e.target.value)} />
          </Col>
        </Row>
        <Row className="mb-2">
          <Col>
            <Form.Control type="text" placeholder="Address Line 2" name="line2" onChange={(e) => lineTwoChange(e.target.value)} />
          </Col>
        </Row>
        <Row className="mb-2">
          <Col>
            <Form.Control type="text" placeholder="City" name="city" onChange={(e) => cityChange(e.target.value)} />
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Control type="text" placeholder="Post Code" name="postcode" onChange={(e) => postcodeChange(e.target.value)} />
          </Col>
        </Row>
      </Form>
    </Container>
  )
};
