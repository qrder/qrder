import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Navbar } from 'react-bootstrap';
import { getBaseUrl } from '/imports/utils/customers';

export const NextStepButton = ({ linkTo, buttonText, fake = false, disabled, clickEvent }) => {
  const history = useHistory();

  const [link, setLink] = useState('#');

  useEffect(() => {
    // Throws error on first load as baseUrl has not been set yet
    // So if session var is undefined, just use current url basically
    // the else condition should only be thrown on first page load anyway (CustomerMenu page)
    const baseUrl = getBaseUrl();
    switch(linkTo) {
      case 'menu':
        setLink(baseUrl);
        break;
      case 'order':
        setLink(`${baseUrl}/order`);
        break;
      case 'information':
        setLink(`${baseUrl}/information`);
        break;
      case 'payment-options':
        setLink(`${baseUrl}/payment-options`);
        break;
      case 'payment-information':
        setLink(`${baseUrl}/payment-information`); 
        break;
      case 'confirmation':
        setLink(`${baseUrl}/confirmation`); 
        break;
    }
  }, [linkTo]);

  if(disabled) {
    return (
      <Navbar bg="light" variant="light" fixed="bottom">
        <Navbar.Text className="mx-auto">
          <h4 className="font-weight-light mb-0 text-center" style={{letterSpacing: '5px'}}>
            {buttonText}
          </h4>
        </Navbar.Text>
      </Navbar>
    )
  }

  return (
    <Navbar 
      style={{ backgroundColor: JSON.parse(Session.get('brandColours')).nextBtn }}
      variant="success" 
      className="text-white" 
      fixed="bottom" 
      onClick={fake ? clickEvent : () => history.push(link)}
    >
      <Navbar.Text className="mx-auto">
        <h4 className="font-weight-light mb-0 text-center" style={{letterSpacing: '5px'}}>
          {buttonText}
        </h4>
      </Navbar.Text>
    </Navbar>
  );
};
