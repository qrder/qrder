import React, { useState, useEffect } from 'react';
import { Session } from 'meteor/session';
import { Modal, Button, Form } from 'react-bootstrap';
import { ModalPrices } from '/imports/client/components/customer/ModalPrices';
import { ModalOptions } from '/imports/client/components/customer/ModalOptions';
import { formattedPrice } from '/imports/utils/customers';
import { calculateItemTotal } from '/imports/utils/cart';
import { add, list } from '/imports/utils/cart';

/**
 * Add To Cart Modal
 * This could probably be refactored into less code & be made more readable.
 * It's slightly confusing so here's a quick overview for future reference:
 * - When the 'Add' button is clicked on an item, it opens the modal & presets the optionsSelected state array (first useEffect)
 * - When a price is selected, it sets the state with the string value (e.g. "Small (5.00)"") and it it runs the second useEffect 
 * - When a checkbox in the options is selected, the useEffect in CustomOptionGroupsSelector runs and checks that it is below the max 
 *        and above the min.  This will then disable / enable the other checkboxes in it's group.  It also then bubbles that change up
 *        to this component - calling the 'handleValidSelection' function.
 *        This function updates the optionsSelected state array with the selected information and with whether it is valid (between min and max)
 * - When an option is changed, it also runs the second useEffect, like the price change selection.  This checks that price is not empty
 *        and that all 'validSelection' bools inside the optionsSelected state array are set to true (between min and max).
 * - If all are true, then it enables the 'Add to Cart' button.
 * - If anything is false, the button remains disabled or is disabled if it was enabled.
 */

export const AddItemToCartModal = ({ show, onHide, item }) => {
  const [addToCartDisabled, setAddToCartDisabled] = useState(false);
  const [priceSelected, setPriceSelected] = useState('');
  const [optionsSelected, setOptionsSelected] = useState([]);
  const [optionGroups, setOptionGroups] = useState();
  const [btnColour, setBtnColour] = useState();

  useEffect(() => {
    if(show) {
      // combine custom option groups with any pre-defined option groups
      const cog = item.customOptionGroups || [];
      setOptionGroups(cog.concat(item.optionGroups || []));

      // Set colour to brand value in session 
      setBtnColour(JSON.parse(Session.get('brandColours')).addBtn);
    }

    // Reset state on modal close
    if(!show) {
      setPriceSelected('');
      setOptionsSelected([]);
      setAddToCartDisabled(false);
    }
  }, [show]);

  useEffect(() => {
    if(show) {
      // Create empty array of custom option groups to be validated against later on (done after combination above)
      if(optionGroups !== undefined) {
        optionGroups.map(og => {
          setOptionsSelected(optionsSelected => [...optionsSelected, {
              name: og.name, 
              optionsChosen: [], 
              validSelection: decideIfSelectionIsValid(og)
            }
          ]);
        });
      }

      // If only 1 price available, preselect it
      if(item.prices !== undefined) {
        if(item.prices.length === 1) {
          setPriceSelected(formattedPrice(item.prices[0].name, item.prices[0].value));
        }
      }
    }
  }, [optionGroups])

  // When state of 'if things are selected' changes, change state of add to cart button
  useEffect(() => {
    if(priceSelected !== '' && areAllOptionsSelected()) {
      setAddToCartDisabled(false);
    } else {
      setAddToCartDisabled(true);
    }
  }, [priceSelected, optionsSelected]);

  // Handle form submit - add item to cart local strorage
  const submit = () => {    
    // Separate actual price from priceSelected state string
    const itemPrice = calculateItemTotal(priceSelected, optionsSelected);
    // Build cart item
    const cartItem = {
      id: `${item._id}-${Date.now()}`,
      name: item.name,
      priceName: priceSelected,
      price: itemPrice, // TODO: Consider if this is safe (i.e. keeping price in local storage as it could be changed)
      options: [...optionsSelected]
    }
    const qty = 1; // TODO: Add ability to add same item >1 when on ADD modal & make this dynamic
    // Add to cart
    add(cartItem, qty);
    // Update Session variable that is responsible for reactive cart number change
    Session.set('cartQty', list().length);
    // Reset state ready for next modals use
    setPriceSelected('');
    setOptionsSelected([]);
    setAddToCartDisabled(false);
    // Hide modal
    onHide();
  };

  // Conditions for if a selection is valid or not (and whether to enable button)
  const decideIfSelectionIsValid = (cog) => {
    if(cog.min == 0) return true;

    return false;
  };

  // Update validSelection every time a checkbox is checked / unchecked
  const handleValidSelection = (name, options, validSelectionBool) => {
    const cogToUpdate = optionsSelected.findIndex(c => c.name === name);
    // -1 means that an object was not found in the array with a name matching 'name' param
    if(cogToUpdate != -1) {
      let tmp = optionsSelected;
      tmp[cogToUpdate] = {...tmp[cogToUpdate], optionsChosen: options, validSelection: validSelectionBool};
      setOptionsSelected([...tmp]);
    }
  }

  // Check for all validSelection: 'true' in optionsSelected array
  const areAllOptionsSelected = () => {
    let validSelectionsArray = optionsSelected.map(cog => cog.validSelection);
    return validSelectionsArray.every(Boolean);
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <h6>Add '{item.name}' to Cart</h6>
        </Modal.Title>
      </Modal.Header>

      <Form>

        <Modal.Body>

          <ModalPrices prices={item.prices} priceSelected={(value) => setPriceSelected(value)} />
          
          {/* TODO: Include these in validating selection */}
          <ModalOptions options={item.optionGroups} handleValidSelection={(name, options, isValid) => handleValidSelection(name, options, isValid)} />

          <ModalOptions options={item.customOptionGroups} handleValidSelection={(name, options, isValid) => handleValidSelection(name, options, isValid)} />

        </Modal.Body>

        <Modal.Footer className="p-0">
          {/* TODO: Split this button into it's own component? It has a lot of conditional logic assoc. with it */}
          <Button 
            style={{ 
              backgroundColor: btnColour,
              borderColor: btnColour
            }}
            onClick={submit}
            className="h-100 w-100 m-0 p-3 rounded-0"
            disabled={addToCartDisabled}
          >
            ADD TO CART
          </Button>
        </Modal.Footer>

      </Form>
    </Modal>
  );
};
