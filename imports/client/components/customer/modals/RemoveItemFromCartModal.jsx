import React from 'react';
import { Session } from 'meteor/session';
import { Modal, Button, Form } from 'react-bootstrap';
import { remove, list } from '/imports/utils/cart';

export const RemoveItemFromCartModal = ({ show, onHide, item }) => {

  // Handle form submit - remove item from cart local strorage & hide modal
  const submit = () => {
    // Remove item from cart local storage
    remove(item.id);
    // Update Session variable that is responsible for reactive cart number change
    Session.set('cartQty', list().length);
    // close modal
    onHide();
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <h6>Remove '{item.name}' from Order</h6>
        </Modal.Title>
      </Modal.Header>

      <Form>

        <Modal.Body>
          Are you sure you want to delete '{item.name}' from the order?
        </Modal.Body>

        <Modal.Footer className="p-0">
          {/* TODO: 3 second delay or so? */}
          <Button 
            variant="danger"
            onClick={submit}
            className="h-100 w-100 m-0 p-3 rounded-0"
          >
            REMOVE
          </Button>
        </Modal.Footer>

      </Form>
    </Modal>
  );
};
