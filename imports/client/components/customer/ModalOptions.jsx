import React from 'react';
import { CustomOptionGroupsSelector } from '/imports/client/components/customer/CustomOptionGroupsSelector';

export const ModalOptions = ({ options, handleValidSelection }) => {

  if(options === undefined || options.length < 1) return "";

  return (
    options.map((cog, idx) => {
      return (
        <CustomOptionGroupsSelector 
          key={idx} 
          customOptionGroup={cog} 
          validSelection={(name, options, isValid) => handleValidSelection(name, options, isValid)} 
        />
      )
    })
  );
};
