import React, { useState, useEffect } from 'react';
import { Checkbox } from '/imports/client/components/customer/Checkbox';

export const CustomOptionGroupsSelector = ({ customOptionGroup, validSelection }) => {
  let noOfChoices;

  const [checkboxesDisabled, setCheckboxesDisabled] = useState(false);
  const [optionsSelected, setOptionsSelected] = useState([]);
  
  useEffect(() => {
    // Handle disabling & enabling of checkboxes
    if(optionsSelected.length === customOptionGroup.max) {
      setCheckboxesDisabled(true);
    } else if(optionsSelected.length < customOptionGroup.max) {
      setCheckboxesDisabled(false);
    }

    // Check if between min and max (i.e. a valid selection) & bubble that info 
    if(optionsSelected.length >= customOptionGroup.min && optionsSelected.length <= customOptionGroup.max) {
      validSelection(customOptionGroup.name, optionsSelected, true);
    } else if(optionsSelected.length < customOptionGroup.min || optionsSelected.length > customOptionGroup.max) {
      validSelection(customOptionGroup.name, optionsSelected, false);
    }
  }, [optionsSelected]);

  // Handle when a checkbox is selected from the optional extras
  const handleCustomOptionsChange = (e) => {
    // Update options selected state array
    if(e.target.checked) {
      setOptionsSelected([...optionsSelected, e.target.value]);
    } else {
      setOptionsSelected(optionsSelected.filter(opt => opt !== e.target.value));
    }
  }

  // If same return as one
  if(customOptionGroup.min === customOptionGroup.max) {
    noOfChoices = customOptionGroup.min;
  } else {
    noOfChoices = `${customOptionGroup.min} - ${customOptionGroup.max}`;
  }

  return (
    <div className="my-3">
      <label htmlFor={customOptionGroup.name}>
        {customOptionGroup.name} <span className="text-secondary">(Choose {noOfChoices})</span>
      </label>
      {
        customOptionGroup.options.map((opt, idx) => {
          return (
            <Checkbox key={idx} option={opt} onSelect={(e) => handleCustomOptionsChange(e)} allDisabled={checkboxesDisabled} /> 
          )
        })
      }
  </div>
  );  
};
