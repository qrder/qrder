import React, { useState } from 'react';
import { list, total, isEmpty } from '/imports/utils/cart';
import { OrderItem } from '/imports/client/components/customer/OrderItem';
import { useEffect } from 'react';

export const Order = ({ onQuantityZero }) => {
  const [orderTotal, setOrderTotal] = useState(0.0);
  // It does not matter what value this is, all that matters is whether it changes or not
  const [changeDetected, setChangeDetected] = useState(false);

  // Update total price state every time the qty of any item is changed
  useEffect(() => {
    setOrderTotal(total());
  }, [changeDetected]);

  return ( 
    <>
      <div className="text-center my-2">
        {/* TODO: Dynamic symbol based on client config */}
        <h4 className="font-weight-normal text-secondary" style={{letterSpacing: '5px'}}>TOTAL : £{orderTotal.toFixed(2)}</h4>
      </div>

      <hr className="m-0" />

      {
        isEmpty() ? 
          <h3 className="font-weight-normal text-center mt-5">Cart is Empty</h3> 
          :
          list().map(item => {
            return <OrderItem key={item.id} item={item} onQuantityZero={(item) => onQuantityZero(item)} changeDetected={() => setChangeDetected(!changeDetected)} />
          })
      }
    </>
  );
};
