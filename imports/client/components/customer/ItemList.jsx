import React from 'react';
import { Container } from 'react-bootstrap';
import { ItemListItem } from "./ItemListItem";

export const ItemList = ({items, onButtonClick}) => {

  return (
    <Container>
      {
        items.map(item => {
          return <ItemListItem key={item._id} item={item} onButtonClick={(buttonClicked, data) => onButtonClick(buttonClicked, data)} />
        })
      }
    </Container>
  );
};
