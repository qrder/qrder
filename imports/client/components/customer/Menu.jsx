import React from 'react';
import { Accordion } from 'react-bootstrap';
import { MenuListItem } from "./MenuListItem";

export const Menu = ({ menus, onButtonClick }) => {

  if(menus == undefined || menus.length == 0) {
    return (
      <div className="text-center mt-4">
        Sorry, there do not appear to be any menus.  Please check with a member of staff.
      </div>
    )
  }

  return (
    <Accordion defaultActiveKey={menus[0]._id}>
      {
        menus.map(menu => {
          return <MenuListItem key={menu._id} menu={menu} onButtonClick={(buttonClicked, data) => onButtonClick(buttonClicked, data)} />
        })
      }
    </Accordion>
  );
};
