import React from 'react';
import { Accordion } from 'react-bootstrap';
import { SectionListItem } from "./SectionListItem";

export const SectionList = ({sections, onButtonClick}) => {

  return (
    <Accordion>
      {
        sections === undefined ? '' : 
        sections.map(section => {
          return <SectionListItem key={section._id} section={section} onButtonClick={(buttonClicked, data) => onButtonClick(buttonClicked, data)} />
        })
      }
    </Accordion>
  );
};
