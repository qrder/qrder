import React from 'react';
import { Accordion, Card } from 'react-bootstrap';
import { SectionList } from './SectionList';
import { ItemList } from './ItemList';

export const MenuListItem = ({menu, onButtonClick}) => {

  return (
    <Card key={menu._id} className="mb-3 rounded-0">

      <Card.Header className="p-0 border-0">
        <Accordion.Toggle as={Card.Header} eventKey={menu._id} className="border-0">
          <h5 className="m-0">{menu.name}</h5>
        </Accordion.Toggle>
      </Card.Header>

      <Accordion.Collapse eventKey={menu._id}>
        <Card.Body className="p-0 border-0">
          {
            menu.items === undefined ? '' : 
            <ItemList items={menu.items} onButtonClick={(buttonClicked, data) => onButtonClick(buttonClicked, data)} />
          }
          {
            menu.sections === undefined ? '' : 
            <SectionList sections={menu.sections} onButtonClick={(buttonClicked, data) => onButtonClick(buttonClicked, data)} />
          }
        </Card.Body>
      </Accordion.Collapse>
      
    </Card>
  );
};
