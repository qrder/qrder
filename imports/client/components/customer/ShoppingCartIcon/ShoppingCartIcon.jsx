import React, { useState } from 'react';
import { Session } from 'meteor/session';
import { Tracker } from 'meteor/tracker';
import { Container } from 'react-bootstrap';
import './ShoppingCartIcon.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBasket } from '@fortawesome/free-solid-svg-icons';

export const ShoppingCartIcon = () => {
  const [cartQty, setCartQty] = useState(0);
  const [colour, setColour] = useState();

  // Update colour when session var is set
  Tracker.autorun(() => {
    if(Session.equals('brandColours', '' || undefined)) return;

    if(JSON.parse(Session.get('brandColours')).navbarIcon !== colour) {
      setColour(JSON.parse(Session.get('brandColours')).navbarIcon);
    }
  });

  // Reactively update the number on the cart icon
  Tracker.autorun(() => {
    if (Session.equals('cartQty', '0')) return;

    if(Session.get('cartQty') !== cartQty) {
      setCartQty(Session.get('cartQty'));
    }
  });

  return (
    <Container className="pr-0">
      <FontAwesomeIcon 
        icon={faShoppingBasket} 
        size="lg" 
        style={{ color: colour }}
      />
      <span className="badge badge-notify text-white bg-success">
        { cartQty }
      </span>
    </Container>
  )  
};