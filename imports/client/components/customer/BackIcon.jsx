import React, { useState } from 'react';
import { Tracker } from 'meteor/tracker';
import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

export const BackIcon = () => {
  const [colour, setColour] = useState();

  // Update colour when session var is set
  Tracker.autorun(() => {
    if(Session.equals('brandColours', '' || undefined)) return;

    if(JSON.parse(Session.get('brandColours')).navbarIcon !== colour) {
      setColour(JSON.parse(Session.get('brandColours')).navbarIcon);
    }
  });

  return (
    <Container className="text-center">
      <FontAwesomeIcon
        icon={faArrowLeft} 
        size="lg" 
        style={{ color: colour }}
      />
    </Container>
  )  
};