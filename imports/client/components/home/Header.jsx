import React from 'react';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';

export const Header = () => {
  const history = useHistory();

  const submit = e => {
    e.preventDefault();
    history.push('/register', { email: e.target.email.value }); 
  };

  return (
    <header className="masthead text-white text-center">
      <div className="overlay"></div>
      <Container>
        <Row className="py-5">
          <Col xl={9} className="mx-auto mb-4">
            <FontAwesomeIcon icon={faQrcode} size="5x" />
            <h1>Simple QR Code Ordering System</h1>
            <h3>for Pubs, Cafés and Restaurants</h3>
          </Col>
          <Col md={10} lg={8} xl={7} className="mx-auto mb-4">
            <Form onSubmit={submit}>
              <Form.Row>
                <Col xs={12} md={9} className="mb-2 mb-md-0">
                  <Form.Control type="email" name="email" placeholder="Enter Your Email..." size="lg" />
                </Col>
                <Col xs={12} md={3}>
                  <Button type="submit" block size="lg">Sign Up</Button>
                </Col>
              </Form.Row>
            </Form>
          </Col>
          <Col md={10} lg={8} xl={7} className="mx-auto">
            <h5>Free 2 month trial.  No credit card required.</h5>
          </Col>
        </Row>
      </Container>
    </header>
  );
};
