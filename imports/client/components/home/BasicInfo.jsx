import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BasicInfoItem } from '/imports/client/components/home/BasicInfoItem';
import { faVirus, faCheck, faCashRegister, faPause } from '@fortawesome/free-solid-svg-icons';

export const BasicInfo = () => {

  return (
    <section className="py-5 bg-light text-center">
      <Container>
        <Row>
          <Col lg={3}>
            <BasicInfoItem 
              icon={faVirus}
              colour="text-success"
              title="COVID-19" 
              text="Improve safety by reducing the chance of COVID-19 spread." 
            />
          </Col>
          <Col lg={3}>
            <BasicInfoItem 
              icon={faPause}
              colour="text-warning"
              title="Pause / Cancel" 
              text="Pause or cancel your subscription at any time." 
            />
          </Col>
          <Col lg={3}>
            <BasicInfoItem 
              icon={faCashRegister}
              colour="text-dark"
              title="Payment Options" 
              text="Handle payments yourself or connect Stripe for online payments." 
            />
          </Col>
          <Col lg={3}>
            <BasicInfoItem 
              icon={faCheck}
              colour="text-primary"
              title="Quick & Easy" 
              text="Create the menu and print your QR codes quickly and easily." 
            />
          </Col>
        </Row>
      </Container>
    </section>
  );
};
