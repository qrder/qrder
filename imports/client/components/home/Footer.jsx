import React from 'react';
import { useHistory } from "react-router-dom";
import { Container, Row, Col, Button } from 'react-bootstrap';

export const Footer = () => {
  const history = useHistory();

  return (
    <footer className="footer bg-light">
      <Container>
        <Row>
          
          <Col lg={4} className="h-100 text-center text-lg-left my-auto">
            <p className="text-muted small mb-4 mb-lg-0">&copy; QRDER 2021. All Rights Reserved.</p>
          </Col>

          <Col lg={4} className="h-100 text-center my-auto">
            <Button variant="link" onClick={() => history.push("/privacy")} size="sm">
              Privacy Policy
            </Button>
          </Col>

          <Col lg={4} className="h-100 text-center text-lg-right my-auto">
            <span className="text-muted small mb-4 mb-lg-0">
              Email Contact: support@qrder.io
            </span>
          </Col>

        </Row>
      </Container>
    </footer>
  );
};
