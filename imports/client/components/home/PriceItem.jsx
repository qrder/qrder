import React from 'react';
import { useHistory } from "react-router-dom";
import { Card, Button } from 'react-bootstrap';

export const PriceItem = ({ tier, price }) => {
  const history = useHistory();

  const hasNumber = (str) => {
    return /\d/.test(str);
  }

  return (
    <Card>
      <Card.Body>
        <Card.Title className="text-muted text-uppercase text-center">{tier}</Card.Title>
        <h6 className="card-price text-center">
          {price}
          { hasNumber(price) ? <span className="period">/month</span> : "" }
        </h6>
        
        <hr/>

        <ul className="fa-ul ml-0 text-center">
          <li>
          {
            tier == 'Enterprise' ? "Custom Number of Venues" : "Single Venue"
          }
          </li>
          <li><strong>Unlimited</strong> Menu's</li>
          <li><strong>Unlimited</strong> Orders</li>
          <li><strong>Unlimited</strong> Tables</li>
          <li>Free Stripe Integration</li>
        </ul>

        {
          tier == 'Enterprise' ? (
            <Button block disabled>Coming Soon</Button>
          ) : (
            <Button onClick={() => history.push("/register")} block>Sign Up</Button>
          )
        }        
      </Card.Body>
    </Card>
  );
};
