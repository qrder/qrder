import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { PriceItem } from '/imports/client/components/home/PriceItem';

export const Prices = () => {

  return (
    <section className="pricing py-5">
      <Container>
        <Row>
          <Col lg={4} className="my-3 my-lg-0">
            <PriceItem tier="Trial" price="£0" />
          </Col>
          
          <Col lg={4} className="my-3 my-lg-0">
            <PriceItem tier="Standard" price="£16.99" />
          </Col>
          
          <Col lg={4} className="my-3 my-lg-0">
            <PriceItem tier="Enterprise" price="Coming Soon" />
          </Col>
        </Row>
      </Container>
    </section>
  );
};
