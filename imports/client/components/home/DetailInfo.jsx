import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { DetailInfoItem } from '/imports/client/components/home/DetailInfoItem';

export const DetailInfo = () => {

  return (
    <section className="showcase">
      <Container fluid className="p-0">
        <DetailInfoItem
          img="/images/qr-code-table-2.webp"
          imgSide="right"
          title="QR Code Ordering"
          text="Customers can order from their tables using their phones by scanning a QR code."
        />
        <DetailInfoItem
          img="/images/digital-menu.webp"
          imgSide="left"
          title="Simple Digital Menu"
          text="A nice and simple menu design makes it easy for customers to view the menu and make an order."
        />
        <DetailInfoItem
          img="/images/orders-page.webp"
          imgSide="right"
          extraClasses="wide-showcase-img"
          title="View & Manage Orders with Ease"
          text="Orders appear in the dashboard for staff to manage.  
                Simply load the dashboard up on a tablet behind the bar.  
                Audio notifications also make it easier to keep an eye out for new orders."
        />
      </Container>
    </section>
  );
};
