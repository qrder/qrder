import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

export const Contact = () => {
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [emailSent, setEmailSent] = useState(false);
  const [error, setError] = useState(false);

  const sendEmail = (e) => {
    e.preventDefault();
    Meteor.call("home.contactUsEmail", email, message, (error) => {
      if(error) {
        // TODO: handle error better
        console.error(error.message);
        setError(true);
      } else {
        setEmailSent(true);
      }
    });
  }

  const disabled = () => {
    if(email !== '' && message !== '') return false;
    return true;
  }

  if(error) {
    return (
      <section className="py-5 bg-light text-center">
        <Container>
          <Row>
            <Col>
              <h2>Contact Us</h2>
              <h4>Error Sending Email</h4>

              <p className="text-danger">We're really sorry but there appears to have been an error sending your email.</p>
              <p className="text-danger">Please try again.</p>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }

  if(emailSent) {
    return (
      <section className="py-5 bg-light text-center">
        <Container>
          <Row>
            <Col>
              <h2>Contact Us</h2>
              <h4>Email Sent</h4>

              <p>Your message has been sent.</p>  
              <p>We will reply to {email}.</p>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }

  return (
    <section className="py-5 bg-light text-center">
      <Container>
        <Row>
          <Col>
            <h2>Contact Us</h2>
          </Col>
        </Row>
        <Row className="pt-2 pb-4">
          <Col>
            <p className="small">We aim to reply within 24 hours.  Replies will come from our 'support@qrder.io' email address.  Please check your junk email if you do not receive a reply.</p>
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 8, offset: 2}}>
            <Form onSubmit={(e) => sendEmail(e)}>
              <Form.Group controlId="formGroupEmail">
                <Form.Control type="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
              </Form.Group>
              <Form.Group controlId="emailMessage">
                <Form.Control as="textarea" rows={10} placeholder="Enter message..."  onChange={(e) => setMessage(e.target.value)} />
              </Form.Group>
              <Row>
                <Col className="text-right">
                  <Button variant="primary" type="submit" disabled={disabled()}>Send</Button>
                </Col>
              </Row>              
            </Form>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
