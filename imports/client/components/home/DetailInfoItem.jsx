import React, { useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';

export const DetailInfoItem = ({ img, imgSide, extraClasses, title, text }) => {
  const [order, setOrder] = useState(['first', 'last']);
  const [textJustification, setTextJustification] = useState('text-lg-left');

  useEffect(() => {
    // formatting
    if(imgSide == 'right') {
      setOrder(['last', 'first']);
      setTextJustification('text-lg-right');
    }
  }, []);

  // create img style object
  const imgStyle = {
    backgroundImage: `url(${img})`
  }

  return (
    <Row noGutters className="showcase-row">
      <Col 
        lg={{ span: 6, order: order[0] }} 
        className={`text-white showcase-img ${extraClasses}`}
        style={imgStyle}
      >
      </Col>
      <Col lg={{ span: 6, order: order[1] }} className={`mt-3 mb-5 my-lg-auto px-1 px-lg-5 text-center ${textJustification}`}>
        <h2>{title}</h2>
        <p className="lead mb-0">{text}</p>
      </Col>
    </Row>
  );
};
