import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const BasicInfoItem = ({ icon, colour, title, text }) => {

  return (
    <div className="mx-auto my-5">
      <div className="d-flex mb-4">
        <FontAwesomeIcon 
          icon={icon} 
          size="lg" 
          className={`m-auto ${colour}`} 
          style={{ fontSize: '50pt' }}
        />
      </div>
      <h3>{title}</h3>
      <p className="lead mb-0">{text}</p>
    </div>
  );
};
