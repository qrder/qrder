import React from 'react';
import { Meteor } from 'meteor/meteor';
import { useHistory } from "react-router-dom";
import { Container, Button, Navbar as BootstrapNavbar } from 'react-bootstrap';
import { useTracker } from 'meteor/react-meteor-data';

export const Navbar = () => {
  const user = useTracker(() => Meteor.user());
  const history = useHistory();

  const logout = () => {
    Meteor.logout();
    history.push("/");
  }

  return (
    <BootstrapNavbar className="navbar navbar-light bg-light static-top" data-test="home-navbar">
      <Container>

        <BootstrapNavbar.Brand onClick={() => history.push("/")} className="pointer" role="button">
          <img src="/images/qrder-logo/qrder-name-logo.png" style={{height: "30px"}} className="" alt="QRDER.IO" />
        </BootstrapNavbar.Brand>

        { user ? (
          <span>
            <Button onClick={() => history.push("/dashboard/venues")} variant="warning" className="mr-2">
              Dashboard
            </Button>
            <Button onClick={logout}>Logout</Button>
          </span>
        ) : (
          <Button onClick={() => history.push("/login")}>Login</Button>
        )}

      </Container>
    </BootstrapNavbar>
  );
};
