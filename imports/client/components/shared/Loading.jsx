import React from 'react';
import { Container, Row, Col, Spinner } from 'react-bootstrap';

export const Loading = ({ viewHeight = true }) => {
  
  return ( 
    <Container className={`${viewHeight ? 'vh-100' : ''}`}>
      <Row className={`${viewHeight ? 'vh-100 align-items-center' : ''} text-center`}>
        <Col>
          <Spinner animation="border" role="status" variant="primary">
            <span className="sr-only">Loading...</span>
          </Spinner>
          <br/>
          <span>Loading...</span>
        </Col>
      </Row>
    </Container>
  );
};
