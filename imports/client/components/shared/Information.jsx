import React from 'react';

export const Information = ({ message }) => {
  
  return ( 
    <>
      {
        message == undefined || message == '' ? '' :
        <p className="text-info">{message}</p>
      }
    </>
  );
};
