import React from 'react';

export const Error = ({ message }) => {
  
  return ( 
    <>
      {
        message == undefined || message == '' ? '' :
        <p className="text-danger">{message}</p>
      }
    </>
  );
};
