import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

/**
 * Handle when a customer clicks the link in the reset password email 
 */
Accounts.onResetPasswordLink((token, done) => {
  Session.set("resetPasswordToken", JSON.stringify(token));
  // TODO: what do I do with done here?
  location.replace(`${Meteor.settings.public.url}/reset-password`);
});


/**
 * Handle email verification link click by verifying email in db
 * & logging user in
 */
Accounts.onEmailVerificationLink((token, done) => {
  Accounts.verifyEmail(token, () => {
    location.replace(`${Meteor.settings.public.url}/dashboard/venues`);
  });
  done();
});