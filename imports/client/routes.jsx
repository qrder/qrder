import React, { Suspense, lazy } from 'react';
import history from "./history";
import { Router, Route, Switch } from 'react-router-dom';
import { GlobalProvider } from "/imports/context/GlobalState";
// Lazy load layouts
const DashboardLayout = lazy(() => import("./layouts/DashboardLayout"));
const CustomerLayout = lazy(() => import("./layouts/CustomerLayout"));
const HomeFormsLayout = lazy(() => import("./layouts/HomeFormsLayout"));
// Lazy load home page
const Home = lazy(() => import("./pages/home/Home"));
const Login = lazy(() => import("./pages/home/Login"));
const Register = lazy(() => import("./pages/home/Register"));
const Privacy = lazy(() => import("./pages/home/Privacy"));
const ForgottenPassword = lazy(() => import("./pages/home/ForgottenPassword"));
const ResetPassword = lazy(() => import("./pages/home/ResetPassword"));
// Lazy load dashboard pages
const Venues = lazy(() => import("./pages/dashboard/Venues"));
const Menu = lazy(() => import("./pages/dashboard/Menu"));
const OptionGroups = lazy(() => import("./pages/dashboard/OptionGroups"));
const Orders = lazy(() => import("./pages/dashboard/Orders"));
const Customisation = lazy(() => import("./pages/dashboard/Customisation"));
const Tables = lazy(() => import("./pages/dashboard/Tables"));
const Settings = lazy(() => import("./pages/dashboard/Settings"));
const LiveChat = lazy(() => import("./pages/dashboard/LiveChat"));
// Lazy load customer pages
const CustomerMenu = lazy(() => import("./pages/customer/CustomerMenu"));
const CustomerOrder = lazy(() => import("./pages/customer/CustomerOrder"));
const CustomerInformation = lazy(() => import("./pages/customer/CustomerInformation"));
const CustomerPaymentOptions = lazy(() => import("./pages/customer/CustomerPaymentOptions"));
const CustomerPaymentInformation = lazy(() => import("./pages/customer/CustomerPaymentInformation"));
const CustomerConfirmation = lazy(() => import("./pages/customer/CustomerConfirmation"));
const CustomerNoOrders = lazy(() => import("./pages/customer/CustomerNoOrders"));
// Lazy load error pages
const Error404 = lazy(() => import("./pages/errors/Error404"));
// Loading spinner
import { Loading } from '/imports/client/components/shared/Loading';

export const Routes = () => {

  return (
    <Suspense fallback={<Loading />}>
      <GlobalProvider>
        <Router history={history}>
          <Switch>

            <Route exact path="/" component={Home} />
            <Route exact path="/privacy" component={Privacy} />
            
            <Route exact path={["/login", "/register", "/forgotten-password", "/reset-password"]}>
              <HomeFormsLayout>
                <Switch>
                  <Route exact path="/login" component={Login}/>
                  <Route exact path="/register" component={Register}/>
                  <Route exact path="/forgotten-password" component={ForgottenPassword}/>
                  <Route exact path="/reset-password" component={ResetPassword}/>
                </Switch>
              </HomeFormsLayout>
            </Route>

            <Route exact 
              path={[ 
                "/dashboard/settings", "/dashboard/venues",, "/dashboard/menu",  
                "/dashboard/menu/options", "/dashboard/orders", "/dashboard/tables",
                "/dashboard/live-chat", "/dashboard/customise"
              ]}>
              <DashboardLayout>
                <Switch>
                  <Route exact path='/dashboard/settings' component={Settings} />
                  <Route exact path='/dashboard/venues' component={Venues} />
                  <Route exact path='/dashboard/menu' component={Menu} />
                  <Route exact path='/dashboard/menu/options' component={OptionGroups} />
                  <Route exact path='/dashboard/orders' component={Orders} />
                  <Route exact path='/dashboard/customise' component={Customisation} />
                  <Route exact path='/dashboard/tables' component={Tables} />
                  <Route exact path='/dashboard/live-chat' component={LiveChat} />
                </Switch>
              </DashboardLayout>
            </Route>

            {/* TODO: Validation on URL so that people don't navigate to random tableId's */}
            <Route exact path='/menu/:path?/:path?/:path?/:path?/:path?'>
              <CustomerLayout>
                <Switch>
                  <Route exact path="/menu/:venueName/:venueId/:tableName/:tableId" component={CustomerMenu} />
                  <Route exact path="/menu/:venueName/:venueId/:tableName/:tableId/order" component={CustomerOrder} />
                  <Route exact path="/menu/:venueName/:venueId/:tableName/:tableId/information" component={CustomerInformation} />
                  <Route exact path="/menu/:venueName/:venueId/:tableName/:tableId/payment-options" component={CustomerPaymentOptions} />
                  <Route exact path="/menu/:venueName/:venueId/:tableName/:tableId/payment-information" component={CustomerPaymentInformation} />
                  <Route exact path="/menu/:venueName/:venueId/:tableName/:tableId/confirmation" component={CustomerConfirmation} />
                  <Route exact path="/menu/:venueName/:venueId/:tableName/:tableId/no-orders" component={CustomerNoOrders} />
                </Switch>
              </CustomerLayout>
            </Route>

            <Route path="*" component={Error404} />

          </Switch>
        </Router>
      </GlobalProvider>
    </Suspense>
  )
}