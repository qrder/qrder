import React, { useState } from 'react';
import { NextStepButton } from '/imports/client/components/customer/NextStepButton';
import { PaymentOptions } from '/imports/client/components/customer/PaymentOptions';
import { total } from '/imports/utils/cart';
import { ExtraScroll } from '/imports/client/components/customer/ExtraScroll';

export default () => {
  // Next page is dependent on which payment option is selected.
  // Cash & card machine go to confirmation
  // Pay now / card online go to payment information page to get card details
  // Default to confirmation page in case things go wrong (worst case scenario, an order is made but not paid for)
  const [nextPage, setNextPage] = useState('confirmation');
  const [buttonText, setButtonText] = useState('SUBMIT ORDER');
  const [anOptionHasBeenSelected, setAnOptionHasBeenSelected] = useState(false);

  // When radio buttons selected, set state of next page
  const handlePaymentOptionChange = (type) => {
    setAnOptionHasBeenSelected(true);

    switch(type) {
      case 'cash':
        setNextPage('confirmation');
        setButtonText('SUBMIT ORDER');
        break;
      case 'cardMachine':
        setNextPage('confirmation');
        setButtonText('SUBMIT ORDER');
        break;
      case 'cardOnline':
        setNextPage('payment-information');
        setButtonText('NEXT');
        break;
      default:
        setNextPage('confirmation');
        setButtonText('SUBMIT ORDER');
    }
  }
  
  return (
    <div className="text-center">
      <div className="mt-5">
        <h4 className="font-weight-normal" style={{letterSpacing: '3px'}}>TOTAL</h4>
        {/* TODO: Dynamic symbol based on client config */}
        <h1 className="font-weight-normal" style={{letterSpacing: '5px'}}>£{total().toFixed(2)}</h1>
      </div>

      <div className="mt-5">
        <h5 className="font-weight-normal">Payment Options</h5>
        <span className="small">Please Select an Option</span>

        <PaymentOptions onPaymentOptionChange={(type) => handlePaymentOptionChange(type)} />
      </div>

      <ExtraScroll />
      
      <NextStepButton linkTo={nextPage} buttonText={buttonText} disabled={!anOptionHasBeenSelected} />
    </div>
  )
};