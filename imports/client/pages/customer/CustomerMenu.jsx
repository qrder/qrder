import React, { useEffect, useState } from 'react';
import { Session } from 'meteor/session';
import { Tracker } from 'meteor/tracker';
import { useHistory } from "react-router-dom";
import { Menu } from '/imports/client/components/customer/Menu';
import { Container, Row, Col } from 'react-bootstrap';
import { isEmpty } from '/imports/utils/cart';
import { AddItemToCartModal } from '/imports/client/components/customer/modals/AddItemToCartModal';
import { NextStepButton } from '/imports/client/components/customer/NextStepButton';
import { ExtraScroll } from '/imports/client/components/customer/ExtraScroll';
import { getVenue, setDefaultSessionVariables, venueNotAcceptingOrders, getBaseUrl } from '/imports/utils/customers';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import menusAndPaymentOptionsByVenue from '/imports/db/menu/query/menusAndPaymentOptionsByVenue';

const CustomerMenu = ({ data, error, isLoading }) => {
  if(error) console.log(error);

  const history = useHistory();

  const [addItemToCartModalShow, setAddItemToCartModalShow] = useState(false);
  const [currentData, setCurrentData] = useState({}); // passed into modal

  // Set session defaults
  useEffect(() => {
    Session.set('loaded', true);

    if(data) {
      setDefaultSessionVariables(data.paymentOptions, data.settings);
      if(venueNotAcceptingOrders(data.isUnderActiveSubscription, data.isAcceptingOrders)) {
        history.push(`${getBaseUrl()}/no-orders`);
      }
    }
  }, [isLoading, data]);

  // Handle button clicked event from any 'Add' clicks
  const onButtonClickedHandler = (buttonClicked, data) => {    
    setCurrentData(data);

    switch(buttonClicked) {
      case 'add-item-to-cart':
        setAddItemToCartModalShow(true);
        break;
    }
  };

  if(isLoading) return 'Loading...';

  return (
    <>
      <Container className="text-center py-1 text-secondary">
        <Row className="small">
          <Col>V (Vegetarian)</Col>
          <Col>Ve (Vegan)</Col>
          <Col>F (Fish)</Col>
        </Row>
        <Row className="small">
          <Col>Gf (Gluten Free)</Col>
          <Col>Alc (Alcohol)</Col>
        </Row>
      </Container>
      
      <Menu menus={data?.menus} onButtonClick={(buttonClicked, data) => onButtonClickedHandler(buttonClicked, data)} />

      <ExtraScroll />

      <NextStepButton linkTo="order" buttonText="VIEW ORDER" disabled={isEmpty()} />
      

      <AddItemToCartModal show={addItemToCartModalShow} onHide={() => setAddItemToCartModalShow(false)} item={currentData} />
    </>
  )
};

// Create a container for the customer menu page so that we can pass props into the withQuery
// (otherwise props would need be passed in through routes.jsx)
const CustomerMenuContainer = withQuery(({ venueId }) => {
  return menusAndPaymentOptionsByVenue.clone({ venueId });
}, {reactive: false, single: true})(CustomerMenu);

// Wrap container so it exports as CustomerMenu and rerender when loaded
export default function({ children }) {
  const [venue, setVenue] = useState('');

  // Update venue id when page has fully loaded.
  Tracker.autorun(() => {
    if(Session.equals('loaded', '' || undefined)) return;

    if(getVenue('id') !== venue) setVenue(getVenue('id'));
  });

  // venueId refers to {venueId} in withQuery
  return (
    <CustomerMenuContainer venueId={venue}>
      {children}
    </CustomerMenuContainer>
  );
}