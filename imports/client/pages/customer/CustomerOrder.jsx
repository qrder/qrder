import React, { useState } from 'react';
import { NextStepButton } from '/imports/client/components/customer/NextStepButton';
import { Order } from '/imports/client/components/customer/Order';
import { RemoveItemFromCartModal } from '/imports/client/components/customer/modals/RemoveItemFromCartModal';
import { isEmpty } from '/imports/utils/cart';
import { ExtraScroll } from '/imports/client/components/customer/ExtraScroll';

export default () => {
  const [removeItemFromCartModalShow, setRemoveItemFromCartModalShow] = useState(false);
  const [currentData, setCurrentData] = useState({});

  // Handle button clicked event from any 'Add' clicks
  const cartChangeHandler = (task, data) => {    
    setCurrentData(data);

    switch(task) {
      case 'remove-item-from-cart':
        setRemoveItemFromCartModalShow(true);
        break;
    }
  };
  
  return (
    <>
      <Order onQuantityZero={(item) => cartChangeHandler("remove-item-from-cart", item)} />

      <ExtraScroll />
      
      {
        isEmpty() ? 
        <NextStepButton linkTo="menu" buttonText="VIEW MENU" />
        :
        <NextStepButton linkTo="information" buttonText="NEXT" />
      }

      <RemoveItemFromCartModal show={removeItemFromCartModalShow} onHide={() => setRemoveItemFromCartModalShow(false)} item={currentData} />
    </>
  )
};