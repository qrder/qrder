import React, { useState, useEffect } from 'react';
import { Meteor } from "meteor/meteor";
import { PaymentInformationForm } from '/imports/client/components/customer/PaymentInformationForm';
import { useStripe, useElements, CardNumberElement, Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { NextStepButton } from '/imports/client/components/customer/NextStepButton';
import { Button } from 'react-bootstrap';
import { total } from '/imports/utils/cart';
import { useHistory } from "react-router-dom";
import { Error } from '/imports/client/components/shared/Error';
import { getVenue, getBaseUrl, cardOnlineIsAcceptedPaymentOption } from '/imports/utils/customers';
import { createBillingDetailsObject } from '/imports/utils/stripe';
import { ExtraScroll } from '/imports/client/components/customer/ExtraScroll';

// Payment information page
const CustomerPaymentInformation = () => {
  const [errorMessage, setErrorMessage] = useState('');

  const history = useHistory();

  // form inputs
  const [name, setName] = useState('');
  const [line1, setLine1] = useState('');
  const [line2, setLine2] = useState('');
  const [city, setCity] = useState('');
  const [postcode, setPostcode] = useState('');

  // stripe related
  const stripe = useStripe();
  const elements = useElements();
  const [clientSecret, setClientSecret] = useState('');

  // On page load, create a Stripe payment intent
  useEffect(() => {
    Meteor.call("orders.createCustomerPaymentIntent", getVenue('id'), total(), (error, result) => {
      if(error) {
        // TODO: Handle better
        console.log(error)
      } else {
        // set client secret for use when form is submitted
        setClientSecret(result);
      }
    });
  }, []);

  // handle submission
  const handleSubmit = async () => {
    // Prevent form submission until stripe has loaded
    if (!stripe || !elements) return;
    
    const result = await stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: elements.getElement(CardNumberElement),
        billing_details: createBillingDetailsObject(name, city, line1, line2, postcode)
      }
    });

    if (result.error) {
      setErrorMessage(result.error.message);
    } else {
      if (result.paymentIntent.status === 'succeeded') {
        history.push(`${getBaseUrl()}/confirmation`, { pi: result.paymentIntent.id });
        // NOTE: Setting the 'paidFor' property on the order is handled with webhooks
      }
    }
  }

  if(errorMessage) {
    return (
      <div className="text-center mt-3">
        <h3 className="font-weight-normal">Payment Information</h3>

        <Error message={errorMessage} />
        <Button variant="outline-dark" size="sm" onClick={() => setErrorMessage('')}>Try Again</Button>
      </div>
    )
  }

  return (
    <div className="text-center mt-3">
      <h3 className="font-weight-normal">Payment Information</h3>

      <PaymentInformationForm 
        nameChange={e => setName(e)} 
        lineOneChange={e => setLine1(e)} 
        lineTwoChange={e => setLine2(e)} 
        cityChange={e => setCity(e)} 
        postcodeChange={e => setPostcode(e)}
      />

      <ExtraScroll />
      
      {/* The actual page navigation is handled in the handleSubmit foo */}
      <NextStepButton linkTo="confirmation" buttonText="CONFIRM PAYMENT INFORMATION" fake={true} clickEvent={handleSubmit} />
    </div>
  )
};

// Wrapper for the CustomerPaymentInformation component as <Elements/> needs to be in a different component to useStripe
export default function() {
  const [stripePromise, setStripePromise] = useState(null);

  // Create stripe promise for use in Elements component
  // NOTE: We're advised against using loadStripe inside a component but I couldn't get the users' stripeAccId otherwise
  useEffect(() => {
    if(getVenue('id') && !stripePromise) {
      if(cardOnlineIsAcceptedPaymentOption()) {
        Meteor.call("venues.getOwnersStripeAccId", getVenue('id'), (error, result) => {
          if(error) {
            // TODO: handle better
            console.log(error);
          } else {
            setStripePromise(loadStripe(Meteor.settings.public.stripe.publishable_key, {
              stripeAccount: result
            }));
          }
        });
      }
    }
  }, []);

  if(cardOnlineIsAcceptedPaymentOption() && !stripePromise) return 'Loading...';

  return (
    <Elements stripe={stripePromise}>
      <CustomerPaymentInformation />
    </Elements>
  )
}