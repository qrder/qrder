import React, { useEffect, useState } from 'react';
import { Meteor } from "meteor/meteor";
import { Session } from 'meteor/session';
import { useHistory } from "react-router-dom";
import { total, destroy } from '/imports/utils/cart';
import { Error } from '/imports/client/components/shared/Error';
import { NextStepButton } from '/imports/client/components/customer/NextStepButton';
import { Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle as SolidCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { ExtraScroll } from '/imports/client/components/customer/ExtraScroll';
import { customDecode } from '/imports/utils/shared';
import { getTable, getVenue, getEmail, createOrderObject, getBaseUrl, clearSession, orderHasBeenSubmitted, getOrderSubmitted, getComments, getChosenPaymentType } from '/imports/utils/customers';

export default (props) => {
  const [serverError, setServerError] = useState('');
  const [price, setPrice] = useState(total());
  const [orderNo, setOrderNo] = useState('');

  const history = useHistory();

  // On page load, insert order into database and display order information.
  useEffect(() => {
    // Ensure same order does not get submitted multiple times 
    if(!orderHasBeenSubmitted()) {
      const order = createOrderObject(props.history.location.state?.pi);

      Meteor.call('orders.insert', order, (error, result) => {
        if(error) {
          setServerError(error.reason);
          Meteor.call('orders.sendErrorEmail', getEmail(), getVenue('name'));
        } else if(!result) {
          // If no orderId is returned, then the order probably failed (even if we do not know why)
          setServerError('Sorry, an error occurred. The order could not be submitted.');
          Meteor.call('orders.sendErrorEmail', getEmail(), getVenue('name'));
        } else {
          // Set orderSubmitted to true & its orderNo so it does not get resubmitted
          Session.setAuth('orderSubmitted', JSON.stringify({id: result, price: total(), submitted: true}));
          setOrderNo(result);
          // Send confirmation email
          Meteor.call('orders.sendConfirmationEmail', getEmail(), getVenue('name'), total(), result);
          // destroy cart & reset cart qty session var (use for cart icon) ready for customers next order
          destroy();
          Session.setAuth('cartQty', 0);
        }
      });
    }
  }, []);

  // Handle 'return to menu' button click manually as need to clear session
  const handleClick = () => {
    clearSession();
    history.push(getBaseUrl()); // back to menu
  }
  
  return (
    <div className="text-center mt-3">
      <FontAwesomeIcon icon={SolidCheckCircle} size="2x" className="text-success" />
      <h2 className="font-weight-light">Thank You!</h2>
      
      <hr className="my-4"/>
      
      <h5 className="font-weight-normal my-2">Order Confirmed</h5>
      <h3 className="font-weight-light">#{orderNo || getOrderSubmitted('id')}</h3>

      {
        Session.get('confirmationMessage') && (
          <>
            <hr className="my-4"/>
            <p className="px-3">{Session.get('confirmationMessage')}</p>
          </>
        )
      }

      <hr className="my-4"/>

      <h5 className="font-weight-normal my-2">Email Sent</h5>
      <p className="px-3">
        An email has been sent to {getEmail()}.  <br/>
        <strong>Please check this to ensure your order was submitted successfully.</strong><br/>
        If you cannot see an email, please check your Junk folder.
      </p>

      <hr className="my-4"/>

      <h5 className="font-weight-normal mb-3">Order Details:</h5>
      <Container>
        <Row>
          <Col className="text-right">
            Table <br/>
            Venue <br/>
            Payment Type <br/>
            Total Price <br/>
          </Col>
          <Col className="text-left">
            {customDecode(getTable('name'))} <br/>
            {getVenue('name')} <br/>
            {getChosenPaymentType()} <br/>
            {/* 
              State needs to be used on first load as the session variable does not get created until first load.
              Session variable needs to be used after first load as state does not work if refreshed.
              Same for orderNo above
            */}
            £{price !== 0.00 ? price.toFixed(2) : Number(getOrderSubmitted('price')).toFixed(2)} <br/>
          </Col>
        </Row>
        <Row>
          <Col className="text-center mt-2">
            Comments <br/>
            {getComments()}
          </Col>
        </Row>
      </Container>

      <Error message={serverError} />

      <ExtraScroll />

      {/* The actual page navigation is handled in the handleClick foo */}
      <NextStepButton linkTo="menu" buttonText="RETURN TO MENU" fake={true} clickEvent={handleClick} />
    </div>
  )
};