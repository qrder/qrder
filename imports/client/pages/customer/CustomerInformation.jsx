import React, { useState } from 'react';
import { NextStepButton } from '/imports/client/components/customer/NextStepButton';
import { Form, Container } from 'react-bootstrap';
import { validEmail } from "/imports/utils/shared";
import { ExtraScroll } from '/imports/client/components/customer/ExtraScroll';

export default () => {
  const [isEmailValid, setIsEmaiValid] = useState(false);

  // Update session with users email address
  const handleEmailChange = (email) => {
    if(validEmail(email)) setIsEmaiValid(true);
    const info = JSON.parse(Session.get('information'));
    info.email = email;
    Session.set('information', JSON.stringify(info));
  }

  // Update session with users order comments
  const handleCommentsChange = (comments) => {
    const info = JSON.parse(Session.get('information'));
    info.comments = comments;
    Session.set('information', JSON.stringify(info));
  }

  return (
    <div className="text-center">
      
      <Container className="mt-5">
        <Form>

          <Form.Group controlId="email">
            <h4 className="font-weight-normal">
              <Form.Label>
                Email<span className="text-danger">*</span>
              </Form.Label>
            </h4>
            <Form.Control 
              type="email" 
              placeholder="Enter Email" 
              onChange={(e) => handleEmailChange(e.target.value)}
            />
            <Form.Text className="text-muted">
              This is used to send an order confirmation to you.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="comments" className="mt-4">
            <h4 className="font-weight-normal">
              <Form.Label>
                Comments
              </Form.Label>
            </h4>
            <Form.Control 
              as="textarea" 
              rows={7} 
              placeholder="Optional Comments"
              onChange={(e) => handleCommentsChange(e.target.value)}
            />
          </Form.Group>

        </Form>
      </Container>

      <ExtraScroll />
      
      <NextStepButton linkTo="payment-options" buttonText="NEXT" disabled={!isEmailValid} />
    </div>
  )
};