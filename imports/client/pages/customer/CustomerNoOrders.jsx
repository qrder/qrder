import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default () => {
  
  return (
    <Container className="d-flex align-items-center justify-content-center" style={{minHeight: '100vh'}}>
      <Row>
        <Col className="text-center">
          <h1>Venue Not Accepting Orders.</h1>

          <p>Sorry, this venue is not currently accepting orders.</p>
        </Col>
      </Row>
    </Container>
  )
};