import { Meteor } from 'meteor/meteor';
import React, { useState, useEffect } from 'react';
import { Error } from '/imports/client/components/shared/Error';
import { Row, Col, Button, Form } from 'react-bootstrap';
import { validEmail } from "/imports/utils/shared";

export default () => {
  const [disabled, setDisabled] = useState(true);
  const [validated, setValidated] = useState(false);
  const [email, setEmail] = useState("");
  const [emailSent, setEmailSent] = useState(false);
  const [serverError, setServerError] = useState('');

  // Enable button when a valid email address is entered
  useEffect(() => {
    validEmail(email) ? setDisabled(false) : setDisabled(true);
  }, [email]);

  const submit = (e) => {
    e.preventDefault();
    // split form from event
    const form = e.currentTarget;
    let valid = false;
    // Check form inputs (Bootstraps built in validation)
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      valid = true;
    }
    // State change to rerender form with errors / ticks
    setValidated(true);
    // If form valid, send reset password email
    if(valid) {
      Meteor.call("users.sendResetPasswordEmail", email, error => {
        error ? setServerError(error) : setEmailSent(true);
      });
    }
  }

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
    setServerError('');
  }

  if(emailSent) {
    return (
      <div data-test="forgot-password-email-sent">
        <h3 className="mb-4 font-weight-light">Forgotten Password</h3>

        <p>An email has been sent to {email}.</p>  
        <p>Please click the link in the email to reset your password.</p>
      </div>
    )
  }

  return (
    <>
      <h3 className="mb-4 font-weight-light">Forgotten Password</h3>

      <Form noValidate validated={validated} onSubmit={submit} data-test="forgot-password-form">

        <Form.Group>
          <Form.Control 
            required
            autoFocus
            name="email"
            type="email" 
            placeholder="Enter Email" 
            className="p-4 rounded-inputs"
            onChange={e => handleEmailChange(e)}
          />
        </Form.Group>

        <Row>
          <Col className="text-center">
            <Error message={serverError} />
          </Col>
        </Row>

        <Row className="text-right">
          <Col>
            <Button type="submit" disabled={disabled}>Send Password Reset Email</Button>                      
          </Col>
        </Row>

      </Form>
    </>
  );
};
