import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { Helmet } from "react-helmet";
import { Error } from '/imports/client/components/shared/Error';
import { useHistory } from "react-router-dom";
import { Row, Col, Button, Form } from 'react-bootstrap';

export default () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [validated, setValidated] = useState(false);
  const [serverError, setServerError] = useState('');

  const history = useHistory();

  // Ensure server error message is not shown after modal close
  useEffect(() => {
    setServerError('');
  }, []);

  useEffect(() => {
    if(Meteor.userId()) history.push("/dashboard/venues");
  }, []);

  const submit = (e) => {
    e.preventDefault();
    // Remove whitespace from form fields
    e.target.username.value = e.target.username.value.trim();
    e.target.password.value = e.target.password.value.trim();
    // split form from event
    const form = e.currentTarget;
    let valid = false;
    // Check form inputs (Bootstraps built in validation)
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      valid = true;
    }
    // State change to rerender form with errors / ticks
    setValidated(true);

    if(valid) {
      Meteor.loginWithPassword(username, password, error => {
        if(error) {
          setServerError(error.reason);
        } else {
          history.push('/dashboard/venues');
        }
      });
    }
  };

  return (
    <>
      <Helmet>
        <title>QRDER: Login</title>
        <link rel="canonical" href="https://www.qrder.io/login" />
      </Helmet>

      <h3 className="mb-4 font-weight-light">Login</h3>

      {/* noValidate prevents different browsers styles for validation errors */}
      <Form noValidate validated={validated} onSubmit={submit} data-test="login-form">

        <Form.Group>
          <Form.Control 
            required
            autoFocus
            name="username"
            type="text" 
            placeholder="Username" 
            className="p-4 rounded-inputs"
            onChange={e => setUsername(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter a username.
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group>
          <Form.Control 
            required
            name="password"
            type="password" 
            placeholder="Password" 
            className="p-4 rounded-inputs"
            onChange={e => setPassword(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter a password.
          </Form.Control.Feedback>
        </Form.Group>

        <Row>
          <Col className="text-center">
            <Error message={serverError} />
          </Col>
        </Row>

        <Row>
          <Col>
            <span>No Account?  </span>
            <a href="" onClick={() => history.push("/register")}>Register</a> 
          </Col>
          <Col className="text-right">
            <Button variant="link" onClick={() => history.push("/forgotten-password")}>
              Forgot Password?
            </Button>
            <Button type="submit">Login</Button>
          </Col>
        </Row>

      </Form>
    </>
  );
};
