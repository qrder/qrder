import React from 'react';
import { Navbar } from '/imports/client/components/home/Navbar';
import { Header } from '/imports/client/components/home/Header';
import { BasicInfo } from '/imports/client/components/home/BasicInfo';
import { DetailInfo } from '/imports/client/components/home/DetailInfo';
import { Prices } from '/imports/client/components/home/Prices';
import { Contact } from '/imports/client/components/home/Contact';
import { Footer } from '/imports/client/components/home/Footer';
import CookieConsent from "react-cookie-consent";

export default () => {

  return (
    <>
      <Navbar />
      <Header />
      <BasicInfo />
      <DetailInfo />
      <Prices />
      <Contact />
      <Footer />
      <CookieConsent>This website uses cookies to enhance the user experience.</CookieConsent>
    </>
  );
}
