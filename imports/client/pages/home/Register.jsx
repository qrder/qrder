import React, { useState, useEffect } from 'react';
import { Session } from 'meteor/session';
import { Meteor } from 'meteor/meteor';
import { Helmet } from "react-helmet";
import { Error } from '/imports/client/components/shared/Error';
import { useHistory } from "react-router-dom";
import { Row, Col, Button, Form } from 'react-bootstrap';
import { Loading } from '/imports/client/components/shared/Loading';
import { createUserObject, passwordsMatch } from '/imports/utils/users';
import axios from 'axios';

export default (props) => {
  const [validated, setValidated] = useState(false);
  const [serverError, setServerError] = useState('');
  const [email, setEmail] = useState('');
  const [nonUkLocation, setNonUkLocation] = useState(false);
  const [loading, setLoading] = useState(true);

  const history = useHistory();
  
  useEffect(() => {
    // Ensure server error message is not shown after modal close
    setServerError('');
    // If email was entered into landing page form, pre-fill it here.
    if(props.history.location.state?.email) {
      setEmail(props.history.location.state.email);
    }

    checkLocation();
  }, []);

  // check location is UK
  const checkLocation = () => {
    axios.get('https://ipapi.co/json/').then((response) => {
      if(response.data.country !== "GB") setNonUkLocation(true);
      setLoading(false);
    }).catch((error) => {
      console.log(error);
    });
  };

  // TODO: Register takes a while to complete when submit is clicked,
  //        so would be good to provide feedback to user that is loading.
  //        Spinner on button? / Disable button click?
  // TODO: Refactor form to use Uniforms / SimplSchema?

  const submit = (e) => {
    e.preventDefault();
    setServerError('');
    // Remove whitespace from form fields
    e.target.username.value = e.target.username.value.trim();
    e.target.email.value = e.target.email.value.trim();
    e.target.password.value = e.target.password.value.trim();
    e.target.confirmPassword.value = e.target.confirmPassword.value.trim();
    // split form from event
    const form = e.currentTarget;
    let valid = false;
    // Check form inputs (basic HTML validation)
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      valid = true;
    }
    // State change to rerender form with errors / ticks
    setValidated(true);
    // Check passwords match
    if(!passwordsMatch(e)) {
      valid = false;
      setServerError("Passwords do not match.");
    }
    // Register user (qrder & stripe), create stripe subscription and log them in
    if(valid) {
      const user = createUserObject(e);
      Meteor.call('users.register', user, error => {
        if (error) {
          setServerError(error.reason);
        } else {
          Meteor.loginWithPassword(user.username, user.password, (err) => {
            if(err) {
              setServerError(err.reason);
            } else {
              Session.setAuth('loggedInForFirstTime', JSON.stringify(true));
              history.push("/dashboard/venues");
            }
          });
        }
      });
    }
  };

  if(loading) return <Loading />;

  if(nonUkLocation) {
    return (
      <>      
        <h3 className="mb-4 font-weight-light">Sorry</h3>
        <p>QRDER is not currently available outside of the UK.</p>
      </>
    )
  }
  
  return (
    <>
      <Helmet>
        <title>QRDER: Registration</title>
        <link rel="canonical" href="https://www.qrder.io/register" />
      </Helmet>

      <h3 className="mb-4 font-weight-light">Register</h3>

      {/* noValidate prevents different browsers styles for validation errors */}
      <Form noValidate validated={validated} onSubmit={submit} data-test="register-form">

        <Form.Group>
          <Form.Control 
            required
            autoFocus
            name="username"
            type="text" 
            placeholder="Username" 
            className="p-4 rounded-inputs"
          />
          <Form.Control.Feedback type="invalid">
            Please choose a username.
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group>
          <Form.Control 
            required
            name="email"
            type="email" 
            placeholder="Email" 
            className="p-4 rounded-inputs"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter an email address.
          </Form.Control.Feedback>
        </Form.Group>

        <hr/>

        <Form.Group>
          <Form.Control 
            required
            name="password"
            type="password" 
            placeholder="Password" 
            className="p-4 rounded-inputs"
          />
          <Form.Control.Feedback type="invalid">
            Please enter a password.
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group>
          <Form.Control 
            required
            name="confirmPassword"
            type="password" 
            placeholder="Confirm Password" 
            className="p-4 rounded-inputs"
          />
          <Form.Control.Feedback type="invalid">
            Please confirm your password.
          </Form.Control.Feedback>
        </Form.Group>

        <hr/>

        <Row>
          <Col className="text-center">
            <Error message={serverError} />
          </Col>
        </Row>

        <Row>
          <Col className="text-right">
            <Button variant="link" onClick={() => history.push("/login")}>
              Login
            </Button>
            <Button type="submit">Register</Button>
          </Col>
        </Row>

      </Form>
    </>
  );
};
