import React, { useState } from 'react';
import { Session } from 'meteor/session';
import { Accounts } from 'meteor/accounts-base';
import { Error } from '/imports/client/components/shared/Error';
import { Row, Col, Button, Form } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import { passwordsMatch } from '/imports/utils/users';

export default () => {
  const [validated, setValidated] = useState(false);
  const [serverError, setServerError] = useState('');

  const history = useHistory();

  const submit = (e) => {
    e.preventDefault();
    // Split form from event 
    const form = e.currentTarget;
    let valid = false;
    // Check form inputs (Bootstraps built in validation)
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      valid = true;
    }
    // State change to rerender form with errors / ticks
    setValidated(true);
    // Check passwords match
    if(!passwordsMatch(e)) {
      valid = false;
      setServerError("Passwords do not match.");
    }
    // Register user (qrder & stripe), create stripe subscription and log them in
    if(valid) {
      Accounts.resetPassword(
        JSON.parse(Session.get('resetPasswordToken')), 
        e.target.password.value, 
        (error) => {
          if(error) {
            setServerError(error.reason)
          } else {
            Session.set('resetPasswordToken', JSON.stringify(""))
            history.push("/dashboard/venues");
          }
        }
      );
    }
  }

  return (
    <>      
      <h3 className="mb-4 font-weight-light">Reset Password</h3>

      {/* noValidate prevents different browsers styles for validation errors */}
      <Form noValidate validated={validated} onSubmit={submit}>

        <Form.Group>
          <Form.Control 
            required
            name="password"
            type="password" 
            placeholder="Password" 
            className="p-4 rounded-inputs"
          />
        </Form.Group>

        <Form.Group>
          <Form.Control 
            required
            name="confirmPassword"
            type="password" 
            placeholder="Confirm Password" 
            className="p-4 rounded-inputs"
          />
        </Form.Group>

        <hr/>

        <Row>
          <Col className="text-center">
            <Error message={serverError} />
          </Col>
        </Row>

        <Row>
          <Col className="text-right">
            <Button type="submit">Reset Password</Button>
          </Col>
        </Row>

      </Form>
    </>
  );
};
