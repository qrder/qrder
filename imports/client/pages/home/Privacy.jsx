import React from 'react';
import { Navbar } from '/imports/client/components/home/Navbar';
import { Footer } from '/imports/client/components/home/Footer';
import { Container } from 'react-bootstrap';

export default () => {

  return (
    <Container data-test="privacy-policy">
      <Navbar />

      <h1 className="mt-5">Privacy Policy</h1>
      <hr/>

      <p>
        <strong>
          QRDER takes your personal information seriously and we’ve created this privacy policy to make sure you’re aware of what information we are collecting, why we are collecting it and how we’re using it. 
          We have also detailed the cookies that we use and why we need them. If you have any questions, please contact us at privacy@qrder.io.
        </strong>
      </p>

      <br/>

      <h3>Terminology</h3>
      <p><strong>User : </strong>A QRDER customer who has registered directly with us on <a href="https://www.qrder.io" target="_blank">www.qrder.io</a>.</p>
      <p><strong>Customer : </strong>An individual who is submitting an order or browsing the menu of a QRDER user.</p>

      <br/>

      <h2>What Information We <u>Collect & Store</u></h2>
      <p>
        We may collect, store and use the following kinds of personal information:
        <br/>
        <ul>
          <li>Information about how you use this website (including your the length of your visit, page views and website navigation paths).</li>
          <li>Information that you provide to us for the purpose of registrating an account (including your name and email address).</li>
          <li>Information relating to any purchases you make on our website (including your name, email address and product information).</li>
          <li>Information relating to your browsing history on this website.</li>
        </ul>
      </p>

      <br/>

      <h2>How This Information Will Be Used</h2>
      <p>
        We will hold and use this information for the following purposes:
        <br/>
        <ul>
          <li>Customer service queries – information may be used to assist you with any enquries you may have.</li>
          <li>If you are a <i>user</i> you will receive email(s) when you request a password reset or email verification.  If you are a <i>customer</i>, you will receive email(s) confirming your order information.</li>
          <li>Information about your use of the website is used for analytical purposes so that your experience with QRDER can be improved.</li>
        </ul>
      </p>

      <br/>

      <h2>What Information We <u>Collect & Do Not Store</u></h2>
      <p>
        We may collect the following kinds of personal information but we do store it:
        <br/>
        <ul>
          <li>Information relating to any purchases you make on our website (including your payment details).</li>
        </ul>

        Why do we collect this data but do not store it? <br/>
        QRDER partners with Stripe to provide a secure payment service.  Payment information is handled by and stored on Stripe's platform.
      </p>

      <br/>

      <h2>How This Information Will Be Used</h2>
      <p>
        We will use this information for the following purposes:
        <br/>
        <ul>
          <li>If you are a <i>customer</i>, your payment details are used to pay for an order from the <i>users</i> menu.</li>
          <li>If you are a <i>customer</i>, your details may be used for fraud checking before and during order processing.</li>
        </ul>
      </p>

      <br/>

      <h2>Security</h2>
      <p>Your account is protected by a username and password.  All information is stored securely on our servers.</p>
      <p>We use JavaScript to speed up your online experience. You will need to ensure JavaScript is enabled to allow you to effectively use the site.</p>
      <p>QRDER partners with Stripe to provide a secure payment service.  QRDER does not store payment information.  Payment information is stored by Stripe.</p>

      <br/>

      <h2>Data Sharing</h2>
      <p>
        Your personal information may, for purposes detailed in this policy, be disclosed to the following:
        <br/>
        <ul>
          <li>Company employees</li>
          <li>Third parties (such as Stripe)</li>
        </ul>
      </p>

      <br/>

      <h2>Email Communications</h2>
      <p>
        Emails are used for account management and order confirmations.  
        If you would like to opt out of receiving these emails at any time, you can click the ‘Unsubscribe’ button at the bottom of the email. 
        <br/>
        <strong>WARNING: </strong> Unsubscribing from account management or order confirmation emails may affect your experience with QRDER.
      </p>

      <br/>

      <h2>Cookies</h2>
      <p>Cookies are small pieces of information that are stored by your browser on your computer's hard drive. It is necessary to store cookies in order to recognise and retrieve your individual user profiles.</p>
      <p>
        Cookies can take 2 forms:
        <br/>
        <ul>
          <li><strong>Persistent : </strong> Stored by the browser and remain until they expire (unless explicitly deleted).</li>
          <li><strong>Session : </strong> Stored by the browser but are deleted when the browser is closed.</li>
        </ul>
        Cookies do not directly contain any personal information but may be linked to information that is stored in cookies.
        <br/><br/>
        We use cookies for the following reasons:
        <br/>
        <ul>
          <li>Site administration</li>
          <li><i>Customer</i> order processsing</li>
          <li><i>User</i> subscription management</li>
          <li>Monitoring and website activity</li>
          <li>Research and development</li>
          <li>Third party financial services</li>
        </ul> 
      </p>

      <br/>

      <h2>Changes to our Privacy Policy</h2>
      <p>Any future changes made to our Privacy Policy will be posted to this page. If changes are particularly relevant to you, they will be emailed.</p>

      <br/>

      <h2>Third Party Sites</h2>
      <p>
        We are not responsible for the Privacy Policies or practises of third party websites, even if they were accessed from links on this website. 
        Always make sure to check any privacy policies of third party websites before disclosing any personal details.
      </p>

      <br/>

      <h2>Contact Us</h2>
      <p>
        Email us : privacy@qrder.io.
      </p>

      <Footer />
    </Container>
  );
};
