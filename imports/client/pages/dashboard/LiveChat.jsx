import React, { useState, useEffect } from 'react';
import { Container, Image } from 'react-bootstrap';
import { Loading } from '/imports/client/components/shared/Loading';

export default () => {
  const [widgetImage, setWidgetImage] = useState("mobile-chat-widget-loading.png");
  const [disabled, setDisabled] = useState(true);

  const handleClick = () => {
    if(!disabled) {
      setWidgetImage("mobile-chat-widget-tap.png");
      window.HubSpotConversations.widget.open();
    }
  }

  // When HubSpot script has loaded and been added DOM, update chat button
  useEffect(() => {
    const intervalId = setInterval(() => {
      if(window.HubSpotConversations && document.querySelector("#hubspot-messages-iframe-container")) {
        setWidgetImage("mobile-chat-widget.png");
        setDisabled(false);
        clearInterval(intervalId);
      }
    }, 200)
  }, []);
  
  return (
    <Container>
      <h2 className="mb-4">Live Chat</h2>

      {
        widgetImage == "mobile-chat-widget-loading.png" ? (
          <Loading viewHeight={false} />
        ) : (
          <Image 
            src={`/images/hubspot/${widgetImage}`} 
            onClick={() => handleClick()}
            onMouseEnter={() => setWidgetImage("mobile-chat-widget-tap.png")}
            onMouseLeave={() => setWidgetImage("mobile-chat-widget.png")}
          />
        )        
      }
    </Container>
  );
};
