import React, { useState, useEffect, useContext } from 'react';
import { GlobalContext } from '/imports/context/GlobalState';
import { Container, Button } from 'react-bootstrap';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import venuesByUser from '/imports/db/venues/query/venuesByUser';
import { AddVenueModal } from '/imports/client/components/dashboard/modals/AddVenueModal';
import { EditVenueModal } from '/imports/client/components/dashboard/modals/EditVenueModal';
import { DeleteVenueModal } from '/imports/client/components/dashboard/modals/DeleteVenueModal';
import { VenueList } from '/imports/client/components/dashboard/venues/VenueList';
import { DeleteNotice } from '/imports/client/components/dashboard/venues/DeleteNotice';
import { getModalOpenStatus } from "/imports/utils/shared";
import { Loading } from '/imports/client/components/shared/Loading';

const Venues = ({ data, isLoading, error }) => {
  if (error) console.log(error);

  const [disabled, setDisabled] = useState(false);
  const [currentVenues, setCurrentVenues] = useState([]);

  const { modals, openModal, closeModal } = useContext(GlobalContext);

  // On load, check if max number of venues hit
  // TODO: This will need to change when Enterprise subscription is added
  useEffect(() => {
    const maxVenues = Number(Meteor.settings.public.subscriptions.maxNumberOfVenuesOnStandard);
    setDisabled(currentVenues.length >= maxVenues ? true : false);
  }, [currentVenues]);

  // When data finished loading, set state
  useEffect(() => { setCurrentVenues(data) }, [data]);

  if(isLoading) return <Loading />;
  
  return (
    <Container>
      <h2>Venues</h2>
      
      <Button
        variant="primary" 
        onClick={() => openModal("add-venue")}
        disabled={disabled}
        id="tour-add-venue-button"
      >
        Add Venue
      </Button>
      {
        !disabled ? "" : (
          <span className="ml-2">Maximum of {Meteor.settings.public.subscriptions.maxNumberOfVenuesOnStandard} venue(s).</span>
        )
      }

      <DeleteNotice />

      <VenueList venues={currentVenues} />

      <AddVenueModal show={getModalOpenStatus("add-venue", modals)} onHide={() => closeModal("add-venue")} />
      <EditVenueModal show={getModalOpenStatus("edit-venue", modals)} onHide={() => closeModal("edit-venue")} />
      <DeleteVenueModal show={getModalOpenStatus("delete-venue", modals)} onHide={() => closeModal("delete-venue")} />
    </Container>
  );
};

export default withQuery(() => {
  return venuesByUser.clone()
}, {reactive: true})(Venues)