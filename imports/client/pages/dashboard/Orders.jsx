import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import { useHistory } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import { customDecode } from '/imports/utils/shared';
import { GlobalContext } from '/imports/context/GlobalState';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import venuesAndOrdersByUser from '/imports/db/venues/query/venuesAndOrdersByUser';
import { OrderList } from '/imports/client/components/dashboard/orders/OrderList';
import { VenueSelect } from '/imports/client/components/dashboard/shared/VenueSelect';
import { ViewOrderModal } from '/imports/client/components/dashboard/modals/ViewOrderModal';
import { CancelOrderModal } from '/imports/client/components/dashboard/modals/CancelOrderModal';
import { UncancelOrderModal } from '/imports/client/components/dashboard/modals/UncancelOrderModal';
import { ReopenOrderModal } from '/imports/client/components/dashboard/modals/ReopenOrderModal';
import { SmallDeviceNotice } from '/imports/client/components/dashboard/shared/SmallDeviceNotice';
import { getModalOpenStatus } from "/imports/utils/shared";
import { usePrevious } from "/imports/client/hooks";
import { sendNotification, groupArrayBy, sortByTime24Hr, comparer } from '/imports/utils/shared';
// import { ExportOrdersModal } from '/imports/client/components/dashboard/modals/ExportOrdersModal';

const Orders = ({ data, isLoading, error, onDateChange }) => {
  if (error) console.log(error);

  const history = useHistory();

  const { modals, closeModal } = useContext(GlobalContext);

  const [dateFilter, setDateFilter] = useState(new Date());
  const [dateFilterRecentlyChanged, setDateFilterRecentlyChanged] = useState(false);

  const [allVenues, setAllVenues] = useState([]);
  const [currentOrders, setCurrentOrders] = useState([]);
  const [currentVenue, setCurrentVenue] = useState('');

  const prevData = usePrevious(data);
  const prevVenue = usePrevious(currentVenue);

  const filterDateIsTodaysDate = () => dateFilter.toDateString() === new Date().toDateString();

  // Split orders by status, order each sub-array by time and append back together
  const sortOrders = (venue) => {
    if(venue === undefined || venue.orders.length === 0) return '';

    // group by status
    const groupedByStatus = groupArrayBy(venue.orders, 'status');
    let statusGroups = [];

    const sortAndAppend = (statusArr) => statusArr ? statusGroups = statusGroups.concat(sortByTime24Hr(statusArr)) : null;

    sortAndAppend(groupedByStatus.OPEN);
    sortAndAppend(groupedByStatus.CLOSED);
    sortAndAppend(groupedByStatus.CANCELLED);

    return statusGroups;
  }

  // Handle venue change - update state so it runs useEffect dependent on currentVenue
  const onVenueChangeHandler = venue => {
    const newSelectedVenue = data.filter(d => d._id === venue._id); 
    setCurrentVenue(newSelectedVenue[0]);
  };

  // Send notification & alarm when new orders come in
  useEffect(() => {
    if(!dateFilterRecentlyChanged && filterDateIsTodaysDate() && data && prevData) {
      // TODO: Using index 0 will need to change when we add support for >1 venue
      if(data[0]?.orders?.length > prevData[0]?.orders?.length) {
        const onlyInNewDataOrders = data[0].orders.filter(comparer(prevData[0].orders));
        onlyInNewDataOrders.map(order => sendNotification(`[${customDecode(order.tableToDeliverTo)}] New Order.`, "info"));
        const audio = new Audio('/sounds/notification_bell_1_2_secs.mp3');
        audio.play();
      }
    }
  }, [data]);

  // When data has finished loading, set initial venue and orders to first in list 
  // & split venue from orders for select component
  useEffect(() => {
    if(data.length > 0) {
      // TODO: Using index 0 will need to change when we add support for >1 venue
      setCurrentVenue(data[0]);
      setCurrentOrders(sortOrders(data[0]));
      const venues = data.map(({orders, ...allOtherAttr}) => allOtherAttr);
      setAllVenues(venues);
    }
  }, [isLoading, data]);

  // Handle reactivity of when new venue is selected & when the orders change
  useEffect(() => {
    if(currentVenue._id && prevVenue._id && currentVenue._id !== prevVenue._id) {
      const newSelectedVenue = data.filter(d => d._id === currentVenue._id)
      setCurrentOrders(sortOrders(newSelectedVenue[0]));
    }
  }, [currentVenue]);

  // Handle reactivity of when a new date is selected.
  // Set 0.5 sec delay to prevent new order notification on date change 
  useEffect(() => { 
    setDateFilterRecentlyChanged(true);
    onDateChange(dateFilter);
    setTimeout(() => { setDateFilterRecentlyChanged(false) }, 500);
  }, [dateFilter]);

  // no venues (as venues are top level in 'data' array - orders are child items)
  if(data.length == 0) {
    return <>
      <h2>Orders</h2>
      <p>
        You must add a <a href="" onClick={() => history.push("/dashboard/venues")}> venue </a> 
        to your account before you can receive any orders.
      </p>
      <p>If you have created a venue and are still seeing this message, please get in contact via email or live chat.</p>
    </>
  }

  return (
    <Container className="p-0">
      <h2>Orders</h2>

      {/* TODO: Add export button */}
      
      <Row>
        {/* <Col xs={12} md={6} className="mb-3 mb-sm-0">
          <VenueSelect venues={allVenues} onVenueChange={(venue) => onVenueChangeHandler(venue)} />
        </Col> */}

        <Col className="d-md-flex justify-content-md-end">
          <span style={{ width: '200px' }}>
            <DatePicker 
              dateFormat="dd/MM/yyyy"
              selected={dateFilter} 
              onChange={date => setDateFilter(date)} 
              className="p-1 rounded text-center w-100" 
            />
          </span>
        </Col>
      </Row>

      <SmallDeviceNotice 
        message="
          Although managing your orders is possible on a smaller device, 
          we advise using a device with a larger screen - a tablet or laptop.
        " 
      />

      <hr/>

      <OrderList loading={isLoading || dateFilterRecentlyChanged} orders={currentOrders} />

      <ViewOrderModal show={getModalOpenStatus("view-order", modals)} onHide={() => closeModal("view-order")} />
      <CancelOrderModal show={getModalOpenStatus("cancel-order", modals)} onHide={() => closeModal("cancel-order")} />
      <UncancelOrderModal show={getModalOpenStatus("uncancel-order", modals)} onHide={() => closeModal("uncancel-order")} />
      <ReopenOrderModal show={getModalOpenStatus("reopen-order", modals)} onHide={() => closeModal("reopen-order")} />
      {/* <ExportOrdersModal show={exportOrdersModalShow} onHide={() => setExportOrdersModalShow(false)} /> */}
    </Container>
  );
};

const OrdersContainer = withQuery(({ date, onDateChange }) => {
  return venuesAndOrdersByUser.clone({ date })
}, {reactive: true})(Orders);

export default function() {
  const [dateFilter, setDateFilter] = useState(new Date());

  return <OrdersContainer date={dateFilter} onDateChange={(date) => setDateFilter(date)} />;
}
