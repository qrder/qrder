import React, { useEffect, useState, useContext } from 'react';
import { useHistory } from "react-router-dom";
import { Container, Button } from 'react-bootstrap';
import "uniforms-bridge-simple-schema-2";
import { GlobalContext } from '/imports/context/GlobalState';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import venuesAndOptionGroupsByUser from '/imports/db/venues/query/venuesAndOptionGroupsByUser';
import { OptionGroupsList } from '/imports/client/components/dashboard/options/OptionGroupsList';
import { AddOptionGroupModal } from '/imports/client/components/dashboard/modals/AddOptionGroupModal';
import { EditOptionGroupModal } from '/imports/client/components/dashboard/modals/EditOptionGroupModal';
import { DuplicateOptionGroupModal } from '/imports/client/components/dashboard/modals/DuplicateOptionGroupModal';
import { DeleteOptionGroupModal } from '/imports/client/components/dashboard/modals/DeleteOptionGroupModal';
import { VenueSelect } from '/imports/client/components/dashboard/shared/VenueSelect';
import { getModalOpenStatus } from "/imports/utils/shared";
import { SmallDeviceNotice } from '/imports/client/components/dashboard/shared/SmallDeviceNotice';

const OptionGroups = ({ data, isLoading, error }) => {
  if (error) console.log(error);

  const history = useHistory();

  const { modals, openModal, closeModal } = useContext(GlobalContext);

  const [allVenues, setAllVenues] = useState([]);
  const [currentOptionGroups, setCurrentOptionGroups] = useState([]);
  const [currentVenue, setCurrentVenue] = useState({});

  // When data has finished loading, set initial venue and menus to first in list 
  // & split menus from venues for select component
  useEffect(() => {
    if(data.length > 0) {
      setCurrentVenue(data[0]);
      setCurrentOptionGroups(data[0].optionGroups);
      const venues = data.map(({menus, ...allOtherAttr}) => allOtherAttr);
      setAllVenues(venues);
    }
  }, [isLoading]);

  // Handle reactivity of when new venue is selected & when the menu is changed
  useEffect(() => {
    const newSelectedVenue = data.filter(d => d._id === currentVenue._id)
    setCurrentOptionGroups(newSelectedVenue[0] !== undefined ? newSelectedVenue[0].optionGroups : '');
  }, [currentVenue, data]);

  // Handle venue change - update state so it runs useEffect above
  const onVenueChangeHandler = venue => {
    const newSelectedVenue = data.filter(d => d._id === venue._id); 
    setCurrentVenue(newSelectedVenue[0]);
  };

  return (
    <Container className="p-0 mb-5">
      <h2>Option Groups</h2>
      <p>These are reusable options that you may want to apply to many different menu items.  For example, common options like ice & lemon or pint or half pint.</p>

      <Button
        variant="primary" 
        onClick={() => openModal("add-option-group")}
        id="tour-add-og-button"
      >
        Add Option Group
      </Button>

      <Button 
        variant="warning" 
        onClick={() => history.push("/dashboard/menu")}
        className="mx-3"
        id="tour-menu-button"
      >
        Menu
      </Button>

      {/* <VenueSelect 
        venues={allVenues} 
        onVenueChange={(venue) => onVenueChangeHandler(venue)} 
        className="mt-3 d-block mt-sm-0 d-sm-inline"
      /> */}

      <SmallDeviceNotice 
        message="
          Although managing your option groups is possible on a smaller device, 
          we advise using a device with a larger screen.
        " 
      />

      <OptionGroupsList options={currentOptionGroups}  />

      {/* Render all modals here so that they do not get rendered within several layers of nested components */}
      <AddOptionGroupModal show={getModalOpenStatus("add-option-group", modals)} onHide={() => closeModal("add-option-group")} currentVenue={currentVenue._id} venues={allVenues} />
      <EditOptionGroupModal show={getModalOpenStatus("edit-option-group", modals)} onHide={() => closeModal("edit-option-group")} venues={allVenues} currentVenue={currentVenue._id} />
      <DuplicateOptionGroupModal show={getModalOpenStatus("duplicate-option-group", modals)} onHide={() => closeModal("duplicate-option-group")} venues={allVenues} currentVenue={currentVenue._id} />
      <DeleteOptionGroupModal show={getModalOpenStatus("delete-option-group", modals)} onHide={() => closeModal("delete-option-group")} />
    </Container>
  );
};

export default withQuery(() => {
  return venuesAndOptionGroupsByUser.clone()
}, {reactive: true})(OptionGroups)