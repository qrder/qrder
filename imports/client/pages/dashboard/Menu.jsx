import React, { useState, useEffect, useContext } from 'react';
import { Container, Button } from 'react-bootstrap';
import "uniforms-bridge-simple-schema-2";
import { useHistory } from "react-router-dom";
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import { GlobalContext } from '/imports/context/GlobalState';
import venuesAndMenusByUser from '/imports/db/venues/query/venuesAndMenusByUser';
import { AddMenuModal } from '/imports/client/components/dashboard/modals/AddMenuModal';
import { EditMenuModal } from '/imports/client/components/dashboard/modals/EditMenuModal';
import { DuplicateMenuModal } from '/imports/client/components/dashboard/modals/DuplicateMenuModal';
import { DeleteMenuModal } from '/imports/client/components/dashboard/modals/DeleteMenuModal';
import { AddSectionModal } from '/imports/client/components/dashboard/modals/AddSectionModal';
import { EditSectionModal } from '/imports/client/components/dashboard/modals/EditSectionModal';
import { DuplicateSectionModal } from '/imports/client/components/dashboard/modals/DuplicateSectionModal';
import { DeleteSectionModal } from '/imports/client/components/dashboard/modals/DeleteSectionModal';
import { AddItemModal } from '/imports/client/components/dashboard/modals/AddItemModal';
import { EditItemModal } from '/imports/client/components/dashboard/modals/EditItemModal';
import { DuplicateItemModal } from '/imports/client/components/dashboard/modals/DuplicateItemModal';
import { DeleteItemModal } from '/imports/client/components/dashboard/modals/DeleteItemModal';
import { MenuTabs } from '/imports/client/components/dashboard/menu/MenuTabs';
import { VenueSelect } from '/imports/client/components/dashboard/shared/VenueSelect';
import { getModalOpenStatus } from "/imports/utils/shared";
import { SmallDeviceNotice } from '/imports/client/components/dashboard/shared/SmallDeviceNotice';

const Menu = ({ data, isLoading, error }) => {
  if (error) console.log(error);

  const history = useHistory();

  const [allVenues, setAllVenues] = useState([]);
  const [currentMenus, setCurrentMenus] = useState([]);
  const [currentVenue, setCurrentVenue] = useState({});

  const { modals, openModal, closeModal } = useContext(GlobalContext);

  // TODO: For shared modals, rather than passing data={data} to certain modals,
  //        I could add either add the whole data array to context
  //        or extract the specific things I need for the parent select options
  //        and store that.  

  // When data has finished loading, set initial venue and menus to first in list 
  // & split menus from venues for select component
  useEffect(() => {
    if(data.length > 0) {
      setCurrentVenue(data[0]);
      setCurrentMenus(data[0].menus);
      const venues = data.map(({menus, ...allOtherAttr}) => allOtherAttr);
      setAllVenues(venues);
    }
  }, [isLoading]);

  // Handle reactivity of when new venue is selected & when the menu is changed
  useEffect(() => {
    const newSelectedVenue = data.filter(d => d._id === currentVenue._id)
    setCurrentMenus(newSelectedVenue[0] !== undefined ? newSelectedVenue[0].menus : '');
  }, [currentVenue, data]);

  // Handle venue change - update state so it runs useEffect above
  const onVenueChangeHandler = venue => {
    const newSelectedVenue = data.filter(d => d._id === venue._id); 
    setCurrentVenue(newSelectedVenue[0]);
  };

  // no venues (as venues are top level in 'data' array - menus are child items)
  if(data.length == 0) {
    return <>
      <h2>Menus</h2>
      <p>You must add a <a href="" onClick={() => history.push("/dashboard/venues")}> venue</a> to your account before adding any menu items.</p>
    </>
  }

  return (
    <Container className="p-0">
      <h2>Menus</h2>

      <Button
        variant="primary" 
        onClick={() => openModal("add-menu")} 
        className="mr-3 add-menu-button"
        id="tour-add-menu-button"
      >
        Add Menu
      </Button>

      <Button 
        variant="warning" 
        onClick={() => history.push("/dashboard/menu/options")} 
        className="mr-3"
        id="tour-option-groups-button"
      >
        Option Groups
      </Button>

      {/* <VenueSelect 
        venues={allVenues} 
        onVenueChange={(venue) => onVenueChangeHandler(venue)} 
        className="mt-3 d-block mt-sm-0 d-sm-inline"
      /> */}

      <SmallDeviceNotice 
        message="
          Although managing your menu is possible on a smaller device, 
          we advise using a device with a larger screen.
        " 
      />

      <MenuTabs loading={isLoading} menus={currentMenus} />

      {/* Render all modals here so that they do not get rendered within several layers of nested components */}
      <AddMenuModal show={getModalOpenStatus("add-menu", modals)} onHide={() => closeModal("add-menu")} currentVenue={currentVenue._id} venues={allVenues} />
      <EditMenuModal show={getModalOpenStatus("edit-menu", modals)} onHide={() => closeModal("edit-menu")} venues={allVenues} currentVenue={currentVenue._id} />
      <DuplicateMenuModal show={getModalOpenStatus("duplicate-menu", modals)} onHide={() => closeModal("duplicate-menu")} venues={allVenues} currentVenue={currentVenue._id} />
      <DeleteMenuModal show={getModalOpenStatus("delete-menu", modals)} onHide={() => closeModal("delete-menu")} />
      <AddSectionModal show={getModalOpenStatus("add-section", modals)} onHide={() => closeModal("add-section")} data={data} />
      <EditSectionModal show={getModalOpenStatus("edit-section", modals)} onHide={() => closeModal("edit-section")} data={data} />
      <DuplicateSectionModal show={getModalOpenStatus("duplicate-section", modals)} onHide={() => closeModal("duplicate-section")} data={data} />
      <DeleteSectionModal show={getModalOpenStatus("delete-section", modals)} onHide={() => closeModal("delete-section")} />
      <AddItemModal show={getModalOpenStatus("add-item", modals)} onHide={() => closeModal("add-item")} data={data} />
      <EditItemModal show={getModalOpenStatus("edit-item", modals)} onHide={() => closeModal("edit-item")} data={data} />
      <DuplicateItemModal show={getModalOpenStatus("duplicate-item", modals)} onHide={() => closeModal("duplicate-item")} data={data} />
      <DeleteItemModal show={getModalOpenStatus("delete-item", modals)} onHide={() => closeModal("delete-item")} />
    </Container>
  );
};

export default withQuery(() => {
  return venuesAndMenusByUser.clone()
}, {reactive: true})(Menu)

