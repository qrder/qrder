import React, { useState, useEffect, useContext } from 'react';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import { GlobalContext } from '/imports/context/GlobalState';
import venuesAndTablesByUser from '/imports/db/venues/query/venuesAndTablesByUser';
import { Container, Button } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import { AddTableModal } from '/imports/client/components/dashboard/modals/AddTableModal';
import { EditTableModal } from '/imports/client/components/dashboard/modals/EditTableModal';
import { DeleteTableModal } from '/imports/client/components/dashboard/modals/DeleteTableModal';
import { GenerateQRCodesModal } from '/imports/client/components/dashboard/modals/GenerateQRCodesModal';
import { TableList } from '/imports/client/components/dashboard/tables/TableList';
import { VenueSelect } from '/imports/client/components/dashboard/shared/VenueSelect';
import { getModalOpenStatus } from "/imports/utils/shared";
import { SmallDeviceNotice } from '/imports/client/components/dashboard/shared/SmallDeviceNotice';

const Tables = ({ data, isLoading, error }) => {
  const history = useHistory();
  
  // It does not really matter if this is true / false - it used to prevent constant 
  // re-calling of the QR Generation function (i.e. re-rendering the modal & closing the modal)
  // The useEffect in the GenerateQRCodesModal uses this value / prop to monitor change
  const [generateQr, setGenerateQr] = useState(false);

  const [allVenues, setAllVenues] = useState([]);
  const [currentTables, setCurrentTables] = useState([]);
  const [currentVenue, setCurrentVenue] = useState('');

  const { modals, openModal, closeModal } = useContext(GlobalContext);

  if (error) console.log(error);

  // When data has finished loading, set initial venue and menus to first in list 
  // & split menus from venues for select component
  useEffect(() => {
    if(data.length > 0) {
      setCurrentVenue(data[0]);
      setCurrentTables(data[0].tables);
      const venues = data.map(({tables, ...allOtherAttr}) => allOtherAttr);
      setAllVenues(venues);
    }
  }, [isLoading]);

  // Handle reactivity of when new venue is selected & when the menu is changed
  useEffect(() => {
    const newSelectedVenue = data.filter(d => d._id === currentVenue._id)
    setCurrentTables(newSelectedVenue[0] !== undefined ? newSelectedVenue[0].tables : '');
  }, [currentVenue, data]);

  // Handle venue change - update state so it runs useEffect above
  const onVenueChangeHandler = venue => {
    const newSelectedVenue = data.filter(d => d._id === venue._id); 
    setCurrentVenue(newSelectedVenue[0]);
  };

  // no venues (as venues are top level in 'data' array - tables are child items)
  if(data.length == 0) {
    return <>
      <h2>Tables</h2>
      <p>You must add a <a href="" onClick={() => history.push("/dashboard/venues")}> venue</a> to your account before adding any tables.</p>
    </>
  }

  return (
    <Container>
      <h2>Tables</h2>

      <Button
        variant="primary" 
        onClick={() => openModal("add-table")} 
        className="mr-3"
        id="tour-add-table-button"
      >
        Add Table
      </Button>

      <Button
        variant="warning" 
        onClick={() => { openModal("generate-qr"); setGenerateQr(!generateQr); } } 
        className="mt-3 mr-3 d-block mt-sm-0 d-sm-inline"
        id="tour-generate-qr-button"
      >
        Generate QR Codes
      </Button>

      {/* <VenueSelect 
        venues={allVenues} 
        onVenueChange={(venue) => onVenueChangeHandler(venue)}  
        className="mt-3 d-block mt-sm-0 d-sm-inline"
      /> */}

      <SmallDeviceNotice 
        message="
          Although printing QR codes is possible on a smaller device, 
          we advise using a device with a larger screen.
        " 
      />

      <TableList columns={9} tables={currentTables} />

      <AddTableModal show={getModalOpenStatus("add-table", modals)} onHide={() => closeModal("add-table")} currentVenue={currentVenue._id} venues={allVenues} />
      <EditTableModal show={getModalOpenStatus("edit-table", modals)} onHide={() => closeModal("edit-table")} venues={allVenues} currentVenue={currentVenue._id} />
      <DeleteTableModal show={getModalOpenStatus("delete-table", modals)} onHide={() => closeModal("delete-table")} />
      <GenerateQRCodesModal show={getModalOpenStatus("generate-qr", modals)} generate={generateQr ? 1 : 0} onHide={() => closeModal("generate-qr")} venue={currentVenue} />
    </Container>
  );
};

export default withQuery(() => {
  return venuesAndTablesByUser.clone()
}, {reactive: true})(Tables)
