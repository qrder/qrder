import React, { useState } from 'react';
import { Session } from 'meteor/session';
import { Tracker } from 'meteor/tracker';
import { Container } from 'react-bootstrap';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import ordersCustomisation from '/imports/db/venues/query/ordersCustomisation';
import { Loading } from '/imports/client/components/shared/Loading';
import { ColourScheme } from '/imports/client/components/dashboard/customise/ColourScheme';
import { LogoSelector } from '/imports/client/components/dashboard/customise/LogoSelector';
import { OrderConfirmationMessage } from '../../components/dashboard/customise/OrderConfirmationMessage';

const Customisation = ({ data, isLoading, error }) => {
  if (error) console.log(error);

  if(isLoading) return <Loading />;

  // no venues 
  if(!data || data.length == 0) {
    return <>
      <h2>Customisation</h2>
      <p>You must add a <a href="" onClick={() => history.push("/dashboard/venues")}> venue</a> to your account before customising the customer order pages.</p>
    </>
  }

  return (
    <Container className="p-0">
      <h2>Customisation</h2>
      <p>Customise the menu that your customers use to submit orders.</p>
      <strong>There may be a 10 minute delay before your changes appear on the menu.</strong>

      <OrderConfirmationMessage data={data} />

      <h2 className="mt-4">Branding</h2>
      <p>Customise the customer menu to match your brand.</p>

      <LogoSelector logo={data?.settings?.logo} venue={data?._id} />

      <ColourScheme colours={data?.settings?.brand?.colour} venue={data?._id} />
    </Container>
  );
};

const CustomisationContainer = withQuery(({ venueId }) => {
  return ordersCustomisation.clone({ venueId })
}, {reactive: true, single: true})(Customisation);

export default function() {
  const [venue, setVenue] = useState();

  // Force rerender when venueId is set (after a venue is created usually)
  Tracker.autorun(() => {
    if(Session.equals('venueId', "") || Session.equals('venueId', undefined)) return;
    if(Session.get('venueId') !== venue) setVenue(Session.get('venueId'));
  });

  console.log(venue)

  return <CustomisationContainer venueId={venue} />;
}