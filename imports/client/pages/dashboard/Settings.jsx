import React, { useState, useMemo } from 'react';
import { Container, Tabs, Tab } from 'react-bootstrap';
import { withQuery } from 'meteor/cultofcoders:grapher-react';
import userInfoQuery from '/imports/db/users/query/userInfoQuery';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { AccountSettings } from '/imports/client/components/dashboard/settings/AccountSettings';
import { SubscriptionSettings } from '/imports/client/components/dashboard/settings/SubscriptionSettings';
import { PaymentSettings } from '/imports/client/components/dashboard/settings/PaymentSettings';
import { Invoices } from '/imports/client/components/dashboard/settings/Invoices';
import { Integrations } from '/imports/client/components/dashboard/settings/Integrations';
import { Loading } from '/imports/client/components/shared/Loading';

const stripePromise = loadStripe(Meteor.settings.public.stripe.publishable_key);

const Settings = ({ data, error, isLoading }) => {
  const [key, setKey] = useState('account');

  if(error) console.log(error);

  const tabs = useMemo(() => [
      {
        key: "account",
        title: "Account",
        component: <AccountSettings data={data} />
      },
      {
        key: "subscription",
        title: "Subscription",
        component: <SubscriptionSettings data={data} />
      },
      {
        key: "payment",
        title: "Payment Information",
        component: <PaymentSettings data={data} />
      },
      {
        key: "invoices",
        title: "Invoices",
        component: <Invoices customerId={data?.subscription?.stripeCustomerId} />
      },
      {
        key: "integrations",
        title: "Integrations",
        component: <Integrations integrations={data?.integrations} />
      },
    ]
  , [isLoading]);

  if(isLoading) return <Loading />;

  return (
    <Elements stripe={stripePromise}>
      <Container>

        <h2>Settings</h2> 

        <Container className="p-0 mb-5">
          <Tabs activeKey={key} onSelect={(k) => setKey(k)} className="mt-3 flex-nowrap qrder-tabs">
            {
              tabs.map((tab, idx) => {
                return (
                  <Tab 
                    key={idx} 
                    eventKey={tab.key} 
                    title={tab.title} 
                    tabClassName="text-nowrap border-top-0 border-left-0 border-right-0"
                  >
                    {tab.component}
                  </Tab>
                )
              })
            }
          </Tabs>
        </Container>

      </Container> 
    </Elements>
  );
};

export default withQuery(() => {
  return userInfoQuery.clone()
}, {reactive: true, single: true})(Settings)