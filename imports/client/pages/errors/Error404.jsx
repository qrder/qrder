import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default () => {

  // TODO: Make this a bit nicer

  return (
    <Container className="d-flex align-items-center justify-content-center" style={{minHeight: '100vh'}}>
      <Row>
        <Col className="text-center">
          <h1>404: Page Not Found</h1>

          <p>Sorry, the page you are looking for does not exist.  Please check the URL and try again.</p>
        </Col>
      </Row>
    </Container>
  );
};