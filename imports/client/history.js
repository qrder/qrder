/**
 * Manually create history variable so that we can 
 * access it outside React components when needed
 */

import { createBrowserHistory } from 'history';

export default createBrowserHistory();