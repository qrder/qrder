import { Meteor } from 'meteor/meteor';
import '/imports/db/grapher';
import '/imports/db/grapher/exposures';
import '/imports/server';
import '/imports/server/migrations';
import '/imports/server/helmet';

Meteor.startup(() => {
  console.log('Migrating Database Schemas...');
  Migrations.unlock();
  Migrations.migrateTo('5');
});
 