import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { Routes } from '/imports/client/routes';
import '/imports/client/redirects';
import 'bootstrap/dist/css/bootstrap.min.css';
import '/imports/db/grapher';
import 'react-toastify/dist/ReactToastify.css';
import '/imports/utils/hubspot/livechat.js';

Meteor.startup(() => {
  render(<Routes />, document.getElementById('react-target'));
});
