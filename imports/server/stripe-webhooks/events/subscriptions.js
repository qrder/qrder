import { Meteor } from 'meteor/meteor';
import moment from 'moment';

/**  
 * Cancel subscription
 * Update customers subscription status in our database & ensure all venues 
 *    reflect this as well (so that orders cannot be made).
 *    Empty price as the new price (if applicable) will be applied if they renew.
 * This will get triggered when a sub is cancelled (either by the user in QRDER or via Stripe dashboard).
 */
export const cancelSubscription = (customerId, status) => {
  // Update users subscription information
  Meteor.users.update({ 'subscription.stripeCustomerId': customerId }, { 
    $set: { 
      'subscription.price': '',
      'subscription.stripeStatus': status,
      'subscription.status': "Cancelled",
      'subscription.currentPeriodEnd': ''
    } 
  });
  // Update all of the users venues
  Meteor.call("venues.updateActiveSubscription", customerId, false);  
}


/**
 * Sent 3 days before trial will end.
 * Check that a payment method is registered for the user/customer.
 * If not, send notification / queue notification to be shown on next login.
 */
export const trialEndingSoon = async (customerId, trialEndUnix) => {
  try {
    const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
    const paymentMethods = await stripe.paymentMethods.list({
      customer: customerId,
      type: 'card'
    });
    // Assume there is a payment method
    let message = `Your trial is ending soon. Billing will start on ${moment.unix(trialEndUnix).format('DD/MM/YYYY')}.`;
    let variant = "warning";
    // If no pm, set notification to display pm required message
    if(paymentMethods.data.length == 0) {
      message = `Your trial is ending soon (${moment.unix(trialEndUnix).format('DD/MM/YYYY')}).  Please add a payment method in Settings.`;
      variant = "error";
    }

    Meteor.users.update({ 'subscription.stripeCustomerId': customerId }, { 
      $push: { 
        notifications: { message, variant }
      } 
    });
  } catch (error) {
    // TODO: error handling
    console.error(error)
  }
}

/**
 * Trial period has ended.
 * Update user subscription information to reflect this:
 * Change tier to 'standard', stripeStatus & our status to 'active'.
 * Notify user.
 */
export const trialEnded = async (customerId, stripeStatus) => {
  try {
    const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);
    const paymentMethods = await stripe.paymentMethods.list({
      customer: customerId,
      type: 'card'
    });

    let status = "Active"; // assume user has payment method
    // If no PM, set status to 'Trial Ended' so that user is constantly redirected to settings page
    if(paymentMethods.data.length == 0) status = "Trial Ended";
    
    Meteor.users.update({ 'subscription.stripeCustomerId': customerId }, { 
      $set: { 
        'subscription.type': 'Standard',
        'subscription.stripeStatus': stripeStatus,
        'subscription.status': status
      } 
    });
  } catch(error) {
    // TODO: Handle this better
    console.log(error)
  }
}


/**
 * Handle subscription creation.
 * This info is usually added in the user registration Method.
 * However, if a subscription is created by other means (e.g. from the Dashboard),
 * this info also needs to be updated.
 */
export const updateUser = (webhookData) => {
  // Update users subscription information
  Meteor.users.update({ 'subscription.stripeCustomerId': webhookData.customer }, { 
    $set: { 
      'subscription.stripeCustomerId': webhookData.customer,
      'subscription.stripeSubscriptionId': webhookData.id,
      'subscription.stripeStatus': webhookData.status,
      'subscription.status': webhookData.status == 'trialing' ? 'Trial' : 'Active',
      'subscription.price': (webhookData.plan.amount / 100).toFixed(2),
      'subscription.freeTrialEnd': moment.unix(webhookData.trial_end).format('DD/MM/YYYY'),
      'subscription.currentPeriodEnd': moment.unix(webhookData.current_period_end).format('DD/MM/YYYY')
    } 
  });
  // Update all of the users venues
  Meteor.call("venues.updateActiveSubscription", webhookData.customer, true);  
}