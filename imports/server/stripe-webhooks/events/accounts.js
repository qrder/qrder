import { Meteor } from 'meteor/meteor';
import moment from 'moment';

/**
 * Called if 'charges_enabled' is true.  This means the user
 * has finished the stripe onboarding process.
 * We need to set a database flag (paymentsEnabled) in our database.
 */
export const accountConnectedSuccessfully = (email) => {
  Meteor.users.update({ 'emails.address': email }, { 
    $set: { 'integrations.stripe.paymentsEnabled': true } 
  });
}

/**
 * Called if 'payouts_enabled' is true.  THis mneans the user
 * has finished their verification and can now recieve payouts from
 * Stripe.
 * We need to set a database flag (payoutsEnabled) in our database.
 */
export const accountVerifiedSuccessfully = (email) => {
  Meteor.users.update({ 'emails.address': email }, { 
    $set: { 'integrations.stripe.payoutsEnabled': true } 
  });
}

/**
 * Called when connected account status moves from 'pending'
 * to 'restricted'.  This means the ID and address proof flow
 * can be done.
 */
export const accountVerificationReady = (email) => {
  Meteor.users.update({ 'emails.address': email }, { 
    $set: { 'integrations.stripe.verificationStepReady': true } 
  });
}

/**
 * Called when a connected account is disconnected, or 'deauthorized'
 * from QRDER.  We must update our application accordingly.
 * Reset all stripe integration information.
 */
export const accountDisconnected = (email) => {
  Meteor.users.update({ 'emails.address': email }, {
    $set: { 
      'integrations.stripe.stripeAccountId': '',
      'integrations.stripe.paymentsEnabled': false,
      'integrations.stripe.verificationStepReady': false,
      'integrations.stripe.payoutsEnabled': false,
    }
  });
}