import { Meteor } from 'meteor/meteor';
import moment from 'moment';

/**
 * 'Customers' refers to the people who submit food orders to QRDER's users.
 * These functions will support connected account webhooks to help pay QRDER's users
 * for customer orders. 
 */

/**
 * Called if 'payment_intent.succeeded' is called. 
 * This means an online card payment for an order has succeeded.
 * We need to set that order's 'paidFor' property to true, so that 
 * it updates in the order list.
 */
export const orderPaidFor = (pi) => {
  Meteor.call("orders.paymentIntentSuccessful", pi, error => {
    if(error) {
      // TODO: Handle errors better
      console.log(error)
    }
  });
}
