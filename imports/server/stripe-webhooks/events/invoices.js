import { Meteor } from 'meteor/meteor';
import moment from 'moment';

/**
 * Update 'currentPeriodEnd' timestamp to be 1 month in the future.
 * A successful payment has been made so we add time to the users currentPeriodEnd.
 * They can continue to use the site.
 */
export const paymentSuccessful = (customerId) => {
  try {
    const user = Meteor.users.findOne({ 'subscription.stripeCustomerId': customerId });
    // if there is no current period end already, set it as today so +1 month works as expected
    if(!user.subscription.currentPeriodEnd) {
      user.subscription.currentPeriodEnd = moment().format('DD/MM/YYYY');
    }
    
    Meteor.users.update({ 'subscription.stripeCustomerId': customerId }, { 
      $set: { 
        // Add 1 month to currentPeriodEnd date so that customer can continue to use app
        'subscription.currentPeriodEnd': 
          moment(user.subscription.currentPeriodEnd, 'DD/MM/YYYY').add(1, 'M').format('DD/MM/YYYY'),
        'subscription.status': "Active"
      } 
    });
  } catch(error) {
    // TODO: Handle better
    console.log(error);
  }
}


/**
 * Triggered if a payment fails or the customer does not have a valid payment method.
 * Subscription becomes past_due.
 * Notify user that a payment has failed
 * Stripe will send an email to the customer when a payment fails.
 * How to test this: 
 * - https://stripe.com/docs/billing/testing#payment-failures
 * - Or send test webhook & hardcode customer Id into function call
 */
export const paymentFailed = (customerId) => {
  // Notify user that a subscription payment has failed
  Meteor.users.update({ 'subscription.stripeCustomerId': customerId }, { 
    $push: { 
      notifications: {
        message: "A payment has failed. Please update your payment information.",
        variant: "error"
      }
    } 
  });
}


/**
 * If the payment fails because it requires additional action.
 * PaymentIntent is 'requires_action'.
 * Send notification to customer that additional action is required.
 * Stripe will send an email to the customer with a link to confirm the payment.
 * How to test this: 
 * - https://stripe.com/docs/billing/testing#payment-failures
 * - Or send test webhook & hardcode customer Id into function call
 */
export const paymentActionRequired = (customerId) => {
  // Notify user that a subscription payment requires further action.
  Meteor.users.update({ 'subscription.stripeCustomerId': customerId }, { 
    $push: { 
      notifications: {
        message: "A payment requires action.  Please go to 'Settings' to finish the payment process.",
        variant: "warning"
      }
    } 
  });
}