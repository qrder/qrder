import { cancelSubscription, trialEndingSoon, trialEnded, updateUser } from '/imports/server/stripe-webhooks/events/subscriptions';
import { paymentSuccessful, paymentFailed, paymentActionRequired } from '/imports/server/stripe-webhooks/events/invoices';
import { 
  accountConnectedSuccessfully, 
  accountVerifiedSuccessfully, 
  accountVerificationReady, 
  accountDisconnected 
} from '/imports/server/stripe-webhooks/events/accounts';
import { orderPaidFor } from '/imports/server/stripe-webhooks/events/customers';
import { customConsoleLog } from '/imports/utils/shared';

/**
 * Handle Stripe Events that get sent to webhook endpoint.
 * Call relevant functions to deal with these.
 * Each function call will have more information in it's own comments / file.
 * IMPORTANT NOTE: Any webhook events beginning with 'account.' or 'payment_intent.'
 *                 are currently automatically handled by the Connect webhook secret,
 *                 not QRDER's webhook secret.  MEANING:  If we need to add events
 *                 for these to QRDER, we will need to change the way we decide which 
 *                 secret to use.  Refer to index.js in this folder and getWebhookSecret
 *                 in utils/stripe/index.js.
 * @param {*} eventObj containing information about the event 
 */

// TODO: Handle server errors better and ensure a response gets sent back
//        to Stripe if any sort of error occurs.
//        Will probably solve this by returning something from each function call
//          to 'result' var in index.js and then handling if that is an error or not.
//        So, the correct res.writeHead & res.end functions send back relevant information.

export const handleStripeEvent = (eventObj) => {
  customConsoleLog('Webhook Event', eventObj.type);
  switch(eventObj.type) {
    case 'invoice.paid':
      // If amount_paid is 0, the invoice was for the trial.  We do not want to trigger anything 
      // for the trial invoice as the trial period has already been added so we do not need to add 
      // more months
      if(eventObj.data.object.amount_paid != 0) {
        paymentSuccessful(eventObj.data.object.customer);
      }
      break;
    case 'invoice.payment_failed':
      paymentFailed(eventObj.data.object.customer);
      break;
    case 'invoice.payment_action_required':
      paymentActionRequired(eventObj.data.object.customer);
      break;
    case 'customer.subscription.created':
      updateUser(eventObj.data.object);
      break;
    case 'customer.subscription.updated':
      // This event can be triggered for a variety of reasons so it's important to figure out
      // exactly what action needs to be taken.  The event objects 'previous_attributes' is useful for this.

      // Trial has ended and is moving to 'Active' and billing will begin.
      if(eventObj.data.previous_attributes.status == 'trialing' && eventObj.data.object.status == 'active') {
        trialEnded(eventObj.data.object.customer, eventObj.data.object.status);
      }
      break;
    case 'customer.subscription.deleted':
      cancelSubscription(eventObj.data.object.customer, eventObj.data.object.status);
      break;
    case 'customer.subscription.trial_will_end':
      trialEndingSoon(eventObj.data.object.customer, eventObj.data.object.trial_end);
      break;
    case 'account.updated':
      // Called when user completes initial onboarding.  User can recieve payments from their own customers now.
      if(eventObj.data.object.charges_enabled && !eventObj.data.object.payouts_enabled) {
        accountConnectedSuccessfully(eventObj.data.object.email);
      }

      // When status gets changed form 'pending' to 'restricted', the ID & address proof flow can be done
      if(
        eventObj.data.object.requirements.currently_due.includes("individual.verification.additional_document") || 
        eventObj.data.object.requirements.currently_due.includes("individual.verification.document")
      ) {
        accountVerificationReady(eventObj.data.object.email);
      }
      
      // Called when user completes verification (ID and address proof).  User can recieve payouts from Stripe now.
      if(eventObj.data.object.payouts_enabled) accountVerifiedSuccessfully(eventObj.data.object.email);
      break;
    case 'account.application.deauthorized':
      // Users Stripe account is no longer linked to QRDER 
      accountDisconnected(eventObj.data.object.email);
      break;
    case 'payment_intent.succeeded':
      orderPaidFor(eventObj.data.object.id);
      break;
    default:
      // Unexpected event type
  }
  // TODO: Should probably return something else rather than the parameter
  return eventObj;
}