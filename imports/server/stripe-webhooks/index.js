import stripe from "stripe";
import { Meteor } from 'meteor/meteor';
import { handleStripeEvent } from '/imports/server/stripe-webhooks/handleStripeEvent';
import { getWebhookSecret } from '/imports/utils/stripe';

const bound = Meteor.bindEnvironment(callback => callback());

/**
 * TESTING:
 * Create URL endpoint for stripe webhooks and handle them accordingly.
 * To test a webhook endpoint, ngrok is required. To use this:
 *    1. Ensure ngrok is installed on your system.
 *    2. Run 'npm start' to start local dev server.
 *    3. Run 'ngrok http 3000' in a different cmd/terminal window
 *    4. Update Stripe webhook endpoint to use the HTTP forwarding URL (e.g. http://1099fbaaf6a7.ngrok.io)
 *          This URL changes each time ngrok is run locally so it needs to be changed
 *          manually each time in Stripe
 */

/**
 * Stripe handles QRDER related webhooks and Connect webhooks separately
 * By 'QRDER', we mean events occurring on our stripe account.
 * 'Connect' refers to events on a connected account (i.e. a customers integrated account).
 * i.e. customers that have connected their stripe accounts in order to
 * recieve online card payments
 */
WebApp.rawConnectHandlers.use("/webhooks/stripe", (req, res, next) => {
  const sig = req.headers["stripe-signature"];
  const stripeObj = stripe(Meteor.settings.private.stripe.secret_key);
  
  if(stripeObj == undefined) {
    throw new Meteor.Error(
      'stripe-secret-key-undefined', 
      'Check secret key exist in settings file.  Stripe secret key is undefined.'
    );
  }

  if (req.method === "POST") {
    let body = "";

    req.on("data", data => bound(() => {
      body += data;
    }));

    req.on("end", () => bound(() => {
      try {
        const endpointSecret = getWebhookSecret(JSON.parse(body).type);
        if(endpointSecret == undefined) {
          throw new Meteor.Error(
            'stripe-webhook-secret-undefined', 
            'Check webhook secrets exist in settings file.  Stripe webhook secret is undefined.'
          );
        }
        const eventObj = stripeObj.webhooks.constructEvent(body, sig, endpointSecret);
        // TODO: Should probably send result back?
        const result = handleStripeEvent(eventObj);
        res.writeHead(200);
        res.end('[200] Webhook recieved.');
      }
      catch (error) {
        res.writeHead(500);
        console.warn("[Webhook Exception]: ", error);
        if (error.type && error.type === "StripeSignatureVerificationError") {
          res.end("We caught you -_-");
        } else {
          res.end(JSON.stringify(error, Object.getOwnPropertyNames(error)));
        }
      }
    }));

  } else {
    next();
  }

});
