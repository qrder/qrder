import { Meteor } from 'meteor/meteor';
import { VenueCollection } from '/imports/db/venues/collection';

/**
 * DB Schema Migrations
 * 
 * Removing 'locked' console error:
 * - meteor mongo
 * - db.migrations.update({_id:"control"}, {$set:{"locked":false}});
 */

Migrations.add({
  version: 1,
  up: function() {
    console.log('Migrate to version 1');

    // No migration code required to convert pre-existing data 
    // as the schema change only affected how new items are created
    // and this was for a staging env deployment so no important / required
    // data to update.

    // Only change was providing a default price name and price value to items
    // that were not given a price name or value.
  }
});

Migrations.add({
  version: 2,
  up: function() {
    console.log('Migrate to version 2');

    // No migration code required to convert pre-existing data 
    // as the schema change only affected how new items are created and updated.

    // The schema change does not cause the UI to break.

    // Added options array to items schema.
    // Basically rereated the option groups schema.
  }
});

Migrations.add({
  version: 3,
  up: function() {
    console.log('Migrate to version 3');

    // No migration code being added as I do not think any users
    // have used the functionality that might break.

    // Removed custom options groups from items schema
    // Handle items prices better with AutoValue
    // Added custom validation to Venues schema
    // Create optiongroup subschema and added autovalue and custom validation
  }
});

Migrations.add({
  version: 4,
  up: function() {
    console.log('Migrate to version 4');

    // Migration should technically be required here to move 
    // order dates and times from US format to UK format.
    // But, not bothering as there are no real production orders yet anyway.

    // Changed order date and time format to UK format
    // Also removed AM/PM (which can be present in either format I think)
  },
  down: function() {
    console.log('Migrate to version 3');
  }
});

Migrations.add({
  version: 5,
  up: function() {
    console.log('Migrate to version 5');

    // Add settings object to venue schema with custom order message
    Meteor.call("migrations.v5Up");
  },
  down: function() {
    console.log('Migrate to version 4');

    // I should probably have down() code to remove the settings object
    // remove each venue but I am currently struggling massively to
    // $unset it specifically - it works but it also removes the userId
    // attached to the venue.  Trying to fix that creates other issues.

    // Leaving the settings object wouldn't cause any harm for now so it can stay.

    // I believe the userId is caused by the schema's autoValue (Meteor.userId()) 
    // is not relevant for migration update.
  }
});