import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { sendContactUsEmail } from "/imports/utils/shared/emails.js";

/**
 * Setup for email / SMTP related functionality 
 */

Meteor.startup(() => {
  // Set ENV VAR & mail defaults for mail
  const protocol = Meteor.settings.private.sendgrid.protocol;
  const api_key = Meteor.settings.private.sendgrid.api_key;
  const server = Meteor.settings.private.sendgrid.server;
  const port = Meteor.settings.private.sendgrid.port;
  process.env.MAIL_URL = `${protocol}://apikey:${api_key}@${server}:${port}`;

  Accounts.emailTemplates.from = 'no-reply@qrder.io';
});

/**
 * General email methods
 */
Meteor.methods({
  "home.contactUsEmail": function(email, message) {
    if(Meteor.isServer) {
      check(email, String);
      check(message, String);
      
      sendContactUsEmail(email, message);
    }
  }
})