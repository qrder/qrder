import { Meteor } from 'meteor/meteor';
import helmet from "helmet";

/**
 * Helmet is used to easily set HTTP headers for improved security.
 * 
 * Helmet documentation: https://helmetjs.github.io/
 * Meteor documentation on Helmet: https://guide.meteor.com/security.html#httpheaders
 */

Meteor.startup(() => {
  console.log("Helmet HTTP Headers...")

  WebApp.connectHandlers.use(helmet.dnsPrefetchControl());
  WebApp.connectHandlers.use(helmet.expectCt());
  WebApp.connectHandlers.use(helmet.frameguard());
  WebApp.connectHandlers.use(helmet.hidePoweredBy());
  WebApp.connectHandlers.use(helmet.hsts());
  WebApp.connectHandlers.use(helmet.ieNoOpen());
  WebApp.connectHandlers.use(helmet.noSniff());
  WebApp.connectHandlers.use(helmet.permittedCrossDomainPolicies());
  WebApp.connectHandlers.use(helmet.xssFilter());
  // 'no-referrer-when-downgrade' policy is important for TawkTo Live Chat to work
  WebApp.connectHandlers.use(helmet.referrerPolicy({ policy: "no-referrer-when-downgrade" }));

  // NOTE: Content Security Policy defined in main.html meta tag as it threw errors here.
});
  