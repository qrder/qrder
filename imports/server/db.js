import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

/**
 * Setup for server-side DB related functionality 
 */

Meteor.startup(() => {
  // Required override of createUser to enable custom properties to be added to user document
  // TODO: Check this is working properly as it has been moved
  Accounts.onCreateUser((options, user) => { return Object.assign({}, user, options) });
});