/**
 * Reducer for Context API
 */

/**
 * Handle case for context change
 */
export default (state, action) => {
  switch (action.type) {
    case 'ADD_USER_INFO':
      return { ...state, userInfo: action.payload };
    case 'UPDATE_USER_INFO':
      return { ...state, userInfo: action.payload };
    case 'DELETE_USER_INFO':
      return { ...state, userInfo: action.payload };
    case 'OPEN_MODAL':
      return { 
        ...state,
        modals: state.modals.map(modal => {
          return modal.name === action.payload.modal ? { ...modal, open: true } : modal;
        }),
        modalData: action.payload.data
      };
    case 'CLOSE_MODAL':
      return { 
        ...state, 
        modals: state.modals.map(modal => {
          return modal.name === action.payload ? { ...modal, open: false } : modal;
        })
      };
    case 'ADD_TABLE':
      return { ...state, tablesChecked: [action.payload, ...state.tablesChecked] };
    case 'SET_TABLES':
      return { ...state, tablesChecked: action.payload };
    case 'DELETE_TABLE':
      return { 
        ...state,
        tablesChecked: state.tablesChecked.filter(
          (tbl) => tbl._id !== action.payload
        )
      };
    case 'TOGGLE_MOBILE_NAV':
      return { ...state, mobileNavOpen: action.payload };
    default:
      return state;
  }
};