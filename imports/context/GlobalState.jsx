/**
 * Context API State
 */

import React, { createContext, useReducer } from 'react';
import RootReducer from './RootReducer';

// Initial State
let initialState = {
  userInfo: {},
  tablesChecked: [],
  modals: [
    { name: "add-venue", open: false },
    { name: "edit-venue", open: false },
    { name: "delete-venue", open: false },
    { name: "add-table", open: false },
    { name: "edit-table", open: false },
    { name: "delete-table", open: false },
    { name: "generate-qr", open: false },
    { name: "add-menu", open: false },
    { name: "edit-menu", open: false },
    { name: "duplicate-menu", open: false },
    { name: "delete-menu", open: false },
    { name: "add-item", open: false },
    { name: "edit-item", open: false },
    { name: "duplicate-item", open: false },
    { name: "delete-item", open: false },
    { name: "add-section", open: false },
    { name: "edit-section", open: false },
    { name: "duplicate-section", open: false },
    { name: "delete-section", open: false },
    { name: "add-option-group", open: false },
    { name: "edit-option-group", open: false },
    { name: "duplicate-option-group", open: false },
    { name: "delete-option-group", open: false },
    { name: "restart-sub", open: false },
    { name: "cancel-sub", open: false },
    { name: "resume-sub", open: false },
    { name: "pause-sub", open: false },
    { name: "view-order", open: false },
    { name: "cancel-order", open: false },
    { name: "uncancel-order", open: false },
    { name: "reopen-order", open: false },
  ],
  modalData: null,
  mobileNavOpen: false
};

// Export default state
export const GlobalContext = createContext(initialState);

// Provider component
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(RootReducer, initialState);

  // Add user info to global context
  function addUser(userInfo) {
    dispatch({
      type: 'ADD_USER_INFO',
      payload: userInfo
    });
  }

  // Update user info already in global context
  function updateUser(userInfo) {
    dispatch({
      type: 'UPDATE_USER_INFO',
      payload: userInfo
    });
  }

  // Delete user info from global context
  function deleteUser() {
    dispatch({
      type: 'DELETE_USER_INFO',
      payload: {}
    });
  }

  // Open venue modal
  function openModal(modal, data) {
    dispatch({
      type: 'OPEN_MODAL',
      payload: { modal, data }
    });
  }

  // Close venue modal
  function closeModal(modal) {
    dispatch({
      type: 'CLOSE_MODAL',
      payload: modal
    });
  }

  // Add table to global context
  function addTable(table) {
    dispatch({
      type: 'ADD_TABLE',
      payload: table
    });
  }

  // Set entire tables context
  function setTables(tables) {
    dispatch({
      type: 'SET_TABLES',
      payload: tables
    });
  }

  // Delete table from global context
  function deleteTable(tableId) {
    dispatch({
      type: 'DELETE_TABLE',
      payload: tableId
    });
  }

  function setMobileNav(status) {
    dispatch({ 
      type: 'TOGGLE_MOBILE_NAV',
      payload: status
    });
  }

  // Return provider to wrap around root component
  return (
    <GlobalContext.Provider
      value={{ 
        userInfo: state.userInfo, 
        tablesChecked: state.tablesChecked,
        modals: state.modals,
        modalData: state.modalData,
        mobileNavOpen: state.mobileNavOpen,
        addUser, 
        updateUser, 
        deleteUser,
        openModal,
        closeModal,
        addUser, 
        updateUser, 
        deleteUser,
        addTable,
        deleteTable,
        setTables,
        setMobileNav
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};