/**
 * General functions to support stripe related functionality.
 */
import moment from 'moment';
import { Meteor } from "meteor/meteor";
import { getEmail } from '/imports/utils/customers';

// Add stripe related information to a user object before adding to db
export const createUserWithStripeInfoObject = (user, customerId, subscription) => {
  return {...user,
    subscription: { 
      stripeCustomerId: customerId,
      stripeSubscriptionId: subscription.id,
      stripeStatus: subscription.status,
      price: (subscription.plan.amount_decimal / 100).toFixed(2),
      freeTrialEnd: moment.unix(subscription.trial_end).format('DD/MM/YYYY'),
      currentPeriodEnd: moment.unix(subscription.current_period_end).format('DD/MM/YYYY')
    }
  };
}

// Check if any users exist in stripe
export const emailExistsInStripe = async (email) => {
  const stripe = require('stripe')(Meteor.settings.private.stripe.secret_key);

  const { data } = await stripe.customers.list({
    email: email,
    limit: 1
  });

  if(data.length > 0) return true;

  return false;
}

// Add payment method
export const addPaymentMethod = ({ customerId, subscriptionId, paymentMethodId }) => {

  Meteor.call("users.addPaymentMethod", customerId, subscriptionId, paymentMethodId, (err, res) => {
    
    if (err && err.error === "402-payment-failed") {
      // TODO: Handle error properly
      console.error(err);
      return;
    }

  });

}

// This may not be the best approach (in case we need to deal with 'account.' or 'payment_intent.')
// events using the non-Connect webhook secret in future (i.e. for QRDER webhook events).
export const getWebhookSecret = (type) => {
  // Events with 'account.' are for connected accounts and need to be handled with a different secret.
  // Events with 'payment_intent.' are also currently only for connected accounts.
  if(type.includes("account.") || type.includes("payment_intent.")) {
    return Meteor.settings.private.stripe.connect_webhook_secret;
  } 

  return Meteor.settings.private.stripe.webhook_secret;
}

// Return users Stripe status based on their current Stripe integration  
// values in the database.
export const getUsersStripeConnectionStatus = (stripe) => {
  if(stripe == undefined) return 'not-connected'; // Not started at all

  if(stripe.stripeAccountId && stripe.paymentsEnabled && stripe.payoutsEnabled) {
    return 'connected'; // Entire setup complete
  } else if(stripe.stripeAccountId && stripe.paymentsEnabled && stripe.verificationStepReady && !stripe.payoutsEnabled) {
    return 'first-stage-complete'; // First stage of setup done
  } else if(stripe.stripeAccountId && stripe.paymentsEnabled && !stripe.verificationStepReady) {
    return 'pending'; // First stage done but waiting for status change to continue
  } else if(stripe.stripeAccountId && !stripe.paymentsEnabled && !stripe.payoutsEnabled) {
    return 'started'; // First stage started but not finished
  } else {
    return 'not-connected'; // Not started at all
  }
} 


// customer billing details
export const createBillingDetailsObject = async ({ name, city, line1, line2, postcode }) => {
  return {
    name,
    email: getEmail(),
    address: { 
      city,
      line1,
      line2,
      postal_code: postcode
    }
  };
}
