import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http'
import { Accounts } from 'meteor/accounts-base';
import moment from 'moment';

/**
 * Functions to support sending emails.
 * These are mostly emails that use the SendGrid templates.
 * (That I could not get to work with Email.send for some reason).
 * Most of them make a call to SendGrid Web API v3 to send the email.
 */

/**
 * Send Verification Email
 */
export const sendVerificationEmail = (email, url) => {
  const endpoint = Meteor.settings.private.sendgrid.api_mail_url;
  const options = {
    headers: {
      "Authorization": `Bearer ${Meteor.settings.private.sendgrid.api_key}`,
      "Content-Type": "application/json"
    },
    data: {
      personalizations: [
        {
          to: [{
            email: email
          }],
          dynamic_template_data: {
            "verificationLink": url
          }
        },
      ],
      from: {
        email: Accounts.emailTemplates.from
      },
      template_id: Meteor.settings.private.sendgrid.templates.email_verification
    }
  };

  const result = HTTP.post(endpoint, options);
  // TODO: Handle result better - if it errors
  if(result.statusCode !== '200' || result.statusCode !== '202') {
    console.log(result)
  }
}


/**
 * Send Password Reset Email
 */
export const sendPasswordResetEmail = (email, url) => {
  const endpoint = Meteor.settings.private.sendgrid.api_mail_url;
  const options = {
    headers: {
      "Authorization": `Bearer ${Meteor.settings.private.sendgrid.api_key}`,
      "Content-Type": "application/json"
    },
    data: {
      personalizations: [
        {
          to: [{
            email: email
          }],
          dynamic_template_data: {
            "resetLink": url
          }
        },
      ],
      from: {
        email: Accounts.emailTemplates.from
      },
      template_id: Meteor.settings.private.sendgrid.templates.reset_password
    }
  };

  const result = HTTP.post(endpoint, options);
  // TODO: Handle result better - if it errors
  if(result.statusCode !== '200' || result.statusCode !== '202') {
    console.log(result)
  }
}

/**
 * Send Customer Order Confirmation Email
 */
export const sendOrderConfirmationEmail = (email, venue, total, orderId) => {
  const endpoint = Meteor.settings.private.sendgrid.api_mail_url;
  const options = {
    headers: {
      "Authorization": `Bearer ${Meteor.settings.private.sendgrid.api_key}`,
      "Content-Type": "application/json"
    },
    data: {
      personalizations: [
        {
          to: [{
            email: email
          }],
          dynamic_template_data: {
            "venue": venue,
            "orderNumber": orderId,
            "total": total.toFixed(2)
          }
        },
      ],
      from: {
        email: Accounts.emailTemplates.from
      },
      template_id: Meteor.settings.private.sendgrid.templates.order_confirmation
    }
  };

  const result = HTTP.post(endpoint, options);
  // TODO: Handle result better - if it errors
  if(result.statusCode !== '200' || result.statusCode !== '202') {
    console.log(result)
  }
}

/**
 * Send Customer Order Error Email
 */
export const sendOrderErrorEmail = (email, venue) => {
  const endpoint = Meteor.settings.private.sendgrid.api_mail_url;
  const options = {
    headers: {
      "Authorization": `Bearer ${Meteor.settings.private.sendgrid.api_key}`,
      "Content-Type": "application/json"
    },
    data: {
      personalizations: [
        {
          to: [{
            email: email
          }],
          dynamic_template_data: {
            "venue": venue
          }
        },
      ],
      from: {
        email: Accounts.emailTemplates.from
      },
      template_id: Meteor.settings.private.sendgrid.templates.order_error
    }
  };

  const result = HTTP.post(endpoint, options);
  // TODO: Handle result better - if it errors
  if(result.statusCode !== '200' || result.statusCode !== '202') {
    console.log(result)
  }
}

/**
 * Send Contact Us Email
 */
export const sendContactUsEmail = (from, message) => {
  // Ensure newlines are sent - SendGrid removes '\n' and '<br/>' but not '<br>'
  const noNewLines = message.replace("\n", "<br>");
  const noClosedBreaks = noNewLines.replace("<br/>", "<br>");
  const formattedMessage = noClosedBreaks;
  
  const endpoint = Meteor.settings.private.sendgrid.api_mail_url;
  const options = {
    headers: {
      "Authorization": `Bearer ${Meteor.settings.private.sendgrid.api_key}`,
      "Content-Type": "application/json"
    },
    data: {
      personalizations: [
        {
          to: [{
            email: "support@qrder.io"
          }],
          dynamic_template_data: {
            "from": from,
            "message": formattedMessage
          }
        },
      ],
      from: {
        email: Accounts.emailTemplates.from
      },
      template_id: Meteor.settings.private.sendgrid.templates.contact_us
    }
  };

  const result = HTTP.post(endpoint, options);
  // TODO: Handle result better - if it errors
  if(result.statusCode !== 200 && result.statusCode !== 202) {
    console.log(result)
  }
}

/**
 * Send Error Email to errors@qrder.io
 * Used for internal purposes when certain functionality 
 * breaks or throws an error
 */
export const sendApplicationErrorEmail = (subject, preheader, page, component, area, error) => {
  const errorsEmail = "errors@qrder.io";
  
  const endpoint = Meteor.settings.private.sendgrid.api_mail_url;
  const options = {
    headers: {
      "Authorization": `Bearer ${Meteor.settings.private.sendgrid.api_key}`,
      "Content-Type": "application/json"
    },
    data: {
      personalizations: [
        {
          to: [{
            email: errorsEmail
          }],
          dynamic_template_data: {
            "subject": subject,
            "preheader": preheader,
            "datetime": moment().format('DD/MM/YYYY HH:mm:ss'),
            "pageTriggered": page,
            "componentTriggered": component,
            "applicationArea": area,
            "errorDescription": error
          }
        },
      ],
      from: {
        email: errorsEmail
      },
      template_id: Meteor.settings.private.sendgrid.templates.application_error
    }
  };

  const result = HTTP.post(endpoint, options);
  // TODO: Handle result better - if it errors
  if(result.statusCode !== 200 && result.statusCode !== 202) {
    console.log(result)
  }
}