import { Session } from 'meteor/session';
import moment from 'moment';

/**
 * Shared functions to support the rest of the code
 */

// Valid email against regular expression
export const validEmail = (email) => {
  const emailRegex = new RegExp(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
  return emailRegex.test(String(email).toLowerCase())
}

// Clean string of HTML characters 
export const sanitizeString = (stringToClean) => {
  return stringToClean?.replace(/<\/?[^>]+(>|$)/g, "");
}

// Split UK date string apart and return a standard date object
export const getDateFromUKDateString = (date) => {
  return new Date(date.split('/')[2], date.split('/')[1] - 1, date.split('/')[0]);
} 

// Send notification by setting session var
// There is a Tracker function inside DashboardLayout that 
// reactively checks for changes to the 'notification' Session var.
// When there is a change, it shows the notification.
export const sendNotification = (message, variant) => {
  Session.set('notification', JSON.stringify({
    message: message,
    variant: variant
  }));
}

// TODO: Move this to it's own class eventually
// TODO: Make sure console logs only happen on server
export const customConsoleLog = (prefix, variable) => {
  console.log(`[${prefix}] : ${variable} : ${moment().format('DD/MM/YYYY HH:mm:ss')}`);
}

/**
 * Custom encode URL
 * '#' character does not get encoded in encodeURI
 * @param {*} url  
 */
export const customEncode = (url) => {
  const tmp = encodeURI(url);
  const handleHash = tmp.replace(/#/g, '%23');
  return handleHash;
}

/**
 * Custom decode URL
 * '#' character does not get decoded in decodeURI
 * @param {*} url  
 */
export const customDecode = (url) => {
  const tmp = decodeURI(url);
  const handleHash = tmp.replace('%23', '#');
  return handleHash;
}

/**
 * Return a given modals open status
 * @param {String} modalToFind pre-defined value for each modal
 * @param {Array} modalsContext all modals in context to search through  
 */
export const getModalOpenStatus = (modalToFind, modalsContext) => {
  return modalsContext.filter(modal => modal.name === modalToFind)[0].open;
}

/**
 * Focus on a text input with given name
 */
export const inputFocus = (name) => {
  setTimeout(() => document.querySelector(`input[name='${name}']`).focus(), 200);
}

/**
 * Unfocus a text input with given name
 */
export const inputUnfocus = (name) => {
  setTimeout(() => document.querySelector(`input[name='${name}']`).blur(), 200);
}

/**
 * Get URL (a different function is used on the customer side)
 */
export const getUrl = () => {
  return window.location.pathname;
}

// Check if current page is NOT settings
export const currentPageIsNotSettings = () => { return (window.location.pathname == "/dashboard/settings" ? false : true); }  
// Check if current IS orders
export const currentPageIsOrders = () => { return (window.location.pathname == "/dashboard/orders" ? true : false); }

// Group array of objects by a certain property.
// Returns an object of the groups, with the group values 
// as object keys.
export const groupArrayBy = (arrayToSort, byThisProp) => {
  var grouped = {};
  for (var i=0; i<arrayToSort.length; i++) {
    var p = arrayToSort[i][byThisProp];
    if (!grouped[p]) { grouped[p] = []; }
    grouped[p].push(arrayToSort[i]);
  }
  return grouped;
}

// Sort array of objects based on a common time property called 'createdAtTime'.
// split assumes format is XX:XX:XX AM (splitting on the space between time and AM/PM)
export const sortByTime24Hr = (arr) => {
  return arr.sort((a, b) => {
    return new Date('1970/01/01 ' + b.createdAtTime.split(" ")[0]) - new Date('1970/01/01 ' + a.createdAtTime.split(" ")[0]);
  })
}; 

// Return the difference between two arrays of objects 
// based on their '_id' property (assuming it is data from a Mongo result)
export const comparer = (otherArray) => {
  return function(current){
    return otherArray.filter(function(other){
      return other._id == current._id
    }).length == 0;
  }
}