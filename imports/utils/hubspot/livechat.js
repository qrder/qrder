/**
 * Every 500 ms (0.5s), check to see if HubSpot chat is ready and the iframe has been loaded.
 * When it has, remove the "widget-align-right" class to allow us more control over the styling.
 * And add a MutationObserver to listen for changes in the hubspot container class list.
 * When the 'active' class appears, reset the right CSS prop to 0 so the chat appears correctly.
 * When the 'active' class disappears, reset the right CSS prop to -100px so the chat does not appear on the page.
 */

 const intervalId = setInterval(() => { 
  if(window.HubSpotConversations) {
    if(window.innerWidth < 992 && document.querySelector("#hubspot-messages-iframe-container")) {
      // Remove class that is preventing us controlling the style
      document.querySelector("#hubspot-messages-iframe-container").classList.remove("widget-align-right");

      const hubspotChatShadowContainer = document.getElementById('hubspot-messages-iframe-container').childNodes.item("shadow-container");
      // Callback to execute when mutations are observed
      const callback = function(mutationsList, observer) {
        for(const mutation of mutationsList) {
          if(mutation.attributeName == "class") {
            const isActive = hubspotChatShadowContainer.classList.contains('active');
            if(isActive) {
              document.querySelector("#hubspot-messages-iframe-container").style.right = 0;
            } else {
              document.querySelector("#hubspot-messages-iframe-container").style.right = '-500px';
            }
          }
        }
      };
      // Create an observer instance linked to the callback function
      const observer = new MutationObserver(callback);
      // Start observing the target node for configured mutations
      observer.observe(hubspotChatShadowContainer, { attributes: true });
      
      // clear interval once we have set observer
      clearInterval(intervalId);
    }
  }
}, 500);
