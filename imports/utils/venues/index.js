/**
 * Support functions for managing the menu
 */

// Create list for dropdown select in add menu and edit menu modals
export const createVenueSelectOptions = (venues) => {
  return venues.map(venue => { 
    return {
      label: venue.name, 
      value: venue._id
    } 
  });
}

// Check users stripe integration status and return a message
export const checkUsersStripeIntegrationStatus = (integrations) => {
  // not setup at all / not started
  if(integrations == undefined || integrations == {} || integrations.stripe == undefined || integrations.stripe == {}) {
    return { 
      integrated: false,
      message: "Integrate Stripe to accept online card payments."
    };
  }
  // fully setup
  if(integrations.stripe.payoutsEnabled) {
    return { integrated: true, message: "Your account is fully setup to receive online payments and receive payouts from Stripe." }
  }
  // semi-setup
  if(integrations.stripe.paymentsEnabled) {
    return {
      integrated: true,
      message: "You may accept online payments from customers but you will need to finish your setup in order to receive payouts from Stripe."
    }
  }
  // TODO: If none of these conditions are met, it returns nothing and throws an error (e.g. prod alex.lord9@hotmail.co.uk)
}