import React from 'react';

/**
 * Support functions for the user tour 
 */

// Figure out if id or class and remove prefix (. / #)
export const removePrefix = (e) => { return e.includes("#") ? removeIdPrefix(e) : removeClassPrefix(e) };
// Remove the '#' from an ID string
const removeIdPrefix = (i) => { return i.replace("#", "") };
// Remove the '.' from a class string
const removeClassPrefix = (c) => { return c.replace(".", "") };

// When an element is clicked, wait 0.5 seconds for the next step to load
// and then go to it (so that the focus changes correctly)
export const createClickEventListenerToChangeStep = (element, step, goTo) => {
  const el = document.querySelector(element);
  el.addEventListener("click", () => setTimeout(() => goTo(step), 500));
}

// When a button is clicked, wait 0.5 seconds for the modal to open
// and then goto it's corresponding modal step (so that the focus changes correctly)
export const createCheckboxListenerToChangeStep = (checkbox, step, goTo) => {
  document.getElementsByClassName(removePrefix(checkbox))[0].addEventListener("change", () => {
    setTimeout(() => goTo(step), 500);
  });
}


// Create event listener to check for errors in the modal form
// Assumes the following step is optional validation step (if error)
// and that the step after that is the continuation of the tour
export const createFormValidationEventListener = (button, form, step, goTo) => {
  const btn = document.querySelector(button);
  btn.addEventListener("click", () => {
    setTimeout(() => {
      const frm = document.querySelector(form);
      if(frm && frm.classList.contains('error')) {
        goTo(step); // go to validation / fix step
      } else {
        goTo(step+1); // form okay - skip validation step
      }
    }, 500)
  });
}

// Check if the given modal is open
export const isModalOpen = (modal) => { 
  return document.getElementsByClassName(removePrefix(modal)).length > 0 ? true : false 
};

export const createTableOfContentsStep = (steps, userCreatedVenue, userCreatedMenu) => {
  
  const checkLinkDisabledStatus = (venueDependent, menuDependent) => {
    if(
      (venueDependent && !userCreatedVenue) || 
      (menuDependent && !userCreatedMenu)
    ) return "disabled";
  }

  // get steps to link to in TOC
  const tocAreas = steps.map((step, idx) => { 
    return { 
      area: step.tocText, 
      idx: idx, 
      isTocStep: step.hasOwnProperty('tocIdentifier'),
      venueDependent: step.hasOwnProperty('venueDependent'),
      menuDependent: step.hasOwnProperty('menuDependent')
    }
  }).filter(step => step.isTocStep);
  
  // return content (the content property for a step can be a function)
  // (hence the function notation being returned to the tocStep.content prop)
  return ({ goTo }) => {
    return <>
      <p>Table of Contents</p>
      {tocAreas.map(ta => (
        <div key={ta.idx} className="my-2 my-xl-1">
          <a 
            href="#" 
            className={checkLinkDisabledStatus(ta.venueDependent, ta.menuDependent)}
            onClick={() => goTo(ta.idx)}
          >{ta.area}</a>
        </div>
      ))}
      <br/>
      <p>Click the next arrow to start the tour from the beginning.</p>
    </>
  }
}