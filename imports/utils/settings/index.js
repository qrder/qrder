import { Meteor } from 'meteor/meteor';
import { getUrl } from '/imports/utils/shared';

/**
 * Support functions for settings related code
 */

// Create new settings object with update currency information
export const addCurrencyInfoToSettingsObject = (data, res) => {
  return {...data, 
    settings: {
      currency: {
        code: res.code,
        name: res.name,
        symbol: res.symbol_native
      }
    }
  }
};

// Temporarily manually update user context with 
// changed subscription status
export const createUpdateUserInfoObj = (userInfo, status, stripeStatus) => {
  return { ...userInfo, 
    subscription: {
      ...userInfo.subscription,
      status: status,
      stripeStatus: stripeStatus
    }
  };
}

// Set subscription cancellation application error email  
export const sendSubCancellationApplicationError = (stripeSubscriptionId) => {
  Meteor.call(
    "shared.sendApplicationErrorEmail", // Method 
    "[CANCELLATION ERROR] Subscription Cancellation Webhook is Failing", // subject
    "User may have unauthorized access to website.", // preheader
    getUrl(),  // page
    "CancelSubscriptionModal", // component
    "Subscription Settings", // general app area
    // error message
    `A user has recently cancelled their subscription but the 
    webhook is failing to update the database (this can lead to unpaid access).
    The Stripe subscription ID is ${stripeSubscriptionId}.`
  );
}

// Set subscription restart application error email  
export const sendSubRestartApplicationError = (stripeCustomerId) => {
  Meteor.call(
    "shared.sendApplicationErrorEmail", // Method 
    "[RESTART ERROR] Subscription Restart Webhook is Failing", // subject
    "User may have any access to website.", // preheader
    getUrl(),  // page
    "RestartSubscriptionModal", // component
    "Subscription Settings", // general app area
    // error message
    `A user has recently restarted their subscription but the 
    webhook is failing to update the database (this can lead to loss of access).
    The Stripe customer ID is ${stripeCustomerId}.`
  );
}