import { list, save, clear, listen } from '/imports/utils/localstorage';

/**
 * Support functions for managing the cart in customer pages.
 * Most of these functions were taken / modified from this GitHub repo:
 * - https://github.com/peet86/cart-localstorage
 */

export { list };

const isValid = (product) => product.id && (product.price >= 0);

const subtotal = (product) => isCalcable(product) ? (product.price * product.quantity) : 0;

const isCalcable = (product) => (product && product.price && product.quantity);

const isCallback = (cb) => cb && typeof cb === "function";

export const get = (id) => list().find((product) => product.id === id);

export const exists = (id) => !!get(id);

export const add = (product, quantity) => isValid(product) ? exists(product.id) ? update(product.id, 'quantity', get(product.id).quantity + (quantity || 1)) : save(list().concat({ ...product, quantity: quantity || 1 })) : null;

export const remove = (id) => save(list().filter((product) => product.id !== id));

export const quantity = (id, diff) => exists(id) && get(id).quantity + diff > 0 ? update(id, 'quantity', get(id).quantity + diff) : remove(id);

export const update = (id, field, value) => save(list().map((product) => product.id === id ? ({ ...product, [field]: value }) : product));

export const total = (cb) => list().reduce((sum, product) => isCallback(cb) ? cb(sum, product) : (sum += subtotal(product)), 0);

export const destroy = () => clear();

export const onChange = (cb) => isCallback(cb) ? listen(cb) : console.log(typeof cb);

export const isEmpty = () => list() == undefined || list().length == 0;

// Split price (like 5.00) from a string ( usually "Small (£5.00)" )
export const ExtractPriceFromString = (stringWithPriceIn) => {
  const priceRegex = /\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})/;
  const match = stringWithPriceIn.match(priceRegex);
  const itemPrice = match[0];
  return itemPrice;
}

// Calculate item total (base price + options)
export const calculateItemTotal = (price, options) => {
  let total = Number(ExtractPriceFromString(price)); // start with price
  options.map(opt => { // iterate each chosen option
    opt.optionsChosen.map(cho => {
      let itemPrice = ExtractPriceFromString(cho);
      total += Number(itemPrice);
    })
  });
  return total;
};
