/**
 * Support functions for menu related code
 */

/**
 * Iterate list of items to add into database
 * @param {Array} items 
 * @param {String} parent id
 */
export const insertItems = (items, parent) => {
  items.map(item => {
    delete item._id; // remove id so it does not try to add a duplicate id
    ItemCollection.insert({ ...item, parentId: parent });
  });
}

/**
 * RECURSIVELY iterate sections to add their items and sub-sections/sub-items to database
 * @param {Array} sections 
 * @param {String} parent id 
 */
export const insertSections = (sections, parent) => {
  sections.map(section => {
    delete section._id; // remove id so it does not try to add a duplicate id
    const insertedSectionId = SectionCollection.insert({ ...section, parentId: parent });

    insertItems(section.items, insertedSectionId);

    insertSections(section.sections, insertedSectionId);
  });
}

/**
 * Recursive function to create list of possible menus and sections to move items and sections to
 * @param {Array} options for select field
 * @param {Array} list of menus / sections to iterate
 * @param {String} prevLabel to append to  
 */
const addOptions = (options, list, prevLabel, currentSection, modalType) => {
  list.map(item => {
    // prevent user moving a section inside of a child / itself
    if(modalType == 'edit' && item._id == currentSection) return;

    const label = `${prevLabel} / ${item.name}`;
    options.push({label, value: item._id});

    addOptions(options, item.sections, label, currentSection, modalType);
  })
}

/**
 * Create select options for moving items / sections 
 * @param {Array} venues grapher query data
 */
export const createParentSelectOptions = (venues, currentSection, modalType) => {
  if(!currentSection) return;

  let options = [];

  venues.map(venue => addOptions(options, venue.menus, venue.name, currentSection, modalType));

  return options;
}
