import { Meteor } from 'meteor/meteor';

/**
 * Support functions for managing users
 */

// Create a user object 
export const createUserObject = (e) => {
  return {
    username: e.target.username.value,
    email: e.target.email.value,
    password: e.target.password.value
  };
};

// Check passwords match
export const passwordsMatch = (e) => {
  return (e.target.password.value === e.target.confirmPassword.value);
}

// Check if status is active
export const isSubscriptionActive = (status) => {
  return ["Active"].indexOf(status) > -1;
}

// Check if status is cancelled or trial ended
export const isSubscriptionInvalid = (status) => {
  return ["Cancelled", "Trial Ended"].indexOf(status) > -1;
}

// Check if status is cancelled or paused
export const isSubscriptionEnding = (status) => {
  return ["Cancelled", "Paused"].indexOf(status) > -1;
}

// Check if status is cancelled
export const isSubscriptionCancelled = (status) => {
  return ["Cancelled"].indexOf(status) > -1;
}

// Check if status is paused
export const isSubscriptionPaused = (status) => {
  return ["Paused"].indexOf(status) > -1;
}

// Check if any users exist in qrder with this email
export const emailExistsInQrder = (email) => {
  const res = Meteor.users.findOne({ 'emails.address': email });
  if(res !== undefined) return true;
  return false;
}

// Check if any users exist in qrder with this username
export const usernameExistsInQrder = (username) => {
  const res = Meteor.users.findOne({ username });
  if(res !== undefined) return true;
  return false;
}
