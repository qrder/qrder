import { Meteor } from "meteor/meteor";
var QRCode = require('qrcode');
import { customEncode } from '/imports/utils/shared';

/**
 * Build URL for QR Code
 * @param {*} venue 
 * @param {*} table 
 */
const buildURL = (venue, table) => {
  return customEncode(`${Meteor.settings.public.url}/menu/${venue.name}/${venue._id}/${table.name}/${table._id}`);
}

/**
 * Generate multiple QR codes
 * @param {*} venue 
 * @param {*} tables 
 */
export const GenerateMultipleQR = (venue, tables) => {
  const qrCodes = [];

  tables.forEach(table => {
    QRCode.toDataURL(buildURL(venue, table), [{width: '150px'}], function (error, qrUrl) {
      // TODO: Figure out error handling
      if(error) console.log(error);
      table.venueName = venue.name;
      qrCodes.push({table: table, qrUrl: qrUrl});
    });
  });

  return qrCodes;
}
