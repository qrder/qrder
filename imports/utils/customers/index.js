import { Session } from 'meteor/session';
import { total, list, destroy } from '/imports/utils/cart';

/**
 * Support functions for managing customers.
 * 'Customers' refers to the customers of QRDER's users.
 * i.e. the people who make orders at venues.
 * These functions are related to them and will be primarily called
 * during the order flow, in the 'CustomerXXX' pages.
 */

// Split the url and return a more usable object
export const splitUrl = () => {
  const path = window.location.pathname.split("/", 7);
  return { 
    venueId: path[3],
    venueName: decodeURI(path[2]),
    tableId: path[5],
    tableName: decodeURI(path[4]),
    subpage: path[6] || 'menu'
  };
}

// Return baseUrl (the customer menu)
export const getBaseUrl = () => {
  const url = window.location.pathname;
  const splitUrl = url.split('/');
  const baseUrl = `/${splitUrl[1]}/${splitUrl[2]}/${splitUrl[3]}/${splitUrl[4]}/${splitUrl[5]}`
  return baseUrl;
}

// Get current customer page
export const getCurrentPage = () => {
  return splitUrl().subpage;
}

// Return venue id and/or name
// '' means both (i.e. no parameter passed)
export const getVenue = (whatToGet = '') => {
  const url = splitUrl();
  
  if(whatToGet == '') {
    return { id: url.venueId, name: url.venueName };
  }

  if(whatToGet == 'id') return url.venueId;

  if(whatToGet == 'name') return url.venueName;
}

// Return venue id and/or name
// '' means both (i.e. no parameter passed)
export const getTable = (whatToGet = '') => {
  var url = splitUrl();
  
  if(whatToGet == '') {
    return { id: url.tableId, name: url.tableName };
  }

  if(whatToGet == 'id') return url.tableId;

  if(whatToGet == 'name') return url.tableName;
}

// Return customer email entered during 'information' step
export const getEmail = () => {
  return JSON.parse(Session.get("information")).email;
}

// Return customer comments entered during 'information' step
export const getComments = () => {
  return JSON.parse(Session.get("information")).comments;
}

// Return customers chosen payment type 
export const getChosenPaymentType = () => {
  return JSON.parse(Session.get('chosenPaymentOption')).paymentType;
}

// Return an object containing order information
export const createOrderObject = (pi) => {
  return {
    items: list(),
    orderTotal: total(),
    email: getEmail(),
    comments: getComments(),
    paymentOptionSelected: getChosenPaymentType(),
    tableToDeliverTo: getTable('name'),
    venueOrderedAt: getVenue(),
    stripePaymentIntent: pi
  }
}

// Destroy session and redirect to home
export const clearSession = () => {
  destroy(); // cart
  Session.clear(); // clear all other session vars
}

// Return a formatted price value for an order item
export const formattedPrice = (name, price) => {
  return `${name} (£${ price.toFixed(2) })`;
}

// Get entire venuePaymentOptions session variable
export const getVenuePaymentOptions = () => {
  return JSON.parse(Session.get('venuePaymentOptions'));
}

// Check if cardOnline is turned on for venue
export const cardOnlineIsAcceptedPaymentOption = () => {
  if(getVenuePaymentOptions() === undefined || getVenuePaymentOptions().length == 0) {
    return false;
  }

  return getVenuePaymentOptions().cardOnline ? true : false;
}

// set default session variables
export const setDefaultSessionVariables = (paymentOptions, settings) => {
  Session.setDefault('chosenPaymentOption', JSON.stringify({}));
  Session.setDefault('orderSubmitted', JSON.stringify({id: '', price: '', submitted: false}));
  Session.setDefault('information', JSON.stringify({email: '', comments: ''}));
  Session.setDefault('cartQty', list().length || 0);
  Session.set('venuePaymentOptions', JSON.stringify(paymentOptions));
  Session.set('confirmationMessage', settings.orders.confirmationMessage);
  Session.set('logo', settings?.logo?.cdnUrl);
  Session.set('brandColours', JSON.stringify(settings?.brand?.colour));
}

// check if venue is not accepting orders / not allowed to 
export const venueNotAcceptingOrders = (underActiveSub, isAcceptingOrders) => {
  return !underActiveSub || !isAcceptingOrders;
}

// return orderSubmitted session var
export const getOrderSubmitted = (whatToGet = '') => {
  const orderSubmitted = JSON.parse(Session.get('orderSubmitted'));
  if(whatToGet == '') return orderSubmitted;
  if(whatToGet == 'id') return orderSubmitted.id;
  if(whatToGet == 'price') return orderSubmitted.price;
  if(whatToGet == 'submitted') return orderSubmitted.submitted;
}

// check if order has been submitted
export const orderHasBeenSubmitted = () => {
  return getOrderSubmitted('submitted') ? true : false;
}